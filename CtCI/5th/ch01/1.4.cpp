#include <iostream>

using namespace std;

void replace_space(string& s) {
  int n = s.size(), nspace = 0;
  for (int i = 0; i < n; ++i) {
    if (s[i] == ' ') {
      ++nspace;
    }
  }

  s.resize(nspace * 2 + n);

  int j = s.size() - 1;
  for (int i = n - 1; i >= 0; --i) {
    if (s[i] != ' ') {
      s[j--] = s[i];
    } else {
      s[j--] = '0';
      s[j--] = '2';
      s[j--] = '%';
    }
  }
}

int main() {
  string s = "Mr John Smith     ";

  cout << "raw string: " << s << "$" << endl;
  replace_space(s);
  cout << "new string: " << s << "$" << endl;

  return 0;
}
