#include "linked_list.h"
#include <unordered_set>

// hash table, O(N) space, O(N^2) time
void delete_dups(ListNode* head) {
  ListNode dummy(0), *cur = &dummy;
  unordered_set<int> hash;

  cur->next = head;
  while (cur and cur->next) {
    if (hash.count(cur->next->data)) {
      ListNode* del = cur->next;
      cur->next = cur->next->next;
      delete del;
    } else {
      hash.insert(cur->next->data);
      cur = cur->next;
    }
  }

  head = dummy.next;
}

ListNode* find_delete(ListNode* head, int data) {
  ListNode dummy(0), *cur = &dummy;
  cur->next = head;

  while (cur and cur->next) {
    if (cur->next->data == data) {
      ListNode* del = cur->next;
      cur->next = cur->next->next;
      delete del;
    } else {
      cur = cur->next;
    }
  }

  return dummy.next;
}

// o(N^2), if the there are many duplicated elements, the time cost will be samll
void delete_dups2(ListNode* head) {
  ListNode dummy(0), *cur = &dummy;
  cur->next = head;

  cur = cur->next;
  while (cur) {
    cur->next = find_delete(cur->next, cur->data);
    cur = cur->next;
  }
}

int main() {
  vector<int> v = {1, 4, 2, 4, 6, 5, 4, 5};

  ListNode* head = get_list(v);
  print_list(head);
  delete_dups2(head);
  print_list(head);

  return 0;
}
