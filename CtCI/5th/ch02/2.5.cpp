#include "linked_list.h"

ListNode* reverse_list(ListNode* head) {
  ListNode dummy(0), *cur = &dummy;
  cur->next = head;

  cur = cur->next;
  while (cur and cur->next) {
    ListNode* move = cur->next;
    cur->next = move->next;
    move->next = dummy.next;
    dummy.next = move;
  }

  return dummy.next;
}

ListNode* add_list(ListNode* a, ListNode* b) {
  a = reverse_list(a);
  b = reverse_list(b);
  ListNode dummy(0), *cur = &dummy;
  int carry = 0;

  while (a || b) {
    int sum = carry;
    if (a) {
      sum += a->data;
      a = a->next;
    }
    if (b) {
      sum += b->data;
      b = b->next;
    }
    cur->next = new ListNode(sum % 10);
    carry = sum / 10;
    cur = cur->next;
  }
  if (carry) {
    cur->next = new ListNode(carry);
  }

  return dummy.next;
}

int main() {
  vector<int> a = {1, 2, 4};
  vector<int> b = {9, 9};

  ListNode* la = get_list(a);
  ListNode* lb = get_list(b);
  ListNode* res = add_list(la, lb);
  print_list(res);

  return 0;
}
