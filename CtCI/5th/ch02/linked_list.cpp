#include "linked_list.h"

ListNode* delete_node(ListNode* head, int data) {
  if (head == nullptr) {
    return nullptr;
  }

  ListNode dummy(0), *cur = &dummy;
  cur->next = head;
  while (cur->next) {
    if (cur->next->data == data) {
      //ListNode* del = cur->next;
      cur->next = cur->next->next;
      //delete del;
      return dummy.next;
    }
    cur = cur->next;
  }
  return dummy.next;
}

int main() {
  vector<int> v = {1, 2, 3, 4, 5};
  ListNode* head = get_list(v);
  ListNode* res = nullptr;

  res = delete_node(head, 6); // corner cases: 0, 6
  print_list(res);

  return 0;
}
