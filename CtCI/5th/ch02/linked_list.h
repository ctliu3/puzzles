#ifndef _LINKED_LISTS_HEADER
#define _LINKED_LISTS_HEADER

#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

struct ListNode {
  int data;
  ListNode* next;
  explicit ListNode(int data): data(data), next(nullptr) {
  }
};

ListNode* get_list(vector<int>& arr) {
  int n = (int)arr.size();
  if (n == 0) {
    return nullptr;
  }

  ListNode* head = new ListNode(arr[0]);
  ListNode* p = head;
  for (int i = 1; i < n; ++i) {
    p->next = new ListNode(arr[i]);
    p = p->next;
  }
  return head;
}

ListNode* get_circle_list(vector<int>& arr) {
  int n = (int)arr.size();
  if (n == 0) {
    return NULL;
  }

  // TODO
}

void print_list(ListNode* head) {
  ListNode* p = head;
  while (p) {
    fprintf(stderr, "%d ", p->data);
    p = p->next;
  }
  fprintf(stderr, "\n");
}

// _LINKED_LISTS_HEADER
#endif
