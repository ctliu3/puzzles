#include <iostream>
#include <vector>
#include <exception>

using namespace std;

// not so easy to understand
// refer
// http://codercareer.blogspot.com/2013/02/no-39-stacks-sharing-array.html
class MulitStack {
public:
  explicit MulitStack(int nstk = 3, int cap = 1000): _capacity(cap), _next(0) {
    _data.resize(_capacity);
    _size.assign(nstk, 0);
    _top.assign(nstk, -1);

    for (int i = 0; i < _capacity - 1; ++i) {
      _data[i].second = i + 1;
    }
    _data[_capacity - 1].second = -1; // no available for (n - 1) position
  }

  void push(int id, int new_data) {
    if (_next == -1) {
      throw exception();
    }
    ++_size[id];
    int next = _data[_next].second;
    _data[_next].first = new_data;
    _data[_next].second = _top[id];
    _top[id] = _next;
    _next = next;
  }

  void pop(int id) {
    if (_top[id] == -1) {
      throw exception();
    }
    --_size[id];
    int pos = _top[id];
    _top[id] = _data[_top[id]].second;
    _data[pos].second = _next;
    _next = pos;
  }

  int top(int id) {
    if (_top[id] == -1) {
      throw exception();
    }
    return _data[_top[id]].first;
  }

  int size(int id) {
    return _size[id];
  }

  bool is_empty(int id) {
    return _size[id] == 0;
  }

  void print() {
    for (auto& it : _data) {
      printf("%4d ", it.first);
    }
    printf("\n");
    for (auto& it : _data) {
      printf("%4d ", it.second);
    }
    printf("\n");
    printf("_top : %4d, %4d, %4d\n", _top[0], _top[1], _top[2]);
    printf("_next: %4d\n", _next);
  }

private:
  int _next;
  int _capacity;
  vector<int> _top;
  vector<int> _size;
  vector<pair<int, int>> _data;
};

int main() {
  MulitStack stk(3, 10);

  stk.push(0, 0);
  try {
    cout << stk.top(1) << endl;
  } catch (exception& ex) {
    cout << "error" << endl;
  }
  stk.push(0, 1);
  stk.push(0, 2);
  stk.push(1, 3);
  stk.push(1, 4);
  stk.push(1, 5);
  stk.pop(1);
  stk.push(1, 6);
  cout << stk.top(0) << endl;
  stk.pop(0);
  stk.push(1, 7);
  stk.push(2, 8);
  stk.push(2, 9);
  stk.print();

  for (int i = 0; i < 3; ++i) {
    printf("stack %d: ", i);
    while (stk.size(i)) {
      cout << stk.top(i) << ' ';
      stk.pop(i);
    }
    cout << endl;
  }

  return 0;
}
