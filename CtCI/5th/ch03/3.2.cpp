#include "Stack.h"

using namespace std;

class MinStack {
public:
  explicit MinStack(int n = 1000): min_stk(n), stk(n) {
  }

  void push(const int& new_data) {
    stk.push(new_data);
    if (min_stk.is_empty() or min_stk.top() >= new_data) { // !
      min_stk.push(new_data);
    }
  }

  void pop() {
    if (stk.size()) {
      if (min_stk.size() and min_stk.top() == stk.top()) {
        min_stk.pop();
      }
      stk.pop();
    }
  }

  int top() {
    if (stk.is_empty()) {
      throw exception();
    }
    return stk.top();
  }

  int min() {
    if (min_stk.is_empty()) {
      throw exception();
    }
    return min_stk.top();
  }

private:
  Stack<int> min_stk;
  Stack<int> stk;
};

int main() {
  MinStack stk;

  try {
    stk.top();
  } catch (exception& e) {
    cout << "error" << endl;
  }
  stk.push(2);
  stk.push(3);
  stk.push(4);
  stk.push(2);
  cout << stk.min() << endl;
  stk.pop();
  cout << stk.min() << endl;
  stk.push(0);
  stk.pop();
  cout << stk.min() << endl;

  return 0;
}
