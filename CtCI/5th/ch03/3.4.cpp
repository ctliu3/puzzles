#include "Stack.h"

class Hanoi {
public:
  explicit Hanoi(int n): _n(n) {
    for (int i = 0; i < n; ++i) {
      _stk[0].push(i);
    }
  }

  Stack<int>& sol() {
    // origin, buffer, destination
    move(_n, 0, 1, 2);
    return _stk[2];
  }

  void push(int from, int to) {
    int top = _stk[from].top();
    _stk[from].pop();
    _stk[to].push(top);
  }

  void move(int n, int origin, int buffer, int dest) {
    if (n < 1) {
      return ;
    }
    move(n - 1, origin, dest, buffer);
    push(origin, dest);
    move(n - 1, buffer, origin, dest);
  }

private:
  int _n;
  Stack<int> _stk[3];
};

int main() {
  Hanoi hanoi(5);

  Stack<int> res = hanoi.sol();

  while (res.size()) {
    cout << res.top() << ' ';
    res.pop();
  }
  cout << endl;

  return 0;
}
