#include "Stack.h"

using namespace std;

class MyQueue {
public:
  explicit MyQueue() {
  }

  int size() const {
    return stk.size() + stk.size();
  }

  bool is_empty() const {
    return !stk.size() and !backup.size();
  }

  void pop() {
    if (!backup.size()) {
      move();
    }
    backup.pop();
  }

  int front() {
    if (!backup.size()) {
      move();
    }
    return backup.top();
  }

  void push(int new_data) {
    stk.push(new_data);
  }

  void move() {
    while (stk.size()) {
      backup.push(stk.top());
      stk.pop();
    }
  }

private:
  Stack<int> stk, backup;
};

int main() {
  MyQueue que;

  que.push(1);
  que.push(2);
  que.push(3);
  cout << que.front() << endl;
  que.pop();
  que.pop();
  que.push(4);
  cout << que.front() << endl;
  que.pop();
  cout << que.front() << endl;

  return 0;
}
