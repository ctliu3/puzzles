#include "Stack.h"

// stack sort
void stack_sort(Stack<int>& stk) {
  Stack<int> backup;

  while (stk.size()) {
    int top = stk.top();
    stk.pop();
    while (backup.size() and backup.top() > top) {
      stk.push(backup.top());
      backup.pop();
    }
    backup.push(top);
  }
  stk = backup;
}

int main() {
  Stack<int> stk;
  vector<int> v = {1, 2, 5, 2, 6, 1, 8};

  for (int& it : v) {
    stk.push(it);
  }
  stack_sort(stk);

  while (stk.size()) {
    cout << stk.top() << ' ';
    stk.pop();
  }
  cout << endl;

  return 0;
}
