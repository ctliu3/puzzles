#ifndef _STACK_HEADER
#define _STACK_HEADER

#include <iostream>
#include <vector>
#include <exception>

using namespace std;

template <typename T>
class Stack {
public:
  // _top indicates the index of top element in the stack, so its initial value
  // is -1
  explicit Stack(int n = 1000): _top(-1), _size(n) {
    _data.resize(_size);
  }

  bool push(const T& new_data) {
    if (_top >= _size - 1) {
      return false;
    }
    _data[++_top] = new_data;
    return true;
  }

  void pop() {
    if (is_empty()) {
      throw exception();
    }
    --_top;
  }

  T top() const {
    if (is_empty()) {
      throw exception();
    }
    return _data[_top];
  }

  bool is_empty() const {
    return _top == -1;
  }

  int size() const {
    return _top + 1;
  }

private:
  int _top;
  int _size;
  vector<T> _data;
};

// _STACK_HEADER
#endif
