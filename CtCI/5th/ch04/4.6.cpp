#include "Tree.cpp"

TreeNode* leftmost(TreeNode* cur) {
  if (!cur) {
    return nullptr;
  }
  while (cur->left) {
    cur = cur->left;
  }
  return cur;
}

TreeNode* inorder_succ(TreeNode* cur) {
  if (!cur) {
    return nullptr;
  }

  if (cur->right) {
    return leftmost(cur->right);
  } else {
    // keep going up till find a turning
    TreeNode* up = cur->parent;
    while (up and up->left != cur) {
      cur = up;
      up = up->parent;
    }
    return up;
  }
}

int main() {
  return 0;
}
