#include "Tree.cpp"

// O(1) space
pair<bool, TreeNode*> _LCA(TreeNode* cur, TreeNode* p, TreeNode* q) {
  if (!cur) {
    return {false, nullptr};
  }
  if (cur == p and cur == q) { // p == q
    return {true, cur};
  }

  auto lres = _LCA(cur->left, p, q);
  if (lres.first) {
    return lres;
  }
  auto rres = _LCA(cur->right, p, q);
  if (rres.first) {
    return rres;
  }

  // neither left tree or right tree has ancestor
  if (lres.second and rres.second) {
    return {true, cur};
  } else if (cur == p or cur == q) {
    bool is_ance = lres.second || rres.second ? true : false;
    return {is_ance, cur};
  } else {
    return {false, lres.second ? lres.second : rres.second};
  }
}

TreeNode* LCA(TreeNode* root, TreeNode* p, TreeNode* q) {
  pair<bool, TreeNode*> res = _LCA(root, p, q);
  if (res.first) {
    return res.second;
  }
  return nullptr;
}

int main() {
  return 0;
}
