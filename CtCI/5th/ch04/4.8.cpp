#include "Tree.cpp"

bool is_same_tree(TreeNode* atree, TreeNode* btree) {
  if (!atree and !btree) {
    return true;
  }
  if (!atree or !btree) {
    return false;
  }
  if (atree->value != btree->value) {
    return false;
  }
  return is_same_tree(atree->left, btree->left) and
    is_same_tree(atree->right, btree->right);
}

bool is_subtree(TreeNode* head, TreeNode* sub_head) {
  if (!sub_head) {
    return true;
  }
  if (!head) {
    return false;
  }

  stack<TreeNode* > stk;
  stk.push(head);
  while (stk.size()) {
    TreeNode* cur = stk.top();
    stk.pop();
    if (is_same_tree(cur, sub_head)) {
      return true;
    }

    if (cur->left) {
      stk.push(cur->left);
    }
    if (cur->right) {
      stk.push(cur->right);
    }
  }
  return false;
}

int main() {
  return 0;
}
