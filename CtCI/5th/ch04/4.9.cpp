#include "Tree.cpp"

void print(vector<int>& path, int start, int end) {
  for (int i = start; i <= end; ++i) {
    printf("%d ", path[i]);
  }
  printf("\n");
}

void _find(TreeNode* cur, vector<int>& path, int dep, const int tot) {
  if (!cur) {
    return ;
  }
  if (path.size() <= dep) {
    path.resize(dep + 1);
  }
  path[dep] = cur->value;

  int sum = 0;
  for (size_t i = dep; i >= 0; --i) { // !
    sum += path[i];
    if (sum == tot) {
      print(path, i, path.size() - 1);
    }
  }

  _find(cur->left, path, dep + 1, tot);
  _find(cur->right, path, dep + 1, tot);
}

void find_sum(TreeNode* head, int tot) {
  vector<int> path;
  _find(head, path, 0, tot);
}

int main() {
  return 0;
}
