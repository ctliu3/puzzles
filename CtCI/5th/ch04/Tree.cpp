#ifndef _TREE_HEADER
#define _TREE_HEADER

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class TreeNode {
public:
  int value;
  TreeNode* left, *right;
  TreeNode* parent;

  explicit TreeNode(int value): value(value), left(nullptr), right(nullptr)
                                , parent(nullptr) {
  }
};

#endif
