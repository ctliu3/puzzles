#include <iostream>
#include <vector>
#include <bitset>

using namespace std;

void insert(int& n, int m, int i, int j) {
  for (int k = i; k <= j; ++k) {
    n &= ~(1 << k);
    int mask = (m & (1 << (k - i))) != 0;
    n |= (mask << k);
  }
}

int main() {
  bitset<11> N("10001000000");
  bitset<6> M("10011");

  int n = N.to_ulong();
  int m = M.to_ulong();
  insert(n, m, 2, 6);
  bitset<11> res(n);
  cout << res.to_string() << endl;

  return 0;
}
