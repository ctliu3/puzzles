#include <iostream>
#include <vector>
#include <exception>

using namespace std;

string dbl2binary(double num) {
  if (num <= 0 or num >= 1) {
    throw exception();
  }

  string res = ".";
  while (num > 0) {
    if (res.length() >= 32) {
      return "error";
    }

    double cur = num * 2;
    if (cur >= 1) {
      res.push_back('1');
      num = cur - 1; // !
    } else {
      res.push_back('0');
      num = cur;
    }
  }

  return res;
}

int main() {
  vector<double> cases = {0.25, 0.4, 0.5};

  for (double& it : cases) {
    try {
      cout << dbl2binary(it) << endl;
    } catch (exception& ex) {
      cout << "exception" << endl;
    }
  }

  return 0;
}
