#include <iostream>
#include <bitset>

using namespace std;

int get_next(int n) {
  int p = 0, c1 = 0;
  while (!c1 || ((1 << p) & n) != 0) {
    if (n & (1 << p)) {
      ++c1;
    }
    ++p;
  }

  n |= 1 << p;
  n &= ~((1 << p) - 1); // set the p bits to 0
  --c1;
  n |= ((1 << c1) - 1);

  return n;
}

int get_prev(int n) {
  int p = 0, c0 = 0;
  while (!c0 || !((1 << p) & n)) {
    if (!(n & (1 << p))) {
      ++c0;
    }
    ++p;
  }

  n &= ~(1 << p);
  n |= (1 << p) - 1;
  --c0;
  n &= ~((1 << c0) - 1);

  return n;
}

void next() {
  bitset<14> n_bits("11011001111100");
  int n = n_bits.to_ulong();

  bitset<14> res(get_next(n));
  cout << res.to_string() << endl;
}

void prev() {
  bitset<14> n_bits("10011110000011");
  int n = n_bits.to_ulong();

  bitset<14> res(get_prev(n));
  cout << res.to_string() << endl;
}

int main() {
  next();
  prev();

  return 0;
}
