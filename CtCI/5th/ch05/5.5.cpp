#include <iostream>
#include <bitset>

using namespace std;

int n_convert(int a, int b) {
  int count = 0;
  for (int c = a ^ b; c; c = c & (c - 1)) {
    ++count;
  }
  return count;
}

int main() {
  const int N = 6;
  bitset<N> n("101101");
  bitset<N> m("000101");

  int res = n_convert(n.to_ulong(), m.to_ulong());
  cout << res << endl;

  return 0;
}
