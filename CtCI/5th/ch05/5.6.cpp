#include <iostream>

using namespace std;

int swap_odd_even(int x) {
  return ((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) <<  1);
}

int main() {
  int n = bitset<8>("10011100").to_ulong();
  int res = swap_odd_even(n);
  cout << bitset<8>(res).to_string() << endl;

  return 0;
}
