#include <iostream>
#include <bitset>
#include <vector>

using namespace std;

// O(N) time
int find_miss_num(vector<int>& num, int p) {
  if (num.empty()) {
    return 0;
  }
  vector<int> zeros, ones;

  for (int i = 0; i < num.size(); ++i) {
    if (num[i] & (1 << p)) {
      ones.push_back(num[i]);
    } else {
      zeros.push_back(num[i]);
    }
  }

  int res = 0;
  if (ones.size() >= zeros.size()) { // locks 0
    res = find_miss_num(zeros, p + 1);
    res <<= 1;
  } else {
    res = find_miss_num(ones, p + 1);
    res = (res << 1) | 1;
  }

  return res;
}

int main() {
  vector<int> num;
  int n = 8, miss = 6;

  for (int i = 0; i <= n; ++i) {
    if (i != miss) {
      num.push_back(i);
    }
  }

  int res = find_miss_num(num, 0);
  cout << res << endl;

  return 0;
}
