#include <iostream>
#include <vector>
#include <bitset>

using namespace std;

// (x1, y) and (x2, y) are the position of pixel
// x1, x2 start from 0
void draw_horizon_line(vector<char>& screen, int width, int x1, int x2, int y) {
  //int height = screen.size() / width;

  int x1_start = x1 / 8, x1_off = x1 % 8;
  int x2_end = x2 / 8, x2_off = x2 % 8;

  if (x1_off != 0) {
    ++x1_start;
  }
  if (x2_off != 7) {
    --x2_end;
  }

  int start = y * width + x1_start;
  int end = y * width + x2_end;
  for (int i = start; i <= end; ++i) {
    screen[i] |= 0xff;
  }

  //int start_mask = 0xff >> x1_off;
  int start_mask = (1 << (8 - x1_off)) - 1;
  //int end_mask = ~(0xff >> (x2_off + 1));
  int end_mask = ~((1 << (8 - x2_off - 1)) - 1);

  if (x1 / 8 == x2 / 8) {
    screen[y * width + x1 + 8] = start_mask & end_mask;
  } else {
    screen[start - 1] |= start_mask;
    screen[end + 1] |= end_mask;
  }
}

int main() {
  int w = 2, h = 5;
  vector<char> screen(w * h);

  draw_horizon_line(screen, w, 2, 10, 4);
  for (int i = 0; i < w * h; ++i) {
    for (int j = 7; j >= 0; --j) {
      printf("%d", (screen[i] & (1 << j)) > 0);
    }
    if ((i + 1) % w == 0) {
      putchar('\n');
    }
  }

  return 0;
}
