#include <iostream>
#include <cmath>

using namespace std;

const double EPS = 1e-5;

class Line {
public:
  double slope;
  double yintercept;

  explicit Line(double slope, double yintercept): slope(slope)
                                                  , yintercept(yintercept) {
  }
};

// same line -> intersect
bool intersect(const Line& l1, const Line& l2) {
  return (fabs(l1.slope - l2.slope) > EPS
    || fabs(l1.yintercept - l2.yintercept) < EPS);
}

int main() {
  return 0;
}
