#include <iostream>
#include <cassert>

using namespace std;

int _negate(int a) {
  int res = 0;
  int sig = a > 0 ? -1 : 1;

  while (a) {
    a += sig;
    res += sig;
  }
  return res;
}

int subtract(int a, int b) {
  return a + _negate(b);
}

int multiply(int a, int b) {
  int res = 0;
  if (a > b) {
    swap(a, b);
  }

  for (int i = abs(a); i > 0; --i) { // !
    res += b;
  }
  return a > 0 ? res : _negate(res);
}

int divide(int a, int b) {
  assert(b != 0);

  int res = 0, sum = 0;
  int absa = abs(a), absb = abs(b);
  while (sum + absb <= absa) {
    sum += absb;
    ++res;
  }
  if ((a < 0 && b > 0) || (a > 0 && b < 0)) {
    return _negate(res);
  }
  return res;
}

int main() {
  cout << subtract(8, 5) << endl;
  cout << multiply(4, 8) << endl;
  cout << divide(-8, 2) << endl;

  return 0;
}
