#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

const double EPS = 1E-5;

class _Point {
public:
  double x;
  double y;
  explicit _Point(double x = 0, double y = 0): x(x), y(y) {
  }
  bool operator==(const _Point& o) const {
    return (x - o.x) < EPS && (y - o.y) < EPS;
  }

  bool operator<(const _Point& o) const {
    return x < o.x || (fabs(x - o.x) < EPS && y < o.y);
  }
};

class Square {
public:
  double x;
  double y;
  double side;
  explicit Square(double x, double y, double side): x(x), y(y), side(side) {
  }

  _Point middle() {
    return _Point((x + side) / 2, (y + side) / 2);
  }
};

class Line {
public:
  _Point x;
  _Point y;
  explicit Line(_Point a, _Point b): x(a), y(b) {
  }
};

_Point extend(_Point a, _Point b, int side) {
  // b -> a
  double xdir = b.x > a.x ? -1 : 1;
  double ydir = b.y > a.y ? -1 : 1;

  if (fabs(a.x - b.x) < EPS) {
    return _Point(a.x, a.y + ydir * (side / 2));
  }

  double slope = (a.y - b.y) / (a.x - b.x);
  _Point res;

  if (fabs(slope - 1) < EPS) {
    res.x = a.x + xdir * side / 2;
    res.y = a.y + ydir * side / 2;
  } else if (slope > 1) {
    res.y = res.y + ydir * side / 2;
    res.x = (res.y - a.y) / slope + a.x;
  } else {
    res.x = res.x + xdir * side / 2;
    res.y = (res.x - a.x) * slope + a.y;
  }

  return res;
}

Line cut(Square sa, Square sb) {
  _Point mida = sa.middle();
  _Point midb = sb.middle();
  _Point p1 = extend(mida, midb, sa.side);
  _Point p2 = extend(mida, midb, -sa.side);
  _Point p3 = extend(midb, mida, sb.side);
  _Point p4 = extend(midb, mida, -sb.side);

  _Point start = p1, end = p1;
  vector<_Point> ps = {p2, p3, p4};
  for (_Point& p : ps) {
    if (p < start) {
      start = p;
    } else if (end < p) {
      end = p;
    }
  }

  return Line(start, end);
}

int main() {
  return 0;
}
