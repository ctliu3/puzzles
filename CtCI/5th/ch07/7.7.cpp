#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int find_kth(vector<int> v, int K) {
  int n = v.size();
  vector<queue<int>> que(n);

  que[0].push(1);
  int minval, k = 0;
  vector<int> mins(n);
  while (k < K) {
    minval = INT_MAX;
    for (int i = 0; i < n; ++i) {
      mins[i] = que[i].empty() ? INT_MAX : que[i].front();
      minval = min(minval, mins[i]);
    }
    printf("%2dth: %4d\n", k + 1, minval);
    if (k == K - 1) {
      break;
    }

    for (int i = 0; i < n; ++i) {
      if (minval == mins[i]) {
        que[i].pop();
        for (int j = i; j < n; ++j) { // !!
          que[j].push(minval * v[j]);
        }
        break;
      }
    }
    ++k;
  }

  return minval;
}

int main() {
  int res = find_kth({3, 5, 7}, 14);
  cout << res << endl;

  return 0;
}
