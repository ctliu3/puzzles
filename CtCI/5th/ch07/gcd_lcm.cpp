#include <iostream>

using namespace std;

int gcd(int a, int b) {
  while (a) {
    int c = a;
    a = b % a;
    b = c;
  }
  return b;
}

// a * b = gcd(a, b) * lcm(a, b)
int lcm(int a, int b) {
  return a * b / gcd(a, b);
}

int main() {
  cout << gcd(4, 6) << endl;
  cout << gcd(5, 10) << endl;
  cout << gcd(4, 9) << endl;

  cout << lcm(4, 8) << endl;
  cout << lcm(4, 9) << endl;
  return 0;
}
