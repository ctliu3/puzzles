#include <iostream>
#include <vector>

using namespace std;

const int MAXN = 1000;

void prime() {
  vector<bool> bprime(MAXN, true);
  vector<int> primes;

  bprime[0] = bprime[1] = false;
  for (int i = 2; i < MAXN; ++i) {
    if (bprime[i]) {
      primes.push_back(i);
      for (int j = i * i; j < MAXN; j += i) {
        bprime[j] = false;
      }
    }
  }

  for (auto& p : primes) {
    cout << p << ' ';
  }
  cout << endl;
}

int main() {
  prime();
  return 0;
}
