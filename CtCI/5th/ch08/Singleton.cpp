#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

class Singleton {
public:
  Singleton* get_instance() {
    if (m_instance == nullptr) { // first test
      m_mutex.lock();
      if (m_instance == nullptr) { // second test
        m_instance = new Singleton();
      }
      m_mutex.unlock();
    }
    return m_instance;
  }

private:
  mutex m_mutex;
  static Singleton* m_instance;
  Singleton() {
  }
};

Singleton *Singleton::m_instance = nullptr;

int main() {
  return 0;
}
