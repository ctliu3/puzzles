#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

class Box {
public:
  int w;
  int h;
  int d;
  explicit Box(int w, int h, int d): w(w), h(h), d(d) {
  }

  bool operator<(const Box& o) const {
    return w < o.w && h < o.h && d < o.d;
  }

  void to_string() {
    printf("{%d, %d, %d}\n", w, h, d);
  }
};

int max_stack_height(vector<Box>& boxes) {
  sort(boxes.begin(), boxes.end());
  //for (Box& box : boxes) {
    //box.to_string();
  //}

  int n = boxes.size();
  int res = 0;
  vector<int> dp(n);

  dp[0] = boxes[0].h;
  for (int i = 1; i < n; ++i) {
    for (int j = 0; j < i; ++j) {
      if (boxes[j] < boxes[i]) {
        dp[i] = max(dp[i], dp[j] + boxes[i].h);
      }
    }
    res = max(res, dp[i]);
  }

  return res;
}

int main() {
  vector<Box> boxes = {Box(3, 2, 3), Box(4, 4, 5), Box(1, 6, 1)};

  int res = max_stack_height(boxes);
  cout << res << endl;

  return 0;
}
