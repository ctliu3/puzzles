#include <iostream>
#include <map>
#include <vector>

using namespace std;

// true
map<char, vector<pair<bool, bool>>> true_next
= {{'&', {{1, 1}}}, {'|', {{0, 1}, {1, 1}, {0, 1}}}, {'^', {{0, 1}, {1, 0}}}};

// false
map<char, vector<pair<bool, bool>>> false_next
= {{'&', {{0, 0}}}, {'|', {{0, 0}}}, {'^', {{0, 0}, {1, 1}}}};

int count(const string& exp, bool res, int start, int end,
  vector<vector<vector<int>>>& dp) {
  int& cur = dp[start][end][res];
  if (cur != -1) {
    return cur;
  }
  if (start == end) { // boundary
    if ((res && exp[start] == '1') || (!res && exp[start] == '0')) {
      return cur = 1;
    }
    return cur = 0;
  }

  cur = 0;
  if (res) {
    for (int i = start + 1; i <= end; i += 2) {
      char op = exp[i];
      for (auto it : true_next[op]) {
        cur += count(exp, it.first, start, i - 1, dp)
          * count(exp, it.second, i + 1, end, dp);
      }
    }
  } else {
    for (int i = start + 1; i <= end; i += 2) {
      char op = exp[i];
      for (auto it : false_next[op]) {
        cur += count(exp, it.first, start, i - 1, dp)
          * count(exp, it.second, i + 1, end, dp);
      }
    }
  }

  //printf("dp[%d][%d][%d] = %d\n", start, end, res, cur);
  return cur;
}

int main() {
  string exp = "1^0|0|1";
  int n = exp.size();
  vector<vector<vector<int>>> dp(n, vector<vector<int>>(n, vector<int>(2, -1)));

  int res = count(exp, true, 0, n - 1, dp);
  cout << res << endl;

  return 0;
}
