#include <iostream>
#include <vector>

using namespace std;

int find_magic(vector<int>& a) {
  int start = 0;
  int end = a.size() - 1;

  while (start <= end) {
    int mid = start + (end - start) / 2;
    if (a[mid] == mid) {
      return mid;
    }
    if (a[mid] > mid) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }
  return -1;
}

int _find_magic_dup(vector<int>& a, int start, int end) {
  //printf("start = %d, end = %d\n", start, end);
  if (end < start || start < 0 || end >= a.size()) {
    return -1;
  }
  int mid = start + (end - start) / 2;
  if (a[mid] == mid) {
    return mid;
  }

  int res = _find_magic_dup(a, start, min(mid - 1, a[mid]));
  if (res > 0) {
    return res;
  }

  return _find_magic_dup(a, max(mid + 1, a[mid]), end);
}

int find_magic_dup(vector<int>& a) {
  return _find_magic_dup(a, 0, a.size() - 1);
}

void test_find_magic() {
  vector<int> v = {-2, -1, 1, 2, 4};
  int res = find_magic(v);
  cout << res << endl;
}

void test_find_magic_dup() {
  vector<int> v = {-10, -5, 2, 2, 2, 3, 4, 7, 9, 12, 13}; // 2
  int res = find_magic_dup(v);
  cout << res << endl;
}

int main() {
  //test_find_magic();
  test_find_magic_dup();

  return 0;
}
