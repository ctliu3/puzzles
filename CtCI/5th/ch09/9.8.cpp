#include <iostream>
#include <vector>

using namespace std;

int calc_ways(vector<int>& cents, int money) {
  int n = cents.size();
  vector<int> dp(money + 1, 0);

  dp[0] = 1;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j <= money; ++j) {
      if (j - cents[i] >= 0) {
        dp[j] += dp[j - cents[i]];
      }
    }
  }

  return dp[money];
}

int main() {
  vector<int> cents = {1, 5, 10, 25};
  int money = 15;
  int res = calc_ways(cents, money);
  cout << res << endl;

  return 0;
}
