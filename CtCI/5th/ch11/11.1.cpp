#include <iostream>

using namespace std;

void merge(int a[], int n, int b[], int m) {
  int i = n - 1, j = m - 1;
  int last = n + m - 1;

  while (i >= 0 && j >= 0) {
    a[last--] = a[i] < b[j] ? b[j--] : a[i--];
  }
  while (j >= 0) {
    a[last--] = b[j--];
  }
}

int main() {
  int a[9] = {3, 4, 9, 10, 11};
  int b[5] = {4, 5, 9, 10};

  merge(a, 5, b, 4);
  for (int& it : a) {
    cout << it << ' ';
  }
  cout << endlj

  return 0;
}
