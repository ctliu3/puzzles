#include <iostream>
#include <vector>
#include <string>

using namespace std;

int binary_serach(vector<string>& strs, const string& target) {
  int start = 0;
  int end = strs.size() - 1;

  while (start <= end) {
    int mid = start + (end - start) / 2;

    // find the non-closet string (element)
    if (strs[mid].empty()) {
      int left = mid - 1, right = mid + 1;
      while (true) {
        if (left < start && right > end) {
          return -1;
        } else if (left >= start && !strs[left].empty()) {
          mid = left;
          break;
        } else if (right <= end && !strs[right].empty()) {
          mid = right;
          break;
        }
        ++left;
        --right;
      }
    }

    if (strs[mid] == target) {
      return mid;
    } else if (strs[mid] < target) {
      start = mid + 1;
    } else {
      end = mid - 1;
    }
  }

  return -1;
}

int main() {
  vector<string> strs = {"apple", "", "", "banana", "", "", "", "carrot", 
    "duck", "", "", "eel", "", "flower"};

  int res = binary_serach(strs, "banana");
  cout << res << endl;

  return 0;
}
