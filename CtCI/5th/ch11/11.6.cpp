#include <iostream>
#include <vector>

using namespace std;

// A little different from `Search a 2D Matrix` in leetcode

class Point {
public:
  int x;
  int y;
  explicit Point(int x,int y): x(x), y(y) {
  }

  bool opeator<(const Point& o) const {
    return x < o.x && y < o.y;
  }

  bool operator==(const Point& o) const {
    return x == o.x && y == o.y;
  }
};

Point get_mid(const Point& a, const point& b) {
  return Point((a.x + b.x) / 2, (a.y + b.y) / 2);
}

Point _search(vector<vector<int>>& matrix, int target, Point tp_lt, Point bt_rt,
  Point tp_rt, Point bt_lt) {
  if (bt_rt < tp_lt) {
    return Point(-1, -1);
  }
  if (matrix[tp_lt.x][tp_lt.y] > target || matrix[bt_rt.x][bt_rt.y] < target) {
    return Point(-1, -1);
  }

  Point start = tp_lt, end = bt_rt;
  while (start <= end) {
    Point mid = get_mid(start, end);
    //
  }

  // check the top right part and bottom left part
  Point mid = get_mid(tp_lt, bt_rt);
}

Point search(vector<vector<int>>& matrix, int target) {
  if (matrix.empty() and matrix[0].empty()) {
    return -1;
  }
  int n = matrix.size(), m = matrix[0].size();

  return _search(matrix, target, Point(0, 0), Point(n - 1, m - 1), Point(0, m - 1),
    Point(n - 1, 0));
}

int main() {
  vector<vector<int>> matrix = {{15, 20, 40, 85}, {20, 35, 80, 95},
    {30, 55, 95, 105}, {40, 80, 100, 120}};

  Point res = search(matrix, 95);

  return 0;
}
