#include <iostream>
#include <vector>

using namespace std;

class Person {
public:
  int height;
  int weight;
  explicit Person(int h, int w) : height(h), weight(w) {
  }

  bool operator<(const Person& o) const {
    return height < o.height && weight < o.weight;
  }
};

int solve(vector<Person>& per) {
  int n = per.size();
  vector<Person> mono;
  for (int i = 0; i < n; ++i) {
    // find the first place, where per[i] can insert into (that is, first place >= per[i])
    int pos = lower_bound(mono.begin(), mono.end(), per[i]) - mono.begin();
    if (pos == -1) {
      mono.push_back(per[i]);
    } else {
      mono[pos] = per[i];
    }
  }
  return mono.size();
}

int main() {
  return 0;
}
