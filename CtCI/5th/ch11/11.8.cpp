#include <iostream>

using namespace std;

class RankNode {
public:
  int value;
  int count; // count the number of left children
  RankNode* left, *right;

  explicit RankNode(int value = 0): value(value), count(0), left(nullptr)
                                    , right(nullptr) {
  }
};

class Tree {
public:
  RankNode* root;

  explicit Tree(): root(nullptr) {
  }

  void track(int x) {
    _track(x, root);
  }

  int get_rank(int x) {
    return _get_rank(x, root);
  }

private:
  void _track(int x, RankNode* cur) {
    if (cur == nullptr) {
      cur = new RankNode(x);
    } else if (cur->value > x) {
      ++cur->count;
      _track(x, cur->left);
    } else {
      _track(x, cur->right);
    }
  }

  int _get_rank(int x, RankNode* cur) {
    if (cur == nullptr) {
      return -1;
    }
    if (x == cur->value) {
      return cur->count;
    } else if (cur->value < x) {
      return _get_rank(x, cur->left);
    } else {
      int right_count = _get_rank(x, cur->right);
      if (right_count == -1) {
        return -1;
      }
      return cur->count + 1 + right_count;
    }
  }
};

int main() {
  return 0;
}
