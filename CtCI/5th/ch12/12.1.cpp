#include <iostream>

using namespace std;

int main() {
  unsigned int i;
  for (i = 100; i >= 0; --i) { // i > 0
    printf("%d\n", i); // %u
  }

  return 0;
}
