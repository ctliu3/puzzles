#include <iostream>

using namespace std;

int** my2DAlloc(int rows, int cols) {
  int** rowptr = (int**) malloc(rows * sizeof(int*));

  for (int i = 0; i < rows; ++i) {
    rowptr[i] = (int*) malloc(cols * sizeof(int));
  }
  return rowptr;
}

void my2DDealloc(int** rowptr, int rows) {
  for (int i = 0; i < rows; ++i) {
    free(rowptr[i]);
  }
  free(rowptr);
}

// more efficient way to allocate 2d array
int** my2DAlloc2(int rows, int cols) {
  int header = rows * sizeof(int*);
  int data = rows * cols * sizeof(int);
  int** rowptr = (int**) malloc(header + data);
  if (! rowptr) {
    return nullptr;
  }

  int* buf = (int*) (rowptr + rows);
  for (int i = 0; i < rows; ++i) {
    rowptr[i] = buf + i * rows;
  }
  return rowptr;
}

int main() {
  return 0;
}
