#include <iostream>
#include <string>
#include <cassert>
#include <vector>

using namespace std;

enum class Piece {
  Empty,
  White,
  Black
};

bool is_valid(vector<vector<Piece>>& board) {
  // check row
  assert(board.size() && board.size() == board[0].size());
  int n = board.size();

  for (int i = 0; i < n; ++i) {
    if (board[i][0] != Piece::Empty) {
      int j = 0;
      for (; j < n; ++j) {
        if (board[i][j] != board[i][0]) {
          break;
        }
      }
      if (j == n) {
        return true;
      }
    }
  }

  // check column
  for (int j = 0; j < n; ++j) {
    if (board[0][j] != Piece::Empty) {
      int i = 0;
      for (; i < n; ++i) {
        if (board[i][j] != board[0][j]) {
          break;
        }
      }
      if (i == n) {
        return true;
      }
    }
  }

  // check principle diagonal
  if (board[0][0] != Piece::Empty) {
    int i = 1;
    for (; i < n; ++i) {
      if (board[i][i] == board[0][0]) {
        break;
      }
    }
    if (i == n) {
      return true;
    }
  }

  // check counter diagonal
  if (board[0][n - 1] != Piece::Empty) {
    int i = 1;
    for (; i < n; ++i) {
      if (board[i][n - i - 1] == board[0][n - 1]) {
        break;
      }
    }
    if (i == n) {
      return true;
    }
  }

  return false;
}

int main() {
  return 0;
}

