int rand_num(int lower, int high) { // inclusive
  return lower + (int)floor(rand() % (high - lower + 1));
}

void shuffle(vector<int>& card) {
  for (int i = 0; i < card.size(); ++i) {
    int rand_index = rand_num(0, i);
    swap(card[i], card[rand_index]);
  }
}
