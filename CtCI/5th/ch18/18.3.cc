int rand(int lower, int high) {
return lower + (int)floor(rand() % (high - lower + 1));
}

vector<int> random_m(vector<int>& arr, int m) {
  int n = arr.size();
  assert(n > m);

  vector<int> rand_arr(m);
  for (int i = 0; i < m; ++i) {
    if (i < m) {
      rand_arr[i] = arr[i];
    } else {
      int rand_index = rand(0, i); // inclusive
      if (rand_index < m) {
        rand_arr[rand_index] = arr[i];
      }
    }
  }
  return rand_arr;
}
