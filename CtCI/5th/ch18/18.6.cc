#include <iostream>

using namespace std;

int partition(int arr[], int start, int end) {
  int pivot_index = start + (end - start) / 2;
  int pivot = arr[pivot_index];
  int left = start;
  swap(arr[pivot_index], arr[end]);
  for (int i = start; i < end; ++i) {
    if (arr[i] <= pivot) {
      swap(arr[left++], arr[i]);
    }
  }
  swap(arr[left], arr[end]);
  return left;
}

int get_max(int arr[], int left, int right) {
  int res = arr[left];
  for (int i = left + 1; i <= right; ++i) {
    res = max(res, arr[i]);
  }
  return res;
}

int get_rank(int arr[], int left, int right, int rank) { // rank is 0-based
  int pivot_index = partition(arr, left, right);
  int left_size = pivot_index - left + 1;

  if (left_size == rank + 1) {
    return get_max(arr, left, pivot_index);
  } else if (left_size < rank + 1) {
    return get_rank(arr, pivot_index + 1, right, rank - left_size);
  } else {
    // if pivot_index does not minus one, then dead recursion
    return get_rank(arr, left, pivot_index - 1, rank);
  }
}

int main() {
  int arr[] = {1, 4, 3, 9, 8, 4, 10, 0};
  //int arr[] = {1, 4, 3, 4, 10, 0};
  int n = sizeof(arr) / sizeof(arr[0]);
  int res = get_rank(arr, 0, n - 1, 5);
  printf("%d\n", res);

  return 0;
}
