#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

bool can_compose(string word, unordered_map<string, bool>& dict, bool is_origin) {
  if (dict.count(word) && !is_origin) {
    return dict[word];
  }

  int n = word.size();
  for (int i = 1; i <n; ++i) {
    string left_string = word.substr(0, i);
    string right_string = word.substr(i);
    bool b_left = false, b_right = false;
    if (can_compose(left_string, dict, false)) {
      dict[left_string] = b_left = true;
    }
    if (b_left && can_compose(right_string, dict, false)) {
      dict[right_string] = b_right = true;
    }
    if (b_left && b_right) {
      return true;
    }
  }
  return dict[word] = false;
}

string find_longest_string(vector<string>& words) {
  unordered_map<string, bool> dict;
  for (auto& str : words) {
    dict[str] = true;
  }
  sort(words.begin(), words.end(), [](const string& lhs, const string& rhs) {
    return lhs.size() > rhs.size();
    });
  auto last = unique(words.begin(), words.end());
  words.resize(distance(words.begin(), last));
  for (auto& str : words) {
    if (can_compose(str, dict, true)) {
      return str;
    }
  }
  return "";
}

int main() {
  vector<string> vs = {"abc", "a", "bc", "abcded", "d", "e", "d"};
  string res = find_longest_string(vs);
  cout << res << endl;

  return 0;
}
