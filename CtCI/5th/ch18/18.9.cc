#include <iostream>
#include <queue>
#include <vector>

using namespace std;

priority_queue<int, vector<int>, greater<int>> minpq;
priority_queue<int, vector<int>, less<int>> maxpq;

void add_num(int num) {
  if (maxpq.size() == minpq.size()) {
    if (minpq.empty() || minpq.top() > num) {
      maxpq.push(num);
    } else {
      maxpq.push(minpq.top());
      minpq.pop();
      minpq.push(num);
    }
  } else {
    if (maxpq.top() < num) {
      minpq.push(num);
    } else {
      minpq.push(maxpq.top());
      maxpq.pop();
      maxpq.push(num);
    }
  }
}

double get_median() {
  if (minpq.empty() && maxpq.empty()) {
    // error
  }
  if (maxpq.size() == minpq.size()) {
    return(minpq.top() + maxpq.top()) / 2.0;
  } else {
    // maxpq.size() is always larger than minpq.size()
    return maxpq.top();
  }
}

int main() {
  add_num(9);
  add_num(3);
  add_num(5);
  add_num(6);
  add_num(0);
  cout << get_median() << endl;

  return 0;
}
