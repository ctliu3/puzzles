# puzzles

Using many kinds of fancy languages to solve algorithms and puzzles.

### CtCI
solutions for [Cracking the Coding Interview][1]

### [codejam][5]
algorithm competition

### problem99
99 interesting problems (solved in `Haskell`, `Scala` and `Lisp`)

### [leetcode][3]
interview algorithm problems

- Algorithms `265/275`

### [projecteuler][4]
mathematics problems 

[1]: http://www.amazon.com/Cracking-Coding-Interview-Programming-Questions/dp/098478280X
[2]: http://www.haskell.org/haskellwiki/H-99:_Ninety-Nine_Haskell_Problems
[3]: https://oj.leetcode.com/problems/
[4]: http://projecteuler.net/
[5]: https://code.google.com/codejam/

## License

[MIT](http://opensource.org/licenses/MIT)