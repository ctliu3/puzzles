#include <iostream>
#include <cstdio>
#include <string>
#include <set>

using namespace std;

int main() {
  freopen("A-small-practice.in", "r", stdin);
  freopen("A-small-practice.out", "w", stdout);

  char dict[27] = "yhesocvxduiglbkrztnwjpfmaq";
  string line;
  int t, cas = 1;
  
  scanf("%d\n", &t);
  while (t--) {
    printf("Case #%d: ", cas++);
    getline(cin, line);
    for (auto i : line) {
      if (i == ' ') {
        putchar(' ');
      } else {
        putchar(dict[i - 'a']);
      }
    }
    putchar('\n');
  }
  return 0;
}
