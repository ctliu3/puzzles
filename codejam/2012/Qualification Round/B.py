t = input()

for cas in xrange(t):
  print 'Case #{0}:'.format(cas + 1),
  sp = map(int, raw_input().split())
  n = sp[0]
  s = sp[1]
  p = sp[2]
  a = sp[3:]
  a.sort()
  
  ans = 0
  for v in a:
    if p + max(p - 1, 0) * 2 > v:
      if s > 0 and v >= p + max(p - 2, 0) * 2:
        ans += 1
        s -= 1
    else:
      ans += 1

  print ans
