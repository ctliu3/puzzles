#include <iostream>

using namespace std;

const int MAXN = 305;

int dx[] = {-1, 1, 0, 0, -1, -1, 1, 1};
int dy[] = {0, 0, -1, 1, -1, 1, -1, 1};

char mat[MAXN][MAXN];
int a[MAXN][MAXN];

bool check(int x, int y, int n) {
  return x >= 0 && x < n && y >= 0 && y < n;
}

void dfs(int x, int y, int n) {
  a[x][y] = -1;

  for (int i = 0; i < 8; ++i) {
    int nx = x + dx[i];
    int ny = y + dy[i];
    if (!check(nx, ny, n)) {
      continue;
    }
    if (a[nx][ny] == 0) {
      dfs(nx, ny, n);
    } else if (a[nx][ny] != -1) {
      a[nx][ny] = -1;
    }
  }
}

int gao(int n) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      if (a[i][j] == 0 && mat[i][j] != '*') {
        dfs(i, j, n);
        ++cnt;
      }
    }
  }

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      if (mat[i][j] != '*' && a[i][j] != -1) {
        ++cnt;
      }
    }
  }

  return cnt;
}

int main() {
  //freopen("in", "r", stdin);

  freopen("A-large-practice.in", "r", stdin);
  freopen("A-large-practice.out", "w", stdout);

  int t, cas = 1;

  cin >> t;
  while (t--) {
    int n;
    cin >> n;

    memset(a, 0, sizeof(a));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        scanf(" %c", &mat[i][j]);
      }
    }

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        if (mat[i][j] == '*') {
          continue;
        }
        int acc = 0;
        for (int k = 0; k < 8; ++k) {
          int x = i + dx[k], y = j + dy[k];
          if (check(x, y, n) && mat[x][y] == '*') {
            ++acc;
          }
        }
        a[i][j] = acc;
      }
    }
    int res = gao(n);

    printf("Case #%d: %d\n", cas++, res);
  }

  return 0;
}
