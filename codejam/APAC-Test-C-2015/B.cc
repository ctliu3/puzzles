#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

bool digit[10];
int dp[1000005];

bool exist(int val) {
  while (val) {
    if (!digit[val % 10]) {
      return false;
    }
    val /= 10;
  }
  return true;
}

int click(int val) {
  int ret = 0;
  while (val) {
    val /= 10;
    ++ret;
  }
  return ret;
}

int gao(int n) {
  if (n == 0 || n == 1 || n == 2 || n == 3 || n == 5 || n == 7) {
    if (exist(n)) {
      dp[n] = click(n);
    } else {
      dp[n] = -1;
    }
    return dp[n];
  }
  if (dp[n] != -1) {
    return dp[n];
  }

  int ret = -1;
  for (int i = 2; i <= ceil(sqrt(n)); ++i) {
    if (n % i == 0) {
      int a = gao(i), b = gao(n / i);
      if (a != -1 && b != -1) {
        int cur = a + b + 1;
        if (ret == -1 || cur < ret) {
          ret = cur;
        }
      }
    }
  }

  if (exist(n)) {
    int cur = click(n);
    if (ret == -1 || cur < ret) {
      ret = cur;
    }
  }
  return dp[n] = ret;
}

int main() {
  //freopen("in", "r", stdin);

  freopen("C-large-practice.in", "r", stdin);
  freopen("C-large-practice.out", "w", stdout);

  int t, cas = 1;
  int n;

  cin >> t;
  while (t--) {
    memset(digit, false, sizeof(digit));

    int temp;
    for (int i = 0; i < 10; ++i) {
      cin >> temp;
      if (temp) {
        digit[i] = true;
      }
    }

    cin >> n;
    memset(dp, -1, sizeof(dp));
    int res = gao(n);

    printf("Case #%d: ", cas++);
    if (res == -1) {
      puts("Impossible");
    } else {
      printf("%d\n", res + 1);
    }
  }

  return 0;
}
