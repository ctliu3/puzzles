#include <iostream>

using namespace std;

const int MAXN = 1005;
int a[MAXN][MAXN];
int c[MAXN][MAXN];
pair<int, int> b[1000005];

int dx[] = {-1, 1, 0, 0};
int dy[] = {0, 0, -1, 1};

bool check(int x, int y, int s) {
  return x >= 0 && x < s && y >= 0 && y < s;
}

int main() {
  //freopen("in", "r", stdin);
  //freopen("out", "w", stdout);
  freopen("A-large-practice.in", "r", stdin);
  freopen("A-large-practice.out", "w", stdout);
  int t, s;
  int cas = 1;

  scanf("%d", &t);
  while (t--) {
    scanf("%d", &s);
    for (int i = 0; i < s; ++i) {
      for (int j = 0; j < s; ++j) {
        scanf("%d", &a[i][j]);
        b[a[i][j]] = {i, j};
        c[i][j] = 1;
      }
    }

    for (int i = s * s; i >= 1; --i) {
      int x = b[i].first, y = b[i].second;
      for (int k = 0; k < 4; ++k) {
        int nx = dx[k] + x;
        int ny = dy[k] + y;
        if (check(nx, ny, s) && a[x][y] - 1 == a[nx][ny]) {
          c[nx][ny] = max(c[nx][ny], c[x][y] + 1);
        }
      }
    }

    int res = -1, room = s * s;
    for (int i = 0; i < s; ++i) {
      for (int j = 0; j < s; ++j) {
        if (c[i][j] > res) {
          res = c[i][j];
          room = a[i][j];
        } else if (c[i][j] == res) {
          room = min(room, a[i][j]);
        }
      }
    }

    printf("Case #%d: %d %d\n", cas++, room, res);
  }

  return 0;
}
