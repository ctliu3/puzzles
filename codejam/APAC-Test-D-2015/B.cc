#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

const int MAXN = 5005;
int in[MAXN], out[MAXN];
vector<int> p[5005];
int ans[5005];

int main() {
  //freopen("in", "r", stdin);
  //freopen("out", "w", stdout);
  freopen("B-large-practice.in", "r", stdin);
  freopen("B-large-practice.out", "w", stdout);
  int t, n, a, b;
  int cas = 1;

  scanf("%d", &t);
  while (t--) {
    scanf("%d", &n);

    memset(in, 0, sizeof(in));
    memset(out, 0, sizeof(out));
    for (int i = 0; i < n; ++i) {
      scanf("%d %d", &a, &b);
      ++in[a];
      ++out[b];
    }

    int m, cur = 0, temp;
    scanf("%d", &m);
    for (int i = 0; i < 5005; ++i) {
      p[i].clear();
    }
    for (int i = 0; i < m; ++i) {
      scanf("%d", &temp);
      p[temp].push_back(i);
    }

    memset(ans, 0, sizeof(ans));
    for (int i = 0; i < 5005; ++i) {
      cur += in[i];
      if (p[i].size()) {
        for (auto& idx : p[i]) {
          ans[idx] = cur;
        }
      }
      cur -= out[i];
    }

    printf("Case #%d:", cas++);
    for (int i = 0; i < m; ++i) {
      printf(" %d", ans[i]);
    }
    printf("\n");
  }

  return 0;
}
