#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

map<string, int> mp;
map<int, string> rmp;
vector<int> e[20005];
int d[20005];
int cnt = 0;

int get_id(string s) {
  if (mp.find(s) != mp.end()) {
    return mp[s];
  }
  mp[s] = cnt;
  rmp[cnt] = s;
  ++cnt;
  return cnt - 1;
}

int main() {
  //freopen("in", "r", stdin);
  //freopen("out", "w", stdout);
  freopen("C-large-practice.in", "r", stdin);
  freopen("C-large-practice.out", "w", stdout);

  int t, cas = 1;
  string sa, sb;

  cin >> t;
  while (t--) {
    int n;
    for (int i = 0; i < 20005; ++i) {
      e[i].clear();
    }
    cnt = 0;
    mp.clear();
    rmp.clear();

    cin >> n;
    memset(d, 0, sizeof(d));
    for (int i = 0; i < n; ++i) {
      cin >> sa >> sb;
      int a = get_id(sa);
      int b = get_id(sb);
      e[a].push_back(b);
      ++d[b];
    }

    printf("Case #%d:", cas++);
    for (int i = 0; i < cnt; ++i) {
      if (!d[i]){
        int u = i;
        while (e[u].size()) {
          int v = e[u][0];
          cout << ' ' << rmp[u] << '-' <<rmp[v];
          u = v;
        }
        break;
      }
    }
    putchar('\n');
  }

  return 0;
}
