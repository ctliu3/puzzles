#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

int dx[] = {-1, 1, 0, 0, -1, -1, 1, 1};
int dy[] = {0, 0, -1, 1, -1, 1, -1, 1};

int knight_move_dx[] = {-1, -1, 1, 1, -2, -2, 2, 2};
int knight_move_dy[] = {-2, 2, -2, 2, -1, 1, -1, 1};

char board[8][8];
vector<pair<int, int>> ps;

bool check(int x, int y) {
  return x >= 0 && x < 8 && y >= 0 && y < 8;
}

bool cankill(pair<int, int>& a, pair<int, int>& b) {
  int x = a.first, y = a.second;
  int p = b.first, q = b.second;
  char src = board[x][y];

  if (src == 'K') {
    for (int k = 0; k < 8; ++k) {
      int nx = x + dx[k];
      int ny = y + dy[k];
      if (nx == p && ny == q) {
        return true;
      }
    }
  } else if (src == 'Q') {
    for (int k = 0; k < 8; ++k) {
      int nx = x + dx[k];
      int ny = y + dy[k];
      while (check(nx, ny)) {
        if (nx == p && ny == q) {
          return true;
        }
        if (board[nx][ny] != '.') {
          break;
        }
        nx += dx[k];
        ny += dy[k];
      }
    }
  } else if (src == 'R') {
    for (int k = 0; k < 4; ++k) {
      int nx = x + dx[k];
      int ny = y + dy[k];
      while (check(nx, ny)) {
        if (nx == p && ny == q) {
          return true;
        }
        if (board[nx][ny] != '.') {
          break;
        }
        nx += dx[k];
        ny += dy[k];
      }
    }
  } else if (src == 'B') {
    for (int k = 4; k < 8; ++k) {
      int nx = x + dx[k];
      int ny = y + dy[k];
      while (check(nx, ny)) {
        if (nx == p && ny == q) {
          return true;
        }
        if (board[nx][ny] != '.') {
          break;
        }
        nx += dx[k];
        ny += dy[k];
      }
    }
  } else if (src == 'N') {
    for (int k = 0; k < 8; ++k) {
      int nx = x + knight_move_dx[k];
      int ny = y + knight_move_dy[k];
      if (nx == p && ny == q) {
        return true;
      }
    }
  } else if (src == 'P') {
    return (x - 1 == p && y - 1 == q) || (x - 1 == p && y + 1 == q);
  }

  return false;
}

int main() {
  //freopen("in", "r", stdin);
  //freopen("out", "w", stdout);
  freopen("D-large-practice.in", "r", stdin);
  freopen("D-large-practice.out", "w", stdout);

  int t, cas = 1;

  cin >> t;
  while (t--) {
    int n;

    ps.clear();
    for (int i = 0; i < 8; ++i) {
      for (int j = 0; j < 8; ++j) {
        board[i][j] = '.';
      }
    }
    cin >> n;
    for (int i = 0; i < n; ++i) {
      string s;
      cin >> s;
      board[7 - (s[0] - 'A')][7 - (s[1] - '1')] = s[3];
      ps.push_back({7 - (s[0] - 'A'), 7 - (s[1] - '1')});
    }
    int res = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        if (i == j) {
          continue;
        }
        if (cankill(ps[i], ps[j])) {
          ++res;
        }
      }
    }
    printf("Case #%d: %d\n", cas++, res);
  }

  return 0;
}
