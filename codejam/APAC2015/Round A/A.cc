#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <queue>
#include <stack>
#include <cstdio>
#include <cmath>
#include <sstream>
#include <set>

using namespace std;

#define dump(x) cerr << #x << " = " << (x) << endl;
#define pb push_back

typedef long long llong;

const double Pi = acos(-1.0);

vector<vector<int>> num = {{0, 1, 2, 3, 4, 5}, {1, 2}, {0, 1, 3, 4, 6}, {0, 1, 6, 2, 3}, {5, 6, 1, 2},
  {0, 5, 6, 2, 3}, {0, 5, 6, 2, 3, 4}, {0, 1, 2}, {0, 1, 2, 3, 4, 5, 6}, {0, 1, 2, 3, 5, 6}};
vector<string> s, vs;
int n;

string gao(int j) {
  int bad[7] = {false};
  int j2 = j;
  for (int i = 0; i < n; ++i) {
    for (int k = 0; k < 7; ++k) {
      if (s[i][k] == '0' and vs[j2][k] == '1') {
        bad[k] = true;
      }
      if (s[i][k] == '1' and vs[j2][k] == '0') {
        return {};
      }
    }
    j2 = ((j2 - 1) + 10) % 10;
  }

  for (int i = 0; i < n; ++i) {
    for (int k = 0; k < 7; ++k) {
      if (s[i][k] == '1' and bad[k]) {
        return {};
      }
      if (vs[j][k] == '1' and (s[i][k] == '1' or bad[k])) {
      } else {
        return {};
      }
    }
    j = ((j - 1) + 10) % 10;
  }

  string res = vs[j];
  for (int i = 0; i < 7; ++i) {
    if (res[i] == '1' && bad[i]) {
      res[i] = '0';
    }
  }
  return res;
}

int main() {
  freopen("A-small-practice.in", "r", stdin);
  freopen("A-small-practice.out", "w", stdout);
  //freopen("in", "r", stdin);
  int cas = 1;
  int T;

  vs.resize(10);
  for (int i = 0; i < 10; ++i) {
    vs[i] = string(7, '0');
    for (int j = 0; j < num[i].size(); ++j) {
      vs[i][num[i][j]] = '1';
    }
  }

  cin >> T;
  while (T--) {
    printf("Case #%d: ", cas++);

    cin >> n;
    s.resize(n);
    for (int i = 0; i < n; ++i) {
      cin >> s[i];
    }

    set<string> res;
    for (int i = 0; i < 10; ++i) {
      string ret = gao(i);
      if (ret.size()) {
        res.insert(ret);
      }
      if (res.size() > 1) {
        break;
      }
    }

    //dump(res.size());
    if (res.size() == 1) {
      cout << *res.begin() << endl;
    } else {
      printf("ERROR!\n");
    }
  }

  return 0;
}
