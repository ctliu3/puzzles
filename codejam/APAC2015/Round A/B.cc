#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <queue>
#include <stack>
#include <cstdio>
#include <cmath>
#include <sstream>
#include <set>

using namespace std;

#define dump(x) cerr << #x << " = " << (x) << endl;
#define pb push_back

typedef long long llong;

const double Pi = acos(-1.0);

const int MAXN = 105;
vector<int> v;
vector<vector<int>> a, res;

int main() {
  freopen("B-large.in", "r", stdin);
  freopen("B-large.out", "w", stdout);
  //freopen("out", "w", stdin);
  int cas = 1;
  int T, n;
  string op;

  cin >> T;
  while (T--) {
    printf("Case #%d:\n", cas++);

    cin >> n >> op;
    a.assign(n, vector<int>(n, 0));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        cin >> a[i][j];
      }
    }
    res.assign(n, vector<int>(n, 0));
    int i, j, k;
    if (op == "down") {
      for (j = 0; j < n; ++j) {
        v.clear();
        for (i = n - 1; i >= 0; --i) {
          if (a[i][j] != 0)
            v.push_back(a[i][j]);
        }
        i = n - 1;
        for (k = 0; k < v.size(); ++k) {
          if (k + 1 < v.size() && v[k] == v[k + 1]) {
            res[i--][j] = v[k] + v[k + 1];
            k++;
          } else
            res[i--][j] = v[k];
        }
      }
    } else if (op == "up") {
      for (j = 0; j < n; ++j) {
        v.clear();
        for (i = 0; i < n; ++i) {
          if (a[i][j] != 0)
            v.push_back(a[i][j]);
        }
        i = 0;
        for (k = 0; k < v.size(); ++k) {
          if (k + 1 < v.size() && v[k] == v[k + 1]) {
            res[i++][j] = v[k] + v[k + 1];
            k++;
          } else
            res[i++][j] = v[k];
        }
      }
    } else if (op == "left") {
      for (i = 0; i < n; ++i) {
        v.clear();
        for (j = 0; j < n; ++j) {
          if (a[i][j] != 0)
            v.push_back(a[i][j]);
        }
        j = 0;
        for (k = 0; k < v.size(); ++k) {
          if (k + 1 < v.size() && v[k] == v[k + 1]) {
            res[i][j++] = v[k] + v[k + 1];
            k++;
          } else
            res[i][j++] = v[k];
        }
      }
    } else if (op == "right") {
      for (i = 0; i < n; ++i) {
        v.clear();
        for (j = n - 1; j >= 0; --j) {
          if (a[i][j] != 0)
            v.push_back(a[i][j]);
        }
        j = n - 1;
        for (k = 0; k < v.size(); ++k) {
          if (k + 1 < v.size() && v[k] == v[k + 1]) {
            res[i][j--] = v[k] + v[k + 1];
            k++;
          } else
            res[i][j--] = v[k];
        }
      }
    }
    for (i = 0; i < n; ++i) {
      for (j = 0; j < n; ++j) {
        if (j != 0) {
          printf(" ");
        }
        printf("%d", res[i][j]);
      }
      puts("");
    }
  }

  return 0;
}
