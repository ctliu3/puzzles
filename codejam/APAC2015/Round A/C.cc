#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <queue>
#include <stack>
#include <cstdio>
#include <cmath>
#include <sstream>
#include <set>

using namespace std;

#define dump(x) cerr << #x << " = " << (x) << endl;
#define pb push_back

typedef long long llong;

const double Pi = acos(-1.0);

int main() {
  freopen("in", "r", stdin);
  freopen("out", "w", stdin);
  int cas = 1;
  int T;

  cin >> T;
  while (T--) {
    printf("Case #%d: ", cas++);

  }

  return 0;
}
