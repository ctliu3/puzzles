#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <queue>
#include <stack>
#include <cstdio>
#include <cmath>
#include <sstream>
#include <set>

using namespace std;

#define dump(x) cerr << #x << " = " << (x) << endl;
#define pb push_back

typedef long long llong;

const double Pi = acos(-1.0);

const int C = 100000;
vector<int> a;
vector<llong> now, one, ned;

int main() {
  //freopen("D-large.in", "r", stdin);
  //freopen("D-large.out", "w", stdout);
  freopen("in", "r", stdin);
  //freopen("out", "w", stdout);
  int cas = 1;
  int T;
  int n, m;

  cin >> T;
  while (T--) {
    printf("Case #%d: ", cas++);
    a.assign(1005, 0);
    now.assign(35, 0);
    one.assign(35, 0);
    ned.assign(35, 0);

    cin >> n >> m;
    for (int i = 30; i >= 0; --i) {
      if (m >= (1 << i)) {
        m -= (1 << i);
        ++one[i];
        int mm = m;
        for (int j = i - 1; j >= 0; --j) {
          if (mm >= (1 << j)) {
            mm -= (1 << j);
            one[j] += 2LL * (1 << (i - j));
          }
        }
      }
      if (one[i] > C) {
        one[i] = C;
      }
    }

    for (int i = 0; i < n; ++i) {
      cin >> a[i];
      ++ned[a[i]];
    }
    int res = 0;
    for (int i = 30; i >= 0; --i) {
      now[i] -= ned[i];
      if (one[i] > C) {
        one[i] = C;
      }
      if (now[i] > C) {
        now[i] = C;
      }
      if (now[i] < 0) {
        llong p = (-now[i] + one[i] - 1) / one[i];
        res += p;
        for (int j = i; j >= 0; --j) {
          now[j] += one[j] * p;
          if (now[j] > C) {
            now[j] = C;
          }
        }
      }
      if (i) {
        one[i - 1] += one[i] * 4;
        now[i - 1] += now[i] * 4;
      }
    }
    printf("%d\n", res);
  }

  return 0;
}

