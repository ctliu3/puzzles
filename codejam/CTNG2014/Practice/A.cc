#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;

vector<pair<string, string>> ps;
vector<vector<int>> e;
map<string, int> mp;
vector<int> visit;
int cnt;

void get_id(const string& s) {
  if (mp.count(s)) {
    return ;
  }
  mp[s] = cnt++;
}

bool dfs(int u, int color) {
  if (visit[u] != -1) {
    return true;
  }
  visit[u] = color;

  for (auto& v : e[u]) {
    if (visit[v] != -1) {
      if (visit[v] == visit[u]) {
        return false;
      }
    } else {
      if (! dfs(v, 1 - color)) {
        return false;
      }
    }
  }

  return true;
}

int main() {
  freopen("A-small-practice-2.in", "r", stdin);
  freopen("A-small-practice-2.out", "w", stdout);
  int t, m;
  int cas = 1;
  string sa, sb;

  cin >> t;
  while (t--) {
    mp.clear();
    cnt = 0;
    printf("Case #%d: ", cas);
    ++cas;
    cin >> m;

    ps.assign(m, {});
    for (int i = 0; i < m; ++i) {
      cin >> sa >> sb;
      ps[i] = {sa, sb};
      get_id(sa);
      get_id(sb);
    }

    e.assign(cnt, {});
    for (auto& p : ps) {
      int u = mp[p.first], v = mp[p.second];
      e[u].push_back(v);
      e[v].push_back(u);
    }

    visit.assign(cnt, -1);
    bool flag = false;
    for (int i = 0; i < cnt; ++i) {
      if (visit[i] == -1) {
        if (! dfs(i, 0)) {
          printf("No\n");
          flag = true;
          break;
        }
      }
    }
    if (! flag) {
      printf("Yes\n");
    }
  }
  return 0;
}
