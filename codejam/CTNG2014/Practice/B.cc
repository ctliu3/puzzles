#include <iostream>
#include <cmath>

using namespace std;

const double PI = acos(-1.0);

int main() {
  freopen("B-small-practice.in", "r", stdin);
  freopen("B-small-practice.out", "w", stdout);
  //freopen("in", "r", stdin);
  int cas = 1;
  int T;
  double v, d;

  cin >> T;
  while (T--) {
    printf("Case #%d: ", cas++);

    cin >> v >> d;
    double theta = asin(9.8 * d / v / v) / 2;
    double res = theta / PI * 180;
    printf("%.8f\n", res);
  }

  return 0;
}

