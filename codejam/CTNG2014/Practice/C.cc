#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>

using namespace std;

int main() {
  freopen("C-small-practice-2.in", "r", stdin);
  freopen("C-small-practice-2.out", "w", stdout);
  //freopen("in", "r", stdin);
  int t, n;
  int cas = 1;
  string buf;
  vector<string> s;

  cin >> t;
  while (t--) {
    printf("Case #%d: ", cas++);

    cin >> n;
    getline(cin, buf);
    s.resize(n);
    for (int i = 0; i < n; ++i) {
      getline(cin, s[i]);
    }

    int res = 0;
    vector<string> ss;
    ss.push_back(s[0]);
    for (int i = 1; i < n; ++i) {
      if (s[i] < ss[ss.size() - 1]) {
        ++res;
      }
      ss.push_back(s[i]);
      sort(ss.begin(), ss.end());
    }

    printf("%d\n", res);
  }

  return 0;
}
