import sys

sys.stdin = open('A-large-practice.in', 'r')
sys.stdout = open('A-large-practice.out', 'w')

n = input()

case = 1
numbers = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
rules = ["", "", "double", "triple", "quadruple", "quintuple", "sextuple", "septuple",
    "octuple", "nonuple", "decuple"];

def gao(s):
  n = len(s)
  i = 0
  while i < n:
    j = i + 1
    while j < n and s[i] == s[j]:
      j += 1
    if j - i > 10:
      sys.stdout.write((' ' + numbers[int(s[i])]) * (j - i))
    else:
      if j - i > 1:
        sys.stdout.write(' ' + rules[j - i])
      sys.stdout.write(' ' + numbers[int(s[i])])
    i = j

for _ in xrange(n):
  sys.stdout.write('Case #%d:' % case)
  case += 1
  sa, sb = raw_input().split()
  s = sb.split('-')
  start = 0
  for i in s:
    cur = sa[start:start + int(i)]
    gao(cur)
    start += int(i)
  print 
