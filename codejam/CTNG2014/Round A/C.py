import sys

sys.stdin = open('C-large-practice.in', 'r')
sys.stdout = open('C-large-practice.out', 'w')

t = input()
case = 1

for _ in xrange(t):
  sys.stdout.write('Case #%d:' % case)
  case += 1
  n = input()
  a = map(int, raw_input().split())
  c = [0 if ele & 1 == 1 else 1 for ele in a]
  res = [0] * n

  b = []
  for ele in a:
    if ele & 1:
      b.append(ele)
  b.sort()
  j = 0
  for i in xrange(n):
    if c[i] == 0:
      res[i] = b[j]
      j += 1

  b = []
  for ele in a:
    if ele & 1 == 0:
      b.append(ele)
  b.sort(reverse = True)
  j = 0
  for i in xrange(n):
    if c[i] == 1:
      res[i] = b[j]
      j += 1

  for i in xrange(n):
    sys.stdout.write(' %d' % res[i])
  print
