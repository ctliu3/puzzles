def solution(A):
  # write your code in Python 2.7
  n, i = len(A), 0
  while i < n:
    if A[i] <= n and A[i] != i + 1 and A[A[i] - 1] != A[i]:
      j = A[i] - 1
      A[i], A[j] = A[j], A[i]
    else:
      i += 1
  
  for i in xrange(n):
    if i + 1 != A[i]:
      return i + 1
  return n + 1
