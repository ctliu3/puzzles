// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {
  // write your code in C++11
  int res = -1;
  long long sum = 0, cur = 0;

  for (int i = 0; i < A.size(); ++i) {
    sum += A[i];
  }
  for (int i = 0; i < A.size() - 1; ++i) {
    cur += A[i];
    sum -= A[i];
    if (res == -1 || abs(sum - cur) < res) {
      res = abs(sum - cur);
    }
  }
  return res;
}
