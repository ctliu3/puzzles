package main

import (
  "fmt"
  "os"
  "bufio"
)

const N = 1005

type Edge struct {
  to, weight int
}

// Graph definition.
type Graph struct {
  n int
  edges [][]Edge
}

func (g *Graph) init(n int) {
  g.n = n
  g.edges = make([][]Edge, n)
  for i := 0; i < n; i++ {
    g.edges[i] = []Edge{}
  }
}

func (g *Graph) addEdge(u, v, w int) {
  // Add Bidirectional edge.
  g.edges[u] = append(g.edges[u], Edge{v, w})
  g.edges[v] = append(g.edges[v], Edge{u, w})
}

func main() {
  // Read data.
  //r := bufio.NewReader(os.Stdin)

  // File operation.
  f, err := os.Open("in")
  if err != nil {
    return
  }
  defer f.Close()

  r := bufio.NewReader(f)
  w := bufio.NewWriter(os.Stdout)

  var n, val int
  fmt.Fscanf(r, "%d ", &n)

  var a []int
  for i := 0; i < n; i++ {
    fmt.Fscanf(r, " %d", &val)
    a = append(a, val)
  }

  // Output data.
  for _, val := range a {
    fmt.Fprintf(w, "%d ", val)
  }
  w.Flush()
}
