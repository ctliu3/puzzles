package main

import (
  "fmt"
  "bufio"
  "os"
)

const N = 1000100

func main() {
  r := bufio.NewReader(os.Stdin)
  //f, err := os.Open("in")
  //if err != nil {
    //return
  //}
  //defer f.Close()

  var n, m int
  var a, b [N]int
  minval := 10001

  //r := bufio.NewReader(f)
  fmt.Fscanf(r, "%d ", &n)
  for i := 0; i < n; i++ {
    fmt.Fscanf(r, "%d ", &a[i])
  }

  fmt.Fscanf(r, "%d ", &m)
  for i := 0; i < m; i++ {
    fmt.Fscanf(r, "%d ", &b[i])
    if b[i] < minval {
      minval = b[i]
    }
  }

  var c [100]int
  for i := 0; i < m; i++ {
    c[b[i] - minval]++
  }

  for i := 0; i < n; i++ {
    c[a[i] - minval]--
  }

  for idx, val := range c {
    if val > 0 {
      fmt.Printf("%d ", idx + minval)
    }
  }
}
