#include <iostream>
#include <set>

using namespace std;

set<int> S;

int main() {
  int n, k, a;

  cin >> n >> k;
  for (int i = 0; i < n; ++i) {
    cin >> a;
    S.insert(a);
  }
  int ans = 0;
  for (set<int>::iterator itr = S.begin(); itr != S.end(); ++itr) {
    a = *itr + k;
    if (S.find(a) != S.end()) {
      ++ans;
    }
  }
  cout << ans << endl;
}
