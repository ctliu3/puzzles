#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int main() {
  //freopen("in", "r", stdin);
  int n, k;
  string s;

  cin >> n >> k;
  cin >> s;

  string res;
  int acc = 0, cnt = 0, idx = 0;
  for (int i = 0; i < n; ++i) {
    if (res.empty()) {
      res.push_back(s[i]);
      acc = s[i] - '0';
    } else {
      int cur = (s[i] - '0') ^ acc;
      res.push_back(cur + '0');
      acc ^= cur;
    }
    ++cnt;
    if (cnt >= k) {
      acc ^= res[idx] - '0';
      ++idx;
    }
  }

  cout << res << endl;

  return 0;
}

