#include <iostream>

using namespace std;

// If cannot find such element, return n
int _lower_bound(int A[], int n, int target) {
  int start = -1;
  int end = n;

  // The invariant relation is A[left] < target <= A[right]
  while (end - start > 1) {
    int mid = start + (end - start) / 2;
    if (A[mid] >= target) {
      end = mid;
    } else {
      start = mid;
    }
  }

  return end;
}

// If cannot find such element, return n
int _upper_bound(int A[], int n, int target) {
  int start = -1;
  int end = n;

  // The invariant relation is A[left] <= target < A[right]
  while (end - start > 1) {
    int mid = start + (end - start) / 2;
    if (A[mid] > target) {
      end = mid;
    } else {
      start = mid;
    }
  }

  return end;
}

// return the index of the target (first appear), if not exist, return -1
int _binary_search(int A[], int n, int target) {
  int start = -1;
  int end = n;

  while (end - start > 1) {
    int mid = start + (end - start) / 2;
    if (A[mid] >= target) {
      end = mid;
    } else {
      start = mid;
    }
  }

  if (end >= n || A[end] != target) {
    return -1;
  }
  return end;
}


int main() {
  int a[] = {1, 2, 3, 3, 5};
  //int res = _lower_bound(a, 5, 6);
  //int res = _upper_bound(a, 5, 0);
  int res = _binary_search(a, 5, 3);
  cout << res << endl;

  return 0;
}
