#include <iostream>
#include <vector>

using namespace std;

class ListNode {
public:
  int value;
  ListNode* next;
  explicit ListNode(int value): value(value), next(nullptr) {
  }
};

class List {
public:
  explicit List(): head(nullptr) {
  }

  void insert(int value) {
    ListNode* new_node = new ListNode(value);

    if (head == nullptr) {
      head = new_node;
    } else {
      ListNode* cur = head;
      while (cur && cur->next) {
        cur = cur->next;
      }
      cur->next = new_node;
    }
  }

  void remove(int value) {
    ListNode dummy(0), *cur = &dummy;
    cur->next = head;

    while (cur && cur->next) {
      if (cur->next->value == value) {
        ListNode* move = cur->next;
        cur->next = move->next;
        head = dummy.next; // !
        delete move;
        return ;
      }
      cur = cur->next;
    }
    printf("node not found.\n");
  }

  void print() {
    if (head == nullptr) {
      printf("This is a empty list.\n");
      return ;
    }
    ListNode* cur = head;
    while (cur) {
      printf("%d, ", cur->value);
      cur = cur->next;
    }
    printf("\n");
  }

private:
  ListNode* head;
};

int main() {
  vector<int> a = {2, 3, 1, 9};
  List list = List();
  for (auto& it : a) {
    list.insert(it);
  }
  list.remove(8);
  list.remove(2);
  list.remove(9);
  list.remove(3);
  list.remove(1);
  list.print();

  return 0;
}
