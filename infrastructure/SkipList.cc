#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

typedef struct SLNode* link;
typedef int Item;
typedef int Key;

#define LGN_MAX 4
#define NULLITEM nullptr
#define MAXKEY INT_MAX
#define MINKEY INT_MIN

#define key(A) (A)
#define less(A, B) (key(A) < key(B))
#define eq(A, B) (!less(A, B) && !less(B, A))

struct SLNode {
  Item item;
  link* next;
  int sz;
};

// end is the virtual node
link head, end, z;
int N, lgN;

// Search
Item* search_r(link cur, Key v, int k) {
  if (eq(v, key(cur->item))) {
    return &cur->item;
  }
  if (less(v, cur->next[k]->item)) {
    if (k == 0) {
      return NULLITEM;
    }
    return search_r(cur, v, k - 1);
  }
  return search_r(cur->next[k], v, k);
}

// Insert
void insert_r(link cur, link node, int k) {
  int v = node->item;

  if (less(v, cur->next[k]->item)) {
    if (k < node->sz) {
      node->next[k] = cur->next[k];
      cur->next[k] = node;
    }
    if (k == 0) {
      return ;
    }
    insert_r(cur, node, k - 1);
    return ;
  }
  insert_r(cur->next[k], node, k);
}

// Delete
void delete_r(link cur, Key v, int k) {
  link nt = cur->next[k];

  if (!less(key(nt->item), v)) {
    if (eq(v, key(nt->item))) {
      cur->next[k] = nt->next[k];
    }
    if (k == 0) {
      free(nt);
      return ;
    }
    delete_r(cur, v, k - 1);
    return ;
  }
  delete_r(cur->next[k], v, k);
}

// Create a node with k level
link create_node(Item item, int k) {
  link cur = (link)malloc(sizeof(link));
  cur->next = (link*)malloc(k * sizeof(link));
  cur->item = item;
  cur->sz = k;
  for (int i = 0; i < k; ++i) {
    cur->next[i] = end;
  }
  return cur;
}

int rand_x() {
  int i = 1, j = 2, t = rand();
  for (; i < LGN_MAX - 1; ++i, j += j) {
    if (t > RAND_MAX / j) {
      break;
    }
  }
  if (i > lgN) {
    lgN = i;
  }
  return i;
}

Item* sl_search(Key v) {
  return search_r(head, v, lgN);
}

void sl_insert(Key v) {
  link node = create_node(v, rand_x());
  insert_r(head, node, lgN);
  ++N;
}

void sl_delete(Key v) {
  delete_r(head, v, lgN);
  --N;
}

void sl_init() {
  N = lgN = 0;
  end = create_node(MAXKEY, 0);
  head = create_node(MINKEY, LGN_MAX);
}

int main() {
  sl_init();

  int n = 10;
  int arr[] = {4, 2, 100, -1, 2, 2, 9, 9, 1, 103};
  for (int i = 0; i < n; ++i) {
    sl_insert(arr[i]);
  }

  Item* res = sl_search(100);
  if (res) {
    printf("Key %d found\n", *res);
  } else {
    printf("Key not found\n");
  }

  sl_delete(100);
  res = sl_search(100);
  if (res) {
    printf("%d\n", *res);
  } else {
    printf("Key not found\n");
  }

  return 0;
}
