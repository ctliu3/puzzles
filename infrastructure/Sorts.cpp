#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>

using namespace std;

// Implementation of six kinds of sort algorithm
// * Bubble Sort
// * Selection Sort
// * Insertion Sort
// * Merge Sort
// * Quick Sort
// * Heap Sort

// bubble sort
void bubble_sort(vector<int>& arr) {
  int n = arr.size();

  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      if (arr[j] < arr[i]) {
        swap(arr[j], arr[i]);
      }
    }
  }
}

// selection sort

void selection_sort(vector<int>& arr) {
  int n = arr.size();

  for (int i = 0; i < n; ++i) {
    int min_ind = i, j = i;
    while (j < n) {
      if (arr[j] < arr[min_ind]) {
        min_ind = j;
      }
      ++j;
    }
    if (min_ind != i) {
      swap(arr[min_ind], arr[i]);
    }
  }
}

// insertion sort

void insertion_sort(vector<int>& arr) {
  int n = arr.size();
  for (int i = 1; i < n; ++i) {
    int temp = arr[i], j = i - 1;
    while (j >= 0 && arr[j] > temp) {
      arr[j + 1] = arr[j];
      --j;
    }
    arr[j + 1] = temp; // !
  }
}

// merge sort

void _merge(vector<int>& arr, vector<int>& temp, int start, int end) {
  int mid = start + (end - start) / 2;
  int i = start, j = mid + 1;
  int ind = start;

  for (int k = start; k <= end; ++k) {
    temp[k] = arr[k];
  }
  while (i <= mid && j <= end) {
    arr[ind++] = temp[j] < temp[i] ? temp[j++] : temp[i++];
  }
  // only the left part needs to be put in the array, why not the right part?
  for (int k = i; k <= mid; ++k) {
    arr[ind++] = temp[k];
  }
}

void _merge_sort(vector<int>& arr, vector<int>& temp, int start, int end) {
  if (start >= end) {
    return ;
  }

  int mid = start + (end - start) / 2;
  // [start, mid] and [mid + 1, end]
  // NOT [start, mid - 1] and [mid, end]
  // Assuming the case that start = 0 and end = 1
  _merge_sort(arr, temp, start, mid);
  _merge_sort(arr, temp, mid + 1, end);
  _merge(arr, temp, start, end);
}

void merge_sort(vector<int>& arr) {
  vector<int> temp(arr.size());
  _merge_sort(arr, temp, 0, arr.size() - 1);
}

// quick sort

int _partition(vector<int>& arr, int start, int end) {
  int index = start + (end - start) / 2;
  int pivot = arr[index];
  int i = start, j = end;

  swap(arr[index], arr[start]);
  while (i < j) {
    while (i < j && arr[j] > pivot) {
      --j;
    }
    if (i < j) {
      arr[i++] = arr[j];
    }
    while (i < j && arr[i] <= pivot) {
      ++i;
    }
    if (i < j) {
      arr[j--] = arr[i];
    }
  }
  arr[i] = pivot;
  return i;
}

void _quick_sort(vector<int>& arr, int start, int end) {
  if (start >= end) {
    return ;
  }
  int mid = _partition(arr, start, end);
  _quick_sort(arr, start, mid - 1);
  _quick_sort(arr, mid + 1, end);
}

void quick_sort(vector<int>& arr) {
  _quick_sort(arr, 0, arr.size() - 1);
}

// heap sort

// makes it to a heap, that is, arr[cur] > arr[l] && arr[cur] > arr[r]
void _pop_heap(vector<int>& arr, int cur, int n) {
  int l = cur * 2 + 1, r = l + 1, best = cur;

  if (l < n && arr[l] > arr[best]) {
    best = l;
  }
  if (r < n && arr[r] > arr[best]) {
    best = r;
  }
  if (cur != best) {
    swap(arr[cur], arr[best]);
    _pop_heap(arr, best, n);
  }
}

void heap_sort(vector<int>& arr) {
  int n = arr.size();
  for (int i = n / 2; i >= 0; --i) {
    _pop_heap(arr, i, n);
  }

  for (int i = n - 1; i > 0; --i) {
    swap(arr[0], arr[i]);
    _pop_heap(arr, 0, i);
  }
}

void test_bubble_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  bubble_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

void test_insertion_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  insertion_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

void test_selection_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  selection_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

void test_merge_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  merge_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

void test_quick_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  quick_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

void test_heap_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  heap_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

int main() {
  vector<int> v = {4, 5, 1, 9, 10, -1, 2};
  test_bubble_sort(v);
  test_insertion_sort(v);
  test_selection_sort(v);
  test_merge_sort(v);
  test_quick_sort(v);
  test_heap_sort(v);

  return 0;
}
