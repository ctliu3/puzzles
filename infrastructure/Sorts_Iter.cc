#include <iostream>
#include <vector>
#include <stack>
#include <cassert>

using namespace std;

int partition(vector<int>& a, int l, int r) {
  int pivot = a[r], j = l;
  for (int i = l; i < r; ++i) {
    if (a[i] < pivot) {
      swap(a[j++], a[i]);
    }
  }
  swap(a[j], a[r]);
  return j;
}

void quick_sort(vector<int>& a) {
  stack<int> stk;

  stk.push(0);
  stk.push(a.size() - 1);
  while (stk.size()) {
    int r = stk.top();
    stk.pop();
    int l = stk.top();
    stk.pop();
    if (l < r) {
      int idx = partition(a, l, r);
      stk.push(l);
      stk.push(idx - 1);
      stk.push(idx + 1);
      stk.push(r);
    }
  }
}

void test_quick_sort(vector<int> v) {
  vector<int> temp(v.begin(), v.end());
  sort(temp.begin(), temp.end());
  quick_sort(v);
  for (size_t i = 0; i < v.size(); ++i) {
    assert(v[i] == temp[i]);
  }
}

int main() {
  vector<int> v = {4, 5, 1, 9, 10, -1, 2};
  test_quick_sort(v);

  return 0;
}
