#include <iostream>
#include <stack>
#include <string>
#include <cctype>
#include <vector>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <queue>

using namespace std;

// TODO
// Zigzag Traversal

// These tree traversals are implemented in iterative way.
// * Preorder traversal
// * Inorder traversal
// * Preorder traversal
// * Level traversal

struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;

  explicit TreeNode(int val = 0): val(val), left(nullptr), right(nullptr) {
  }
};

TreeNode* build(string s) {
  for (auto& c : s) {
    if (c == ',' || c == '{' || c == '}') {
      c = ' ';
    }
  }

  vector<TreeNode* > tree;
  istringstream sin(s);
  string sub;
  while (sin >> sub) {
    tree.push_back(sub == "#" ? nullptr : new TreeNode(atoi(sub.c_str())));
  }

  if (tree.size() == 0) {
    return nullptr;
  }

  int n = (int)tree.size();
  for (int i = 0; i < n; ++i) {
    int l = (i + 1) * 2 - 1, r = l + 1;
    if (l < n && tree[l]) {
      tree[i]->left = tree[l];
    }
    if (r < n && tree[r]) {
      tree[i]->right = tree[r];
    }
  }

  return tree[0];
}

void level_order(TreeNode* root) {
  if (root == nullptr) {
    return ;
  }
  fprintf(stderr, "Level order: \n");
  queue<TreeNode* > q;
  TreeNode* cur = root;

  q.push(cur);
  while (q.size() && root) {
    cur = q.front();
    q.pop();
    fprintf(stderr, "%d ", cur->val);
    if (cur->left) {
      q.push(cur->left);
    }
    if (cur->right) {
      q.push(cur->right);
    }
  }

  fprintf(stderr, "\n\n");
}

void preorder(TreeNode* root) {
  if (root == nullptr) {
    return ;
  }
  fprintf(stderr, "Preorder: \n");
  stack<TreeNode*> stk;

  stk.push(root);
  while (!stk.empty()) {
    TreeNode* top = stk.top();
    stk.pop();
    fprintf(stderr, "%d ", top->val);
    // Push the right child first so that the left child can be visited before
    // right child
    if (top->right) {
      stk.push(top->right);
    }
    if (top->left) {
      stk.push(top->left);
    }
  }
  fprintf(stderr, "\n\n");
}

void inorder(TreeNode* root) {
  if (root == nullptr) {
    return ;
  }

  fprintf(stderr, "Inorder: \n");
  stack<TreeNode*> stk;
  TreeNode* cur = root;

  while (stk.size() || cur) {
    if (cur) {
      stk.push(cur);
      cur = cur->left;
    } else {
      cur = stk.top();
      stk.pop();
      fprintf(stderr, "%d ", cur->val);
      cur = cur->right;
    }
  }
  fprintf(stderr, "\n\n");
}

// Post order traversal is the most pesky problem...
void postorder(TreeNode* root) {
  if (root == nullptr) {
    return ;
  }

  fprintf(stderr, "Postorder: \n");
  stack<TreeNode*> stk;
  TreeNode* cur = root, *last = nullptr;

  while (cur || stk.size()) {
    if (cur) {
      stk.push(cur);
      cur = cur->left;
    } else {
      TreeNode* subroot = stk.top();
      // To avoid `subroot->right` -> `subroot` -> `subroot->right` -> `subroot`
      // -> ... endless loop.
      if (subroot->right && subroot->right != last) {
        cur = subroot->right;
      } else {
        fprintf(stderr, "%d ", stk.top()->val);
        last = stk.top();
        stk.pop();
      }
    }
  }

  fprintf(stderr, "\n\n");
}

int main() {
  string stree = "{1,2,3,4,5,#,6,7}";
  TreeNode* root = build(stree);

  level_order(root);
  preorder(root);
  inorder(root);
  postorder(root);
  return 0;
}
