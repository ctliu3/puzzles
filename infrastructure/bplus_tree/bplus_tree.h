#ifndef _BPLUS_TREE_H_
#define _BPLUS_TREE_H_

#include <iostream>

const int MAXN = MAX_KEY;

struct Node {
public:
  explicit Node(std::string name) {
  }
};

struct BPNode {
public:
  explicit BPNode (size_t nkey = MAX_KEY);

  void put();

  void del();

  void get();

private:
  size_t nkey_;
  BPNode* parent_;
  BPNode* child_;
  KeyNode* key_node_;
};

#define _BPLUS_TREE_H_
