#include "leetcode.h"

class Solution {
  bool inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

  vector<vector<int>> updateMatrix1(vector<vector<int>>& matrix) {
    int n = matrix.size(), m = matrix[0].size();
    vector<vector<int>> ans(n, vector<int>(m, 0));

    const int dx[] = {-1, 1, 0, 0};
    const int dy[] = {0, 0, -1, 1};

    queue<pair<int, int>> que;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (matrix[i][j] == 1) {
          continue;
        }
        for (int k = 0; k < 4; ++k) {
          int ni = i + dx[k];
          int nj = j + dy[k];
          if (inside(ni, nj, n, m) && matrix[ni][nj] == 1) {
            que.push({i, j});
            break;
          }
        }
      }
    }
    while (!que.empty()) {
      auto cur = que.front();
      que.pop();
      for (int k = 0; k < 4; ++k) {
        int ni = cur.first + dx[k];
        int nj = cur.second + dy[k];
        if (inside(ni, nj, n, m) && matrix[ni][nj] == 1 && ans[ni][nj] == 0) {
          ans[ni][nj] = ans[cur.first][cur.second] + 1;
          que.push({ni, nj});
        }
      }
    }

    return ans;
  }

public:
  vector<vector<int>> updateMatrix(vector<vector<int>>& matrix) {
    return updateMatrix1(matrix);
  }
};

int main() {
  return 0;
}
