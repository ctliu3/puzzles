#include <vector>
#include <iostream>

using namespace std;

struct SegTree {
  #define LL(x) ((x) << 1)
  #define RR(x) (LL(x) | 1)

  struct TreeNode {
    int l, r;
    int min;
    int max;
    int mid() {
      return l + ((r - l) >> 1);
    }
  };
  vector<TreeNode> tree;

  explicit SegTree(vector<int>& nums) {
    tree.resize(nums.size() * 4);
    build(nums, 1, 0, nums.size() - 1);
  }

  void build(const vector<int>& nums, int id, int left, int right) {
    auto& cur = tree[id];
    cur.l = left, cur.r = right;

    if (cur.l == cur.r) {
      cur.min = cur.max = nums[left];
      return ;
    }
    int mid = cur.mid();
    build(nums, LL(id), left, mid);
    build(nums, RR(id), mid + 1, right);
    cur.max = max(tree[LL(id)].max, tree[RR(id)].max);
    cur.min = min(tree[LL(id)].min, tree[RR(id)].min);
  }

  int getMax(int id, int left, int right) {
    auto& cur = tree[id];
    if (cur.l == left and cur.r == right) {
      return cur.max;
    }
    int mid = cur.mid();
    if (right <= mid) {
      return getMax(LL(id), left, right);
    } else if (mid < left) {
      return getMax(RR(id), left, right);
    }
    return max(getMax(LL(id), left, mid), getMax(RR(id), mid + 1, right));
  }
};

class Solution {
  bool find132pattern1(vector<int>& nums) {
    if (nums.size() < 3) {
      return false;
    }
    vector<pair<int, int>> num_pos(nums.size());
    for (int i = 0; i < (int)nums.size(); ++i) {
      num_pos[i] = {nums[i], i};
    }

    SegTree st(nums);
    sort(num_pos.begin(), num_pos.end());
    int i = 0, j = num_pos.size() - 1;
    while (j - i > 1) {
      if (num_pos[j].first - num_pos[i].first <= 1) {
        break;
      }
    }
  }

public:
  bool find132pattern(vector<int>& nums) {
    return find132pattern1(nums);
  }
};

int main() {
  vector<int> nums = {-2,1,2,-2,1,2};
  Solution sol;
  bool ret = sol.find132pattern(nums);
  cout << ret << endl;
  return ret;
}
