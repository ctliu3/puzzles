#include "leetcode.h"

class Solution {
  int minSteps1(int n) {
    if (n <= 1) {
      return 0;
    }
    int ans = 0;
    int m = n, i = 2;
    while (i <= m && n > 1) {
      while (n % i == 0) {
        ans += i;
        n /= i;
      }
      ++i;
    }
    return n > 1 ? n : ans;
  }

public:
  int minSteps(int n) {
    return minSteps1(n);
  }
};

int main() {
  return 0;
}
