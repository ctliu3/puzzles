#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int threeSumSmaller(vector<int>& nums, int target) {
    if (nums.size() < 3) {
      return 0;
    }
    sort(nums.begin(), nums.end());
    int n = (int)nums.size();
    int ret = 0;
    for (int i = 0; i < n - 2; ++i) {
      int st = i + 1, ed = n - 1;
      while (st < ed) {
        int sum = nums[i] + nums[st] + nums[ed];
        if (sum >= target) {
          --ed;
        } else {
          ret += ed - (st + 1) + 1;
          ++st;
        }
      }
    }

    return ret;
  }
};

int main() {
  return 0;
}
