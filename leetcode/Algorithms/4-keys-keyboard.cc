#include "leetcode.h"

class Solution {
public:
  int maxA(int N) {
    if (N == 1) {
      return 1;
    }
    vector<int> dp(N + 1, 0);
    dp[1] = 1;
    for (int i = 2; i <= N; ++i) {
      dp[i] = dp[i - 1] + 1;
      for (int j = 2; j <= i; ++j) {
        dp[i] = max(dp[i], dp[j] * (j - 1));
      }
    }
    return dp[N];
  }
};

int main() {
  return 0;
}
