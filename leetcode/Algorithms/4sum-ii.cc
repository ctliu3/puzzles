#include <vector>
#include <iostream>
#include <unordered_map>

using namespace std;

class Solution {
  void compose(vector<int>& a, vector<int>& b, unordered_map<int, int>& ab) {
    for (int i = 0; i < (int)a.size(); ++i) {
      for (int j = 0; j < (int)b.size(); ++j) {
        ++ab[a[i] + b[j]];
      }
    }
  }

  int fourSumCount1(vector<int>& A, vector<int>& B, vector<int>& C, vector<int>& D) {
    unordered_map<int, int> AB, CD;
    compose(A, B, AB);
    compose(C, D, CD);

    int ret = 0;
    for (auto itr = AB.begin(); itr != AB.end(); ++itr) {
      auto p = CD.find(-(itr->first));
      if (p != CD.end()) {
        ret += itr->second * p->second;
      }
    }
    return ret;
  }

public:
  int fourSumCount(vector<int>& A, vector<int>& B, vector<int>& C, vector<int>& D) {
    return fourSumCount1(A, B, C, D);
  }
};

int main() {
  vector<int> A = {1, 0};
  vector<int> B = {0, 1};
  vector<int> C = {0, -1};
  vector<int> D = {-1, 0};
  Solution sol;
  int ret = sol.fourSumCount(A, B, C, D);
  cout << ret << endl;
}
