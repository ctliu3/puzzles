#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
  int hIndex(vector<int>& citations) {
    int n = (int)citations.size();
    if (n == 0) {
      return 0;
    }

    sort(citations.rbegin(), citations.rend());
    for (int i = 0; i < n - 1; ++i) {
      int h = i + 1;
      if (citations[i] >= h && citations[i + 1] <= h) {
        return h;
      }
    }
    if (citations[n - 1] >= n) {
      return n;
    }

    return 0;
  }
};

int main() {
  return 0;
}
