#include <vector>
#include <iostream>

using namespace std;

class Solution {
  bool canCover(vector<int>& houses, vector<int>& heaters, int radius) {
    int i = 0, j = 0;
    int l = heaters[0] - radius;
    int r = heaters[0] + radius;
    while (i < (int)houses.size()) {
      while (i < (int)houses.size() && houses[i] >= l && houses[i] <= r) {
        ++i;
      }
      if (i == (int)houses.size()) {
        return true;
      }
      if (j == (int)heaters.size() - 1) {
        return false;
      }
      ++j;
      l = heaters[j] - radius;
      r = heaters[j] + radius;
    }

    return true;
  }

  int findRadius1(vector<int>& houses, vector<int>& heaters) {
    sort(houses.begin(), houses.end());
    sort(heaters.begin(), heaters.end());
    int n = (int)houses.size(), m = (int)heaters.size();
    int maxr = houses[n - 1] - houses[0];
    maxr = max(maxr, houses[n - 1] - heaters[0]);
    maxr = max(maxr, heaters[m - 1] - houses[0]);

    cout << maxr << endl;

    int start = 0, end = maxr;
    int ret = INT_MAX;
    while (start <= end) {
      int mid = start + (end - start) / 2;
      if (canCover(houses, heaters, mid)) {
        end = mid - 1;
        ret = min(ret, mid);
      } else {
        start = mid + 1;
      }
    }
    return ret;
  }

public:
  int findRadius(vector<int>& houses, vector<int>& heaters) {
    return findRadius1(houses, heaters);
  }
};

int main() {
  vector<int> houses = {282475249,622650073,984943658,144108930,470211272,101027544,457850878,458777923};
  vector<int> heaters = {823564440,115438165,784484492,74243042,114807987,137522503,441282327,16531729,823378840,143542612};
  Solution sol;
  int ret = sol.findRadius(houses, heaters);
  cout << ret << endl;
}
