#include <iostream>

using namespace std;

class TrieNode {
public:
  // Initialize your data structure here.
  TrieNode* next[26];
  bool end;

  TrieNode() {
    for (int i = 0; i < 26; ++i) {
      next[i] = nullptr;
    }
    end = false;
  }
};

class Trie {
public:
  Trie() {
    root = new TrieNode();
  }

  int getIndx(char& chr) {
    return chr - 'a';
  }

  // Inserts a word into the trie.
  void insert(string s) {
    TrieNode* cur = root;
    for (auto& chr : s) {
      int idx = getIndx(chr);
      if (!cur->next[idx]) {
        cur->next[idx] = new TrieNode();
      }
      cur = cur->next[idx];
    }
    cur->end = true;
  }

  // Returns if the word is in the trie.
  bool search(string key) {
    TrieNode* cur = root;
    for (auto& chr : key) {
      int idx = getIndx(chr);
      if (!cur->next[idx]) {
        return false;
      }
      cur = cur->next[idx];
    }
    return cur->end;
  }

  // Returns if there is any word in the trie
  // that starts with the given prefix.
  bool startsWith(string prefix) {
    TrieNode* cur = root;
    for (auto& chr : prefix) {
      int idx = getIndx(chr);
      if (!cur->next[idx]) {
        return false;
      }
      cur = cur->next[idx];
    }
    return true;
  }

private:
  TrieNode* root;
};

int main() {
  return 0;
}
