import re

class Solution:
  # @param s, a string
  # @return a boolean
  def isNumber(self, s):
    pat = re.compile(r'^[+-]?(\d+\.?|\d*\.?\d+)([Ee][+-]?\d+|\d*)$')
    return True if re.match(pat, s.strip()) else False

