#include <iostream>
#include <string>

using namespace std;

class WordDictionary {
  struct TrieNode {
    TrieNode* child[26];
    bool isWord;

    explicit TrieNode() {
      for (int i = 0; i < 26; ++i) {
        child[i] = nullptr;
      }
      isWord = false;
    }
  };

  TrieNode* root;
public:

  explicit WordDictionary() {
    root = new TrieNode();
  }

  // Adds a word into the data structure.
  void addWord(string word) {
    TrieNode* cur = root;
    for (auto& chr : word) {
      if (!cur->child[chr - 'a']) {
        cur->child[chr - 'a'] = new TrieNode();
      }
      cur = cur->child[chr - 'a'];
    }
    cur->isWord = true;
  }

  // Returns if the word is in the data structure. A word could
  // contain the dot character '.' to represent any one letter.
  bool search(string word) {
    if (word.empty()) {
      return true;
    }

    return _search(root, word, 0);
  }

  bool _search(TrieNode* cur, string& word, int pos) {
    if ((int)word.size() == pos) {
      if (cur) {
        return cur->isWord == true;
      }
      return false;
    }
    if (!cur) {
      return false;
    }

    char chr = word[pos];
    if (chr != '.') {
      if (cur->child[chr - 'a']) {
        if (_search(cur->child[chr - 'a'], word, pos + 1)) {
          return true;
        }
      }
    } else {
      for (int i = 0; i < 26; ++i) {
        if (cur->child[i]) {
          if (_search(cur->child[i], word, pos + 1)) {
            return true;
          }
        }
      }
    }

    return false;
  }
};

// Your WordDictionary object will be instantiated and called as such:
// WordDictionary wordDictionary;
// wordDictionary.addWord("word");
// wordDictionary.search("pattern");
