#include "leetcode.h"

class Solution {
  // DFS

  void dfs1(TreeNode* cur, int v, int d, int dep) {
    if (dep == d - 1) {
      TreeNode* l = new TreeNode(v);
      TreeNode* r = new TreeNode(v);
      TreeNode* tmp = cur->left;
      cur->left = l;
      l->left = tmp;

      tmp = cur->right;
      cur->right = r;
      r->right = tmp;
      return ;
    }
    if (cur->left) {
      dfs1(cur->left, v, d, dep + 1);
    }
    if (cur->right) {
      dfs1(cur->right, v, d, dep + 1);
    }
  }

  TreeNode* addOneRow1(TreeNode* root, int v, int d) {
    if (root == nullptr) {
      return root;
    }
    if (d == 1) {
      TreeNode* cur = new TreeNode(v);
      cur->left = root;
      return cur;
    }
    dfs1(root, v, d, 1);
    return root;
  }

public:
  TreeNode* addOneRow(TreeNode* root, int v, int d) {
    return addOneRow1(root, v, d);
  }
};

int main() {
  return 0;
}
