#include <string>

using namespace std;

class Solution {
  string addStrings1(string num1, string num2) {
    int i = (int)num1.size() - 1;
    int j = (int)num2.size() - 1;

    string ret(max(i, j) + 1, 0);
    int carry = 0, k = ret.size() - 1;
    while (i >= 0 || j >= 0) {
      if (i >= 0) {
        carry += num1[i--] - '0';
      }
      if (j >= 0) {
        carry += num2[j--] - '0';
      }
      ret[k--] = carry % 10 + '0';
      carry /= 10;
    }
    return carry ? "1" + ret : ret;
  }

public:
  string addStrings(string num1, string num2) {
    return addStrings1(num1, num2);
  }
};
