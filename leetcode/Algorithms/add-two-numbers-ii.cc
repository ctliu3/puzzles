#include "leetcode.h"

class Solution {
  ListNode* dfs(ListNode* l1, ListNode* l2, int dep) {
    if (l1->next == nullptr && l2->next == nullptr) {
      return new ListNode(l1->val + l2->val);
    }

    ListNode* node = nullptr;
    ListNode* cur = new ListNode(0);
    if (dep > 0) {
      node = dfs(l1->next, l2, dep - 1);
      cur->val = l1->val;
    } else {
      node = dfs(l1->next, l2->next, dep);
      cur->val = l1->val + l2->val;
    }
    cur->next = node;
    if (node->val >= 10) {
      cur->val += 1;
      node->val % 10;
    }
    return cur;
  }

  int getListLen(ListNode* l) {
    int ret = 0;
    while (l) {
      l = l->next;
      ret += 1;
    }
    return ret;
  }

public:
  ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    if (l1 == nullptr) {
      return l2;
    }
    if (l2 == nullptr) {
      return l1;
    }
    int n1 = getListLen(l1);
    int n2 = getListLen(l2);
    ListNode* node = nullptr;
    if (n1 > n2) {
      node = dfs(l1, l2, n1 - n2);
    } else {
      node = dfs(l2, l1, n2 - n1);
    }

    if (node->val < 10) {
      return node;
    }
    ListNode* new_node = new ListNode(node->val / 10);
    node->val %= 10;
    new_node->next = node;
    return new_node;
  }
};

int main() {
  return 0;
}
