#include <iostream>
#include <string>

using namespace std;

class Solution {
  bool check(const string& num, int st1, int ed1, int st2, int ed2) {
    if (st1 and ed2 == (int)num.size() - 1) {
      return true;
    }
    if (ed1 - st1 > 0 and num[st1] == '0') {
      return false;
    }
    if (ed2 - st2 > 0 and num[st2] == '0') {
      return false;
    }
    string sum = add(num, st1, ed1, st2, ed2);
    int i = 0;
    for (; i < (int)sum.size() && i + ed2 + 1 < (int)num.size(); ++i) {
      if (sum[i] != num[ed2 + 1 + i]) {
        return false;
      }
    }
    if (i != (int)sum.size()) {
      return false;
    }

    return check(num, st2, ed2, ed2 + 1, ed2 + sum.size());
  }

  string add(const string& num, int st1, int ed1, int st2, int ed2) {
    string ret;
    int i = ed1, j = ed2, carry = 0;
    while (i >= st1 || j >= st2) {
      int cur = carry;
      if (i >= st1) {
        cur += num[i--] - '0';
      }
      if (j >= st2) {
        cur += num[j--] - '0';
      }
      ret.push_back(cur % 10 + '0');
      carry = cur / 10;
    }
    if (carry > 0) {
      ret.push_back(carry + '0');
    }
    reverse(ret.begin(), ret.end());
    return ret;
  }

public:
  bool isAdditiveNumber(string num) {
    int n = (int)num.size();
    for (int la = 1; la <= n / 2; ++la) {
      for (int lb = 1; lb <= n / 2; ++lb) {
        if (check(num, 0, la - 1, la, la + lb - 1)) {
          return true;
        }
      }
    }
    return false;
  }
};

int main() {
  string s = "10";
  Solution sol;
  int ret = sol.isAdditiveNumber(s);
  cout << ret << endl;

  return 0;
}
