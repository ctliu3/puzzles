#include <iostream>
#include <string>
#include <vector>
#include <queue>

using namespace std;

class Solution {
  const static int N = 26;
  vector<int> e[N];

public:
  string alienOrder(vector<string>& words) {
    if (words.empty()) {
      return {};
    }

    for (int i = 0; i < N; ++i) {
      e[i].clear();
    }

    vector<int> deg(N, 0);
    int n = (int)words.size();
    for (int k = 0; k < n - 1; ++k) {
      string& sa = words[k], &sb = words[k + 1];
      int i = 0, j = 0;
      while (i < (int)sa.size() && j < (int)sb.size()) {
        if (sa[i] != sb[j]) {
          int u = sa[i] - 'a', v = sb[j] - 'a';
          if (u != v) {
            e[u].push_back(v);
            ++deg[v];
            break;
          }
        } else {
          ++i;
          ++j;
        }
      }
    }

    vector<bool> hash(N, false);
    for (string& word : words) {
      for (char& chr : word) {
        hash[chr - 'a'] = true;
      }
    }

    queue<int> que;
    int count = 0;
    for (int i = 0; i < N; ++i) {
      if (!deg[i] && hash[i]) {
        que.push(i);
      }
      count += hash[i];
    }
    
    string ret;
    while (!que.empty()) {
      int u = que.front();
      que.pop();

      ret.push_back(u + 'a');
      for (auto& v : e[u]) {
        --deg[v];
        if (!deg[v]) {
          que.push(v);
        }
      }
    }

    if ((int)ret.size() != count) {
      return {};
    }
    return ret;
  }
};

int main() {
  vector<string> words = {"wrt", "wrf", "er", "ett", "rftt"};
  //vector<string> words = {"z", "z"};
  Solution sol;
  string ret = sol.alienOrder(words);
  cout << ret << endl;

  return 0;
}
