#include <iostream>
#include <vector>

using namespace std;

const int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1, -2, -2, -1, -1, 1, 1, 2, 2};
const int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1, -1, 1, -2, 2, -2, 2, -1, 1};

class Solution {
  bool inside(int x, int y) {
    return x >= 0 && x < 3 && y >= 0 && y < 3;
  }

  void dfs1(int cur, int dep, vector<bool>& used, int n, int m, int& ans) {
    if (dep >= n && dep <= m) {
      ++ans;
      if (dep == m) {
        return ;
      }
    }
    for (int k = 0; k < 16; ++k) {
      int nx = cur / 3 + dx[k];
      int ny = cur % 3 + dy[k];
      if (!inside(nx, ny)) {
        continue;
      }
      int idx = ny + nx * 3;
      if (!used[idx]) {
        used[idx] = true;
        dfs1(idx, dep + 1, used, n, m, ans);
        used[idx] = false;
      } else {
        nx = cur / 3 + dx[k] * 2;
        ny = cur % 3 + dy[k] * 2;
        if (inside(nx, ny)) {
          idx = ny + nx * 3;
          if (!used[idx]) {
            used[idx] = true;
            dfs1(idx, dep + 1, used, n, m, ans);
            used[idx] = false;
          }
        }
      }
    }
  }

  int numberOfPatterns1(int m, int n) {
    vector<bool> used(9, false);
    int ans = 0;
    for (int i = 0; i < 9; ++i) {
      used[i] = true;
      dfs1(i, 1, used, m, n, ans);
      used[i] = false;
    }
    return ans;
  }

public:
  int numberOfPatterns(int m, int n) {
    return numberOfPatterns1(m, n);
  }
};

int main() {
  return 0;
}
