#include "leetcode.h"

class Solution {
public:
  int numberOfArithmeticSlices(vector<int>& A) {
    int n = A.size();
    if (n <= 2) {
      return 0;
    }

    vector<unordered_map<long long, int>> dp(n);
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < i; ++j) {
        long long diff = (long long)A[i] - A[j];
        int count = dp[j].count(diff) ? dp[j][diff] : 0;
        dp[i][diff] += count + 1;
        ans += count;
      }
    }

    return ans;
  }
};

int main() {
  return 0;
}
