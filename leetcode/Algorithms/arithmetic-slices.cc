#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int numberOfArithmeticSlices(vector<int>& A) {
    if (A.size() < 3) {
      return 0;
    }

    int ret = 0;
    int cnt = 1;
    int prev = A[1] - A[0];
    for (int i = 2; i < A.size(); ++i) {
      int cur = A[i] - A[i - 1];
      if (prev == cur) {
        ++cnt;
      } else {
        if (cnt >= 2) {
          ret += (long long)((cnt - 1) + 1) * (cnt - 1) / 2;
        }
        prev = cur;
        cnt = 1;
      }
    }
    if (cnt >= 2) {
      ret += (long long)((cnt - 1) + 1) * (cnt - 1) / 2;
    }
    return ret;
  }
};

int main() {
  return 0;
}
