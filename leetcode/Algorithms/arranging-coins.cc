class Solution {
  int arrangeCoins1(int n) {
    int k = 0;
    while (n >= (k + 1)) {
      ++k;
      n -= k;
    }
    return k;
  }

public:
  int arrangeCoins(int n) {
    return arrangeCoins1(n);
  }
};

