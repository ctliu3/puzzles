#include "leetcode.h"

class Solution {
  void dfs1(int u, vector<vector<int>>& edges, vector<bool>& visited, int& cur) {
    if (edges[u].size() > 1) {
      ++cur;
    }
    visited[u] = true;
    for (int v : edges[u]) {
      if (!visited[v]) {
        dfs1(v, edges, visited, cur);
      }
    }
  }

  int arrayNesting1(vector<int>& nums) {
    int n = nums.size();
    vector<vector<int>> edges(n);
    for (int i = 0; i < (int)nums.size(); ++i) {
      edges[i].push_back(nums[i]);
      edges[nums[i]].push_back(i);
    }
    vector<bool> visited(n, false);
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      if (!visited[i]) {
        int cur = 0;
        dfs1(i, edges, visited, cur);
        ans = max(ans, cur);
      }
    }
    return ans;
  }

public:
  int arrayNesting(vector<int>& nums) {
    return arrayNesting1(nums);
  }
};

int main() {
  return 0;
}
