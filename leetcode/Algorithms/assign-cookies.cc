class Solution {
  int findContentChildren1(vector<int>& g, vector<int>& s) {
    sort(g.begin(), g.end());
    sort(s.begin(), s.end());

    int i = 0, j = 0, ret = 0;
    while (i < (int)g.size() && j < (int)s.size()) {
      int cur = g[i];
      while (j < (int)s.size() && cur > s[j]) {
        ++j;
      }
      if (j == (int)s.size()) {
        break;
      }
      ++ret;
      ++i;
      ++j;
    }
    return ret;
  }

public:
  int findContentChildren(vector<int>& g, vector<int>& s) {
    return findContentChildren1(g, s);
  }
};
