#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, vector<double>& ave, vector<int>& count, int dep) {
    if (!cur) {
      return ;
    }
    if (dep >= count.size()) {
      ave.resize(dep + 1);
      count.resize(dep + 1);
    }
    ave[dep] += cur->val;
    count[dep] += 1;
    dfs1(cur->left, ave, count, dep + 1);
    dfs1(cur->right, ave, count, dep + 1);
  }

  vector<double> averageOfLevels1(TreeNode* root) {
    vector<double> ave;
    vector<int> count;
    dfs1(root, ave, count, 0);
    for (int i = 0; i < (int)ave.size(); ++i) {
      ave[i] /= count[i];
    }
    return ave;
  }

public:
  vector<double> averageOfLevels(TreeNode* root) {
    return averageOfLevels1(root);
  }
};

int main() {
  return 0;
}
