#include "leetcode.h"

class Solution {
public:
  string convertToBase7(int num) {
    if (num == 0) {
      return "0";
    }
    bool is_neg = false;
    if (num < 0) {
      is_neg = true;
      num = -num;
    }
    string ans;
    while (num) {
      ans.push_back('0' + num % 7);
      num /= 7;
    }
    reverse(ans.begin(), ans.end());
    return is_neg ? "-" + ans : ans;
  }
};
