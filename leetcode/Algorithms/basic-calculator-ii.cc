#include <iostream>
#include <string>
#include <stack>
#include <cstdio>

using namespace std;

class Solution {
  long long oper(long long a, string& op, long long b) {
    int ret = 0;
    switch (op[0]) {
    case '-':
      ret = a - b;
      break;
    case '+':
      ret = a + b;
      break;
    case '*':
      ret = a * b;
      break;
    case '/':
      ret = a / b;
      break;
    }
    return ret;
  }

  void calc(stack<string>& stk) {
    long long num2 = stoll(stk.top());
    stk.pop();
    string op = stk.top();
    stk.pop();
    long long num1 = stoll(stk.top());
    stk.pop();
    long long ret = oper(num1, op, num2);
    stk.push(to_string(ret));
  }

public:
  int calculate(string s) {
    stack<string> stk;

    int n = (int)s.size();
    for (int i = 0; i < n; ) {
      while (i < n && s[i] == ' ') {
        ++i;
      }
      if (i >= n) {
        break;
      }

      if (isdigit(s[i])) {
        int j = i;
        while (j < n && isdigit(s[j])) {
          ++j;
        }
        bool flag = false;
        if (!stk.empty() && (stk.top() == "*" || stk.top() == "/")) {
          flag = true;
        }
        stk.push(s.substr(i, j - i));
        if (flag) {
          calc(stk);
        }
        i = j;
      } else {
        if (s[i] == '+' || s[i] == '-') {
          if (stk.size() >= 3) {
            calc(stk);
          }
          stk.push(string(1, s[i]));
        } else {
          if (stk.size() > 3) {
            calc(stk);
          }
          stk.push(string(1, s[i]));
        }
        ++i;
      }
    }

    if (stk.empty()) {
      return 0;
    } else {
      if (stk.size() == 1) {
        return stoll(stk.top());
      }
      calc(stk);
      if (stk.size() > 1) {
        calc(stk);
      }
      return stoll(stk.top());
    }
  }
};

int main() {
  Solution sol;
  //string s = "3+2*2";
  //string s = " 3/2 ";
  //string s = " 3+5 / 2 ";
  string s = "100000000/1/2/3/4/5/6/7/8/9/10";

  int res = sol.calculate(s);
  cout << res << endl;
  return 0;
}
