#include <iostream>
#include <string>
#include <cassert>
#include <stack>

using namespace std;

class Solution {

public:
  int calculate(string s) {
    assert(!s.empty());
    int res = 0;
    stack<char> stk; // {1, -1}

    int n = 0;
    for (auto& c : s) {
      if (c != ' ') {
        s[n++] = c;
      }
    }

    int i = 0;
    while (i < n) {
      if (s[i] == '(') {
        if (i == 0 || s[i - 1] == '+') {
          stk.push(stk.empty() ? 1 : stk.top());
        } else {
          stk.push(stk.empty() ? -1 : stk.top() * -1);
        }
        ++i;
      } else if (s[i] == ')') {
        stk.pop();
        ++i;
      } else if (isdigit(s[i])) {
        int cur = 1;
        if (i > 0 && s[i - 1] == '-') {
          cur = stk.empty() ? -1 : -1 * stk.top();
        } else {
          cur = stk.empty() ? 1 : stk.top();
        }
        int num = 0;
        while (i < n && isdigit(s[i])) {
          num = num * 10 + (s[i] - '0');
          ++i;
        }
        res += cur * num;
      } else {
        ++i;
      }
    }

    return res;
  }
};

int main() {
  //string s = "(1+(4+5+2)-3)+(6+8)";
  string s = " 2-1 + 2 ";
  //string s = "1+(4+5+2)-3";
  Solution sol;

  int res = sol.calculate(s);
  cout << res << endl;

  return 0;
}
