#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int getBoard(vector<vector<char>>& board, vector<vector<bool>>& visit, int i, int j) {
    int n = (int)board.size(), m = (int)board[0].size();

    visit[i][j] = true;
    if (j + 1 < m && board[i][j + 1] == 'X') {
      int c = 1;
      while (j + 1 < m && board[i][j + 1] == 'X') {
        ++j;
        ++c;
        visit[i][j] = true;
      }
      return c;
    }

    if (i + 1 < n && board[i + 1][j] == 'X') {
      int c = 1;
      while (i + 1 < n && board[i + 1][j] == 'X') {
        ++i;
        ++c;
        visit[i][j] = true;
      }
      return c;
    }

    return 1;
  }

  int countBattleships1(vector<vector<char>>& board) {
    if (board.empty()) {
      return 0;
    }
    int n = (int)board.size(), m = (int)board[0].size();
    vector<vector<bool>> visit(n, vector<bool>(m, false));
    int ret = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (board[i][j] == 'X' and !visit[i][j]) {
          getBoard(board, visit, i, j);
          ++ret;
        }
      }
    }

    return ret;
  }

public:
  int countBattleships(vector<vector<char>>& board) {
    return countBattleships1(board);
  }
};

int main() {
}
