#include "leetcode.h"

class Solution {
  vector<int> constructArray1(int n, int k) {
    vector<bool> used(n, false);
    vector<int> ans = {1};
    int gap = k;
    used[ans[0] - 1] = true;
    while (k--) {
      ans.push_back(ans.back() + gap);
      used[ans.back() - 1] = true;
      if (gap > 0) {
        gap = -(gap - 1);
      } else {
        gap = -gap - 1;
      }
    }
    for (int i = 0; i < n; ++i) {
      if (!used[i]) {
        ans.push_back(i + 1);
      }
    }
    return ans;
  }

public:
  vector<int> constructArray(int n, int k) {
    return constructArray1(n, k);
  }
};

int main() {
  return 0;
}
