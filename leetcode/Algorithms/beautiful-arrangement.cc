#include "leetcode.h"

class Solution {
  void dfs(vector<bool>& used, int& ans, int dep) {
    int n = (int)used.size();
    if (dep == n) {
      ++ans;
      return ;
    }

    for (int i = 0; i < n; ++i) {
      if (used[i]) {
        continue;
      }
      if ((i + 1) % (dep + 1) == 0 || (dep + 1) % (i + 1) == 0) {
        used[i] = true;
        dfs(used, ans, dep + 1);
        used[i] = false;
      }
    }
  }

public:
  int countArrangement(int N) {
    int ans = 0;
    vector<bool> used(N, false);
    dfs(used, ans, 0);
    return ans;
  }
};

int main() {
  return 0;
}
