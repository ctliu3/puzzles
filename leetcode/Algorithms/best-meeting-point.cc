#include <vector>

using namespace std;

class Solution {
  int minTotalDistance1(vector<vector<int>>& grid) {
    if (grid.empty()) {
      return 0;
    }
    int n = grid.size(), m = grid[0].size();
    vector<int> row, col;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (grid[i][j]) {
          row.push_back(i);
          col.push_back(j);
        }
      }
    }
    return minsum(row) + minsum(col);
  }

  int minsum(vector<int>& nums) {
    sort(nums.begin(), nums.end());

    int ret = 0;
    int i = 0, j = nums.size() - 1;
    while (i < j) {
      ret += nums[j--] - nums[i++];
    }
    return ret;
  }

public:
  int minTotalDistance(vector<vector<int>>& grid) {
    return minTotalDistance1(grid);
  }
};
