#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int _profit(vector<int>& prices) {
    int res = 0;
    for (int i = 1; i < (int)prices.size(); ++i) {
      res += max(prices[i] - prices[i - 1], 0);
    }
    return res;
  }

  int maxProfit1(int k, vector<int>& prices) {
    if (prices.size() < 2) {
      return 0;
    }
    int n = (int)prices.size();
    if (k * 2 >= n) {
      return _profit(prices);
    }

    vector<int> dp(2 * k + 1, INT_MIN);
    dp[0] = 0;
    for (int i = 0; i < (int)prices.size(); ++i) {
      for (int j = min(2 * k, i + 1); j >= 1; --j) {
        int stock = j & 1 ? -prices[i] : prices[i];
        dp[j] = max(dp[j], dp[j - 1] + stock);
      }
    }

    int res = 0;
    for (int i = 2; i < (int)dp.size(); i += 2) {
      res = max(res, dp[i]);
    }

    return res;
  }

public:
  int maxProfit(int k, vector<int>& prices) {
    return maxProfit1(k, prices);
  }
};

int main() {
  Solution sol;
  vector<int> prices = {3, 8, 2, 6, 7, 8, 2};
  int res = sol.maxProfit(1, prices);
  cout << res << endl;
  return 0;
}
