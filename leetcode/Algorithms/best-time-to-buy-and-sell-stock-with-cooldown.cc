#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int maxProfit1(vector<int>& prices) {
    if (prices.empty()) {
      return 0;
    }

    int n = (int)prices.size();
    vector<int> sell(n), buy(n), cool(n), noop(n);
    sell[0] = cool[0] = 0;
    noop[0] = INT_MIN;
    buy[0] = -prices[0];
    for (int i = 1; i < n; ++i) {
      sell[i] = max(buy[i - 1], noop[i - 1]) + prices[i];
      buy[i] = cool[i - 1] - prices[i];
      cool[i] = max(sell[i - 1], cool[i - 1]);
      noop[i] = max(buy[i - 1], noop[i - 1]);
    }

    return max(sell[n - 1], cool[n - 1]);
  }

public:
  int maxProfit(vector<int>& prices) {
    return maxProfit1(prices);
  }
};

int main() {
  vector<int> nums = {1, 2, 3, 0, 2};
  Solution sol;
  int ret = sol.maxProfit(nums);
  cout << ret << endl;
  return 0;
}
