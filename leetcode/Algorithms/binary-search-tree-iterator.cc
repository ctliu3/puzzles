#include "leetcode.h"

class BSTIterator {
  struct Frame {
    TreeNode* node;
    int state;

    explicit Frame(TreeNode* node = nullptr)
      : node(node), state(0) {
    }
  };

  stack<Frame> stk;
  TreeNode* next_node;

public:
  BSTIterator(TreeNode *root): next_node(nullptr) {
    stk.push(Frame(root));
  }

  /** @return whether we have a next smallest number */
  bool hasNext() {
    next_node = nullptr;

    while (!stk.empty()) {
      Frame& cur = stk.top();
      if (!cur.node) {
        stk.pop();
        continue;
      }

      if (cur.state == 0) {
        cur.state = 1;
        stk.push(Frame(cur.node->left));
      } else if (cur.state == 1) {
        cur.state = 2;
        stk.push(Frame(cur.node->right));
        next_node = cur.node;
        return true;
      } else if (cur.state == 2) {
        stk.pop();
      }
    }

    return false;
  }

  /** @return the next smallest number */
  int next() {
    assert(next_node);
    return next_node->val;
  }
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */

int main() {
  string s = "{7,3,9,2,5,8,15,#,#,#,#,#,#,#,#}";
  TreeNode* root = getTree(s);

  BSTIterator i = BSTIterator(root);
  while (i.hasNext()) {
    printf("%d\n", i.next());
  }

  return 0;
}
