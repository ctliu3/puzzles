#include "leetcode.h"

class Solution {
  int longestConsecutive1(TreeNode* root) {
    if (!root) {
      return 0;
    }

    queue<pair<TreeNode*, int>> que;
    que.push({root, 1});
    int ret = 0;
    while (!que.empty()) {
      auto cur = que.front();
      que.pop();
      TreeNode* node = cur.first;
      int len = cur.second;

      ret = max(ret, len);

      TreeNode* l = node->left, *r = node->right;
      if (l) {
        que.push({l, node->val + 1 == l->val ? len + 1 : 1});
      }
      if (r) {
        que.push({r, node->val + 1 == r->val ? len + 1 : 1});
      }
    }

    return ret;
  }

public:
  int longestConsecutive(TreeNode* root) {
    return longestConsecutive1(root);
  }
};

int main() {
  return 0;
}
