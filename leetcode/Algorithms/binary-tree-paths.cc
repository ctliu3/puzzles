#include "leetcode.h"

class Solution {
  string to_s(long long num) {
    string s;

    bool flag = false;
    if (num < 0) {
      num = -num;
      flag = true;
    }
    while (num) {
      s.push_back(num % 10 + '0');
      num /= 10;
    }
    reverse(s.begin(), s.end());
    if (flag) {
      s.insert(s.begin(), '-');
    }
    return s;
  }

  void _binaryTreePaths(TreeNode* cur, string& path, vector<string>& ret) {
    if (!cur) {
      return ;
    }

    if (!path.empty()) {
      path.append("->");
    }
    path.append(to_s((long long)cur->val));

    if (cur && !cur->left && !cur->right) {
      ret.push_back(path);
      return ;
    }

    int m = (int)path.size();
    _binaryTreePaths(cur->left, path, ret);
    path.resize(m);
    _binaryTreePaths(cur->right, path, ret);
    path.resize(m);
  }

public:
  vector<string> binaryTreePaths(TreeNode* root) {
    if (!root) {
      return {};
    }
    vector<string> ret;
    string path;
    _binaryTreePaths(root, path, ret);
    return ret;
  }
};

int main() {
  string tree = {"1,2,-30,#,5"};
  TreeNode* root = getTree(tree);
  Solution sol;
  auto paths = sol.binaryTreePaths(root);
  for (string& path : paths) {
    cout << path << endl;
  }
  return 0;
}
