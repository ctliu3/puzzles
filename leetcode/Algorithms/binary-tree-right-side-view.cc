#include "leetcode.h"

class Solution {

  vector<int> rightSideView1(TreeNode* root) {
    if (!root) {
      return {};
    }
    vector<int> res;
    queue<pair<TreeNode*, int>> que;

    que.push({root, 0});
    while (!que.empty()) {
      TreeNode* cur = que.front().first;
      int dep = que.front().second;
      que.pop();

      if ((int)res.size() < dep + 1) {
        res.push_back(cur->val);
      } else {
        res[dep] = cur->val;
      }

      if (cur->left) {
        que.push({cur->left, dep + 1});
      }
      if (cur->right) {
        que.push({cur->right, dep + 1});
      }
    }

    return res;
  }

  void _dfs2(TreeNode* cur, int dep, vector<int>& res) {
    if (!cur) {
      return ;
    }

    if (dep + 1 > (int)res.size()) {
      res.push_back(cur->val);
    } else {
      res[dep] = cur->val;
    }

    if (cur->left) {
      _dfs2(cur->left, dep + 1, res);
    }
    if (cur->right) {
      _dfs2(cur->right, dep + 1, res);
    }
  }

  vector<int> rightSideView2(TreeNode* root) {
    if (!root) {
      return {};
    }
    vector<int> res;
    _dfs2(root, 0, res);
    return res;
  }

public:
  vector<int> rightSideView(TreeNode* root) {
    //return rightSideView1(root);
    return rightSideView2(root);
  }
};

int main() {
  return 0;
}
