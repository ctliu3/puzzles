#include "leetcode.h"

class Solution {
  int dfs1(TreeNode* cur, int& sum) {
    if (!cur) {
      return 0;
    }
    int lsum = dfs1(cur->left, sum);
    int rsum = dfs1(cur->right, sum);
    sum += abs(lsum - rsum);
    return lsum + rsum + cur->val;
  }

  int findTilt1(TreeNode* root) {
    int ret = 0;
    dfs1(root, ret);
    return ret;
  }

public:
  int findTilt(TreeNode* root) {
    return findTilt1(root);
  }
}; 

int main() {
  return 0;
}
