#include "leetcode.h"

class Solution {

  // Recursive solution
  TreeNode* upsideDownBinaryTree1(TreeNode *root) {
    if (!root) {
      return nullptr;
    }
    TreeNode* newroot = nullptr;
    upsidedown1(root, newroot);
    return newroot;
  }

  void upsidedown1(TreeNode* cur, TreeNode*& newroot) {
    if (cur && !cur->left && !cur->right) {
      newroot = cur;
      return ;
    }
    upsidedown1(cur->left, newroot);
    TreeNode* left = cur->left;
    left->right = cur;
    left->left = cur->right;
    cur->left = cur->right = nullptr;
  }

  // Non-recursive solutioin
  TreeNode* upsideDownBinaryTree2(TreeNode *root) {
    if (!root) {
      return nullptr;
    }

    stack<TreeNode*> stk;
    TreeNode* newroot = nullptr, *cur = root;
    while (cur->left) {
      stk.push(cur);
      cur = cur->left;
    }
    if (stk.empty()) {
      return root;
    }
    newroot = stk.top()->left;

    while (!stk.empty()) {
      TreeNode* cur = stk.top();
      stk.pop();

      TreeNode* left = cur->left;
      left->left = cur->right;
      left->right = cur;
      cur->left = cur->right = nullptr;
    }

    return newroot;
  }

public:
  TreeNode* upsideDownBinaryTree(TreeNode *root) {
    //return upsideDownBinaryTree1(root);
    return upsideDownBinaryTree2(root);
  }
};

int main() {
  string s_tree = "{1,2,3,4,5}";
  TreeNode* root = getTree(s_tree);
  Solution sol;

  root = sol.upsideDownBinaryTree(root);
  auto res = level_order(root);
  print_vector2(res);

  return 0;
}
