#include "leetcode.h"

class Solution {
  vector<vector<int>> verticalOrder1(TreeNode* root) {
    if (!root) {
      return {};
    }

    vector<pair<int, int>> store;

    queue<pair<TreeNode*, int>> que;
    que.push({root, 0});
    int minw = INT_MAX, maxw = INT_MIN;
    while (!que.empty()) {
      auto cur = que.front();
      que.pop();

      TreeNode* node = cur.first;
      int column = cur.second;
      store.push_back({column, node->val});
      minw = min(minw, column);
      maxw = max(maxw, column);

      if (node->left) {
        que.push({node->left, column - 1});
      }
      if (node->right) {
        que.push({node->right, column + 1});
      }
    }

    int width = maxw - minw + 1;
    vector<vector<int>> ret(width, vector<int>());
    for (auto& p : store) {
      ret[p.first - minw].push_back(p.second);
    }

    return ret;
  }

public:
  vector<vector<int>> verticalOrder(TreeNode* root) {
    return verticalOrder1(root);
  }
};

int main() {
  string tree = "3,9,20,4,5,2,7";
  TreeNode* root = getTree(tree);
  Solution sol;
  auto ret = sol.verticalOrder(root);
  print_vector2(ret);

  return 0;
}
