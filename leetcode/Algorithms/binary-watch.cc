#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {

  string timeStr(int minute, int second) {
    string ret;
    ret.append(to_string(minute));
    ret.push_back(':');
    if (second < 10) {
      ret.push_back('0');
    }
    ret.append(to_string(second));
    return ret;
  }

public:
  vector<string> readBinaryWatch(int num) {
    vector<string> ret;
    for (int mask = 0; mask < (1 << 10); ++mask) {
      int count = 0;
      for (int j = 0; j < 10; ++j) {
        if (mask & (1 << j)) {
          ++count;
        }
      }
      if (count == num) {
        int minute = mask >> 6, second = mask & ((1 << 6) - 1);
        if (minute < 12 and second < 60) {
          ret.push_back(timeStr(minute, second));
        }
      }
    }
    return ret;
  }
};
