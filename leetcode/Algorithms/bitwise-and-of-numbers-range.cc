#include <iostream>

using namespace std;

class Solution {
public:
  int rangeBitwiseAnd(int m, int n) {
    int res = 0;
    bool flag = false;
    for (int i = 31; i >= 0; --i) {
      int a = (1 << i) & m;
      int b = (1 << i) & n;
      if ((a && !b) || (!a && b)) {
        break;
      }
      if (a && b) {
        flag = true;
        res |= (1 << i);
      }
    }
    return res;
  }
};

int main() {
  return 0;
}
