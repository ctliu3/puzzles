#include "leetcode.h"

class Solution {
  void dfs1_left(TreeNode* cur, vector<TreeNode*>& left_nodes, int dep) {
    if (!cur) {
      return ;
    }
    if (dep >= left_nodes.size()) {
      left_nodes.push_back(cur);
    }
    
    if (cur->left) {
      dfs1_left(cur->left, left_nodes, dep + 1);
    } else {
      dfs1_left(cur->right, left_nodes, dep + 1);
    }
  }

  void dfs1_right(TreeNode* cur, vector<TreeNode*>& right_nodes, int dep) {
    if (!cur) {
      return ;
    }

    if (dep >= right_nodes.size()) {
      right_nodes.push_back(cur);
    }

    if (cur->right) {
      dfs1_right(cur->right, right_nodes, dep + 1);
    } else {
      dfs1_right(cur->left, right_nodes, dep + 1);
    }
  }

  void dfs1_leaves(TreeNode* cur, vector<TreeNode*>& leaves, int dep) {
    if (!cur) {
      return ;
    }
    if (!cur->left && !cur->right) {
      leaves.push_back(cur);
    }

    dfs1_leaves(cur->left, leaves, dep + 1);
    dfs1_leaves(cur->right, leaves, dep + 1);
  }

  vector<int> boundaryOfBinaryTree1(TreeNode* root) {
    if (!root) {
      return vector<int>();
    }
    vector<TreeNode*> nodes[3];
    if (root->left) {
      dfs1_left(root->left, nodes[0], 0);
    }
    if (root->right) {
      dfs1_right(root->right, nodes[2], 0);
    }
    dfs1_leaves(root, nodes[1], 0);

    vector<int> ans;
    unordered_set<TreeNode*> hash;
    ans.push_back(root->val);
    hash.insert(root);
    if (root->left) {
      for (int i = 0; i < (int)nodes[0].size(); ++i) {
        if (hash.count(nodes[0][i])) {
          continue;
        }
        ans.push_back(nodes[0][i]->val);
        hash.insert(nodes[0][i]);
      }
    }

    for (int i = 0; i < nodes[1].size(); ++i) {
      if (hash.count(nodes[1][i])) {
        continue;
      }
      ans.push_back(nodes[1][i]->val);
      hash.insert(nodes[1][i]);
    }

    if (root->right) {
      for (int i = (int)nodes[2].size() - 1; i >= 0; --i) {
        if (hash.count(nodes[2][i])) {
          continue;
        }
        ans.push_back(nodes[2][i]->val);
        hash.insert(nodes[2][i]);
      }
    }

    return ans;
  }

public:
  vector<int> boundaryOfBinaryTree(TreeNode* root) {
    return boundaryOfBinaryTree1(root);
  }
};

int main() {
  return 0;
}
