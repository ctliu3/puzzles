#include "leetcode.h"

class Solution {
public:
  int leastBricks(vector<vector<int>>& wall) {
    unordered_map<int, int> counter;
    for (auto w : wall) {
      int point = 0;
      for (int i = 0; i < w.size() - 1; ++i) {
        point += w[i];
        if (!counter.count(point)) {
          counter[point] = 0;
        }
        counter[point] += 1;
      }
    }

    int ans = INT_MAX;
    for (auto& p : counter) {
      ans = min(ans, (int)wall.size() - p.second);
    }
    return ans == INT_MAX ? wall.size() : ans;
  }
};

int main() {
  return 0;
}
