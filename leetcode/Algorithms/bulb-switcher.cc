#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Solution {

  // MLE, TLE
  int bulbSwitch1(int n) {
    if (n == 0) {
      return 0;
    }
    vector<int> fact(n + 1, 1);
    for (int i = 2; i <= n; ++i) {
      int j = i;
      while (j <= n) {
        fact[j] += 1;
        j += i;
      }
    }

    int ret = 0;
    for (int i = 1; i <= n; ++i) {
      ret += fact[i] & 1;
    }
    return ret;
  }

  int bulbSwitch2(int n) {
    return sqrt(n);
  }

  int bulbSwitch3(int n) {
    return _sqrt(n);
  }

  int _sqrt(int n) {
    long long start = -1, end = (long long)n + 1;
    while (end - start > 1) {
      long long mid = start + (end - start) / 2;
      if (mid * mid <= (long long)n) {
        start = mid;
      } else {
        end = mid;
      }
    }
    return start;
  }

public:
  int bulbSwitch(int n) {
    // return bulbSwitch1(n);
    // return bulbSwitch2(n);
    return bulbSwitch3(n);
  }
};

int main() {
  Solution sol;
  for (int i = 1; i <= 1000; ++i) {
    int ret = sol.bulbSwitch(i);
    printf("i = %d, ret = %d\n", i, ret);
  }
  return 0;
}
