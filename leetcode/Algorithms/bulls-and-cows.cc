#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
  string getHint(string secret, string guess) {
    if (secret.empty()) {
      return "";
    }
    vector<int> hash(10, 0);
    int bulls = 0, cows = 0;
    for (int i = 0; i < (int)secret.size(); ++i) {
      if (secret[i] == guess[i]) {
        ++bulls;
      } else {
        ++hash[secret[i] - '0'];
      }
    }
    for (int i = 0; i < (int)guess.size(); ++i) {
      if (secret[i] != guess[i] && hash[guess[i] - '0'] > 0) {
        ++cows;
        --hash[guess[i] - '0'];
      }
    }

    string ret;
    ret.append(to_string(bulls));
    ret.append("A");
    ret.append(to_string(cows));
    ret.append("B");
    return ret;
  }
};

int main() {
}
