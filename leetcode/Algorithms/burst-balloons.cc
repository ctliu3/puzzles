#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int dfs(const vector<int>& nums, vector<vector<int>>& dp, int lt, int rt) {
    if (lt > rt) {
      return 0;
    }
    int& cur = dp[lt][rt];
    if (cur != -1) {
      return cur;
    }

    cur = 0;
    for (int i = lt; i <= rt; ++i) {
      int val = nums[i];
      val *= lt - 1 == -1 ? 1 : nums[lt - 1];
      val *= rt + 1 == (int)nums.size() ? 1 : nums[rt + 1];
      cur = max(cur, val + dfs(nums, dp, lt, i - 1) + dfs(nums, dp, i + 1, rt));
    }

    return cur;
  }

  int maxCoins1(const vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    int n = (int)nums.size();
    vector<vector<int>> dp(n, vector<int>(n, -1));
    return dfs(nums, dp, 0, n - 1);
  }

public:
  int maxCoins(vector<int>& nums) {
    return maxCoins1(nums);
  }
};

int main() {
  return 0;
}
