#include "leetcode.h"

class Solution {
  bool canPlaceFlowers1(vector<int>& flowerbed, int n) {
    for (int i = 0; i < flowerbed.size() && n > 0; ++i) {
      if (flowerbed[i] == 1) {
        continue;
      }
      if ((i - 1 < 0 || flowerbed[i - 1] == 0) && (i + 1 >= flowerbed.size() || flowerbed[i + 1] != 1)) {
        --n;
        flowerbed[i] = 1;
      }
    }
    return n == 0;
  }

public:
  bool canPlaceFlowers(vector<int>& flowerbed, int n) {
    return canPlaceFlowers1(flowerbed, n);
  }
};

int main() {
  return 0;
}
