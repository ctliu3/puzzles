#include "leetcode.h"

class Solution {

  vector<int> closestKValues1(TreeNode* root, double target, int k) {
    if (k == 0) {
      return {};
    }

    vector<int> inorder, ret(k);
    dfs(root, inorder);

    double mindist = 1e12;
    ret[0] = 0;
    for (int i = 0; i < (int)inorder.size(); ++i) {
      if (fabs(inorder[i] - target) < mindist) {
        mindist = fabs(inorder[i] - target);
        ret[0] = i;
      }
    }

    int l = ret[0] - 1, r = ret[0] + 1;
    int idx = 1;
    ret[0] = inorder[ret[0]];
    while (idx < k && (l >= 0 || r < (int)inorder.size())) {
      if (l < 0) {
        ret[idx++] = inorder[r++];
        continue;
      }
      if (r >= (int)inorder.size()) {
        ret[idx++] = inorder[l--];
        continue;
      }
      if (fabs(inorder[l] - target) < fabs(inorder[r] - target)) {
        ret[idx++] = inorder[l--];
      } else {
        ret[idx++] = inorder[r++];
      }
    }

    return ret;
  }

  void dfs(TreeNode* cur, vector<int>& inorder) {
    if (!cur) {
      return ;
    }
    dfs(cur->left, inorder);
    inorder.push_back(cur->val);
    dfs(cur->right, inorder);
  }

public:
  vector<int> closestKValues(TreeNode* root, double target, int k) {
    return closestKValues1(root, target, k);
  }
};

int main() {
  return 0;
}
