#include "leetcode.h"

class Solution {
  bool dfs(TreeNode* cur, double target, int& val) {
    if (!cur) {
      return false;
    }

    if (dfs(cur->left, target, val)) {
      return true;
    }
    if (fabs(cur->val - target) < fabs(target - val)) {
      val = cur->val;
    }
    if (cur->val >= target) {
      return true;
    }
    if (dfs(cur->right, target, val)) {
      return true;
    }

    return false;
  }

public:
  int closestValue(TreeNode* root, double target) {
    int val = root->val;
    dfs(root, target, val);
    return val;
  }
};

int main() {
  return 0;
}
