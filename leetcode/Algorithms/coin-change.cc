#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

const int INF = 1e8;

class Solution {
  int coinChange1(vector<int>& coins, int amount) {
    if (coins.empty()) {
      return -1;
    }

    int n = (int)coins.size();
    vector<int> dp(amount + 1, INF);
    dp[0] = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = coins[i]; j <= amount; ++j) {
        if (dp[j - coins[i]] != INF) {
          dp[j] = min(dp[j - coins[i]] + 1, dp[j]);
        }
      }
    }
    return dp[amount] == INF ? -1 : dp[amount];
  }

public:
  int coinChange(vector<int>& coins, int amount) {
    return coinChange1(coins, amount);
  }
};

int main() {
  vector<int> coins = {1, 2, 5};
  Solution sol;
  int ret = sol.coinChange(coins, 100);
  cout << ret << endl;
}
