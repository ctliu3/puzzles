#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<int> restSum;

  void _combinationSum3(const int n, int k, int cur, int sum,
    vector<vector<int>>& res, vector<int>& sub) {
    if (k == 0) {
      if (sum == n) {
        res.push_back(sub);
      }
      return ;
    }

    for (int i = cur; i <= 9; ++i) {
      if (sum + restSum[k] < n) {
        continue;
      }
      sub.push_back(i);
      _combinationSum3(n, k - 1, i + 1, sum + i, res, sub);
      sub.pop_back();
    }
  }

public:
  vector<vector<int>> combinationSum3(int k, int n) {
    int maxn = (9 + (9 - k + 1)) * k / 2;
    if (n > maxn) {
      return {};
    }

    restSum.assign(10, 0);
    for (int i = 1; i < (int)restSum.size(); ++i) {
      restSum[i] = restSum[i - 1] + (10 - i);
    }

    vector<vector<int>> res;
    vector<int> sub;
    _combinationSum3(n, k, 1, 0, res, sub);
    return res;
  }
};

int main() {
  Solution sol;

  vector<vector<int>> res = sol.combinationSum3(3, 7);
  for (auto& vec : res) {
    for (auto& element : vec) {
      printf("%d ", element);
    }
    printf("\n");
  }

  return 0;
}
