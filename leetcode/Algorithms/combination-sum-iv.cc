#include "leetcode.h"

class Solution {
  void dfs1(vector<int>& nums, int cur, int target, vector<pair<int, int>>& sub, int& ans) {
    if (target == 0) {
      int sum = 0;
      for (int i = 0; i < (int)sub.size(); ++i) {
        sum += sub[i].second;
      }
      long long c = 1;
      for (auto& itr : sub) {
        int minval = min(itr.second, sum - itr.second);
        for (int i = 0; i < minval; ++i) {
          c *= sum - i;
        }
        for (int i = 1; i <= minval; ++i) {
          c /= i;
        }
        sum -= itr.second;
      }
      ans += c;
      return ;
    }
    if ((int)nums.size() == cur) {
      return ;
    }

    for (int i = 0; i * nums[cur] <= target; ++i) {
      if (i) {
        sub.push_back({nums[cur], i});
      }
      dfs1(nums, cur + 1, target - i * nums[cur], sub, ans);
      if (i) {
        sub.pop_back();
      }
    }
  }

  // TLE
  int combinationSum41(vector<int>& nums, int target) {
    vector<pair<int, int>> sub;
    int ans = 0;
    dfs1(nums, 0, target, sub, ans);
    return ans;
  }

  int dfs2(vector<int>& nums, vector<int>& dp, int target) {
    if (dp[target] != -1) {
      return dp[target];
    }
    int sum = 0;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (target >= nums[i]) {
        sum += dfs2(nums, dp, target - nums[i]);
      }
    }
    return dp[target] = sum;
  }

  int combinationSum42(vector<int>& nums, int target) {
    vector<int> dp(target + 1, -1);
    dp[0] = 1;
    return dfs2(nums, dp, target);
  }

public:
  int combinationSum4(vector<int>& nums, int target) {
    // return combinationSum41(nums, target);
    return combinationSum42(nums, target);
  }
};


int main() {
  return 0;
}
