#include "leetcode.h"

class Solution {
  void convert(const string& version, vector<int>& ver) {
    int idx = 0;

    for (const char& chr : version) {
      if (chr == '.') {
        ++idx;
        ver.resize(idx + 1);
        continue;
      }
      ver[idx] = ver[idx] * 10 + (chr - '0');
    }
  }

  bool allzero(vector<int>& ver, int idx) {
    while (idx < (int)ver.size()) {
      if (ver[idx++] != 0) {
        return false;
      }
    }
    return true;
  }

  int compareVersion1(string version1, string version2) {
    vector<int> ver1(1, 0), ver2(1, 0);

    convert(version1, ver1);
    convert(version2, ver2);

    int n1 = ver1.size(), n2 = ver2.size();
    for (int i = 0; i < min(n1, n2); ++i) {
      if (ver1[i] != ver2[i]) {
        return ver1[i] > ver2[i] ? 1 : -1;
      }
    }

    vector<int>& longer = n1 > n2 ? ver1 : ver2;
    if (n1 != n2 && !allzero(longer, min(n1, n2))) {
      return n1 > n2 ? 1 : -1;
    }
    return 0;
  }

  int get_ver(const string& ver, int& idx) {
    if (idx >= (int)ver.size()) {
      return -1;
    }

    int val = 0;
    while (idx < (int)ver.size()) {
      if (ver[idx] == '.') {
        ++idx;
        break;
      }
      val = val * 10 + (ver[idx] - '0');
      ++idx;
    }

    return val;
  }

  bool allzero(const string& version, int idx) {
    while (idx < (int)version.size()) {
      if (version[idx] != '.' && version[idx] != '0') {
        return false;
      }
      ++idx;
    }
    return true;
  }

  int compareVersion2(string version1, string version2) {
    int ver1 = 0, ver2 = 0;
    int idx1 = 0, idx2 = 0;

    while (true) {
      ver1 = get_ver(version1, idx1);
      ver2 = get_ver(version2, idx2);
      if (ver1 == -1 || ver2 == -1) {
        break;
      }
      if (ver1 != ver2) {
        return ver1 > ver2 ? 1 : -1;
      }
    }

    if (ver1 == -1 && ver2 == -1) {
      return 0;
    }
    if (ver1 == -1) {
      return !ver2 && allzero(version2, idx2) ? 0 : -1;
    } else {
      return !ver1 && allzero(version1, idx1) ? 0 : 1;
    }

    return 0;
  }

public:
  int compareVersion(string version1, string version2) {
    //return compareVersion1(version1, version2);
    return compareVersion2(version1, version2);
  }
};

int main() {
  Solution sol;
  int res = sol.compareVersion("1", "1.1");
  cout << res << endl;
}
