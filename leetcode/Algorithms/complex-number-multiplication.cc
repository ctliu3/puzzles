#include "leetcode.h"

class Solution {
  void parse(const string& s, int& a, int& b) { 
    bool is_neg = false;
    int i = 0, n = (int)s.size();
    a = 0 ;
    while (i < n && s[i] != '+') {
      if (s[i] == '-') {
        is_neg = true;
      } else {
        a = a * 10 + s[i] - '0';
      }
      ++i;
    }
    a = is_neg ? -a : a;

    b = 0;
    is_neg = false;
    ++i;
    while (i < n && s[i] != 'i') {
      if (s[i] == '-') {
        is_neg = true;
      } else {
        b = b * 10 + s[i] - '0';
      }
      ++i;
    }
    b = is_neg ? -b : b;
  }

public:
  string complexNumberMultiply(string sa, string sb) {
    int a, b, c, d;
    parse(sa, a, b);
    parse(sb, c, d);
    int x = a * c - b * d;
    int y = b * c + a * d;
    return to_string(x) + "+" + to_string(y) + "i";
  }
};

int main() {
  return 0;
}
