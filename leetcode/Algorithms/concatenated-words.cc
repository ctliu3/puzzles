#include "leetcode.h"

class TrieNode {
public:
  TrieNode* child[26];
  bool word;

  explicit TrieNode() {
    for (int i = 0; i < 26; ++i) {
      child[i] = nullptr;
    }
    word = false;
  }
};

class Trie {
public:
  TrieNode* root;
  explicit Trie() {
    root = new TrieNode();
  }
  ~Trie() {
    if (root != nullptr) {
      delete root;
      root = nullptr;
    }
  }

  void insert(string& s) {
    if (s.empty()) {
      return ;
    }
    TrieNode* cur = root;
    for (auto& chr : s) {
      if (!cur->child[chr - 'a']) {
        cur->child[chr - 'a'] = new TrieNode();
      }
      cur = cur->child[chr - 'a'];
    }
    cur->word = true;
  }

  // MLE or TLE?
  bool search_(TrieNode* node, const string& s, int st, int count) {
    if (st == (int)s.size()) {
      return count >= 2;
    }
    TrieNode* cur = node;
    for (int i = st; i < (int)s.size(); ++i) {
      int pos = s[i] - 'a';
      if (!cur->child[pos]) {
        return false;
      }
      cur = cur->child[pos];
      if (cur->word) {
        if (search_(root, s, i + 1, count + 1)) { // => i + 1
          return true;
        }
      }
    }
    return false;
  }

  // MLE
  bool search2_(TrieNode* node, const string& s, int st, int count) {
    if (st == (int)s.size()) {
      return count >= 2;
    }
    TrieNode* cur = node;
    vector<int> ends;
    for (int i = st; i < (int)s.size(); ++i) {
      int pos = s[i] - 'a';
      if (!cur->child[pos]) {
        break;
      }
      cur = cur->child[pos];
      if (cur->word) {
        ends.push_back(i);
      }
    }
    int n = (int)ends.size();
    for (int i = n - 1; i >= 0; --i) {
      if (search2_(root, s, ends[i] + 1, count + 1)) {
        return true;
      }
    }

    return false;
  }

  bool search(const string& s) {
    // return search_(root, s, 0, 0);
    return search2_(root, s, 0, 0);
  }
};

class Solution {
  vector<string> findAllConcatenatedWordsInADict1(vector<string>& words) {
    int n = (int)words.size();
    vector<int> ids(n);
    for (int i = 0; i < n; ++i) {
      ids[i] = i;
    }
    sort(ids.begin(), ids.end(), [&](const int lhs, const int rhs) {
      return words[lhs].size() < words[rhs].size();
    });

    Trie tree;
    vector<string> ans;
    unordered_map<string, int> counter;
    for (int i = 0; i < n; ++i) {
      string& word = words[ids[i]];
      if (tree.search(word)) {
        ans.push_back(word);
      }
      tree.insert(word);
    }
    return ans;
  }

  bool treedp2(const string& s, int st, unordered_set<string>& hash, bool is_frist) {
    if (!is_frist) {
      auto itr = hash.find(s.substr(st));
      if (itr != hash.end()) {
        return true;
      }
    }
    for (int i = st; i < (int)s.size() - 1; ++i) {
      if (hash.count(s.substr(st, i - st + 1)) == 0) {
        continue;
      }
      if (treedp2(s, i + 1, hash, false)) {
        return true;
      }
    }
    return false;
  }

  vector<string> findAllConcatenatedWordsInADict2(vector<string>& words) {
    int n = (int)words.size();

    unordered_set<string> hash;
    for (int i = 0; i < n; ++i) {
      hash.insert(words[i]);
    }
    vector<string> ans;
    for (int i = 0; i < n; ++i) {
      string& word = words[i];
      if (word.size() > 1 && treedp2(word, 0, hash, true)) {
        ans.push_back(word);
      }
    }
    return ans;
  }

public:
  vector<string> findAllConcatenatedWordsInADict(vector<string>& words) {
    // return findAllConcatenatedWordsInADict1(words);
    return findAllConcatenatedWordsInADict2(words);
  }
};

int main() {
  return 0;
}
