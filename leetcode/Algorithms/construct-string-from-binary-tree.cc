#include "leetcode.h"

class Solution {
  string tree2str1(TreeNode* t) {
    if (!t) {
      return "";
    }
    if (!t->left && !t->right) {
      return to_string(t->val);
    }
    if (t->left && !t->right) {
      return to_string(t->val) + "(" + tree2str1(t->left) + ")";
    }
    return to_string(t->val) + "(" + tree2str1(t->left) + ")" + "(" + tree2str1(t->right) + ")";
  }

public:
  string tree2str(TreeNode* t) {
    return tree2str1(t);
  }
};

int main() {
  return 0;
}
