class Solution {
public:
  vector<int> constructRectangle(int area) {
    int side = sqrt(area);
    if (side * side == area) {
      return {side, side};
    }
    while (area % side != 0) {
      side -= 1;
    }
    return {area / side, side};
  }
};
