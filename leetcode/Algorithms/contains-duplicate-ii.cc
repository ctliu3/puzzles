#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
  int n_duplicates(vector<int>& vec, int i, int k) {
    int prev = i - k;
    return vec.end() - lower_bound(vec.begin(), vec.end(), prev) > 0;
  }

  bool containsNearbyDuplicate1(vector<int>& nums, int k) {
    if (nums.size() < 2) {
      return false;
    }

    unordered_map<int, vector<int>> hash;
    int flag = false;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (hash.count(nums[i])) {
        int m = n_duplicates(hash[nums[i]], i, k);
        if (m > 1) {
          return false;
        } else if (m == 1) {
          if (flag) {
            return false;
          } else {
            flag = true;
          }
        }
      }
      hash[nums[i]].push_back(i);
    }

    return flag;
  }

  bool containsNearbyDuplicate2(vector<int>& nums, int k) {
    unordered_set<int> hash;
    int start = 0, end = 0;

    while (end < (int)nums.size()) {
      if (hash.count(nums[end])) {
        return true;
      }
      hash.insert(nums[end]);
      ++end;
      if (end - start > k) {
        hash.erase(nums[start]);
        ++start;
      }
    }
    return false;
  }

public:
  bool containsNearbyDuplicate(vector<int>& nums, int k) {
    //return containsNearbyDuplicate1(nums, k);
    return containsNearbyDuplicate2(nums, k);
  }
};

int main() {
  Solution sol;
  vector<int> vec = {1, 2};

  bool res = sol.containsNearbyDuplicate(vec, 2);
  cout << res << endl;

  return 0;
}
