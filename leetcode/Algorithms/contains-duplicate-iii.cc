#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>

using namespace std;

class Solution {
  // TLE
  bool containsNearbyAlmostDuplicate1(vector<int>& nums, int k, int t) {
    if (nums.size() < 2) {
      return false;
    }

    unordered_map<int, int> mp;
    int start = 0, end = 0;
    while (end < (int)nums.size()) {
      for (auto& pair : mp) {
        if (abs(pair.first - nums[end]) <= t) {
          return true;
        }
      }

      mp[nums[end]] = end;

      ++end;
      if (end - start > k) {
        if (mp[nums[start]] == start) {
          mp.erase(nums[start]);
        }
        ++start;
      }
    }

    return false;
  }

  int containsNearbyAlmostDuplicate2(vector<int>& nums, int k, int t) {
    if (nums.size() < 2) {
      return false;
    }

    map<int, int> mp;
    int start = 0, end = 0;
    while (end < (int)nums.size()) {
      if (!mp.empty()) {
        map<int, int>::iterator itr = mp.lower_bound(nums[end] - t);
        if (itr != mp.end() && abs(itr->first - nums[end]) <= t) {
          return true;
        }
      }

      mp[nums[end]] = end;

      ++end;
      if (end - start > k) {
        if (mp[nums[start]] == start) {
          mp.erase(nums[start]);
        }
        ++start;
      }
    }

    return false;
  }

public:

  bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
    return containsNearbyAlmostDuplicate1(nums, k, t);
    //return containsNearbyAlmostDuplicate2(nums, k, t);
  }
};

int main() {
  Solution sol;
  vector<int> nums = {-1, -1};
  int res = sol.containsNearbyAlmostDuplicate(nums, 1, 0);
  cout << res << endl;
  return 0;
}
