#include <iostream>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
public:
  bool containsDuplicate(vector<int>& nums) {
    if (nums.size() < 2) {
      return false;
    }

    unordered_set<int> hash;
    for (auto& element : nums) {
      if (hash.count(element)) {
        return true;
      }
      hash.insert(element);
    }
    return false;
  }
};

int main() {
}
