#include "leetcode.h"

class Solution {
public:
  int findMaxLength(vector<int>& nums) {
    int n = (int)nums.size();
    int acc = 0, ans = 0;
    unordered_map<int, int> mp;
    for (int i = 0; i < n; ++i) {
      int cur = nums[i] ? 1 : -1;
      acc += cur;
      auto itr = mp.find(acc);
      if (itr != mp.end()) {
        ans = max(ans, i - itr->second);
      }
      if (acc == 0) {
        ans = i + 1;
      }
      if (mp.find(acc) == mp.end()) {
        mp[acc] = i;
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
