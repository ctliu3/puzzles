#include "leetcode.h"

class Solution {
public:
  bool checkSubarraySum(vector<int>& nums, int k) {
    if (nums.size() < 2) {
      return false;
    }
    if (k == 0) {
      for (int i = 0; i < nums.size() - 1; ++i) {
        if (nums[i] + nums[i + 1] == 0) {
          return true;
        }
      }
      return false;
    }

    int n = nums.size();
    int acc = 0;
    unordered_map<int, int> mp;
    for (int i = 0; i < n; ++i) {
      acc += nums[i];
      int mod = acc % k;
      auto ptr = mp.find(mod);
      if (ptr == mp.end()) {
        mp[mod] = i;
      }
      if (ptr != mp.end() && i - ptr->second > 0) {
        return true;
      }
      if (acc % k == 0 && i > 0) {
        return true;
      }
    }

    return false;
  }
};

int main() {
  return 0;
}
