#include <string>
#include <iostream>

using namespace std;

class Solution {
  string pos2hex(long long num) {
    string ret;
    long long h = 16;
    while (h * 16 <= num) {
      h *= 16;
    }
    while (h) {
      ret.push_back(num / h);
      num %= h;
      h /= 16;
    }
    return ret;
  }

public:
  string toHex(int num) {
    if (num == 0) {
      return "0";
    }

    string ret;
    if (num > 0) {
      ret = pos2hex(num);
    } else {
      long long positive = -(long long)num;
      ret = pos2hex(positive);
      ret = string(8 - ret.size(), 0) + ret;
      for (int i = 0; i < (int)ret.size(); ++i) {
        ret[i] = 15 - ret[i];
      }
      ret[(int)ret.size() - 1] += 1;
      for (int i = (int)ret.size() - 1; i >= 0; --i) {
        if (ret[i] >= 16) {
          ret[i] -= 16;
          ret[i - 1] += 1;
        }
      }
    }
    for (int i = 0; i < (int)ret.size(); ++i) {
      ret[i] = ret[i] < 10 ? ret[i] + '0' : ret[i] - 10 + 'a';
    }
    int st = 0;
    while (st + 1 < (int)ret.size() && ret[st] == '0') {
      ++st;
    }
    return ret.substr(st);
  }
};

int main() {
  Solution sol;
  string ret = sol.toHex(-100000);
  cout << ret << endl;
}
