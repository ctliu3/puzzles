#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, vector<int>& order) {
    if (!cur) {
      return ;
    }
    dfs1(cur->left, order);
    order.push_back(cur->val);
    dfs1(cur->right, order);
  }

  void replaceVal1(TreeNode* cur, vector<int>& order, vector<int>& acc) {
    if (!cur) {
      return ;
    }
    int pos = upper_bound(order.begin(), order.end(), cur->val) - order.begin();
    if (pos < acc.size()) {
      cur->val += acc[pos];
    }
    replaceVal1(cur->left, order, acc);
    replaceVal1(cur->right, order, acc);
  }

  TreeNode* convertBST1(TreeNode* root) {
    if (!root) {
      return nullptr;
    }
    vector<int> order;
    dfs1(root, order);
    vector<int> acc(order.size());
    acc.back() = order.back();
    for (int i = order.size() - 1; i >= 0; --i) {
      acc[i] = acc[i + 1] + order[i];
    }
    replaceVal1(root, order, acc);
    return root;
  }

public:
  TreeNode* convertBST(TreeNode* root) {
    return convertBST1(root);
  }
};

int main() {
  return 0;
}
