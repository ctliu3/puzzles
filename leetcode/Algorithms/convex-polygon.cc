#include "leetcode.h"

class Solution {
  int cross_product(const vector<int>& p0, const vector<int>& p1, const vector<int>& p2) {
    int x1 = p1[0] - p0[0];
    int y1 = p1[1] - p0[1];
    int x2 = p2[0] - p1[0];
    int y2 = p2[1] - p1[1];
    return x1 * y2 - y1 * x2;
  }

  bool isConvex1(vector<vector<int>>& points) {
    int n = (int)points.size();
    if (n < 3) {
      return false;
    }
    if (cross_product(points[1], points[0], points[n - 1]) < 0) {
      return false;
    }
    if (cross_product(points[0], points[n - 1], points[n - 2]) < 0) {
      return false;
    }
    for (int i = 1; i < n - 1; ++i) {
      if (cross_product(points[i + 1], points[i], points[i - 1]) < 0) {
        return false;
      }
    }
    return true;
  }

public:
  bool isConvex(vector<vector<int>>& points) {
    if (isConvex1(points)) {
      return true;
    }
    reverse(points.begin(), points.end());
    return isConvex1(points);
  }
};

int main() {
  return 0;
}
