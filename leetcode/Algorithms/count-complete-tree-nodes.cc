#include "leetcode.h"

class Solution {

  int _pow(int x, int n) {
    int res = 1, base = x;
    while (n) {
      if (n & 1) {
        res *= base;
      }
      n >>= 1;
      base *= base;
    }
    return res;
  }

  int countNodes1(TreeNode* root) {
    if (!root) {
      return 0;
    }
    return _dfs(root);
  }

  int _dfs(TreeNode* subroot) {
    if (subroot && !subroot->left && !subroot->right) {
      return 1;
    }
    if (!subroot) {
      return 0;
    }

    int lh = 0, rh = 0;
    TreeNode* cur = subroot;
    while (cur) {
      cur = cur->left;
      ++lh;
    }
    cur = subroot;
    while (cur) {
      cur = cur->right;
      ++rh;
    }
    if (lh == rh) {
      return _pow(2, lh) - 1;
    }
    return 1 + _dfs(subroot->left) + _dfs(subroot->right);
  }

  int getLeftHeight(TreeNode* cur) {
    int height = 0;
    while (cur) {
      ++height;
      cur = cur->left;
    }
    return height;
  }

  int getRightHeight(TreeNode* cur) {
    int height = 0;
    while (cur) {
      ++height;
      cur = cur->right;
    }
    return height;
  }

  bool isExist(int val, TreeNode* cur, int nbit) {
    for (int i = 0; i < nbit && cur; ++i) {
      int flag = val & (1 << (nbit - i - 1));
      if (flag) {
        cur = cur->right;
      } else {
        cur = cur->left;
      }
    }
    return cur ? true : false;
  }

  int countNodes2(TreeNode* root) {
    if (!root) {
      return 0;
    }
    int lh = getLeftHeight(root);
    int rh = getRightHeight(root);
    if (lh == rh) {
      return pow(2, lh) - 1;
    }
    int st = 0, ed = pow(2, rh) - 1;
    int offset = 0;
    while (st <= ed) {
      int mid = st + (ed - st) / 2;
      if (isExist(mid, root, rh)) {
        offset = mid + 1;
        st = mid + 1;
      } else {
        ed = mid - 1;
      }
    }
    return pow(2, rh) - 1 + offset;
  }

public:
  int countNodes(TreeNode* root) {
    // return countNodes1(root);
    return countNodes2(root);
  }
};

int main() {
  return 0;
}
