#include <iostream>
#include <iostream>

using namespace std;

class Solution {
public:
  int countNumbersWithUniqueDigits(int n) {
    if (n == 0) {
      return 1;
    }

    int ret = 0;
    for (int i = 1; i <= n; ++i) {
      int count = 9;
      for (int j = 1; j < i; ++j) {
        count *= 10 - j;
      }
      ret += count;
    }
    return ret + 1;
  }
};

int main() {
  Solution sol;
  int ans = sol.countNumbersWithUniqueDigits(3);
  cout << ans << endl;
  return 0;
}
