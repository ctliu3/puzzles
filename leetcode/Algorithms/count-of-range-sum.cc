#include <vector>
#include <iostream>

using namespace std;

class Solution {
  int _countRangeSum1(vector<int>& nums, int st, int ed, int lower, int upper) {
    if (st > ed) {
      return 0;
    }
    if (st == ed) { 
      return nums[st] >= lower and nums[st] <= upper;
    }

    int mid = st + (ed - st) / 2;
    vector<long long> prefix_sum(mid - st + 1);
    for (int i = mid; i >= st; --i) {
      prefix_sum[mid - i] = i != mid ? prefix_sum[mid - i - 1] + nums[i] : nums[i];
    }
    sort(prefix_sum.begin(), prefix_sum.end());

    int ret = 0;
    long long sum = 0;
    for (int i = mid + 1; i <= ed; ++i) {
      sum += nums[i];
      long long l =  lower - sum, r = upper - sum;
      int pos1 = lower_bound(prefix_sum, l);
      int pos2 = lower_bound(prefix_sum, r + 1) - 1;
      if (pos1 != (int)prefix_sum.size() and pos2 >= pos1) {
        ret += pos2 - pos1 + 1;
      }
    }
    return ret + _countRangeSum1(nums, st, mid, lower, upper)
      + _countRangeSum1(nums, mid + 1, ed, lower, upper);
  }

  // Invariant relation.
  int lower_bound(vector<long long>& nums, long long target) {
    // invariatn relation is A[start] < target <= A[end]
    int start = -1, end = nums.size();

    while (end - start > 1) {
      int mid = start + (end - start) / 2;
      if (nums[mid] >= target) { // design to fit the invariatn relation
        end = mid;
      } else {
        start = mid;
      }
    }

    return end;
  }

  int countRangeSum1(vector<int>& nums, int lower, int upper) {
    return _countRangeSum1(nums, 0, (int)nums.size() - 1, lower, upper);
  }

public:
  int countRangeSum(vector<int>& nums, int lower, int upper) {
    return countRangeSum1(nums, lower, upper);
  }
};

int main() {
  vector<int> nums = {0,-3,-3,1,1,2};
  Solution sol;
  int ret = sol.countRangeSum(nums, 3, 5);
  cout << ret << endl;

  return 0;
}
