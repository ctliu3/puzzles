#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
  unordered_map<int, int> count;

  void _merge_sort(vector<pair<int, int>>& pairs, vector<pair<int, int>>& temp,
    int l, int r, vector<int>& ret) {
    if (l >= r) {
      return ;
    }
    int mid = l + (r - l) / 2;
    _merge_sort(pairs, temp, l, mid, ret);
    _merge_sort(pairs, temp, mid + 1, r, ret);
    
    int i = l, j = mid + 1;
    int k = 0;
    while (i <= mid and j <= r) {
      if (pairs[i].first > pairs[j].first) {
        ret[pairs[i].second] += r - j + 1;
        temp[k++] = pairs[i++];
      } else {
        temp[k++] = pairs[j++];
      }
    }
    while (i <= mid) {
      temp[k++] = pairs[i++];
    }
    while (j <= r) {
      temp[k++] = pairs[j++];
    }
    for (int o = 0; o < k; ++o) {
      pairs[l + o] = temp[o];
    }
  }

  void merge_sort(vector<pair<int, int>>& pairs, vector<int>& ret) {
    vector<pair<int, int>> temp(pairs.size());
    _merge_sort(pairs, temp, 0, pairs.size() - 1, ret);
  }

public:
  vector<int> countSmaller(vector<int>& nums) {
    if (nums.empty()) {
      return {};
    }

    vector<pair<int, int>> pairs(nums.size());
    for (int i = 0; i < (int)nums.size(); ++i) {
      pairs[i] = make_pair(nums[i], i);
    }
    vector<int> ret(nums.size());
    merge_sort(pairs, ret);
    return ret;
  }
};

int main() {
  vector<int> nums = {5, 2, 6, 1};
  Solution sol;
  auto ret = sol.countSmaller(nums);
  for (auto& ele : ret) {
    cout << ele << ' ';
  }
  cout << endl;
}
