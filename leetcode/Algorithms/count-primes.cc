#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int countPrimes(int n) {
    if (n <= 2) {
      return 0;
    }

    vector<bool> is_prime(n, true);
    for (int i = 2; i * i < n; ++i) {
      if (!is_prime[i]) {
        continue;
      }
      for (int j = i * i; j < n; j += i) {
        is_prime[j] = false;
      }
    }
    int res = 0;
    for (int i = 2; i < n; ++i) {
      if (is_prime[i]) {
        ++res;
      }
    }
    return res;
  }
};

int main() {
  return 0;
}
