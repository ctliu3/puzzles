#include <string>
#include <vector>
#include <iostream>

using namespace std;

class Solution {
  int getMaxRepetitions1(string s1, int n1, string s2, int n2) {
    if (n1 * s1.size() < n2 * s2.size()) {
      return 0;
    }

    int sn1 = (int)s1.size(), sn2 = (int)s2.size();
    vector<vector<pair<int, int>>> dp(sn1, vector<pair<int, int>>(sn2, {-1, -1}));
    int i = 0, j = 0;

    while (i < sn1 * n1) {
      while (i < sn1 * n1 && s1[i % sn1] != s2[j % sn2]) {
        ++i;
      }
      if (i >= sn1 * n1) {
        break;
      }
      if (dp[i % sn1][j % sn2].first == -1) {
        dp[i % sn1][j % sn2] = {i, j};
      } else {
        auto prev = dp[i % sn1][j % sn2];
        int round = (sn1 * n1 - 1 - i) / (i - prev.first);
        i += round * (i - prev.first);
        j += round * (j - prev.second);
      }
      ++i;
      ++j;
    }
    return j / (n2 * sn2);
  }

  // TLE
  int getMaxRepetitions2(string s1, int n1, string s2, int n2) {
    if (n1 * s1.size() < n2 * s2.size()) {
      return 0;
    }

    int sn1 = (int)s1.size(), sn2 = (int)s2.size();
    int i = 0, j = 0;
    while (i < sn1 * n1) {
      if (s1[i % sn1] != s2[j % sn2]) {
        ++i;
      } else {
        ++i;
        ++j;
      }
    }
    return j / (n2 * sn2);
  }

public:
  int getMaxRepetitions(string s1, int n1, string s2, int n2) {
    // return getMaxRepetitions1(s1, n1, s2, n2);
    return getMaxRepetitions2(s1, n1, s2, n2);
  }
};

int main() {
  Solution sol;
  int ret = sol.getMaxRepetitions("aabcabca", 1, "abc", 1);
  cout << ret << endl;
  return 0;
}
