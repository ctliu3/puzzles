#include "leetcode.h"

class Solution {
  pair<bool, int> dfs(TreeNode* cur, int& count) {
    if (!cur->left && !cur->right) {
      ++count;
      return {true, cur->val};
    }

    pair<bool, int> lret, rret;
    int nok = 0;
    if (cur->left) {
      lret = dfs(cur->left, count);
      nok += lret.first;
    } else {
      ++nok;
    }

    if (cur->right) {
      rret = dfs(cur->right, count);
      nok += rret.first;
    } else {
      ++nok;
    }

    if (nok != 2 || (lret.first && rret.first && lret.second != rret.second)) {
      return {false, -1};
    }

    int val = lret.first ? lret.second : rret.second;
    if (val == cur->val) {
      ++count;
      return {true, cur->val};
    }
    return {false, -1};
  }

public:
  int countUnivalSubtrees(TreeNode* root) {
    if (!root) {
      return 0;
    }
    int count = 0;
    dfs(root, count);
    return count;
  }
};

int main() {
  return 0;
}
