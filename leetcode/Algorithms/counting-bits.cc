#include "leetcode.h"

class Solution {
public:
  vector<int> countBits(int num) {
    vector<int> ans = {0, 1, 1 , 2};
    if (num + 1 <= (int)ans.size()) {
      ans.resize(num + 1);
      return ans;
    }
    ans.resize(num + 1);
    int m = 4, i = 4;
    while (i <= num) {
      for (int j = 0; j < m && i <= num; ++j) {
        ans[i++] = ans[j] + 1;
      }
      m = i;
    }
    return ans;
  }
};

int main() {
  return 0;
}
