#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Solution {
  vector<int> getOrder(vector<vector<int>>& e, vector<int>& deg) {
    queue<int> que;

    for (int i = 0; i < (int)deg.size(); ++i) {
      if (!deg[i]) {
        que.push(i);
      }
    }

    vector<int> order;
    while (!que.empty()) {
      int u = que.front();
      que.pop();

      order.push_back(u);
      for (auto& v : e[u]) {
        --deg[v];
        if (!deg[v]) {
          que.push(v);
        }
      }
    }

    if (order.size() == deg.size()) {
      return order;
    }
    return {};
  }

public:
  vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {

    vector<vector<int>> e(numCourses, vector<int>());
    vector<int> deg(numCourses, 0);
    for (auto& item : prerequisites) {
      e[item.second].push_back(item.first);
      ++deg[item.first];
    }
    return getOrder(e, deg);
  }
};

int main() {
  return 0;
}
