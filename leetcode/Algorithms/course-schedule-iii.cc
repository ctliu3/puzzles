#include "leetcode.h"

using namespace std;

class Solution {
public:
  int scheduleCourse(vector<vector<int>>& courses) {
    if (courses.empty()) {
      return 0;
    }
    sort(courses.begin(), courses.end(),
      [](const vector<int>& lhs, const vector<int>& rhs) {
      if (lhs[1] == rhs[1]) {
        return lhs[1] < rhs[1];
      }
      return lhs[0] > rhs[0];
      });

    for (auto one : courses) {
      cout << one[0] << ' ' << one[1] << endl;
    }
    int n = (int)courses.size();
    vector<int> dp(n, 0);
    dp[0] = 1;
    int ret = 1;
    for (int i = 1; i < n; ++i) {
      dp[i] = 1;
      for (int j = 0; j < i; ++j) {
        if (courses[j][1] <= courses[i][0]) {
          dp[i] = max(dp[i], dp[j] + 1);
        } else {
          break;
        }
      }
      ret = max(ret, dp[i]);
    }
    return ret;
  }
};

int main() {
  vector<vector<int>> cases = {{5, 5}, {4, 6}, {2, 6}};
  Solution sol;
  int ret = sol.scheduleCourse(cases);
  cout << ret << endl;
  return 0;
}
