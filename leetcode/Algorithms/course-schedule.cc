#include <iostream>
#include <queue>
#include <vector>

using namespace std;

class Solution {
  bool hasCircle(vector<vector<int>>& e, vector<int>& deg) {
    queue<int> que;
    for (int i = 0; i < (int)deg.size(); ++i) {
      if (!deg[i]) {
        que.push(i);
      }
    }

    int cnt = 0;
    while (!que.empty()) {
      int u = que.front();
      que.pop();
      ++cnt;

      for (auto& v : e[u]) {
        --deg[v];
        if (!deg[v]) {
          que.push(v);
        }
      }
    }
    return cnt != (int)deg.size();
  }

public:
  bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
    if (prerequisites.empty()) {
      return true;
    }
    vector<vector<int>> e(numCourses, vector<int>());
    vector<int> deg(numCourses, 0);

    for (auto& item : prerequisites) {
      e[item.second].push_back(item.first);
      ++deg[item.first];
    }
    return !hasCircle(e, deg);
  }
};

int main() {
  return 0;
}
