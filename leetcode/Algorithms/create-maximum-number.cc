#include <iostream>
#include <vector>

using namespace std;

class Solution {
  void max_number_in_array(vector<int>& num, int k, vector<int>& number) {
    number.clear();
    if (k <= 0) {
      return ;
    }
    int n = (int)num.size();
    for (int i = 0; i < n; ++i) {
      while (!number.empty() && num[i] > number.back()
        && n - i + number.size() > k) {
        number.pop_back();
      }
      if (number.size() < k) {
        number.push_back(num[i]);
      }
    }
  }

  vector<int> merge(vector<int>& number1, vector<int>& number2) {
    int i = 0, j = 0, n1 = (int)number1.size(), n2 = (int)number2.size();
    vector<int> ret;

    while (i < n1 || j < n2) {
      int o = 0;
      while (i + o < n1 && j + o < n2 && number1[i + o] == number2[j + o]) {
        ++o;
      }
      if (j + o == n2
        || (i + o < n1 && j + o < n2 && number1[i + o] > number2[j + o])) {
        ret.push_back(number1[i++]);
      } else {
        ret.push_back(number2[j++]);
      }
    }
    return ret;
  }

  bool greater(vector<int>& lhs, vector<int>& rhs) {
    if (lhs.size() != rhs.size()) {
      return lhs.size() > rhs.size();
    }
    for (int i = 0; i < (int)lhs.size(); ++i) {
      if (lhs[i] != rhs[i]) {
        return lhs[i] > rhs[i];
      }
    }
    return false;
  }

  vector<int> maxNumber1(vector<int>& nums1, vector<int>& nums2, int k) {
    vector<int> ret, number1, number2;

    for (int i = 0; i <= k; ++i) {
      max_number_in_array(nums1, i, number1);
      max_number_in_array(nums2, k - i, number2);
      vector<int> cur = merge(number1, number2);
      if (greater(cur, ret)) {
        ret = cur;
      }
    }

    return ret;
  }

public:
  vector<int> maxNumber(vector<int>& nums1, vector<int>& nums2, int k) {
    return maxNumber1(nums1, nums2, k);
  }
};

int main() {
  vector<int> nums1 = {8, 6, 9}, nums2 = {1, 7, 5};
  Solution sol;
  vector<int> ret = sol.maxNumber(nums1, nums2, 3);
  for (auto& ele : ret) {
    cout << ele << ' ';
  }
  cout << endl;
  return 0;
}
