#include "leetcode.h"

struct Comp {
  bool operator() (const Interval& lhs, const Interval& rhs) const {
    return lhs.start < rhs.start;
  }
};

class SummaryRanges {
  unordered_map<int, pair<int, int>> mp;
  set<Interval, Comp> st;

public:
  /** Initialize your data structure here. */
  SummaryRanges() {
  }

  void addNum(int val) {
    if (mp.find(val) != mp.end()) {
      return ;
    }

    int start = val, end = val;
    bool left = false, right = false;
    auto p = mp.find(val - 1);
    if (p != mp.end()) {
      start = p->second.first;
      st.erase(Interval(start, val - 1));
      left = true;
    }
    p = mp.find(val + 1);
    if (p != mp.end()) {
      end = p->second.second;
      st.erase(Interval(val + 1, end));
      right = true;
    }
    if (left) {
      mp[start].second = end;
    }
    if (right) {
      mp[end].first = start;
    }
    mp[val] = {start, end};
    st.insert(Interval(start, end));
  }

  vector<Interval> getIntervals() {
    vector<Interval> ret;
    for (auto& p : st) {
      ret.push_back(p);
    }
    return ret;
  }
};

/**
 * Your SummaryRanges object will be instantiated and called as such:
 * SummaryRanges obj = new SummaryRanges();
 * obj.addNum(val);
 * vector<Interval> param_2 = obj.getIntervals();
 */
