#include "leetcode.h"

class Solution {
  TreeNode* deleteNode1(TreeNode* root, int key) {
    if (!root) {
      return nullptr;
    }

    TreeNode** cur = &root;
    while (*cur && (*cur)->val != key) {
      cur = key > (*cur)->val ? &(*cur)->right : &(*cur)->left;
    }
    if (!(*cur)) {
      return root;
    }
    bool flag = key == root->val;

    if ((*cur)->right) {
      TreeNode* left = (*cur)->left;
      (*cur) = (*cur)->right;
      TreeNode* temp = *cur;
      while (temp && temp->left) {
        temp = temp->left;
      }
      temp->left = left;
    } else if ((*cur)->left) {
      *cur = (*cur)->left;
    } else {
      *cur = nullptr;
    }

    return flag ? *cur : root;
  }

public:
  TreeNode* deleteNode(TreeNode* root, int key) {
    return deleteNode1(root, key);
  }
};

int main() {
  string tree = "5,3,6,2,4,#,7";
  TreeNode* root = getTree(tree);
  Solution sol;
  root = sol.deleteNode(root, 3);
  auto ret = level_order(root);
  print_vector2(ret);
}
