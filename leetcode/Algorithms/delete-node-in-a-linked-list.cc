#include "leetcode.h"

class Solution {
  void deleteNode2(ListNode* node) {
    ListNode* cur = node->next;
    while (cur->next) {
      node->val = cur->val;
      cur = cur->next;
      node = node->next;
    }
    node->val = cur->val;
    node->next = nullptr;
    delete cur;
  }

  void deleteNode1(ListNode* node) {
    if (!node || !node->next) {
      return ;
    }
    ListNode* del = node->next;
    node->val = del->val;
    node->next = del->next;
    delete del;
  }

public:
  void deleteNode(ListNode* node) {
    return deleteNode1(node);
    // return deleteNode2(node);
  }
};

int main() {
  return 0;
}
