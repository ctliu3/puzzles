#include "leetcode.h"

class Solution {
  int lcs(const string& s1, const string& s2) {
    int n = (int)s1.size();
    int m = (int)s2.size();
    vector<vector<int>> dp(n + 1, vector<int>(m + 1, 0));
    int ans = 0;
    for (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= m; ++j) {
        if (s1[i - 1] == s2[j - 1]) {
          dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + 1);
        }
        dp[i][j] = max(dp[i][j], dp[i - 1][j]);
        dp[i][j] = max(dp[i][j], dp[i][j - 1]);
      }
    }
    return dp[n][m];
  }

public:
  int minDistance(string word1, string word2) {
    int len = lcs(word1, word2);
    return word1.size() + word2.size() - 2 * len;
  }
};

int main() {
  return 0;
}
