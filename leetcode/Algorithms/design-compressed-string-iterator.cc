class StringIterator {
  int index;
  int cur;
  int count;
  string s;

public:
  StringIterator(string compressedString) {
    s = compressedString;
    index = 0;
    cur = ' ';
    count = 0;
  }

  char next() {
    if (hasNext()) {
      process();
      return cur;
    }
    return ' ';
  }

  void process() {
    if (count > 0) {
      count -= 1;
    } else {
      int num = 0;
      cur = s[index++];
      while (index < s.size() && isdigit(s[index])) {
        num = num * 10 + s[index] - '0';
        index += 1;
      }
      count = num;
      --count;
    }
  }

  bool hasNext() {
    if (count == 0 && index >= s.size()) {
      cur = ' ';
      return false;
    }
    return true;
  }
};

/**
 * Your StringIterator object will be instantiated and called as such:
 * StringIterator obj = new StringIterator(compressedString);
 * char param_1 = obj.next();
 * bool param_2 = obj.hasNext();
 */
