#include <queue>

using namespace std;

class HitCounter {
  deque<int> que;
  int sum;
public:
  /** Initialize your data structure here. */
  HitCounter() {
    que.clear();
    sum = 0;
  }

  /** Record a hit.
    @param timestamp - The current timestamp (in seconds granularity). */
  void hit(int timestamp) {
    que.push_back(timestamp);
    ++sum;
  }

  /** Return the number of hits in the past 5 minutes.
    @param timestamp - The current timestamp (in seconds granularity). */
  int getHits(int timestamp) {
    int start = timestamp - 300 + 1;
    while (que.size() && que.front() < start) {
      que.pop_front();
      --sum;
    }
    return sum;
  }
};
