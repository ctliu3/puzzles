#include "leetcode.h"

// class PhoneDirectory {
// public:
  // unordered_set<int> hash;
  /** Initialize your data structure here
   *         @param maxNumbers - The maximum numbers that can be stored in the phone directory. */
  // PhoneDirectory(int maxNumbers) {
    // hash.clear();
    // for (int i = 0; i < maxNumbers; ++i) {
      // hash.insert(i);
    // }
  // }

  /** Provide a number which is not assigned to anyone.
   *         @return - Return an available number. Return -1 if none is available. */
  // int get() {
    // if (hash.empty()) {
      // return -1;
    // }
    // auto p = hash.begin();
    // int ret = *p;
    // hash.erase(p);
    // return ret;
  // }

  // [>* Check if a number is available or not. <]
  // bool check(int number) {
    // return hash.find(number) != hash.end();
  // }

  // [>* Recycle or release a number. <]
  // void release(int number) {
    // hash.insert(number);
  // }
// };

class PhoneDirectory {
public:
  set<int> st;
  vector<bool> hash;
  
  /** Initialize your data structure here
   *         @param maxNumbers - The maximum numbers that can be stored in the phone directory. */
  PhoneDirectory(int maxNumbers) {
    st.clear();
    for (int i = 0; i < maxNumbers; ++i) {
      st.insert(i);
    }
    hash.assign(maxNumbers, true);
  }

  /** Provide a number which is not assigned to anyone.
   *         @return - Return an available number. Return -1 if none is available. */
  int get() {
    if (st.empty()) {
      return -1;
    }
    auto p = st.begin();
    int ret = *p;
    st.erase(p);
    hash[ret] = false;
    return ret;
  }

  /** Check if a number is available or not. */
  bool check(int number) {
    return hash[number];
  }

  /** Recycle or release a number. */
  void release(int number) {
    st.insert(number);
    hash[number] = true;
  }
};
