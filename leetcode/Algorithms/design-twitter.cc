#include "leetcode.h"
#include <iostream>
#include <vector>
#include <queue>
#include <list>
#include <unordered_map>

using namespace std;

typedef pair<int, int> PII;
const int MAX_FEED_NUM = 10;

class Twitter {
public:
  /** Initialize your data structure here. */
  Twitter(): post_count(0) {
  }

  /** Compose a new tweet. */
  void postTweet(int userId, int tweetId) {
    user_post_map[userId].push_back({tweetId, post_count});
    ++post_count;
  }

  /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
  vector<int> getNewsFeed(int userId) {
    auto comp = [](const PII& rhs, const PII& lhs) {
      return rhs.second > lhs.second;
    };
    priority_queue<PII, vector<PII>, decltype(comp)> pq(comp);

    auto itr = user_post_map.find(userId);
    if (itr != user_post_map.end()) {
      for (auto& feed : itr->second) {
        pq.push(feed);
        if (pq.size() > MAX_FEED_NUM) {
          pq.pop();
        }
      }
    }

    for (auto& followeeId : connect[userId]) {
      for (auto& feed : user_post_map[followeeId]) {
        pq.push(feed);
        if (pq.size() > MAX_FEED_NUM) {
          pq.pop();
        }
      }
    }

    vector<int> feeds;
    while (!pq.empty()) {
      feeds.push_back(pq.top().first);
      pq.pop();
    }
    reverse(feeds.begin(), feeds.end());
    return feeds;
  }

  /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
  void follow(int followerId, int followeeId) {
    if (followerId == followeeId) {
      return ;
    }
    if (connect.count(followerId) == 0) {
      connect[followerId] = {};
    }

    auto con = connect.find(followerId);
    if (find(con->second.begin(), con->second.end(), followeeId)
          != con->second.end()) {
      return ;
    }
    con->second.push_back(followeeId);
  }

  /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
  void unfollow(int followerId, int followeeId) {
    if (followerId == followeeId) {
      return ;
    }
    auto con = connect.find(followerId);
    if (con == connect.end()) {
      return ;
    }

    auto pos = find(con->second.begin(), con->second.end(), followeeId);
    if (pos == con->second.end()) {
      return ;
    }

    con->second.erase(pos);
  }

private:
  int post_count;
  unordered_map<int, vector<pair<int, int>>> user_post_map;
  unordered_map<int, list<int>> connect;
};

int main() {
  Twitter obj = Twitter();
  obj.postTweet(1, 19);
  obj.postTweet(2, 12);
  obj.follow(1, 2);

  vector<int> feeds;
  feeds = obj.getNewsFeed(1);
  print_vector(feeds);

  obj.unfollow(1, 2);
  feeds = obj.getNewsFeed(1);
  print_vector(feeds);

  return 0;
}
