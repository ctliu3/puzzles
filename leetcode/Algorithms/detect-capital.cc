#include "leetcode.h"

class Solution {
public:
  bool detectCapitalUse(string word) {
    if (word.empty()) {
      return true;
    }
    int n = word.size();
    int a = 0, b = 0;
    for (int i = 0; i < n; ++i) {
      if (islower(word[i])) {
        a += 1;
      }
      if (isupper(word[i])) {
        b += 1;
      }
      if (islower(word[0])) {
        if (b > 0) {
          return false;
        }
      } else {
        if (b > 1 && a > 0) {
          return false;
        }
      }
    }

    return true;
  }
};

int main() {
  return 0;
}
