#include "leetcode.h"

class Solution {
public:
  vector<int> findDiagonalOrder(vector<vector<int>>& matrix) {
    int n = matrix.size();
    if (n == 0) {
      return {};
    }
    int m = matrix[0].size();
    vector<int> ans(n * m, 0);

    int x = 0, y = 0, index = 0;
    int flag = 1, turn = 0;
    for (int k = 0; k < n + m - 1; ++k) {
      int x1 = x, y1 = y, dist = min(y - 0, n - 1 - x);
      int x2 = x + dist, y2 = y - dist;
      if (flag) {
        for (int i = 0; i <= dist; ++i) {
          ans[index++] = matrix[x2 - i][y2 + i];
        }
      } else {
        for (int i = 0; i <= dist; ++i) {
          ans[index++] = matrix[x1 + i][y1 - i];
        }
      }

      if (turn) {
        ++x;
      } else {
        if (y == m - 1) {
          x = 1;
          turn = 1;
        } else {
          ++y;
        }
      }
      flag = 1 - flag;
    }

    return ans;
  }
};

int main() {
  return 0;
}
