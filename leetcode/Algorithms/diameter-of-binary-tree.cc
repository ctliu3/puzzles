#include "leetcode.h"

class Solution {
  int dfs1(TreeNode* cur, int& ans) {
    if (!cur) {
      return 0;
    }
    if (!cur->left && !cur->right) {
      ans = max(ans, 1);
      return 1;
    }
    int l = dfs1(cur->left, ans);
    int r = dfs1(cur->right, ans);
    ans = max(l + r + 1, ans);
    return max(l, r) + 1;
  }

  int diameterOfBinaryTree1(TreeNode* root) {
    if (!root) {
      return 0;
    }
    int ans = 0;
    dfs1(root, ans);
    return ans - 1;
  }

public:
  int diameterOfBinaryTree(TreeNode* root) {
    return diameterOfBinaryTree1(root);
  }
};

int main() {
  return 0;
}
