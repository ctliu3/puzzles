#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
  void ops1(char op, const vector<int>& lnum, const vector<int>& rnum,
            vector<int>& res) {
    for (auto num1 : lnum) {
      for (auto num2 : rnum) {
        switch (op){
        case '+':
          res.push_back(num1 + num2);
          break;
        case '-':
          res.push_back(num1 - num2);
          break;
        case '*':
          res.push_back(num1 * num2);
          break;
        }
      }
    }
  }

  vector<int> diffWaysToCompute1(string input) {
    vector<int> ret;

    int n = (int)input.size();
    bool has_op = false;
    for (int i = 0; i < n; ++i) {
      if (string("+-*").find(input[i]) == string::npos) {
        continue;
      }
      has_op = true;
      vector<int> lnum = diffWaysToCompute1(input.substr(0, i));
      vector<int> rnum = diffWaysToCompute1(input.substr(i + 1));
      ops1(input[i], lnum, rnum, ret);
    }

    if (!has_op) {
      return { atoi(input.c_str()) };
    }
    return ret;
  }

public:
  vector<int> diffWaysToCompute(string input) {
    return diffWaysToCompute1(input);
  }
};

int main() {
  string input = "2*3-4*5";
  Solution sol;
  auto ret = sol.diffWaysToCompute(input);
  for (auto ele : ret) {
    cout << ele << endl;
  }
  return 0;
}
