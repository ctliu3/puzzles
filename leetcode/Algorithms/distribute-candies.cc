#include "leetcode.h"

class Solution {
public:
  int distributeCandies(vector<int>& candies) {
    if (candies.empty()) {
      return 0;
    }
    map<int, int> counter;
    for (int i = 0; i < (int)candies.size(); ++i) {
      counter[candies[i]] += 1;
    }
    int n = (int)candies.size();
    return min(n / 2, (int)counter.size());
  }
};

int main() {
  return 0;
}
