#include <iostream>
#include <vector>
#include <queue>
#include <map>

using namespace std;

class Solution {
public:
  int calculateMinimumHP(vector<vector<int>>& dungeon) {
    int n = dungeon.size(), m = dungeon[0].size();
    vector<vector<int>> dp(n, vector<int>(m, -1));
    const vector<int> dx = {1, 0};
    const vector<int> dy = {0, 1};

    for (int i = n - 1; i >= 0; --i) {
      for (int j = m - 1; j >= 0; --j) {
        if (i == n - 1 and j == m - 1) {
          dp[i][j] = dungeon[i][j] >= 0 ? 1 : -dungeon[i][j] + 1;
          continue;
        }
        for (int k = 0; k < 2; ++k) {
          int ni = i + dx[k];
          int nj = j + dy[k];
          if (ni < n && nj < m) {
            int hp = dungeon[i][j] - dp[ni][nj];
            hp = hp >= 0 ? 1 : -hp;
            if (dp[i][j] == -1 || hp < dp[i][j]) {
              dp[i][j] = hp;
            }
          }
        }
      }
    }

    return dp[0][0];
  }
};

int main() {
  vector<vector<int>> dungeon = {{0, 0, 0}, {1, 1, -1}};
  Solution sol;

  int res = sol.calculateMinimumHP(dungeon);
  cout << res << endl;

  return 0;
}
