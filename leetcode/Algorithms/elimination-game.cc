#include "leetcode.h"

class Solution {
public:
  int lastRemaining(int n) {
    if (n == 1) {
      return 1;
    }
    int m = n, count = 0;
    while (m != 1) {
      m -= m / 2;
      count += 1;
    }
    if (n & 1) {
      return n - count;
    }
    return count + 1;
  }
};
