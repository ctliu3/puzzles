#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class Codec {
public:
  // Encodes a list of strings to a single string.
  string encode(vector<string>& strs) {
    ostringstream out;
    for (auto& str : strs) {
      out << str.size();
      out << ',';
      out << str;
    }
    return out.str();
  }

  // Decodes a single string to a list of strings.
  vector<string> decode(string s) {
    vector<string> ret;
    int i = 0, n = (int)s.size();
    while (i < n) {
      int j = i, num = 0;
      while (j < n && s[j] != ',') {
        num = num * 10 + s[j] - '0';
        ++j;
      }
      ++j;
      string cur;
      for (int k = 0; k < num; ++k) {
        cur.push_back(s[j + k]);
      }
      i = j + num;
      ret.push_back(cur);
    }
    return ret;
  }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.decode(codec.encode(strs));

int main() {
  vector<string> strs = {"0"};
  Codec codec;
  codec.decode(codec.encode(strs));

  return 0;
}
