#include "leetcode.h"

class Solution {
  unordered_map<string, string> encode_mp;
  unordered_map<string, string> decode_mp;
  const string prefix = "http://tinyurl.com/";
public:

  // Encodes a URL to a shortened URL.
  string encode(string longUrl) {
    const auto itr = encode_mp.find(longUrl);
    if (itr == encode_mp.end()) {
      int size = (int)encode_mp.size();
      string str = to_string(size);
      encode_mp[longUrl] = str;
      decode_mp[str] = longUrl;
      return str;
    }
    return prefix + itr->second;
  }

  // Decodes a shortened URL to its original URL.
  string decode(string shortUrl) {
    return decode_mp[shortUrl];
  }
};

// Your Solution object will be instantiated and called as such:
// Solution solution;
// solution.decode(solution.encode(url));

int main() {
  return 0;
}
