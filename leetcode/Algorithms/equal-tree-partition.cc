#include "leetcode.h"

class Solution {
  int getSum1(TreeNode* cur) {
    if (!cur) {
      return 0;
    }
    return cur->val + getSum1(cur->left) + getSum1(cur->right);;
  }

  int partition(TreeNode* cur, int sum, bool& flag) {
    if (!cur) {
      return 0;
    }
    if (!cur->left && !cur->right) {
      return cur->val;
    }
    if (flag) {
      return -1;
    }

    int cur_sum = cur->val, lsum = 0, rsum = 0;
    if (cur->left) {
      lsum = partition(cur->left, sum, flag);
      cur_sum += lsum;
      if (lsum * 2 == sum) {
        flag = true;
        return -1;
      }
    }

    if (cur->right) {
      rsum = partition(cur->right, sum, flag);
      cur_sum += rsum;
      if (rsum * 2 == sum) {
        flag = true;
        return -1;
      }
    }
    return cur_sum;
  }

  bool checkEqualTree1(TreeNode* root) {
    int sum = getSum1(root);
    bool flag = false;
    partition(root, sum, flag);
    return flag;
  }

public:
  bool checkEqualTree(TreeNode* root) {
    return checkEqualTree1(root);
  }
};

int main() {
  return 0;
}
