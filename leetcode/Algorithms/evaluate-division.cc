#include "leetcode.h"

const double EPS = 1e-6;

bool is_equal(double a, double b) {
  return fabs(a - b) < EPS;
}

class Solution {
  unordered_map<string, vector<pair<string, double>>> adj;

  double calc(pair<string, string> query) {
    if (!adj.count(query.first)) {
      return -1.0;
    }
    if (query.first == query.second) {
      return 1;
    }

    queue<pair<string, double>> q;
    q.push({query.first, 1});
    set<string> visit;

    while (!q.empty()) {
      string cur = q.front().first;
      double val = q.front().second;
      q.pop();
      visit.insert(cur);
      if (cur == query.second) {
        return val;
      }
      for (auto& v : adj[cur]) {
        if (!visit.count(v.first)) {
          q.push({v.first, val * v.second});
        }
      }
    }

    return -1.0;
  }

public:
  vector<double> calcEquation(vector<pair<string, string>> equations, vector<double>& values, vector<pair<string, string>> queries) {
    if (queries.empty()) {
      return {};
    }

    for (int i = 0; i < (int)equations.size(); ++i) {
      if (equations[i].first == equations[i].second) {
        continue;
      }
      adj[equations[i].first].push_back({equations[i].second, values[i]});
      adj[equations[i].second].push_back({equations[i].first, 1 / values[i]});
    }

    vector<double> ret;
    for (auto& query : queries) {
      ret.push_back(calc(query));
    }
    return ret;
  }
};
