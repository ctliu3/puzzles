#include <iostream>
#include <string>

using namespace std;

class Solution {
  int titleToNumber1(string s) {
    int base = 1;
    int res = 0;
    for (int i = s.size() - 1; i >= 0; --i) {
      res += base * (s[i] - 'A' + 1);
      base *= 26;
    }
    return res;
  }

  int titleToNumber2(string s) {
    int res = 0;
    for (char& c : s) {
      res = res * 26 +(c - 'A' + 1);
    }
    return res;
  }

public:
  int titleToNumber(string s) {
    //return titleToNumber1(s);
    return titleToNumber2(s);
  }
};

int main() {
  return 0;
}
