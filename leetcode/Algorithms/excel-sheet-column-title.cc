#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  string convertToTitle(int n) {
    string res;
    --n;
    int base = 26;
    while (n >= 0) {
      res.push_back(n % base + 'A');
      n /= base;
      --n;
    }
    reverse(res.begin(), res.end());
    return res;
  }
};

int main() {
  Solution sol;
  string res = sol.convertToTitle(52);
  cout << res << endl;
  return 0;
}
