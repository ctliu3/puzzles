#include <iostream>
#include <vector>
#include <string>

using namespace std;

typedef long long ll;

class Solution {
  // pre_op: preous operator
  // pre_val: preous value
  // pos: current position
  // val: current value
  void dfs(const string& num, int target, char pre_op, ll pre_val, int pos,
           ll val, string expr, vector<string>& ret) {
    if (pos == (int)num.size()) {
      if (val == target) {
        ret.push_back(expr);
      }
      return ;
    }

    for (int i = pos; i < (int)num.size(); ++i) {
      string sub = num.substr(pos, i - pos + 1);
      if (sub.size() > 1 && sub[0] == '0') {
        break;
      }
      ll cur = stol(sub);

      dfs(num, target, '+', cur, i + 1, val + cur, expr + '+' + sub, ret);
      dfs(num, target, '-', cur, i + 1, val - cur, expr + '-' + sub, ret);
      ll next = pre_val * cur;
      if (pre_op == '-') {
        next = val + pre_val - pre_val * cur;
      } else if (pre_op == '+') {
        next = val - pre_val + pre_val * cur;
      }
      dfs(num, target, pre_op, pre_val * cur, i + 1, next, expr + '*' + sub,
          ret);
    }
  }

public:
  vector<string> addOperators(string num, int target) {
    if (num.empty()) {
      return {};
    }
    vector<string> ret;
    ll cur = 0;
    for (int i = 0; i < (int)num.size(); ++i) {
      cur = cur * 10 + num[i] - '0';
      if (i >= 1 && num[0] == '0') {
        break;
      }
      dfs(num, target, ' ', cur, i + 1, cur, num.substr(0, i + 1), ret);
    }
    return ret;
  }
};

int main() {
  Solution sol;
  vector<string> ret = sol.addOperators("3456237490", 9191);
  for (auto& expr : ret) {
    cout << expr << endl;
  }
  return 0;
}
