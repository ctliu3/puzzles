#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;

class Solution {
  void dfs(int n, int cur, vector<int>& comb, vector<vector<int>>& ret) {
    if (!comb.empty()) {
      ret.push_back(comb);
      ret.back().push_back(n);
    }

    for (int i = cur; (ll) i * i <= n; ++i) {
      if (n % i == 0) {
        comb.push_back(i);
        dfs(n / i, i, comb, ret);
        comb.pop_back();
      }
    }
  }

public:
  vector<vector<int>> getFactors(int n) {
    if (n <= 2) {
      return {};
    }
    vector<vector<int>> ret;
    vector<int> comb;
    dfs(n, 2, comb, ret);

    return ret;
  }
};

int main() {
  Solution sol;
  auto ret = sol.getFactors(12);
  for (auto& ele : ret) {
    for (auto& val : ele) {
      cout << val << ' ';
    }
    cout << endl;
  }
  return 0;
}
