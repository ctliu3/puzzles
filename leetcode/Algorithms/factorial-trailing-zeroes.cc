#include <iostream>

using namespace std;

class Solution {
public:
  int trailingZeroes(int n) {
    if (!n) {
      return 0;
    }
    long long base = 5;
    int res = 0;
    while (base <= n) {
      res += n / base;
      base *= 5;
    }
    return res;
  }
};

int main() {
  return 0;
}
