class Solution {
  vector<int> findAnagrams1(string s, string p) {
    if (p.size() > s.size()) {
      return {};
    }
    vector<int> hash(26, 0);
    for (auto c : p) {
      ++hash[c - 'a'];
    }

    vector<int> ret;
    vector<int> count(26, 0);
    for (int i = 0; i < (int)s.size(); ++i) {
      if (i < p.size() - 1) {
        ++count[s[i] - 'a'];
      } else {
        ++count[s[i] - 'a'];
        if (hash == count) {
          ret.push_back(i - p.size() + 1);
        }
        --count[s[i - p.size() + 1] - 'a'];
      }
    }
    return ret;
  }

public:
  vector<int> findAnagrams(string s, string p) {
    return findAnagrams1(s, p);
  }
};
