#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<int> findDuplicates1(vector<int>& nums) {
    if (nums.size() < 2) {
      return {};
    }
    vector<int> ret;
    for (int i = 0; i < (int)nums.size(); ) {
      if (nums[i] == 0) {
        ++i;
        continue;
      }
      if (nums[i] - 1 != i && nums[nums[i] - 1] == nums[i]) {
        ret.push_back(nums[i]);
        nums[nums[i] - 1] = nums[i] = 0;
        ++i;
      } else {
        if (nums[nums[i] - 1] != nums[i]) {
          swap(nums[i], nums[nums[i] - 1]);
        } else {
          ++i;
        }
      }
    }
    return ret;
  }

public:
  vector<int> findDuplicates(vector<int>& nums) {
    return findDuplicates1(nums);
  }
};

int main() {
  vector<int> nums = {4,3,2,7,8,2,3,1};
  Solution sol;
  auto ret = sol.findDuplicates(nums);

  for (auto val : ret) {
    cout << val << ' ';
  }
  cout << endl;
  return 0;
}
