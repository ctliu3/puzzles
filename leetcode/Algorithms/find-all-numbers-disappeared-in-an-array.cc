class Solution {
  vector<int> findDisappearedNumbers1(vector<int>& nums) {
    if (nums.empty()) {
      return {};
    }

    int n = (int)nums.size();
    for (int i = 0; i < n; ) {
      if (nums[i] - 1 != i && nums[nums[i] - 1] != nums[i]) {
        swap(nums[i], nums[nums[i] - 1]);
      } else {
        ++i;
      }
    }
    vector<int> ret;
    for (int i = 0; i < n; ++i) {
      if (nums[i] - 1 != i) {
        ret.push_back(i + 1);
      }
    }
    return ret;
  }

public:
  vector<int> findDisappearedNumbers(vector<int>& nums) {
    return findDisappearedNumbers1(nums);
  }
};
