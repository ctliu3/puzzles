#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, int dep, int& maxdep, int& maxval) {
    if (!cur) {
      return ;
    }
    if (dep > maxdep) {
      maxdep = dep;
      maxval = cur->val;
    }
    dfs1(cur->left, dep + 1, maxdep, maxval);
    dfs1(cur->right, dep + 1, maxdep, maxval);
  }

  int findBottomLeftValue1(TreeNode* cur) {
    int maxdep = 0, maxval = cur->val;
    dfs1(cur, 0, maxdep, maxval);
    return maxval;
  }

public:
  int findBottomLeftValue(TreeNode* root) {
    return findBottomLeftValue1(root);
  }
};

int main() {
  return 0;
}
