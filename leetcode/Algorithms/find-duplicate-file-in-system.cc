#include "leetcode.h"

class Solution {

  vector<vector<string>> findDuplicate1(vector<string>& paths) {
    if (paths.empty()) {
      return {};
    }

    unordered_map<string, vector<string>> mp;
    for (auto& path : paths) {
      int len = (int)path.size();
      int p1 = path.find(' ');
      string prefix = path.substr(0, p1);
      while (p1 < len) {
        int p2 = path.find(' ', p1 + 1);
        int p3 = path.find('(', p1 + 1);
        int p4 = path.find(')', p3 + 1);
        string content = path.substr(p3 + 1, p4 - p3 - 1);
        if (!mp.count(content)) {
          mp[content] = {};
        }
        mp[content].push_back(prefix + "/" + path.substr(p1 + 1, p3 - (p1 + 1)));
        p1 = p4 + 1;
      }
    }

    vector<vector<string>> ans;
    for (auto& p : mp) {
      if (p.second.size() > 1) {
        ans.push_back(p.second);
      }
    }

    return ans;
  }

public:
  vector<vector<string>> findDuplicate(vector<string>& paths) {
    return findDuplicate1(paths);
  }
};
