#include "leetcode.h"

class Solution {
  struct NodeInfo {
    int sum;
    int node_num;
    TreeNode* node;
    explicit NodeInfo(int sum = 0, int node_num = 0, TreeNode* node = nullptr)
      : sum(sum), node_num(node_num), node(node) {
    }
  };

  NodeInfo getNodeInfo1(TreeNode* cur, unordered_map<int, vector<NodeInfo> >& hash) {
    if (!cur) {
      return NodeInfo();
    }

    NodeInfo info;
    NodeInfo l = getNodeInfo1(cur->left, hash);
    NodeInfo r = getNodeInfo1(cur->right, hash);
    info.sum = cur->val + l.sum + r.sum;
    info.node_num = l.node_num + r.node_num + 1;
    info.node = cur;
    hash[cur->val].push_back(info);
    return info;
  }

  bool isSame(TreeNode* ltree, TreeNode* rtree) {
    if (!ltree && !rtree) {
      return true;
    }
    if (ltree && !rtree) {
      return false;
    }
    if (!ltree && rtree) {
      return false;
    }
    if (ltree && rtree && ltree->val != rtree->val) {
      return false;
    }
    return isSame(ltree->left, rtree->left) and isSame(ltree->right, rtree->right);
  }

  void dfs1(TreeNode* cur, unordered_map<int, vector<NodeInfo> >& hash, set<TreeNode*>& node_set, vector<TreeNode*>& ans) {
    if (!cur) {
      return ;
    }
    if (node_set.find(cur) != node_set.end()) {
      return ;
    }

    auto& candidates = hash[cur->val];
    NodeInfo cur_info;
    for (auto& info : candidates) {
      if (info.node == cur) {
        cur_info = info;
        break;
      }
    }

    bool flag = false;
    for (auto& info : candidates) {
      if (node_set.find(info.node) != node_set.end()) {
        continue;
      }
      if (info.node != cur_info.node && info.sum == cur_info.sum && info.node_num == cur_info.node_num) {
        if (isSame(cur, info.node)) {
          flag = true;
          node_set.insert(info.node);
        }
      }
    }
    if (flag) {
      ans.push_back(cur);
    }

    dfs1(cur->left, hash, node_set, ans);
    dfs1(cur->right, hash, node_set, ans);
  }

  vector<TreeNode*> findDuplicateSubtrees1(TreeNode* root) {
    unordered_map<int, vector<NodeInfo> > hash;
    getNodeInfo1(root, hash);

    set<TreeNode* > node_set;
    vector<TreeNode* > ans;
    dfs1(root, hash, node_set, ans);

    return ans;
  }

public:
  vector<TreeNode*> findDuplicateSubtrees(TreeNode* root) {
    return findDuplicateSubtrees1(root);
  }
};

int main() {
  return 0;
}
