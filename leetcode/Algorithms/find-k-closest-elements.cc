#include "leetcode.h"

class Solution {
  vector<int> findClosestElements1(vector<int>& arr, int k, int x) {
    if (arr.empty()) {
      return {};
    }

    queue<int> que;
    stack<int> stk;
    int n = (int)arr.size();
    int pos = lower_bound(arr.begin(), arr.end(), x) - arr.begin();
    int st = pos - 1, ed = pos;
    while (k--) {
      if (st >= 0 && ed < n) {
        if (x - arr[st] <= arr[ed] - x) {
          stk.push(arr[st]);
          --st;
        } else {
          que.push(arr[ed]);
          ++ed;
        }
      } else if (st >= 0) {
        stk.push(arr[st--]);
      } else if (ed < n) {
        que.push(arr[ed++]);
      } else {
        break;
      }
    }

    vector<int> ans;
    while (!stk.empty()) {
      ans.push_back(stk.top());
      stk.pop();
    }
    while (!que.empty()) {
      ans.push_back(que.front());
      que.pop();
    }
    return ans;
  }

public:
  vector<int> findClosestElements(vector<int>& arr, int k, int x) {
    return findClosestElements1(arr, k, x);
  }
};

int main() {
  return 0;
}
