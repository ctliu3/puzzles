#include <vector>
#include <queue>
#include <iostream>

using namespace std;

int f(int a, int b) {
  return - a - b;
}

class Solution {
  vector<pair<int, int>> kSmallestPairs1(vector<int>& nums1, vector<int>& nums2, int k) {
    int n1 = (int)nums1.size();
    int n2 = (int)nums2.size();
    if (n1 == 0 || n2 == 0) {
      return {};
    }

    vector<pair<int, int>> ret;
    priority_queue<pair<int, pair<int, int>>> pq;

    for (int i = 0; i < n2; ++i) {
      pq.push({f(nums2[i], nums1[0]), {nums1[0], nums2[i]}});
    }
    for (int i = 1; i < n1; ++i) {
      pq.push({f(nums1[i], nums2[0]), {nums1[i], nums2[0]}});
    }
    int i = 1, j = 1;
    while (!pq.empty()) {
      auto cur = pq.top();
      pq.pop();
      ret.push_back(cur.second);
      if ((int)ret.size() == k) {
        break;
      }

      auto& next = pq.top();
      if (i < n1 && j < n2) {
        if (f(nums1[i], nums2[j]) < next.first || k - pq.size() > 0) { // !!
          for (int o = j; o < n2; ++o) {
            pq.push({f(nums2[o], nums1[i]), {nums1[i], nums2[o]}});
          }
          ++i;
          for (int o = i; o < n1; ++o) {
            pq.push({f(nums1[o], nums2[j]), {nums1[o], nums2[j]}});
          }
          ++j;
        }
      }
    }
    return ret;
  }

public:
  vector<pair<int, int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k) {
    return kSmallestPairs1(nums1, nums2, k);
  }
};

int main() {
  vector<int> nums1 = {1,7,11};
  vector<int> nums2 = {2,4,6};
  Solution sol;
  auto ret = sol.kSmallestPairs(nums1, nums2, 3);
  for (auto& item : ret) {
    cout << item.first << ' ' << item.second << endl;
  }
}
