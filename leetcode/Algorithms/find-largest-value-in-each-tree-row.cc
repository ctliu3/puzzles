#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, vector<int>& level_max, int dep) {
    if (!cur) {
      return ;
    }
    if (dep >= level_max.size()) {
      level_max.push_back(cur->val);
    } else {
      level_max[dep] = max(level_max[dep], cur->val);
    }
    dfs1(cur->left, level_max, dep + 1);
    dfs1(cur->right, level_max, dep + 1);
  }

  vector<int> largestValues1(TreeNode* root) {
    vector<int> ret;
    dfs1(root, ret, 0);
    return ret;
  }

public:
  vector<int> largestValues(TreeNode* root) {
    return largestValues1(root);
  }
};

int main() {
  return 0;
}
