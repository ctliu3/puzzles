#include "leetcode.h"

class Solution {
  int dfs1(TreeNode* cur, vector<vector<int>>& ans) {
    if (!cur) {
      return 0;
    }

    int l = dfs1(cur->left, ans);
    int r = dfs1(cur->right, ans);
    int dep = max(l, r);
    if (dep >= ans.size()) {
      ans.resize(dep + 1);
    }
    ans[dep].push_back(cur->val);
    return dep + 1;
  }

  vector<vector<int>> findLeaves1(TreeNode* root) {
    if (!root) {
      return {};
    }
    vector<vector<int>> ans;
    dfs1(root, ans);
    return ans;
  }

public:
  vector<vector<int>> findLeaves(TreeNode* root) {
    return findLeaves1(root);
  }
};

int main() {
  return 0;
}
