#include <iostream>
#include <queue>

using namespace std;

class MedianFinder {
  priority_queue<int, vector<int>, greater<int>> min_que;
  priority_queue<int, vector<int>, less<int>> max_que;

public:

  // Adds a number into the data structure.
  void addNum(int num) {
    if (min_que.size() > max_que.size()) {
      max_que.push(min_que.top());
      min_que.pop();
    }
    max_que.push(num);
    if (max_que.size() - min_que.size() > 1
      || (max_que.size() - min_que.size() == 1 && min_que.size() > 0
        && max_que.top() > min_que.top())) {
      min_que.push(max_que.top());
      max_que.pop();
    }
  }

  // Returns the median of current data stream
  double findMedian() {
    if (min_que.size() == max_que.size()) {
      return (min_que.top() + max_que.top()) / 2.0;
    } else if (max_que.size() > min_que.size()) {
      return max_que.top();
    } else {
      return min_que.top();
    }
  }
};

int main() {
  MedianFinder finder;
  finder.addNum(1);
  cout << finder.findMedian() << endl;
  finder.addNum(2);
  cout << finder.findMedian() << endl;
  finder.addNum(3);
  cout << finder.findMedian() << endl;
  finder.addNum(4);
  cout << finder.findMedian() << endl;
  finder.addNum(5);
  cout << finder.findMedian() << endl;

  return 0;
}

// Your MedianFinder object will be instantiated and called as such:
// MedianFinder mf;
// mf.addNum(1);
// mf.findMedian();
