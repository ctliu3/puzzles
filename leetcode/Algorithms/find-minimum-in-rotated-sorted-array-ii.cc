// Author:     ct.Liu, lcndn3@gmail.com
// Date:       Oct 27, 2014
// Update:     Oct 27, 2014
//
// Find Minimum in Rotated Sorted Array
// Suppose a sorted array is rotated at some pivot unknown to you beforehand.
//
// (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
//
// Find the minimum element.
//
// The array may contain duplicates.

#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

class Solution {
public:
  int findMin(vector<int> &nums) {
    assert(!nums.empty());

    int start = 0, end = (int)nums.size() - 1;
    int res = nums[0];

    while (start <= end) {
      int mid = start + (end - start) / 2;

      if (nums[start] == nums[mid]) {
        res = min(res, nums[start]);
        while (nums[start] == nums[mid]) {
          ++start;
        }
      } else if (nums[start] < nums[mid]) {
        res = min(res, nums[start]);
        start = mid + 1;
      } else {
        res = min(res, nums[mid]);
        end = mid - 1;
      }
    }
    return res;
  }
};

int main() {
  vector<int> num = {2, 0, 1, 2, 2, 2, 2};
  Solution sol;

  cout << sol.findMin(num) << endl;

  return 0;
}

