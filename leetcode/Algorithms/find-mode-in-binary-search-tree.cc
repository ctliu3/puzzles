#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, int& prev, int& count, int& max_count, vector<int>& ans) {
    if (!cur) {
      return ;
    }

    dfs1(cur->left, prev, count, max_count, ans);

    if (count == 0) {
      count = 1;
    } else if (prev == cur->val) {
      count += 1;
    } else { // prev != cur->val
      if (count > max_count) {
        max_count = count;
        ans = { prev };
      } else if (count == max_count) {
        ans.push_back(prev);
      }
      count = 1;
    }
    prev = cur->val;

    dfs1(cur->right, prev, count, max_count, ans);
  }

  vector<int> findMode1(TreeNode* root) {
    if (!root) {
      return {};
    }
    vector<int> ans;
    int prev = 0, count = 0, max_count = 0;
    dfs1(root, prev, count, max_count, ans);
    if (max_count == count) {
      ans.push_back(prev);
    } else if (count > max_count) {
      ans = { prev };
    }
    return ans;
  }

public:
  vector<int> findMode(TreeNode* root) {
    return findMode1(root);
  }
};

int main() {
  return 0;
}
