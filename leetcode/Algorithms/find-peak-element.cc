#include "leetcode.h"

class Solution {
public:
  int findPeakElement(const vector<int> &num) {
    int n = (int)num.size();
    if (n < 2) {
      return n - 1;
    }
    if (num[0] > num[1]) {
      return 0;
    }
    if (num[n - 1] > num[n - 2]) {
      return n - 1;
    }

    int start = 1, end = n - 2;
    while (start <= end) {
      int mid = start + (end - start) / 2;
      if (num[mid] > num[mid - 1] && num[mid] > num[mid + 1]) {
        return mid;
      }
      if (num[mid - 1] > num[mid]) {
        end = mid - 1;
      } else {
        start = mid + 1;
      }
    }

    return start;
  }
};

int main() {
  vector<int> num = {1, 2, 3, 2, 1, 6, 2, 1};
  Solution sol;
  int res = sol.findPeakElement(num);
  printf("res = %d\n", res);
  return 0;
}
