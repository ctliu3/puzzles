#include <iostream>
#include <vector>

using namespace std;

// Forward declaration of the knows API.
bool knows(int a, int b);

class Solution {
public:
  int findCelebrity(int n) {
    int candicate = 0;
    for (int i = 1; i < n; ++i) {
      if (!knows(i, candicate)) {
        candicate = i;
      }
    }

    for (int i = 0; i < n; ++i) {
      if (i == candicate) {
        continue;
      }
      if (!knows(i, candicate) || knows(candicate, i)) {
        return -1;
      }
    }

    return candicate;
  }
};

int main() {
  return 0;
}
