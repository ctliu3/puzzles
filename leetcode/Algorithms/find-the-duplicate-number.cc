#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int findDuplicate1(vector<int>& nums) {
    int i = 0;
    while (i < (int)nums.size()) {
      if (nums[i] < 0) {
        ++i;
      } else {
        if (nums[nums[i] - 1] < 0) {
          --nums[nums[i] - 1];
          ++i;
        } else {
          int pos = nums[i] - 1;
          swap(nums[i], nums[pos]);
          nums[pos] = -nums[pos];
        }
      }
    }

    int ans = -1;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (nums[i] < 0 && nums[i] < -(i + 1)) {
        ans = i + 1;
        break;
      }
    }
    return ans;
  }

  int findDuplicate2(vector<int>& nums) {
    int start = 1, end = nums.size() - 1;
    while (start < end) { // 
      int mid = start + (end - start) / 2;
      int count = 0;
      for (int num : nums) {
        if (num <= mid) {
          count += 1;
        }
      }
      if (count > mid) {
        end = mid;
      } else {
        start = mid + 1;
      }
    }
    return start;
  }

public:
  int findDuplicate(vector<int>& nums) {
    // return findDuplicate1(nums);
    return findDuplicate2(nums);
  }
};

int main() {
  vector<int> nums = {1, 3, 2, 8, 1};
  Solution sol;
  int ret = sol.findDuplicate(nums);
  cout << ret << endl;
  return 0;
}
