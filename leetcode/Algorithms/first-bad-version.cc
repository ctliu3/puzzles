#include <iostream>

// Forward declaration of isBadVersion API.
bool isBadVersion(int version);

class Solution {
  int firstBadVersion1(int n) {
    if (n == 1) {
      return n;
    }
    long long start = 0, end = (long long)n + 1;

    while (end - start > 1) {
      long long mid = start + (end - start) / 2;
      if (isBadVersion(mid)) {
        end = mid;
      } else {
        start = mid;
      }
    }

    return end;
  }

public:
  int firstBadVersion(int n) {
    return firstBadVersion1(n);
  }
};

int main() {
}
