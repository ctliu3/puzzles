#include <vector>

using namespace std;

class Vector2D {
  int i_;
  int j_;
  int val_;
  vector<vector<int>> vec2d_;

public:
  Vector2D(vector<vector<int>>& vec2d) {
    i_ = 0;
    j_ = -1;
    vec2d_ = vec2d;
  }

  int next() {
    return val_;
  }

  bool hasNext() {
    ++j_;
    while (i_ < vec2d_.size()) {
      if (i_ < vec2d_.size() && j_ < vec2d_[i_].size()) {
        val_ = vec2d_[i_][j_];
        return true;
      }
      if (j_ == vec2d_[i_].size()) {
        ++i_;
        j_ = 0;
      } else {
        ++j_;
      }
    }
    return false;
  }
};

int main() {
  return 0;
}

/**
 * Your Vector2D object will be instantiated and called as such:
 * Vector2D i(vec2d);
 * while (i.hasNext()) cout << i.next();
 */
