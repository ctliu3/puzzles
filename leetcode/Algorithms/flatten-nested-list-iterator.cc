#include "leetcode.h"

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds, if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds, if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */

class NestedIterator {
  stack<NestedInteger> stk;
  bool has_next;
  int val;

public:
  NestedIterator(vector<NestedInteger> &nestedList) {
    while (!stk.empty()) {
      stk.pop();
    }
    has_next = false;

    int n = (int)nestedList.size();
    for (int i = n - 1; i >= 0; --i) {
      stk.push(nestedList[i]);
    }
  }

  int next() {
    return val;
  }

  bool hasNext() {
    if (stk.empty()) {
      has_next = false;
    } else {
      auto cur = stk.top();
      while (!cur.isInteger()) {
        auto list = cur.getList();
        stk.pop();
        for (int i = (int)list.size() - 1; i >= 0; --i) {
          stk.push(list[i]);
        }
        if (!stk.empty()) {
          cur = stk.top();
        } else {
          break;
        }
      }
      if (!stk.empty()) {
        val = cur.getInteger();
        stk.pop();
        has_next = true;
      } else {
        has_next = false;
      }
    }

    return has_next;
  }
};

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) cout << i.next();
 */

int main() {
  return 0;
}
