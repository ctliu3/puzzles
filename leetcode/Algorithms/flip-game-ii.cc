#include <string>
#include <iostream>

using namespace std;

class Solution {
public:
  bool canWin(string s) {
    if ((int)s.size() < 2) {
      return false;
    }

    for (int i = 0; i < (int)s.size() - 1; ++i) {
      if (s[i] == '+' and s[i + 1] == '+') {
        s[i] = s[i + 1] = '-';
        if (!canWin(s)) {
          return true;
        }
        s[i] = s[i + 1] = '+';
      }
    }

    return false;
  }
};

int main() {
}
