class Solution {
public:
  vector<string> generatePossibleNextMoves(string s) {
    if (s.size() < 2) {
      return {};
    }

    vector<string> ret;

    for (int i = 0; i + 1 < (int)s.size(); ++i) {
      if (s[i] == '+' and s[i + 1] == '+') {
        string sub(s);
        sub[i] = sub[i + 1] = '-';
        ret.push_back(sub);
      }
    }

    return ret;
  }
};
