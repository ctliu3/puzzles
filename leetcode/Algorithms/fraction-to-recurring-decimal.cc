#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
  string fractionToDecimalLL(long long numerator, long long denominator) {
    string ans;
    bool flag = true;
    if ((numerator > 0 && denominator < 0) || (numerator < 0 && denominator > 0)) {
      flag = false;
      ans.push_back('-');
    }
    if (numerator < 0) {
      numerator = -numerator;
    }
    if (denominator < 0) {
      denominator = - denominator;
    }

    ans.append(to_string(numerator / denominator));

    string rest;
    vector<long long> res;
    long long cur = numerator % denominator;

    while (cur) {
      int pos = find(res.begin(), res.end(), cur) - res.begin();
      if (pos != (int)res.size()) {
        rest.insert(pos, 1, '(');
        rest.push_back(')');
        break;
      }
      res.push_back(cur);

      cur *= 10;
      long long ret = cur / denominator;
      rest.push_back(ret + '0');
      cur = cur % denominator;
    }

    if (!rest.empty()) {
      ans.push_back('.');
      ans.append(rest);
    }

    return ans;
  }

public:
  string fractionToDecimal(int numerator, int denominator) {
    return fractionToDecimalLL((long long)numerator, (long long)denominator);
  }
};

int main() {
  Solution sol;
  string res = sol.fractionToDecimal(-1, -2147483648);
  cout << res << endl;
  return 0;
}
