#include "leetcode.h"

class Solution {
  void dfs1(const vector<vector<int>>& M, vector<bool>& visited, int u) {
    visited[u] = true;
    for (int i = 0; i < M[u].size(); ++i) {
      if (M[u][i] && !visited[i]) {
        dfs1(M, visited, i);
      }
    }
  }

  int findCircleNum1(vector<vector<int>>& M) {
    int n = M.size();
    if (n == 0) {
      return 0;
    }
    vector<bool> visited(n, false);
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      if (!visited[i]) {
        dfs1(M, visited, i);
        ++ans;
      }
    }
    return ans;
  }

public:
  int findCircleNum(vector<vector<int>>& M) {
    return findCircleNum1(M);
  }
};

int main() {
  return 0;
}
