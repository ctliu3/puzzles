#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Solution {
  bool dfs(unordered_set<int>& stone_set, unordered_map<int, unordered_map<int, bool>>& dp, int pos, int k) {
    if (k < 0 || (pos > 0 && k == 0)) {
      return false;
    }
    auto pitr = dp.find(pos);
    if (pitr != dp.end()) {
      auto kitr = pitr->second.find(k);
      if (kitr != pitr->second.end()) {
        return kitr->second;
      }
    }
    if (!stone_set.count(pos)) {
      return false;
    }

    int prev = pos - k;
    if (stone_set.count(prev)) {
      for (int delta = -1; delta <= 1; ++delta) {
        bool ret = dfs(stone_set, dp, prev, k + delta);
        if (ret) {
          return dp[pos][k] = true;
        }
      }
    }

    return dp[pos][k] = false;
  }

public:
  bool canCross(vector<int>& stones) {
    if (stones.empty()) {
      return false;
    }

    unordered_set<int> stone_set(stones.begin(), stones.end());
    for (int i = 0; i <= (int)stones.size(); ++i) {
      unordered_map<int, unordered_map<int, bool>> dp;
      dp[0][0] = true;
      dp[1][1] = true;
      if (dfs(stone_set, dp, stones.back(), i)) {
        return true;
      }
    }
    return false;
  }
};

int main() {
  Solution sol;
  vector<int> stones = {0,1,3,5,6,8,12,17};
  bool ret = sol.canCross(stones);
  cout << ret << endl;
}
