#include <iostream>
#include <vector>

using namespace std;

const vector<int> dx = {0, 0, 1, -1, -1, -1, 1, 1};
const vector<int> dy = {-1, 1, 0, 0, -1, 1, -1, 1};

class Solution {
  bool inBoard(int x, int y, int n, int m) {
    return x >=0 && x < n && y >=0 && y < m;
  }

public:
  void gameOfLife(vector<vector<int>>& board) {
    int n = (int)board.size(), m = (int)board[0].size();

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        board[i][j] = board[i][j] ? 2 : 0;
      }
    }

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        int nlive = 0;
        for (int k = 0; k < 8; ++k) {
          int x = i + dx[k], y = j + dy[k];
          if (inBoard(x, y, n, m)) {
            nlive += board[x][y] & 2 ? 1 : 0;
          }
        }
        if ((board[i][j] & 2) && (nlive == 2 || nlive == 3)) {
          board[i][j] |= 1;
        }
        if (!board[i][j] && nlive == 3) {
          board[i][j] |= 1;
        }
      }
    }

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        board[i][j] = board[i][j] & 1 ? 1 : 0;
      }
    }
  }
};

int main() {
  return 0;
}
