#include <string>
#include <vector>

using namespace std;

class Solution {
  void dfs(const string& word, int pos, vector<string>& ret, vector<string>& prev) {
    if (pos == (int)word.size()) {
      ret = prev;
      return ;
    }

    ret.clear();
    for (auto& sub : prev) {
      ret.push_back(sub);
      ret.back().push_back(word[pos]);
      if (!isdigit(sub.back())) {
        ret.push_back(sub);
        ret.back().push_back('1');
      } else {
        int st = (int)sub.size() - 1;
        while (st >= 0 && isdigit(sub[st])) {
          --st;
        }
        int num = stoi(sub.substr(st + 1));
        ret.push_back(sub.substr(0, st + 1) + to_string(num + 1));
      }
    }
    prev = ret;

    dfs(word, pos + 1, ret, prev);
  }

public:
  vector<string> generateAbbreviations(string word) {
    if (word.empty()) {
      return {""};
    }
    vector<string> ret, prev;
    prev = {string(1, word[0]), "1"};
    dfs(word, 1, ret, prev);
    return ret;
  }
};

int main() {
  string str = "approximations";
  Solution sol;
  vector<string> ret = sol.generateAbbreviations(str);
  printf("#ret: %d\n", (int)ret.size());
  // for (auto& abbr : ret) {
    // printf("%s ", abbr.c_str());
  // }
  // printf("\n");
  return 0;
}

