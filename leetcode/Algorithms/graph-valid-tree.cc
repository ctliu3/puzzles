#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Solution {
  bool validTree1(int n, vector<pair<int, int>>& edges) {
    vector<vector<int>> e(n, vector<int>{});

    for (auto& edge : edges) {
      int u = edge.first, v = edge.second;
      e[u].push_back(v);
      e[v].push_back(u);
    }

    vector<int> breadth(n, -1);
    queue<int> que;

    que.push(0);
    breadth[0] = 0;
    while (!que.empty()) {
      int u = que.front();
      que.pop();

      for (auto v : e[u]) {
        if (breadth[v] == -1) {
          breadth[v] = breadth[u] + 1;
          que.push(v);
        } else if (breadth[u] - breadth[v] != 1) {
          return false;
        }
      }
    }

    int m = 0;
    for (int i = 0; i < n; ++i) {
      if (breadth[i] != -1) {
        ++m;
      }
    }
    return n == m;
  }

public:
  bool validTree(int n, vector<pair<int, int>>& edges) {
    return validTree1(n, edges);
  }
};

int main() {
  vector<pair<int, int>> edges = {{0,1}, {0,2}, {2,3}, {2,4}};
  Solution sol;
  bool ret = sol.validTree(5, edges);
  cout << ret << endl;
  return 0;
}
