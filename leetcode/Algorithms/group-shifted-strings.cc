#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
  vector<vector<string>> groupStrings(vector<string>& strings) {
    if (strings.empty()) {
      return {};
    }

    vector<vector<string>> group;
    for (string& str : strings) {
      int which = -1;
      for (int i = 0; i < (int)group.size() && which == -1; ++i) {
        if (group[i][0].size() != str.size()) {
          continue;
        }
        int offset = group[i][0][0] - str[0];
        if (offset < 0) {
          offset += 26;
        }
        int j = 1;
        for (; j < (int)str.size(); ++j) {
          int target = (int)str[j] + offset;
          if (target > 'z') {
            target = 'a' + (target - 'z') - 1;
          }
          if (target != group[i][0][j]) {
            break;
          }
        }
        if (j == (int)str.size()) {
          which = i;
          break;
        }
      }
      if (which == -1) {
        group.push_back(vector<string>(1, str));
      } else {
        group[which].push_back(str);
      }
    }


    for (auto& item : group) {
      sort(item.begin(), item.end());
    }

    return group;
  }
};

int main() {
  vector<string> strings = {"qzq", "sbs"};
  Solution sol;
  auto ret = sol.groupStrings(strings);
  for (auto& group : ret) {
    for (auto& str : group) {
      cout << str << ' ';
    }
    cout << endl;
  }

  return 0;
}
