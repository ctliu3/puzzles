#include "leetcode.h"

class Solution {
  int treedp1(int st, int ed, vector<vector<int>>& dp) {
    int& res = dp[st - 1][ed - 1];
    if (ed - st == 0) {
      return res = 0;
    }
    if (ed - st == 1) {
      return res = st;
    }
    if (dp[st - 1][ed - 1] != -1) {
      return res;
    }

    res = INT_MAX;
    for (int i = st + 1; i < ed; ++i) {
      int a = treedp1(st, i - 1, dp);
      int b = treedp1(i + 1, ed, dp);
      res = min(res, max(a, b) + i);
    }
    return res;
  }

  int getMoneyAmount1(int n) {
    vector<vector<int>> dp(n, vector<int>(n, -1));
    return treedp1(1, n, dp);
  }

public:
  int getMoneyAmount(int n) {
    return getMoneyAmount1(n);
  }
};

int main() {
  return 0;
}
