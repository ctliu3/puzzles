int guess(int num);

class Solution {
public:
  int guessNumber(int n) {
    int start = 1, end = n;
    while (start <= end) {
      int mid = start + (end - start) / 2;
      int ret = guess(mid);
      if (ret == 0) {
        return mid;
      }
      else if (ret == 1) {
        start = mid + 1;
      } else {
        end = mid - 1;
      }
    }
    return -1;
  }
};
