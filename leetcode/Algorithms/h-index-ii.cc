#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int hIndex(vector<int>& citations) {
    if (citations.empty()) {
      return 0;
    }
    int n = (int)citations.size();
    int start = 1, end = n;
    int ret = 0;

    while (start <= end) {
      int h = start + (end - start) / 2;
      if (citations[n - h] >= h && (n - h - 1 < 0 || citations[n - h - 1] <= h)) {
        ret = max(ret, h);
      }
      if (citations[n - h] >= h) {
        start = h + 1;
      } else {
        end = h - 1;
      }
    }

    return ret;
  }
};

int main() {
  return 0;
}
