class Solution {
  int hammingDistance1(int x, int y) {
    int ret = 0;
    for (int i = 0; i < 32; ++i) {
      if (((1 << i) & x) ^ ((1 << i) & y)) {
        ++ret;
      }
    }
    return ret;
  }

  int hammingDistance2(int x, int y) {
    int val = x ^ y;
    int ans = 0;
    while (val) {
      val &= val - 1;
      ans += 1;
    }
    return ans;
  }

public:
  int hammingDistance(int x, int y) {
    // return hammingDistance1(x, y);
    return hammingDistance2(x, y);
  }
};
