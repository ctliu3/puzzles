#include <iostream>
#include <unordered_set>

using namespace std;

class Solution {
  int square[10];

  int getSquareSum(int n) {
    int sum = 0;
    while (n) {
      sum += square[n % 10];
      n /= 10;
    }
    return sum;
  }

public:
  bool isHappy(int n) {
    if (n == 1) {
      return true;
    }

    for (int i = 0; i < 10; ++i) {
      square[i] = i * i;
    }
    unordered_set<int> hash;
    while (!hash.count(n) && n != 1) {
      hash.insert(n);
      n = getSquareSum(n);
    }
    return n == 1;
  }
};
