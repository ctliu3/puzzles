#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int _rob(vector<int>& nums, int start, int end) {
    int res = nums[start];
    vector<int> dp(end - start + 1, 0);

    dp[0] = nums[start];
    for (int i = 1; i < (int)dp.size(); ++i) {
      dp[i] = max(dp[i - 1], nums[start + i]);
      if (i - 2 >= 0) {
        dp[i] = max(dp[i], dp[i - 2] + nums[start + i]);
      }
      res = max(res, dp[i]);
    }

    return res;
  }

public:
  int rob(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    if (nums.size() == 1) {
      return nums[0];
    }
    return max(_rob(nums, 0, nums.size() - 2), _rob(nums, 1, nums.size() - 1));
  }
};

int main() {
  Solution sol;
  vector<int> nums = {1, 2, 3, 4, 5, 1, 2, 3, 4, 5};
  int res = sol.rob(nums);
  cout << res << endl;
  return 0;
}
