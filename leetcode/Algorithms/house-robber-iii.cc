#include "leetcode.h"

class Solution {
  unordered_map<TreeNode*, vector<int>> dp;

  void dfs(TreeNode* cur) {
    if (dp.count(cur) == 0) {
      dp[cur] = {0, 0};
    }

    if (cur && !cur->left && !cur->right) {
      dp[cur][0] = 0;
      dp[cur][1] = cur->val;
      return ;
    }
    TreeNode* l = cur->left, *r = cur->right;
    if (l) {
      dfs(l);
    }
    if (r) {
      dfs(r);
    }

    dp[cur][0] += l ? max(dp[l][0], dp[l][1]) : 0;
    dp[cur][0] += r ? max(dp[r][0], dp[r][1]) : 0;

    dp[cur][1] = cur->val;
    dp[cur][1] += l ? dp[l][0] : 0;
    dp[cur][1] += r ? dp[r][0] : 0;
  }

public:
  int rob(TreeNode* root) {
    if (!root) {
      return 0;
    }

    dfs(root);
    return max(dp[root][0], dp[root][1]);
  }
};

int main() {
  return 0;
}
