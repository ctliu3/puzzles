#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int rob(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    int n = nums.size();
    int res = nums[0];
    vector<int> dp(n, 0);
    dp[0] = nums[0];
    for (int i = 1; i < n; ++i) {
      dp[i] = dp[i - 1];
      dp[i] = max(dp[i], nums[i] + (i - 2 >= 0 ? dp[i - 2] : 0));
      res = max(dp[i], res);
    }
    return res;
  }
};

int main() {
  return 0;
}
