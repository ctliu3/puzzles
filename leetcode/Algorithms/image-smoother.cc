#include "leetcode.h"

class Solution {
  bool inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

public:
  vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
    if (M.empty())  {
      return {};
    }
    const int dx[] = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
    const int dy[] = {-1, 0, 1, -1, 0, 1, -1, 0, 1};

    int n = M.size(), m = M[0].size();
    vector<vector<int>> ans(n, vector<int>(m, 0));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        int sum = 0, count = 0;
        for (int k = 0; k < 9; ++k) {
          int ni = dx[k] + i;
          int nj = dy[k] + j;
          if (inside(ni, nj, n, m)) {
            sum += M[ni][nj];
            count += 1;
          }
        }
        ans[i][j] = floor(sum * 1. / count);
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
