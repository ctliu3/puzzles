#include "leetcode.h"

class TrieNode {
public:
  TrieNode* child[26];
  bool word;

  explicit TrieNode() {
    for (int i = 0; i < 26; ++i) {
      child[i] = nullptr;
    }
    word = false;
  }
};

class Trie {
public:
  TrieNode* root;
  explicit Trie() {
    root = new TrieNode();
  }

  void insert(string& s) {
    TrieNode* cur = root;
    for (auto& chr : s) {
      if (!cur->child[chr - 'a']) {
        cur->child[chr - 'a'] = new TrieNode();
      }
      cur = cur->child[chr - 'a'];
    }
    cur->word = true;
  }

  bool search_(TrieNode* cur, const string& s, int i, int dep) {
    if (!cur) {
      return false;
    }
    if (cur->word == true && i == s.size()) {
      return true;
    }
    if (i == s.size()) {
      return false;
    }
    int index = s[i] - 'a';
    if (i == dep) {
      for (int j = 0; j < 26; ++j) {
        if (!cur->child[j] || index == j) {
          continue;
        }
        if (search_(cur->child[j], s, i + 1, dep)) {
          return true;
        }
      }
    } else {
      if (cur->child[index]) {
        if (search_(cur->child[index], s, i + 1, dep)) {
          return true;
        }
      }
    }
    return false;
  }

  bool search(const string& s, int dep) {
    return search_(root, s, 0, dep);
  }
};

class MagicDictionary {
  Trie* tree;

public:
  /** Initialize your data structure here. */
  MagicDictionary() {
    tree = new Trie();
  }

  /** Build a dictionary through a list of words */
  void buildDict(vector<string> dict) {
    for (auto& word : dict) {
      tree->insert(word);
    }
  }

  /** Returns if there is any word in the trie that equals to the given word after modifying exactly one character */
  bool search(string word) {
    for (int i = 0; i < word.size(); ++i) {
      if (tree->search(word, i)) {
        return true;
      }
    }
    return false;
  }
};

/**
 * Your MagicDictionary object will be instantiated and called as such:
 * MagicDictionary obj = new MagicDictionary();
 * obj.buildDict(dict);
 * bool param_2 = obj.search(word);
 */
