#include <iostream>
#include <stack>

using namespace std;

class Queue {
  stack<int> primary, backup;

  void _forward_to_backup() {
    while (!primary.empty()) {
      backup.push(primary.top());
      primary.pop();
    }
  }

public:
  // Push element x to the back of queue.
  void push(int x) {
    primary.push(x);
  }

  // Removes the element from in front of queue.
  void pop() {
    if (!backup.empty()) {
      backup.pop();
    } else {
      _forward_to_backup();
      backup.pop();
    }
  }

  // Get the front element.
  int peek() {
    if (!backup.empty()) {
      return backup.top();
    }
    _forward_to_backup();
    return backup.top();
  }

  // Return whether the queue is empty.
  bool empty() {
    return primary.empty() && backup.empty();
  }
};

int main() {
  return 0;
}
