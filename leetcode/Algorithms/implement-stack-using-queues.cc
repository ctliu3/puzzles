#include <iostream>
#include <queue>

using namespace std;

class Stack {
  queue<int> que[2];
  int which;

public:
  explicit Stack() {
    which = 0;
  }

  // Push element x onto stack.
  void push(int x) {
    if (!que[which].empty()) {
      que[which ^ 1].push(que[which].front());
      que[which].pop();
    }
    que[which].push(x);
  }

  // Removes the element on top of the stack.
  void pop() {
    if (!que[which].empty()) {
      que[which].pop();
    } else {
      while (que[which ^ 1].size() > 1) {
        que[which].push(que[which ^ 1].front());
        que[which ^ 1].pop();
      }
      que[which ^ 1].pop();
      which ^= 1;
    }
  }

  // Get the top element.
  int top() {
    if (que[which].empty()) {
      while (que[which ^ 1].size() > 1) {
        que[which].push(que[which ^ 1].front());
        que[which ^ 1].pop();
      }
      which ^= 1;
    }
    return que[which].front();
  }

  // Return whether the stack is empty.
  bool empty() {
    return que[which].empty() && que[which ^ 1].empty();
  }
};

int main() {
  return 0;
}
