#include "leetcode.h"

class Solution {
  void dfs1(vector<int>& nums, int dep, vector<int>& sub, vector<vector<int>>& ans) {
    if (sub.size() > 1) {
      ans.push_back(sub);
    }
    unordered_set<int> hash;
    for (int i = dep; i < nums.size(); ++i) {
      if (sub.empty() || nums[i] >= sub.back()) {
        if (!hash.count(nums[i])) {
          sub.push_back(nums[i]);
          dfs1(nums, i + 1, sub, ans);
          sub.pop_back();
          hash.insert(nums[i]);
        }
      }
    }
  }

  vector<vector<int>> findSubsequences1(vector<int>& nums) {
    vector<int> sub;
    vector<vector<int>> ans;
    dfs1(nums, 0, sub, ans);
    return ans;
  }

public:
  vector<vector<int>> findSubsequences(vector<int>& nums) {
    return findSubsequences1(nums);
  }
};

int main() {
  return 0;
}
