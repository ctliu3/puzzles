class Solution {
public:
  bool increasingTriplet(vector<int>& nums) {
    if (nums.size() < 3) {
      return false;
    }
    vector<int> ans(3, 0);
    int j = 0;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (j == 1 && nums[i] < ans[0]) {
        ans[0] = nums[i];
        continue;
      }
      if (j == 2 && nums[i] < ans[1] && nums[i] > ans[0]) {
        ans[1] = nums[i];
        continue;
      }
      if (j == 3 && ans[2] < ans[1] && nums[i] < ans[1] && nums[i] > ans[2]) {
        ans[0] = ans[2];
        ans[1] = nums[i];
        --j;
        continue;
      }
      if (j == 3 && nums[i] > ans[2]) {
        ans[2] = nums[i];
      }
      if (j == 0 || nums[i] > ans[j - 1] || (j == 2 && nums[i] < ans[1])) {
        ans[j++] = nums[i];
      }
      if (j >= 3 && ans[0] < ans[1] && ans[1] < ans[2]) {
        return true;
      }
    }
    return false;
  }
};
