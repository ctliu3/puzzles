#include "leetcode.h"

class Solution {

  TreeNode* inorderSuccessor1(TreeNode* root, TreeNode* p) {
    if (!root) {
      return nullptr;
    }

    TreeNode* ret = nullptr;
    int cnt = 0;
    inorder(root, p, ret, cnt);
    return ret;
  }

  bool inorder(TreeNode* cur, TreeNode* p, TreeNode*& ret, int& cnt) {
    if (!cur) {
      return false;
    }

    if (cur->left) {
      if (inorder(cur->left, p, ret, cnt)) {
        return true;
      }
    }

    if (cnt == 1) {
      ret = cur;
      return true;
    }
    if (cur == p) {
      ++cnt;
    }

    if (cur->right) {
      if (inorder(cur->right, p, ret, cnt)) {
        return true;
      }
    }

    return false;
  }

public:
  TreeNode* inorderSuccessor(TreeNode* root, TreeNode* p) {
    return inorderSuccessor1(root, p);
  }
};

int main() {
  return 0;
}
