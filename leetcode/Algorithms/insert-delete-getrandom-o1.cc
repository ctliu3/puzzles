#include "leetcode.h"

class RandomizedSet {
  vector<int> nums;
  unordered_map<int, int> mp;

public:
  /** Initialize your data structure here. */
  RandomizedSet() {
    nums.clear();
    mp.clear();
  }

  /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
  bool insert(int val) {
    if (mp.find(val) != mp.end()) {
      return false;
    }
    nums.push_back(val);
    mp[val] = nums.size() - 1;
    return true;
  }

  /** Removes a value from the set. Returns true if the set contained the specified element. */
  bool remove(int val) {
    if (mp.find(val) == mp.end()) {
      return false;
    }
    int index = mp[val];
    int n = (int)nums.size();
    swap(nums[index], nums[n - 1]);
    mp[nums[index]] = index;
    mp.erase(val);
    nums.pop_back();
    return true;
  }

  /** Get a random element from the set. */
  int getRandom() {
    return nums[rand() % nums.size()];
  }
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * bool param_1 = obj.insert(val);
 * bool param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
