#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int dfs(int n, vector<int>& dp) {
    if (dp[n] != -1) {
      return dp[n];
    }

    int& cur = dp[n];
    cur = 0;
    for (int i = 1; i < n; ++i) {
      int a = max(i, dfs(i, dp));
      int b = max(n - i, dfs(n - i, dp));
      cur = max(cur, a * b);
    }
    return cur;
  }


  int integerBreak1(int n) {
    vector<int> dp(n + 1, -1);
     return dfs(n, dp);
  }

public:
  int integerBreak(int n) {
    return integerBreak1(n);
  }
};

int main() {
  Solution sol;
  int ret = sol.integerBreak(8);
  cout << ret << endl;
  return 0;
}
