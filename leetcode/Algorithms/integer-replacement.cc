using LL = long long;

class Solution {
  // BFS
  int integerReplacement1(int n) {
    if (n <= 1) {
      return 0;
    }
    queue<pair<LL, int>> q;
    q.push({n, 0});
    while (!q.empty()) {
      LL val = q.front().first;
      int step = q.front().second;
      q.pop();
      if (val == 1) {
        return step;
      }
      if (val & 1) {
        q.push({(LL)val + 1, step + 1});
        q.push({(LL)val - 1, step + 1});
      } else {
        q.push({(LL)val >> 1, step + 1});
      }
    }
    return -1;
  }

  // DP

public:
  int integerReplacement(int n) {
    return integerReplacement1(n);
  }
};
