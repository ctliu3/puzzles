#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  string numberToWords(int num) {
    if (num == 0) {
      return "Zero";
    }
    vector<int> nums = {(int)1e9, (int)1e6, (int)1e3, (int)1e2, 1};
    vector<string> eng = {"Billion", "Million", "Thousand", "Hundred", ""};

    vector<string> e2x
      = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy",
        "Eighty", "Ninety"};
    vector<string> e1x
      = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
        "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
        "Sixteen", "Seventeen", "Eighteen","Nineteen"};

    string ret;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (num >= nums[i]) {
        int val = num / nums[i];

        if (val >= 100) {
          ret.append(e1x[val / 100] + " " + eng[3]);
          ret.push_back(' ');
          val %= 100;
        }
        if (val >= 20) {
          ret.append(e2x[val / 10]);
          ret.push_back(' ');
          val %= 10;
        } else if (val >= 10) {
          ret.append(e1x[val]);
          ret.push_back(' ');
        }
        if (val >= 1 && val < 10) {
          ret.append(e1x[val]);
          ret.push_back(' ');
        }

        if (i != (int)nums.size() - 1) {
          ret.append(eng[i]);
          ret.push_back(' ');
          num %= nums[i];
        }
      }
    }

    if (ret.back() == ' ') {
      ret.resize(ret.size() - 1);
    }
    return ret;
  }
};

int main() {
  Solution sol;
  string ret = sol.numberToWords(123);
  cout << ret << '$' << endl;
  return 0;
}
