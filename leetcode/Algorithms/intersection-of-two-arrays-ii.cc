#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
  vector<int> intersect1(vector<int>& nums1, vector<int>& nums2) {
    unordered_map<int, int> mp;
    for (int i = 0; i < (int)nums1.size(); ++i) {
      ++mp[nums1[i]];
    }
    vector<int> ret;
    for (int i = 0; i < (int)nums2.size(); ++i) {
      if (mp[nums2[i]] > 0) { // dont use count()
        --mp[nums2[i]];
        ret.push_back(nums2[i]);
      }
    }
    return ret;
  }

public:
  vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
    return intersect1(nums1, nums2);
  }
};

int main() {
  vector<int> nums1 = {1};
  vector<int> nums2 = {1, 1};
  Solution sol;

  auto ret = sol.intersect(nums1, nums2);
  for (auto val : ret) {
    cout << val << endl;
  }
  return 0;
}
