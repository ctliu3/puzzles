#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution {
public:
  vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
    auto& minlen_nums = nums1.size() > nums2.size() ? nums2 : nums1;
    auto& maxlen_nums = nums1.size() > nums2.size() ? nums1 : nums2;

    unordered_set<int> set(minlen_nums.begin(), minlen_nums.end());
    vector<int> ret;
    for (int val : maxlen_nums) {
      if (set.count(val)) {
        ret.push_back(val);
        set.erase(val);
      }
    }
    return ret;
  }
};

int main() {
  return 0;
}
