#include "leetcode.h"

class Solution {

  // Convert it to a circle and then find the intersection.
  ListNode *getIntersectionNode1(ListNode *headA, ListNode *headB) {
    if (!headA || !headB) {
      return nullptr;
    }
    ListNode* cur = headA, *last = nullptr;

    while (cur->next) {
      cur = cur->next;
    }

    last = cur;
    last->next = headB;

    ListNode* slow = headA, *fast = headA;
    while (fast && fast->next) {
      slow = slow->next;
      fast = fast->next->next;
      if (slow == fast) {
        break;
      }
    }

    if (slow != fast) {
      last->next = nullptr;
      return nullptr;
    }

    cur = headA;
    while (slow != cur) {
      cur = cur->next;
      slow = slow->next;
    }

    last->next = nullptr;
    return slow;
  }

  int get_length(ListNode* head) {
    int len = 0;
    while (head) {
      head = head->next;
      ++len;
    }
    return len;
  }

  ListNode *getIntersectionNode2(ListNode *headA, ListNode *headB) {
    int lenA = get_length(headA);
    int lenB = get_length(headB);

    ListNode* curA = headA, *curB = headB;
    int diff = abs(lenA - lenB);
    ListNode*& longer = lenA > lenB ? curA : curB;

    for (int i = 0; i < diff; ++i) {
      longer = longer->next;
    }

    while (curA && curB && curA != curB) {
      curA = curA->next;
      curB = curB->next;
    }

    if (curA && curA == curB) {
      return curA;
    }
    return nullptr;
  }

public:
  ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
    //return getIntersectionNode1(headA, headB);
    return getIntersectionNode2(headA, headB);
  }
};

int main() {
  return 0;
}
