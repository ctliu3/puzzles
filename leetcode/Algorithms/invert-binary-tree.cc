#include "leetcode.h"

class Solution {
  TreeNode* invertTree1(TreeNode* cur) {
    if (!cur) {
      return nullptr;
    }

    TreeNode* temp = cur->left;
    cur->left = cur->right;
    cur->right = temp;

    invertTree1(cur->left);
    invertTree1(cur->right);

    return cur;
  }

  TreeNode* invertTree2(TreeNode* root) {
    if (!root) {
      return nullptr;
    }

    queue<TreeNode*> que;
    que.push(root);
    while (!que.empty()) {
      TreeNode* cur = que.front();
      que.pop();

      if (!cur) {
        continue;
      }
      TreeNode* temp = cur->left;
      cur->left = cur->right;
      cur->right = temp;

      que.push(cur->left);
      que.push(cur->right);
    }

    return root;
  }

public:
  TreeNode* invertTree(TreeNode* root) {
    //return invertTree1(root);
    return invertTree2(root);
  }
};

int main() {
  return 0;
}
