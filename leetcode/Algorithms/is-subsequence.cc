class Solution {
  bool isSubsequence1(string s, string t) {
    if (s.size() > t.size()) {
      return false;
    }
    int i = 0, j = 0;
    while (i < s.size()) {
      while (j < t.size() && t[j] != s[i]) {
        ++j;
      }
      if (j == t.size()) {
        break;
      }
      ++i;
      ++j;
    }

    return i == (int)s.size();
  }

public:
  bool isSubsequence(string s, string t) {
    return isSubsequence1(s, t);
  }
};
