#include <iostream>
#include <vector>

using namespace std;

const int dx[] = {-1, 1, 0, 0};
const int dy[] = {0, 0, -1, 1};

class Solution {
  bool inBound(int n, int m, int i, int j) {
    return i >= 0 && i < n && j >= 0 && j < m;
  }

  void dfs1(vector<vector<int>>& grid, vector<vector<int>>& visit, int i, int j) {
    visit[i][j] = 1;
    for (int k = 0; k < 4; ++k) {
      int ni = i + dx[k];
      int nj = j + dy[k];
      if (inBound(grid.size(), grid[0].size(), ni, nj) && grid[ni][nj] == 1 && visit[ni][nj] == 0) {
        dfs1(grid, visit, ni, nj);
      }
    }
  }

  void dfs1(vector<vector<int>>& grid, vector<vector<int>>& visit, int i, int j, int& ret) {
    visit[i][j] = 2;
    for (int k = 0; k < 4; ++k) {
      int ni = i + dx[k];
      int nj = j + dy[k];
      if (!inBound(grid.size(), grid[0].size(), ni, nj) || visit[ni][nj] < 1) {
        ++ret;
      }
      if (inBound(grid.size(), grid[0].size(), ni, nj) && visit[ni][nj] == 1) {
        dfs1(grid, visit, ni, nj, ret);
      }
    }
  }

  int islandPerimeter1(vector<vector<int>>& grid) {
    if (grid.empty()) {
      return 0;
    }
    int n = grid.size(), m = grid[0].size();
    vector<vector<int>> visit(n, vector<int>(m, 0));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (grid[i][j] == 1 and !visit[i][j]) {
          dfs1(grid, visit, i, j);
          int ret = 0;
          dfs1(grid, visit, i, j, ret);
          return ret;
        }
      }
    }
    return 0;
  }

public:
  int islandPerimeter(vector<vector<int>>& grid) {
    return islandPerimeter1(grid);
  }
};

int main() {
  return 0;
}
