#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  bool isIsomorphic(string s, string t) {
    if (s.length() == 0) {
      return true;
    }

    int n = s.length();
    vector<char> mp(256, -1);
    vector<bool> hash(256, false);
    for (int i = 0; i < n; ++i) {
      char schar = s[i], tchar = t[i];
      if (mp[schar] == -1) {
        if (hash[tchar]) {
          return false;
        }
        mp[schar] = tchar;
        hash[tchar] = true;
      } else {
        if (tchar != mp[schar]) {
          return false;
        }
      }
    }

    return true;
  }
};

int main() {
}
