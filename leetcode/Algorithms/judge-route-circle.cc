#include "leetcode.h"

class Solution {
  bool judgeCircle1(const string& moves) {
    int x = 0, y = 0;
    for (int i = 0; i < moves.size(); ++i) {
      char c = moves[i];
      if (c == 'R') {
        y += 1;
      } else if (c == 'L') {
        y -= 1;
      } else if (c == 'U') {
        x -= 1;
      } else if (c == 'D') {
        x += 1;
      }
    }
    return x == 0 && y == 0;
  }

public:
  bool judgeCircle(string moves) {
    return judgeCircle1(moves);
  }
};

int main() {
  return 0;
}
