#include "leetcode.h"

class Solution {
public:
  int findPairs(vector<int>& nums, int k) {
    int ans = 0;
    unordered_map<int, int> mp;
    unordered_set<int> hash;
    for (auto num : nums) {
      mp[num] += 1;
    }
    for (auto num : nums) {
      if (hash.count(num)) {
        continue;
      }
      if (num + k >= num) {
        if (k == 0 && mp[num] > 1) {
          ans += 1;
        }
        if (k != 0 && mp.count(num + k)) {
          ans += 1;
        }
      }
      hash.insert(num);
    }
    return ans;
  }
};

int main() {
  return 0;
}
