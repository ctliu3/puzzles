#include "leetcode.h"

class Solution {
  vector<string> findWords1(vector<string>& words) {
    vector<string> ans, rows(3);
    vector<int> hash(26);
    rows[0] = "qwertyuiop";
    rows[1] = "asdfghjkl";
    rows[2] = "zxcvbnm";
    for (int i = 0; i < 3; ++i) {
      for (auto c : rows[i]) {
        hash[c - 'a'] = i;
      }
    }

    for (auto& w : words) {
      int row = hash[tolower(w[0]) - 'a'];
      bool ok = true;
      for (auto& c: w) {
        if (row != hash[tolower(c) - 'a']) {
          ok = false;
          break;
        }
      }
      if (ok) {
        ans.push_back(w);
      }
    }
    return ans;
  }

public:
  vector<string> findWords(vector<string>& words) {
    return findWords1(words);
  }
};

int main() {
  return 0;
}
