#include "leetcode.h"

class Solution {

  vector<int> killProcess1(vector<int>& pid, vector<int>& ppid, int kill) {
    // build tree
    unordered_map<int, vector<int>> tree;
    for (int i = 0; i < (int)pid.size(); ++i) {
      tree[ppid[i]].push_back(pid[i]);
    }

    vector<int> ans;
    queue<int> que;
    que.push(kill);
    while (!que.empty()) {
      int cur = que.front();
      que.pop();
      ans.push_back(cur);
      for (auto& next : tree[cur]) {
        que.push(next);
      }
    }
    return ans;
  }

public:
  vector<int> killProcess(vector<int>& pid, vector<int>& ppid, int kill) {
    return killProcess1(pid, ppid, kill);
  }
};

int main() {
  return 0;
}
