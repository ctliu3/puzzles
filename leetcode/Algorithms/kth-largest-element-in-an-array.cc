#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int _find(vector<int>& nums, int start, int end, int k) {
    int pivot_idx = start + (end - start) / 2;
    int pos = start;
    swap(nums[pivot_idx], nums[end]);

    for (int i = start; i <= end; ++i) {
      if (nums[i] >= nums[end]) {
        swap(nums[pos], nums[i]);
        ++pos;
      }
    }
    --pos;

    int rank = pos - start + 1;
    if (rank == k) {
      return nums[pos];
    } else if (rank < k) {
      return _find(nums, pos + 1, end, k - rank);
    } else {
      return _find(nums, start, pos - 1, k);
    }
  }

  int _findKthLargest(vector<int>& nums, int k) {
    if (nums.empty() || k == 0) {
      return 0;
    }
    return _find(nums, 0, nums.size() - 1, k);
  }

public:
  int findKthLargest(vector<int>& nums, int k) {
    return _findKthLargest(nums, k);
  }
};

int main() {
  Solution sol;
  vector<int> vec = {3, 2, 1, 5, 6, 4};

  int res = sol.findKthLargest(vec, 2);
  cout << res << endl;
  return 0;
}
