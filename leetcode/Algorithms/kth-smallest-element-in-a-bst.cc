#include "leetcode.h"

using namespace std;

class Solution {

  int _dfs(TreeNode* cur, unordered_map<TreeNode*, int>& mp) {
    if (!cur) {
      return 0;
    }
    int lc = _dfs(cur->left, mp);
    int rc = _dfs(cur->right, mp);
    return mp[cur] = lc + rc + 1;
  }

  int _get_kth(TreeNode* cur, int k, unordered_map<TreeNode*, int>& mp) {
    int lc = mp[cur->left];

    if (lc >= k) {
      return _get_kth(cur->left, k, mp);
    } else if (lc + 1 == k) {
      return cur->val;
    } else {
      return _get_kth(cur->right, k - lc - 1, mp);
    }
  }

public:
  int kthSmallest(TreeNode* root, int k) {
    unordered_map<TreeNode*, int> mp;
    _dfs(root, mp);
    return _get_kth(root, k, mp);
  }
};

int main() {
  return 0;
}
