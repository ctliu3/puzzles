#include "leetcode.h"

typedef pair<int, int> PI;

struct Comp {
  bool operator()(const PI& lhs, const PI& rhs) const {
    if (lhs.second == rhs.second) {
      return lhs.first > rhs.first;
    }
    return lhs.second > rhs.second;
  }
};

class Solution {
  int kthSmallest1(vector<vector<int>>& matrix, int k) {
    int n = matrix.size();
    priority_queue<PI, vector<PI>, Comp> pq;
    for (int i = 0; i < n; ++i) {
      pq.push({i * n, matrix[i][0]});
    }
    int ans = 0;
    while (!pq.empty()) {
      auto cur = pq.top();
      pq.pop();
      k--;
      if (k == 0) {
        ans = cur.second;
        break;
      }
      int x = cur.first / n;
      int y = cur.first % n;
      if (y != n - 1) {
        pq.push({x * n + y + 1, matrix[x][y + 1]});
      }
    }
    return ans;
  }

  int kthSmallest2(vector<vector<int>>& matrix, int k) {
    int n = (int)matrix.size();
    int start = matrix[0][0], end = matrix[n - 1][n - 1];
    while (start < end) {
      int mid = start + (end - start) / 2;
      int num = 0;
      for (int i = 0; i < n; ++i) {
        int pos = upper_bound(matrix[i].begin(), matrix[i].end(), mid) - matrix[i].begin();
        num += pos;
      }
      if (num >= k) {
        end = mid;
      } else {
        start = mid + 1;
      }
    }
    return start;
  }

public:
  int kthSmallest(vector<vector<int>>& matrix, int k) {
    // return kthSmallest1(matrix, k);
    return kthSmallest2(matrix, k);
  }
};

int main() {
  return 0;
}
