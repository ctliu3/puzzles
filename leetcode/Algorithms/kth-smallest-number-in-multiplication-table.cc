#include "leetcode.h"

class Solution {
  int count1(int m, int n, int key) {
    int ret = 0;
    for (int i = 1; i <= m; ++i) {
      ret += min(key / i, n);
    }
    return ret;
  }

  int findKthNumber1(int m, int n, int k) {
    int start = 1, end = m * n;
    while (start < end) {
      int mid = start + (end - start) / 2;
      int count = count1(m, n, mid);
      if (count < k) {
        start = mid + 1;
      } else {
        end = mid;
      }
    }
    return start;
  }

public:
  int findKthNumber(int m, int n, int k) {
    return findKthNumber1(m, n, k);
  }
};

int main() {
  return 0;
}
