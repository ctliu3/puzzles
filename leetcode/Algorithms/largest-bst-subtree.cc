#include "leetcode.h"

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
  bool dfs(TreeNode* cur, int& ret, int& minval, int& maxval, int& count) {
    if (cur && !cur->left && !cur->right) {
      ret = max(ret, 1);
      count = 1;
      minval = maxval = cur->val;
      return true;
    }

    int lmin = INT_MAX, lmax = INT_MIN, lcount = 0, lisbst = true;
    int rmin = INT_MAX, rmax = INT_MIN, rcount = 0, risbst = true;
    if (cur->left) {
      lisbst = dfs(cur->left, ret, lmin, lmax, lcount);
    }
    if (cur->right) {
      risbst = dfs(cur->right, ret, rmin, rmax, rcount);
    }
    if (!lisbst || !risbst) {
      return false;
    }

    count = 1 + lcount + rcount;
    minval = min(cur->val, min(lmin, rmin));
    maxval = max(cur->val, max(lmax, rmax));
    if (cur->left && cur->val < lmax) {
      return false;
    }
    if (cur->right && cur->val > rmin) {
      return false;
    }
    ret = max(ret, count);
    return true;
  }

public:
  int largestBSTSubtree(TreeNode* root) {
    if (!root) {
      return 0;
    }
    int ret = 0, minval = INT_MAX, maxval = INT_MIN, count = 0;
    dfs(root, ret, minval, maxval, count);
    return ret;
  }
};

int main() {
  return 0;
}
