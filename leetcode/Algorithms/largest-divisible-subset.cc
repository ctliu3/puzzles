#include <vector>

using namespace std;

struct Node {
  int count;
  int val;
  int prev;
};

class Solution {
  vector<int> largestDivisibleSubset1(vector<int>& nums) {
    if (nums.size() < 2) {
      return nums;
    }
    sort(nums.begin(), nums.end());

    int n = (int)nums.size();
    int maxind = 0;
    vector<pair<int, int>> dp(n);
    for (int i = 0; i < n; ++i) {
      dp[i] = {1, i};
      int prev = i, maxval = 1;
      for (int j = 0; j < i; ++j) {
        if (nums[i] % nums[j] == 0 && dp[j].first + 1 > maxval) {
          maxval = dp[j].first + 1;
          prev = j;
        }
      }
      dp[i] = {maxval, prev};
      if (maxval > dp[maxind].first) {
        maxind = i;
      }
    }

    int prev = maxind;
    vector<int> ret;
    while (prev != dp[prev].second) {
      ret.push_back(nums[prev]);
      prev = dp[prev].second;
    }
    ret.push_back(nums[prev]);

    return ret;
  }

public:
  vector<int> largestDivisibleSubset(vector<int>& nums) {
    return largestDivisibleSubset1(nums);
  }
};

int main() {
  return 0;
}
