#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  string largestNumber(vector<int>& nums) {
    sort(nums.begin(), nums.end(), [](const int& lhs, const int& rhs) {
      string sa = to_string(lhs);
      string sb = to_string(rhs);
      return sa + sb > sb + sa;
    });

    string res;
    for (auto& element : nums) {
      res.append(to_string(element));
    }
    while (res.size() > 1 && res[0] == '0') {
      res.erase(0, 1);
    }
    return res;
  }
};

int main() {
  return 0;
}
