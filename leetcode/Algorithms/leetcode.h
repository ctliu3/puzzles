#ifndef _LEETCODE_HEADER
#define _LEETCODE_HEADER

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cassert>
#include <vector>
#include <stack>
#include <queue>
#include <sstream>
#include <string>
#include <algorithm>
#include <limits> // std::numeric_limits
#include <climits> // INT_MIN, INT_MAX
#include <cfloat>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

using namespace std;

#define dump(x) cerr << #x << " = " << x << endl;

// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
};

// Definition for singly-linked list with a random pointer.
struct RandomListNode {
  int label;
  RandomListNode *next, *random;
  RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
};

// Definition for binary tree.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

// Definition of Interval.
struct Interval {
  int start;
  int end;
  Interval() : start(0), end(0) {}
  Interval(int s, int e) : start(s), end(e) {}
};

// Definition of TreeLinkNode.
struct TreeLinkNode {
  TreeLinkNode *left;
  TreeLinkNode *right;
  TreeLinkNode *next;
};

// Definition for a point.
struct Point {
  int x;
  int y;
  Point() : x(0), y(0) {}
  Point(int a, int b) : x(a), y(b) {}
};

// Definition for undirected graph.
struct UndirectedGraphNode {
  int label;
  vector<UndirectedGraphNode *> neighbors;
  UndirectedGraphNode(int x) : label(x) {};
};

// The format of parameter `s` is like "{3,9,20,#,#,15,7}".
TreeNode* getTree(string s) {
  for (int i = 0; i < (int)s.size(); ++i) {
    if (s[i] == '{' || s[i] == ',' || s[i] == '}') {
      s[i] = ' ';
    }
  }

  istringstream sin(s);
  vector<string> vs;
  string t;
  while (sin >> t) {
    vs.push_back(t);
  }

  if (vs.empty() || vs[0] == "#") {
    return NULL;
  }
  TreeNode* root = new TreeNode(atoi(vs[0].c_str()));
  TreeNode* p = root;
  vector<TreeNode *> v;
  int n = 0;

  for (size_t i = 1; i < s.size(); i += 2) {
    if (i < vs.size() && vs[i] != "#") {
      p->left = new TreeNode(atoi(vs[i].c_str()));
      v.push_back(p->left);
    }

    if (i + 1 < vs.size() && vs[i + 1] != "#") {
      p->right = new TreeNode(atoi(vs[i + 1].c_str()));
      v.push_back(p->right);
    }

    if (n < (int)v.size()) {
      p = v[n++];
    }
  }

  return root;
}

ListNode* convert_to_list(int a[], int n) {
  ListNode* ret = new ListNode(0);
  ListNode* p = ret;
  for (int i = 0; i < n; ++i) {
    p->val = a[i];
    if (i + 1 < n) {
      p->next = new ListNode(0);
      p = p->next;
    }
  }
  return ret;
}

void print_list(ListNode* cur) {
  while (cur) {
    printf("%d ", cur->val);
    cur = cur->next;
  }
  putchar('\n');
}

void _level_order_dfs(TreeNode* root, int dep, vector<vector<int> >& vs) {
  if (root == NULL) {
    return ;
  }
  if ((int)vs.size() < dep + 1) {
    vs.resize(dep + 1);
  }
  vs[dep].push_back(root->val);
  _level_order_dfs(root->left, dep + 1, vs);
  _level_order_dfs(root->right, dep + 1, vs);
}

vector<vector<int> > level_order(TreeNode* root) {
  vector<vector<int> > ret;

  _level_order_dfs(root, 0, ret);
  return ret;
}

void print_vector(vector<int>& v) {
  for (auto i : v) {
    fprintf(stderr, "%d ", i);
  }
  fprintf(stderr, "\n");
}

void print_vector2(vector<vector<int> >& vs) {
  fprintf(stderr, "[\n");
  for (int i = 0; i < (int)vs.size(); ++i) {
    fprintf(stderr, "  [");
    for (int j = 0; j < (int)vs[i].size(); ++j) {
      fprintf(stderr, "%d%c", vs[i][j], j == (int)vs[i].size() - 1 ? ']' : ',');
    }
    fprintf(stderr, "%c\n", i == (int)vs.size() - 1 ? ' ' : ',');
  }
  fprintf(stderr, "]\n");
}

// _LEETCODE_HEADER
#endif 
