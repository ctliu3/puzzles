#include "leetcode.h"

class Solution {
  void dfs(int num, int n, vector<int>& ans) {
    if (num >= 1 && num <= n) {
      ans.push_back(num);
    }
    for (int i = 0; i <= 9; ++i) {
      int cur = num * 10 + i;
      if (cur >= 1 && cur <= n) {
        dfs(cur, n, ans);
      }
    }
  }

public:
  vector<int> lexicalOrder(int n) {
    vector<int> ans;
    dfs(0, n, ans);
    return ans;
  }
};

int main() {
  return 0;
}
