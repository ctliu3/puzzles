#include "leetcode.h"

class Solution {
  ListNode* head;
  int len;

public:
  /** @param head The linked list's head.
    Note that the head is guaranteed to be not null, so it contains at least one node. */
  Solution(ListNode* head) {
    this->head = head;
    ListNode* cur = head;
    len = 0;
    while (cur) {
      ++len;
      cur = cur->next;
    }
  }

  /** Returns a random node's value. */
  int getRandom() {
    ListNode* cur = this->head;
    int m = rand() % len;
    while (m--) {
      cur = cur->next;
    }
    return cur->val;
  }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(head);
 * int param_1 = obj.getRandom();
 */

int main() {
  return 0;
}
