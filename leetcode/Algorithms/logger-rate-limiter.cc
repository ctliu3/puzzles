#include "leetcode.h"

class Logger {
public:
  unordered_map<string, int> record;
  /** Initialize your data structure here. */
  Logger() {
    record.clear();
  }

  /** Returns true if the message should be printed in the given timestamp, otherwise returns false.
    If this method returns false, the message will not be printed.
    The timestamp is in seconds granularity. */
  bool shouldPrintMessage(int timestamp, string message) {
    auto ptr = record.find(message);
    if (ptr == record.end()) {
      record[message] = timestamp;
      return true;
    }
    if (timestamp - ptr->second >= 10) {
      record[message] = timestamp;
      return true;
    }
    return false;
  }
};

/**
 * Your Logger object will be instantiated and called as such:
 * Logger obj = new Logger();
 * bool param_1 = obj.shouldPrintMessage(timestamp,message);
 */

int main() {
  return 0;
}
