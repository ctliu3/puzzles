#include "leetcode.h"

class Solution {
public:
  int findLonelyPixel(vector<vector<char>>& picture) {
    if (picture.empty()) {
      return 0;
    }
    int n = picture.size(), m = picture[0].size();
    vector<int> rows(n, 0);
    vector<int> cols(m, 0);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (picture[i][j] == 'B') {
          cols[j] += 1;
        }
      }
    }

    for (int j = 0; j < m; ++j) {
      for (int i = 0; i < n; ++i) {
        if (picture[i][j] == 'B') {
          rows[i] += 1;
        }
      }
    }

    int ans = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (picture[i][j] != 'B') {
          continue;
        }
        if (rows[i] == 1 && cols[j] == 1) {
          ++ans;
        }
      }
    }
    return ans;
  }
};
