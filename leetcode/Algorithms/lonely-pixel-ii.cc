#include "leetcode.h"

class Solution {
  int findBlackPixel1(vector<vector<char>>& picture, int N) {
    if (picture.empty()) {
      return 0;
    }
    int n = picture.size(), m = picture[0].size();
    vector<int> cols(m, 0);
    vector<int> rows(n, 0);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (picture[i][j] == 'B') {
          cols[j] += 1;
        }
      }
    }
    for (int j = 0; j < m; ++j) {
      for (int i = 0; i < n; ++i) {
        if (picture[i][j] == 'B') {
          rows[i] += 1;
        }
      }
    }

    vector<vector<bool> > is_same(n, vector<bool>(n, false));
    for (int i = 0; i < n; ++i) {
      is_same[i][i] = true;
      for (int j = i + 1; j < n; ++j) {
        bool flag = true;
        for (int k = 0; k < m && flag; ++k) {
          if (picture[i][k] != picture[j][k]) {
            flag = false;
          }
        }
        if (flag) {
          is_same[i][j] = true;
        }
      }
    }

    int ans = 0;
    for (int j = 0; j < m; ++j) {
      if (cols[j] != N) {
        continue;
      }
      for (int i = 0; i < n; ++i) {
        if (cols[j] != rows[i]) {
          continue;
        }
        int index = -1;
        bool flag = true;
        for (int k = 0; k < n; ++k) {
          if (picture[k][j] == 'B') {
            if (!is_same[i][k]) {
              flag = false;
              break;
            }
          }
        }
        if (flag) {
          ans += cols[j];
          break;
        }
      }
    }
    return ans;
  }

public:
  int findBlackPixel(vector<vector<char>>& picture, int N) {
    return findBlackPixel1(picture, N);
  }
};

int main() {
  return 0;
}
