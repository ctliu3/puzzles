#include "leetcode.h"

class Solution {
  int lengthLongestPath1(string input) {
    if (input.empty()) {
      return 0;
    }
    int ans = 0, level = 0;
    int val = 0;
    stack<int> stk;
    int n = (int)input.size();
    for (int i = 0; i < n; ) {
      int j = i;
      level = 0;
      while (j < n && input[j] == '\t') {
        level += 1;
        j += 1;
      }
      while (stk.size() > level) {
        val -= stk.top();
        stk.pop();
      }

      bool has_dot = false;
      int k = j;
      while (k < n && input[k] != '\n') {
        if (input[k] == '.') {
          has_dot = true;
        }
        ++k;
      }
      int cur = k - j;
      stk.push(cur);
      val += cur;
      if (has_dot) {
        ans = max(ans, val + (int)stk.size() - 1);
      }

      i = k + 1;
    }

    return ans;
  }

public:
  int lengthLongestPath(string input) {
    return lengthLongestPath1(input);
  }
};

int main() {
  return 0;
}
