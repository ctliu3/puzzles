class Solution {
public:
  int findLengthOfLCIS(vector<int>& nums) {
    int n = nums.size();
    if (n < 1) {
      return 0;
    }
    int ans = 1;
    for (int i = 0; i < n; ) {
      int j = i + 1;
      while (j < n && nums[j - 1] < nums[j]) {
        ++j;
      }
      ans = max(j - i, ans);
      i = j;
    }
    return ans;
  }
};
