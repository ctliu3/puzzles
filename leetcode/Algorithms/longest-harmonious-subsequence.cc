#include "leetcode.h"

class Solution {
public:
  int findLHS(vector<int>& nums) {
    unordered_map<int, int> counter;
    unordered_set<int> st;
    for (int i = 0; i < (int)nums.size(); ++i) {
      counter[nums[i]] += 1;
      st.insert(nums[i]);
    }
    
    int ans = 0;
    for (auto& num : st) {
      int l = num - 1;
      int r = num + 1;

      int c = counter[num];
      auto ptr = counter.find(l);
      if (ptr != counter.end()) {
        ans = max(ans, c + ptr->second);
      }
      ptr = counter.find(r);
      if (ptr != counter.end()) {
        ans = max(ans, c + ptr->second);
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
