#include <iostream>
#include <vector>

using namespace std;

vector<int> dx = {-1, 1, 0, 0};
vector<int> dy = {0, 0, -1, 1};

class Solution {
  int inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

  int getmax(const vector<vector<int>>& matrix, vector<vector<int>>& dp,
    int x, int y, int n, int m) {
    if (dp[x][y] != -1) {
      return dp[x][y];
    }

    int& cur = dp[x][y] = 1;
    for (int i = 0; i < 4; ++i) {
      int nx = x + dx[i], ny = y + dy[i];
      if (inside(nx, ny, n, m) && matrix[nx][ny] < matrix[x][y]) {
        cur = max(getmax(matrix, dp, nx, ny, n, m) + 1, cur);
      }
    }

    return cur;
  }

public:
  int longestIncreasingPath(vector<vector<int>>& matrix) {
    if (matrix.empty()) {
      return 0;
    }

    int n = matrix.size(), m = matrix[0].size();
    vector<vector<int>> dp(n, vector<int>(m, -1));
    int ret = 1;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        ret = max(ret, getmax(matrix, dp, i, j, n, m));
      }
    }

    return ret;
  }
};

int main() {
  vector<vector<int>> matrix = {
    {9, 9, 4},
    {6, 6, 8},
    {2, 1, 1}
  };
  Solution sol;
  int ret = sol.longestIncreasingPath(matrix);
  cout << ret << endl;

  return 0;
}
