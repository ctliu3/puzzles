#include <vector>

using namespace std;

class Solution {

  // O(n^2) time, O(n) space
  int lengthOfLIS1(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    int n = (int)nums.size();
    vector<int> dp(n, 1);
    for (int i = 1; i < n; ++i) {
      for (int j = 0; j < i; ++j) {
        if (nums[i] > nums[j]) {
          dp[i] = max(dp[i], dp[j] + 1);
        }
      }
    }
    return *max_element(dp.begin(), dp.end());
  }

  // O(nlogn) time, O(n) space
  int lengthOfLIS2(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }

    int n = (int)nums.size();
    vector<int> LIS;
    for (int i = 0; i < n; ++i) {
      int pos = lower_bound(LIS.begin(), LIS.end(), nums[i]) - LIS.begin();
      if (pos == (int)LIS.size()) {
        LIS.push_back(nums[i]);
      } else {
        LIS[pos] = min(LIS[pos], nums[i]);
      }
    }
    return (int)LIS.size();
  }

public:
  int lengthOfLIS(vector<int>& nums) {
    // return lengthOfLIS1(nums);
    return lengthOfLIS2(nums);
  }
};

int main() {
  return 0;
}
