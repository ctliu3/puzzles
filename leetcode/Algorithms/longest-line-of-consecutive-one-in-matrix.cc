#include "leetcode.h"

class Solution {
  int line(const vector<vector<int> >& M, int x1, int y1, int x2, int y2, int dx, int dy) {
    int ans = 0, acc = 0;
    for (int x = x1, y = y1; true; x += dx, y += dy) {
      if (M[x][y] == 1) {
        acc += 1;
        ans = max(ans, acc);
      } else {
        acc = 0;
      }
      if (x==x2 && y==y2) {
        break;
      }
    }
    return ans;
  }

public:
  int longestLine(vector<vector<int> >& M) {
    if (M.empty()) {
      return 0;
    }
    int n = M.size(), m = M[0].size();
    // horizontal
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      int ret = line(M, i, 0, i, m - 1, 0, 1);
      ans = max(ans, ret);
    }
    // vertical
    for (int j = 0; j < m; ++j) {
      int ret = line(M, 0, j, n - 1, j, 1, 0);
      ans = max(ans, ret);
    }
    // diag
    int c = 0;
    while (c < m + n - 1) {
      int ret = line(M, c<=m-1?0:c-(m-1), c<=m-1?m-1-c:0, c<=n-1?c:n-1, c<=n-1?m-1:m-1-(c-(n-1)), +1, +1);
      ans = max(ans, ret);
      c += 1;
    }
    // anti-diag
    c = 0;
    while (c < m + n - 1) {
      int ret = line(M, c<=n-1?c:n-1, c<=n-1?0:c-(n-1), c<=m-1?0:c-(m-1), c<=m-1?c:m-1, -1, 1);
      ans = max(ans, ret);
      c += 1;
    }
    return ans;
  }
};

int main() {
  return 0;
}
