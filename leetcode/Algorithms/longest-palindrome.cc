class Solution {
  int longestPalindrome1(string s) {
    vector<int> hash(52, 0);
    for (size_t i = 0; i < s.size(); ++i) {
      if (s[i] >= 'a'  && s[i] <= 'z') {
        hash[s[i] - 'a'] += 1;
      } else {
        hash[s[i] - 'A' + 26] += 1;
      }
    }

    int even_sum = 0;
    bool has_odd = false;
    for (size_t i = 0; i < hash.size(); ++i) {
      if (hash[i] & 1) {
        has_odd = true;
        even_sum += hash[i] - 1;
      } else {
        even_sum += hash[i];
      }
    }
    return even_sum + has_odd;
  }

public:
  int longestPalindrome(string s) {
    return longestPalindrome1(s);
  }
};
