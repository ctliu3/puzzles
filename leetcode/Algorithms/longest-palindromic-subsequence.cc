#include "leetcode.h"

class Solution {
  int lcs(const string& s1, const string& s2) {
    int n = (int)s1.size();
    vector<vector<int>> dp(n + 1, vector<int>(n + 1, 0));
    int ans = 0;
    for (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= n; ++j) {
        if (s1[i - 1] == s2[j - 1]) {
          dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + 1);
        }
        dp[i][j] = max(dp[i][j], dp[i - 1][j]);
        dp[i][j] = max(dp[i][j], dp[i][j - 1]);
      }
    }
    return dp[n][n];
  }

public:
  int longestPalindromeSubseq(string s) {
    string s2 = s;
    reverse(s.begin(), s.end());
    return lcs(s, s2);
  }
};

int main() {
  return 0;
}
