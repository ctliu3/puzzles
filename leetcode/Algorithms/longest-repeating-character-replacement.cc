#include "leetcode.h"

class Solution {
  int characterReplacement1(const string& s, int k) {
    if (s.empty()) {
      return 0;
    }
    int n = (int)s.size();
    int st = 0, ed = 0;
    int ans = 0;
    vector<int> counter(26, 0);
    while (ed < n) {
      int index = s[ed] - 'A';
      counter[index] += 1;

      int count = 0, letter = 0, total = 0;
      for (int i = 0; i < 26; ++i) {
        if (counter[i] >= count) {
          count = counter[i];
          letter = i;
        }
        total += counter[i];
      }

      while (st < ed && total - count > k) {
        counter[s[st] - 'A'] -= 1;
        if (s[st] - 'A' == letter) {
          count -= 1;
        }
        total -= 1;
        ++st;
      }
      ++ed;

      ans = max(ans, ed - st);
    }
    return ans;
  }

public:
  int characterReplacement(string s, int k) {
    return characterReplacement1(s, k);
  }
};

int main() {
  return 0;
}
