#include "leetcode.h"

class Solution {
  void partition1(const string& s, vector<bool>& flag, vector<vector<int>>& count,
                  int st, int ed, int k, int& ans) {
    if (ed - st + 1 < k) {
      return ;
    }
    bool ok = true;
    vector<int> split = {st - 1, ed + 1};
    for (int i = 0; i < (int)count.size(); ++i) {
      int p1 = lower_bound(count[i].begin(), count[i].end(), st) - count[i].begin();
      int p2 = upper_bound(count[i].begin(), count[i].end(), ed) - count[i].begin();
      if (p2 - p1 < k && p1 != p2) {
        for (int j = p1; j <= p2 - 1; ++j) {
          flag[count[i][j]] = false;
          split.push_back(count[i][j]);
        }
        ok = false;
      }
    }
    if (ok) {
      ans = max(ans, ed - st + 1);
      return ;
    }

    sort(split.begin(), split.end());
    for (int i = 1; i < (int)split.size(); ++i) {
      partition1(s, flag, count, split[i - 1] + 1, split[i] - 1, k, ans);
    }
  }

  int longestSubstring1(string s, int k) {
    vector<vector<int>> count(26);
    for (int i = 0; i < (int)s.size(); ++i) {
      count[s[i] - 'a'].push_back(i);
    }
    int ans = 0;
    vector<bool> flag(s.size(), false);
    partition1(s, flag, count, 0, (int)s.size() - 1, k, ans);
    return ans;
  }

public:
  int longestSubstring(string s, int k) {
    return longestSubstring1(s, k);
  }
};

int main() {
  Solution sol;
  string s = "baaabcb";
  int ans = sol.longestSubstring(s, 3);
  cout << ans << endl;
  return 0;
}
