#include "leetcode.h"

class Solution {
public:
  int lengthOfLongestSubstringKDistinct(string s, int k) {
    if (s.empty() || k == 0) {
      return 0;
    }
    vector<int> hash(256, 0);
    int n = (int)s.size();
    int count = 0, st = 0, ans = 0;
    for (int i = 0; i < n; ++i) {
      int pos = s[i]; // !!!
      hash[pos] += 1;
      if (hash[pos] == 1) {
        count += 1;

        if (count > k) {
          while (st + k - 1 < i) {
            hash[s[st]] -= 1;
            if (hash[s[st]] == 0) {
              st += 1;
              break;
            }
            st += 1;
          }
          count -= 1;
        }
      }
      ans = max(ans, i - st + 1);
    }
    return ans;
  }
};

int main() {
  return 0;
}
