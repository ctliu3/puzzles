#include "leetcode.h"

class Solution {
public:
  int lengthOfLongestSubstringTwoDistinct(string s) {
    if (s.size() < 3) {
      return s.size();
    }

    int n = (int)s.size();
    int res = 0, ndist = 0;
    int st = 0, ed =0;
    vector<int> count(256, 0);
    while (ed < n) {
      char c = s[ed];
      if (!count[c]) {
        ++ndist;
      }
      ++count[c];
      if (ndist > 2) {
        while (st < ed && count[s[st]] > 0) {
          ++st;
          if (--count[s[st - 1]] == 0) {
            break;
          }
        }
        --ndist;
      }
      res = max(res, ed - st + 1);
      ++ed;
    }
    return res;
  }
};

int main() {
  Solution sol;
  int res = sol.lengthOfLongestSubstringTwoDistinct("a");
  cout << res << endl;
}
