#include "leetcode.h"

class TrieNode {
public:
  TrieNode* child[26];
  bool word;

  explicit TrieNode() {
    for (int i = 0; i < 26; ++i) {
      child[i] = nullptr;
    }
    word = false;
  }
};

class Trie {
public:
  TrieNode* root;
  explicit Trie() {
    root = new TrieNode();
  }

  void insert(string& s) {
    TrieNode* cur = root;
    for (auto& chr : s) {
      if (!cur->child[chr - 'a']) {
        cur->child[chr - 'a'] = new TrieNode();
      }
      cur = cur->child[chr - 'a'];
    }
    cur->word = true;
  }

  void search_(TrieNode* cur, const string& s, string sub, int dep, string& ans, int& maxlen) {
    if (cur->word) {
      if (sub.size() > maxlen) {
        maxlen = sub.size();
        ans = sub;
      } else if (sub.size() == maxlen) {
        if (sub < ans) {
          ans = sub;
        }
      }
    }
    if (dep == s.size()) {
      return ;
    }
    vector<bool> hash(26, false);
    for (int i = dep; i < s.size(); ++i) {
      int idx = s[i] - 'a';
      if (hash[idx]) {
        continue;
      }
      if (sub.size() + (s.size() - i) < maxlen) {
        break;
      }
      hash[idx] = true;
      if (cur->child[idx]) {
        sub.push_back(s[i]);
        search_(cur->child[idx], s, sub, i + 1, ans, maxlen);
        sub.pop_back();
      }
    }
  }

  void search(const string& s, string sub, int dep, string& ans, int& maxlen) {
    search_(root, s, sub, dep, ans, maxlen);
  }
};

class Solution {
  string findLongestWord1(string s, vector<string>& d) {
    if (d.empty()) {
      return "";
    }
    Trie tree;
    for (auto& w : d) {
      tree.insert(w);
    }
    string sub, ans;
    int maxlen = 0;
    tree.search(s, sub, 0, ans, maxlen);
    return ans;
  }

  string findLongestWord2(string s, vector<string>& d) {
    if (d.empty()) {
      return "";
    }
    int n = (int)s.size();
    int maxlen = 0;
    string ans;
    for (auto& w : d) {
      int m = w.size();
      if (m > n) {
        continue;
      }
      int j = 0;
      for (int i = 0; i < n; ++i) {
        if (s[i] == w[j]) {
          ++j;
        }
      }
      if (j == m) {
        if (m > maxlen || (m == maxlen && w < ans)) {
          maxlen = m;
          ans = w;
        }
      }
      // int i = 0, j = 0;
      // while (i < n || j < m) {
        // if (j == m) {
          // if (m > maxlen || (m == maxlen && w < ans)) {
            // maxlen = m;
            // ans = w;
          // }
        // }
        // if (i == n) {
          // break;
        // }
        // while (i < n && w[j] != s[i]) {
          // ++i;
        // }
        // ++j;
      // }
    }
    return ans;
  }

public:
  string findLongestWord(string s, vector<string>& d) {
    // return findLongestWord1(s, d);
    return findLongestWord2(s, d);
  }
};
