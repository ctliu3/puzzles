#include "leetcode.h"

class Solution {
public:
  TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    if ((!p && q) || (!q && p)) {
      return nullptr;
    }
    if (root == p || root == q) {
      return root;
    }

    if (p->val > q->val) {
      swap(p, q);
    }

    TreeNode* cur = root;
    while (cur) {
      if (cur->val >= p->val && cur->val <= q->val) {
        break;
      }
      if (cur == p || cur == q) {
        break;
      }
      if (cur->val > p->val) {
        cur = cur->left;
      } else {
        cur = cur->right;
      }
    }

    return cur;
  }
};

int main() {
  return 0;
}
