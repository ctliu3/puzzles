#include "leetcode.h"

class Solution {
  int _lca(TreeNode* cur, TreeNode* p, TreeNode* q, TreeNode*& res) {
    if (!cur) {
      return 0;
    }

    int state = 0;
    for (int i = 0; i < 2; ++i) {
      TreeNode* child = i == 0 ? cur->left : cur->right;

      state += _lca(child, p, q, res);
      if (state == 3) {
        res = res ? res : cur;
        return state;
      }
      if ((state == 1 && cur == q) || (state == 2 && cur == p)) {
        res = cur;
        return 3;
      }
    }
    if (cur == p) {
      state += 1;
    }
    if (cur == q) {
      state += 2;
    }

    return state;
  }

public:
  TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    if ((!p && q) || (p && !q)) {
      return nullptr;
    }
    if (p == root || q == root) {
      return root;
    }
    if (p == q) {
      return p;
    }
    TreeNode* res = nullptr;
    _lca(root, p, q, res);
    return res;
  }
};

int main() {
  return 0;
}
