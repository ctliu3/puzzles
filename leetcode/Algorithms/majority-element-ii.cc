#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> majorityElement(vector<int>& nums) {
    int a1, a2;
    int count1 = 0, count2 = 0;

    for (auto& num : nums) {
      if (count1 > 0 && num == a1) {
        ++count1;
        continue;
      }
      if (count2 > 0 && num == a2) {
        ++count2;
        continue;
      }
      if (count1 == 0) {
        a1 = num;
        ++count1;
        continue;
      }
      if (count2 == 0) {
        a2 = num;
        ++count2;
        continue;
      }

      if (num != a1 && num != a2) {
        --count1;
        --count2;
      }
    }

    int c1 = 0, c2 = 0;
    for (auto& num : nums) {
      if (count1 > 0) {
        c1 += num == a1;
      }
      if (count2 > 0) {
        c2 += num == a2;
      }
    }

    vector<int> res;
    int n = (int)nums.size();
    if (c1 > n / 3) {
      res.push_back(a1);
    }
    if (c2 > n / 3) {
      res.push_back(a2);
    }
    return res;
  }
};

int main() {
  return 0;
}
