#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int majorityElement(vector<int>& nums) {
    int res = 0, count = 0;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (!count) {
        res = nums[i];
        ++count;
      } else if (nums[i] == res) {
        ++count;
      } else {
        --count;
      }
    }
    return res;
  }
};

int main() {
  Solution sol;
  vector<int> nums = {1, 3, 3, 3};
  int res = sol.majorityElement(nums);
  cout << res << endl;
  return 0;
}
