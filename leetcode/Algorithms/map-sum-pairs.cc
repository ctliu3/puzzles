#include "leetcode.h"

class TrieNode {
public:
  TrieNode* child[26];
  bool end;
  int val;
  int sum;

  explicit TrieNode() {
    for (int i = 0; i < 26; ++i) {
      child[i] = nullptr;
    }
    end = false;
    val = sum = 0;
  }
};

class Trie {
public:
  TrieNode* root;
  explicit Trie() {
    root = new TrieNode();
  }

  void insert(string& s, int val) {
    TrieNode* cur = root;
    for (auto& chr : s) {
      if (!cur->child[chr - 'a']) {
        cur->child[chr - 'a'] = new TrieNode();
      }
      cur = cur->child[chr - 'a'];
    }
    cur->end = true;
    int diff = val - cur->val;
    cur->val = val;

    cur = root;
    cur->sum += diff;
    for (auto& chr : s) {
      cur = cur->child[chr - 'a'];
      cur->sum += diff;
    }
  }

  int prefixSum(const string& prefix) {
    int sum = 0;
    TrieNode* cur = root;
    for (auto& c : prefix) {
      int index = c - 'a';
      if (!cur->child[index]) {
        return 0;
      }
      cur = cur->child[index];
    }
    return cur->sum;
  }
};

class MapSum {
  Trie* tree;
public:
  /** Initialize your data structure here. */
  MapSum() {
    tree = new Trie;
  }
  ~MapSum() {
    if (tree != nullptr) {
      delete tree;
      tree = nullptr;
    }
  }

  void insert(string key, int val) {
    tree->insert(key, val);
  }

  int sum(string prefix) {
    if (prefix.empty()) {
      return 0;
    }
    return tree->prefixSum(prefix);
  }
};

/**
 * Your MapSum object will be instantiated and called as such:
 * MapSum obj = new MapSum();
 * obj.insert(key,val);
 * int param_2 = obj.sum(prefix);
 */
