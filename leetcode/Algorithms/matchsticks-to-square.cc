#include <iostream>
#include <vector>

using namespace std;

class Solution {
  bool dfs(vector<int>& nums, int cur, int side, vector<int>& sides) {
    if (cur == (int)nums.size()) {
      for (int i = 0; i < 4; ++i) {
        if (sides[i] != side) {
          return false;
        }
      }
      return true;
    }

    for (int i = 0; i < 4; ++i) {
      if (sides[i] + nums[cur] <= side) {
        sides[i] += nums[cur];
        if (dfs(nums, cur + 1, side, sides)) {
          return true;
        }
        sides[i] -= nums[cur];
      }
    }

    return false;
  }

  bool makesquare1(vector<int>& nums) {
    if (nums.size() < 4) {
      return false;
    }
    int sum = 0;
    int maxval = 0;
    for (auto val : nums) {
      sum += val;
      maxval = max(maxval, val);
    }
    if (sum % 4 or maxval > sum / 4) {
      return false;
    }
    int side = sum / 4;

    vector<int> sides(4, 0);
    sort(nums.rbegin(), nums.rend());
    return dfs(nums, 0, side, sides);
  }

public:
  bool makesquare(vector<int>& nums) {
    return makesquare1(nums);
  }
};

int main() {
  vector<int> nums = {2, 4, 1, 6, 7, 7};
  Solution sol;
  bool ret = sol.makesquare(nums);
  cout << ret << endl;
  return 0;
}
