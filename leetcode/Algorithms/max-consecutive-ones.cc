#include "leetcode.h"

class Solution {
public:
  int findMaxConsecutiveOnes(vector<int>& nums) {
    int ans = 0, i = 0;
    while (i < nums.size()) {
      int j = i;
      while (j < nums.size() && nums[j] == 1) {
        ++j;
      }
      ans = max(ans, j - i);
      i = j + 1;
    }
    return ans;
  }
};

int main() {
  return 0;
}
