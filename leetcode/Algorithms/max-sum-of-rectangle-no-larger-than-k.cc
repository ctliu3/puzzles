#include "leetcode.h"

class Solution {
public:
  int maxSumSubmatrix(vector<vector<int>>& matrix, int k) {
    int n = (int)matrix.size(), m = (int)matrix[0].size();
    set<int> st;
    vector<int> sub(m, 0);
    int ans = INT_MIN;
    int target = k;
    for (int i = 0; i < n; ++i) {
      fill(sub.begin(), sub.end(), 0);
      for (int j = i; j >= 0; --j) {
        if (matrix[i][j] <= target) {
          ans = max(matrix[i][j], ans);
          if (ans == target) {
            return ans;
          }
        }
        for (int k = 0; k < m; ++k) {
          sub[k] += matrix[j][k];
        }
        // solve the sub problem
        int acc = 0;
        st.clear();
        for (int k = 0; k < m; ++k) {
          acc += sub[k];
          auto itr = st.lower_bound(acc - target);
          if (itr != st.end()) {
            ans = max(ans, acc - *itr);
            if (ans == target) {
              return ans;
            }
          }
          if (acc <= target) {
            ans = max(ans, acc);
            if (ans == target) {
              return ans;
            }
          }
          st.insert(acc);
        }
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
