#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int maximalSquare(vector<vector<char>>& matrix) {
    if (matrix.empty()) {
      return 0;
    }

    int n = matrix.size(), m = matrix[0].size();
    int res = 0;
    vector<vector<int>> dp(n + 1, vector<int>(m + 1, 0));
    for (int i = 1; i <= n; ++i) {
      int count = 0;
      for (int j = 1; j <= m; ++j) {
        if (matrix[i - 1][j - 1] == '1') {
          ++count;
        }
        dp[i][j] = count;
        dp[i][j] += dp[i - 1][j];
      }
      if (count) {
        res = 1;
      }
    }

    if (!res) {
      return 0;
    }
    res = min(n, m);
    while (res > 1) {
      int k = res, area = k * k;
      for (int i = n; i - k + 1 >= 1; --i) {
        for (int j = m; j - k + 1 >= 1; --j) {
          if (dp[i][j] - dp[i - k][j] - dp[i][j - k] + dp[i - k][j - k] == area) {
            return area;
          }
        }
      }
      --res;
    }

    return 1;
  }
};

int main() {
  vector<vector<char>> matrix = {
    {'1', '0', '1', '0', '0'},
    {'1', '0', '1', '1', '1'},
    {'1', '1', '1', '1', '1'},
    {'1', '0', '0', '1', '0'},
  };

  Solution sol;
  int res = sol.maximalSquare(matrix);
  cout << res << endl;
  return 0;
}
