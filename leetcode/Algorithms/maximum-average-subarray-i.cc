#include "leetcode.h"

class Solution {
public:
  double findMaxAverage(vector<int>& nums, int k) {
    if (nums.size() < k) {
      return 0;
    }
    int sum = 0;
    double ans = -1e15;
    for (int i = 0; i < (int)nums.size(); ++i) {
      sum += nums[i];
      if (i == k - 1) {
        ans = max(ans, sum * 1. / k);
      } else if (i >= k) {
        sum -= nums[i - k];
        ans = max(ans, sum * 1. / k);
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
