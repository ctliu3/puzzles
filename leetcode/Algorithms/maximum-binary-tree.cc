#include "leetcode.h"

class Solution {
  TreeNode* construct1(vector<int>& nums, int l, int r) {
    if (l > r) {
      return nullptr;
    }
    int index = l, maxval = nums[l];
    for (int i = l; i <= r; ++i) {
      if (nums[i] > maxval) {
        maxval = nums[i];
        index = i;
      }
    }
    TreeNode* cur = new TreeNode(maxval);
    cur->left = construct1(nums, l, index - 1);
    cur->right = construct1(nums, index + 1, r);
    return cur;
  }

  TreeNode* constructMaximumBinaryTree1(vector<int>& nums) {
    return construct1(nums, 0, (int)nums.size() - 1);
  }

public:
  TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
    return constructMaximumBinaryTree1(nums);
  }
};

int main() {
  return 0;
}
