#include "leetcode.h"

class Solution {
public:
  int maxDistance(vector<vector<int>>& arrays) {
    int n = arrays.size();
    map<int, int> min_mp, max_mp;
    vector<pair<int, int>> peaks(n);
    for (int i = 0; i < n; ++i) {
      int maxval = INT_MIN, minval = INT_MAX;
      for (int val : arrays[i]) {
        maxval = max(maxval, val);
        minval = min(minval, val);
      }
      peaks[i] = {minval, maxval};
      min_mp[minval] += 1;
      max_mp[-maxval] += 1;
    }

    int ans = 0;
    for (int i = 0; i < n; ++i) {
      int minval = peaks[i].first, maxval = peaks[i].second;
      min_mp[minval] -= 1;
      max_mp[-maxval] -= 1;
      if (min_mp[minval] == 0) {
        min_mp.erase(minval);
      }
      if (max_mp[-maxval] == 0) {
        max_mp.erase(-maxval);
      }
      ans = max(ans, maxval - min_mp.begin()->first);
      ans = max(ans, -max_mp.begin()->first - minval);
      min_mp[minval] += 1;
      max_mp[-maxval] += 1;
    }
    return ans;
  }
};
