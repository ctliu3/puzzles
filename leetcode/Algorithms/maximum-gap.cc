#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>

using namespace std;

class Solution {
  int maximumGap1(vector<int>& nums) {
    if (nums.size() < 2) {
      return 0;
    }

    sort(nums.begin(), nums.end());
    int res = 0;
    for (int i = 1; i < (int)nums.size(); ++i) {
      res = max(res, nums[i] - nums[i - 1]);
    }
    return res;
  }

  int maximumGap2(vector<int>& nums) {
    if (nums.size() < 2) {
      return 0;
    }
    int minval = nums[0], maxval = nums[0];
    for (auto& num : nums) {
      minval = min(minval, num);
      maxval = max(maxval, num);
    }

    int len = ceil((maxval - minval) * 1.0 / (nums.size() - 1));
    int n_bucket = (maxval - minval) / len + 1;
    vector<int> min_bucket(n_bucket, -1), max_bucket(n_bucket, -1);
    for (auto& num : nums) {
      int which = (num - minval) / len;
      if (min_bucket[which] == -1 || min_bucket[which] > num) {
        min_bucket[which] = num;
      }
      if (max_bucket[which] == -1 || max_bucket[which] < num) {
        max_bucket[which] = num;
      }
    }

    int res = -1;
    int maxidx = -1;
    for (int i = 0; i < n_bucket; ++i) {
      if (max_bucket[i] != -1) {
        if (maxidx != -1) {
          res = max(res, min_bucket[i] - max_bucket[maxidx]);
        }
        maxidx = i;
      }
    }

    return res;
  }

public:
  int maximumGap(vector<int>& nums) {
    //return maximumGap1(nums);
    return maximumGap2(nums);
  }
};

int main() {
  Solution sol;
  vector<int> nums = {1, 2, 8, 9, 1};

  int res = sol.maximumGap(nums);
  cout << res << endl;

  return 0;
}
