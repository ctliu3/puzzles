#include "leetcode.h"

class Solution {
public:
  int findLongestChain(vector<vector<int>>& pairs) {
    sort(pairs.begin(), pairs.end(),
      [&](const vector<int>& lhs, const vector<int>& rhs) {
        if (lhs[0] == rhs[0]) {
          return lhs[1] < rhs[1];
        }
        return lhs[0] < rhs[0];
      });

    int n = pairs.size();
    vector<int> dp(n, 0);
    dp[0] = 1;
    int ans = 1;
    for (int i = 1; i < n; ++i) {
      dp[i] = 1;
      for (int j = 0; j < i; ++j) {
        if (pairs[j][1] < pairs[i][0]) {
          dp[i] = max(dp[i], dp[j] + 1);
        }
      }
      ans = max(ans, dp[i]);
    }
    return ans;
  }
};

int main() {
  return 0;
}
