#include "leetcode.h"

class Solution {
public:
  int maximumProduct(vector<int>& nums) {
    for (int i = 0; i < 3; ++i) {
      int val = nums[i], index = i;
      int j = i + 1;
      while (j < (int)nums.size()) {
        if (nums[j] > val) {
          val = nums[j];
          index = j;
        }
        ++j;
      }
      if (index != i) {
        swap(nums[i], nums[index]);
      }
    }

    int ans = nums[0] * nums[1] * nums[2];
    int maxval = nums[0];

    for (int i = 0; i < 3; ++i) {
      int val = nums[i], index = i;
      int j = i + 1;
      while (j < (int)nums.size()) {
        if (nums[j] < val) {
          val = nums[j];
          index = j;
        }
        ++j;
      }
      if (index != i) {
        swap(nums[i], nums[index]);
      }
    }

    if (maxval != nums[0] && nums[0] * nums[1] * maxval > ans) {
      ans = nums[0] * nums[1] * maxval;
    }
    return ans;
  }
};

int main() {
  return 0;
}
