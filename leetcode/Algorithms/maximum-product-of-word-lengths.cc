#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <cstdlib>

using namespace std;

class Solution {
  int maxProduct1(vector<string>& words) {
    int n = (int)words.size();
    if (n < 2) {
      return 0;
    }

    auto cmp = [&words] (const string& lhs, const string& rhs) {
      return lhs.size() > rhs.size();
    };
    sort(words.begin(), words.end(), cmp);

    vector<int> bits(n, 0);
    for (int i = 0; i < n; ++i) {
      for (char& c : words[i]) {
        bits[i] |= 1 << (c - 'a');
      }
    }
    int ret = 0;
    for (int i = 0; i < n - 1; ++i) {
      for (int j = i + 1; j < n; ++j) {
        if (int(words[i].size() * words[j].size()) <= ret) {
          break;
        }
        if (!(bits[i] & bits[j])) {
          ret = max(ret, int(words[i].size() * words[j].size()));
        }
      }
    }

    return ret;
  }

public:
  int maxProduct(vector<string>& words) {
    return maxProduct1(words);
  }
};

int main() {
}
