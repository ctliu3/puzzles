// Author:     ct.Liu, lcndn3@gmail.com
// Date:       Oct 01, 2014
// Update:     Oct 01, 2014
//
// Find the contiguous subarray within an array (containing at least one number)
// which has the largest product.
//
// For example, given the array [2,3,-2,4],
// the contiguous subarray [2,3] has the largest product = 6.

#include <iostream>
#include <algorithm>
#include <climits>
#include <vector>

using namespace std;

class Solution {
public:
  int maxProduct(vector<int>& nums) {
    int n = nums.size();
    if (n == 1) {
      return nums[0];
    }

    int minpro = nums[0], maxpro = nums[0];
    int res = nums[0];

    for (int i = 1; i < n; ++i) {
      int tmin = minpro, tmax = maxpro;
      if (nums[i] > 0) {
        maxpro = max(nums[i], tmax * nums[i]);
        minpro = min(nums[i], nums[i] * tmin);
      } else if (nums[i] < 0) {
        minpro = min(nums[i], tmax * nums[i]);
        maxpro = max(nums[i], tmin * nums[i]);
      } else {
        minpro = maxpro = 0;
      }
      res = max(res, maxpro);
    }

    return res;
  }
};

int main() {
  vector<int> nums = {-2, 0, -1};
  //vector<int> nums = {-4, -3};
  Solution sol;

  int res = sol.maxProduct(nums);
  cout << res << endl;

  return 0;
}
