#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
  int maxSubArrayLen(vector<int>& nums, int k) {
    if (nums.empty()) {
      return 0;
    }

    unordered_map<int, int> first_small;
    first_small[0] = -1;
    int ret = 0, sum = 0;
    for (int i = 0; i < (int)nums.size(); ++i) {
      sum += nums[i];
      auto pos = first_small.find(sum - k);
      if (pos != first_small.end() && i - pos->second > ret) {
        ret = i - pos->second;
      }
      pos = first_small.find(sum);
      if (pos == first_small.end()) {
        first_small[sum] = i;
      }
    }
    return ret;
  }
};

int main() {
  return 0;
}
