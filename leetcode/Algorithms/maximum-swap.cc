#include "leetcode.h"

class Solution {
public:
  int maximumSwap(int num) {
    vector<int> nums;
    while (num) {
      nums.push_back(num % 10);
      num /= 10;
    }
    reverse(nums.begin(), nums.end());
    int n = (int)nums.size();
    for (int i = 0; i < n; ++i) {
      int cur = nums[i];
      int maxval = cur, index = i;
      for (int j = i + 1; j < n; ++j) {
        if (nums[j] >= maxval) { // !!!
          maxval = max(maxval, nums[j]);
          index = j;
        }
      }
      if (cur != maxval) {
        swap(nums[i], nums[index]);
        break;
      }
    }
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      ans = ans * 10 + nums[i];
    }
    return ans;
  }
};

int main() {
  return 0;
}
