#include "leetcode.h"

class Solution {
  void dfs(TreeNode* cur, vector<pair<int, int> >& ends, int num, int dep) {
    if (!cur) {
      return ;
    }
    if (dep >= ends.size()) {
      ends.push_back(pair<int, int>(INT_MAX, INT_MIN));
    }
    ends[dep].first = min(ends[dep].first, num);
    ends[dep].second = max(ends[dep].second, num);
    dfs(cur->left, ends, num * 2, dep + 1);
    dfs(cur->right, ends, num * 2 - 1, dep + 1);
  }

  int widthOfBinaryTree1(TreeNode* root) {
    if (!root) {
      return 0;
    }
    vector<pair<int, int> > ends;
    dfs(root, ends, 1, 0);
    int ans = 1;
    for (auto& p : ends) {
      ans = max(p.second - p.first + 1, ans);
    }
    return ans;
  }

public:
  int widthOfBinaryTree(TreeNode* root) {
    return widthOfBinaryTree1(root);
  }
};

int main() {
  return 0;
}
