#include "leetcode.h"

class TrieNode {
public:
  TrieNode* child[2];
  bool word;

  explicit TrieNode() {
    for (int i = 0; i < 2; ++i) {
      child[i] = nullptr;
    }
    word = false;
  }
};

class Trie {
public:
  TrieNode* root;
  explicit Trie() {
    root = new TrieNode();
  }

  void insert(int val) {
    TrieNode* cur = root;
    for (int i = 31; i >= 0; --i) {
      int index = (val >> i) & 1;
      if (!cur->child[index]) {
        cur->child[index] = new TrieNode();
      }
      cur = cur->child[index];
    }
    cur->word = true;
  }

  int search(int val) {
    TrieNode* cur = root;
    int ret = 0;
    for (int i = 31; i >= 0; --i) {
      int index = 1 - ((val >> i) & 1);
      if (cur->child[index]) {
        cur = cur->child[index];
        ret |= (1 << i);
      } else {
        cur = cur->child[1 - index];
      }
    }
    return ret;
  }
};

class Solution {
public:
  int findMaximumXOR(vector<int>& nums) {
    Trie* tree = new Trie;
    for (int num : nums) {
      tree->insert(num);
    }
    int ans = 0;
    for (int num : nums) {
      ans = max(ans, tree->search(num));
    }
    return ans;
  }
};
