class Solution {
public:
  int minMeetingRooms(vector<Interval>& intervals) {
    if (intervals.empty()) {
      return 0;
    }

    map<int, int> order;
    for (auto& interval : intervals) {
      ++order[interval.start];
      --order[interval.end];
    }
    int rooms = 0;
    int cur = 0;
    for (auto& element : order) {
      cur += element.second;
      rooms = max(rooms, cur);
    }

    return rooms;
  }
};
