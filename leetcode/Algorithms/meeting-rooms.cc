#include "leetcode.h"

class Solution {
public:
  bool canAttendMeetings(vector<Interval>& intervals) {
    if (intervals.empty()) {
      return true;
    }

    sort(intervals.begin(), intervals.end(),
      [](const Interval& lhs, const Interval& rhs) {
      return lhs.start < rhs.start;
      });

    for (int i = 1; i < (int)intervals.size(); ++i) {
      if (intervals[i].start < intervals[i - 1].end) {
        return false;
      }
    }
    return true;
  }
};
