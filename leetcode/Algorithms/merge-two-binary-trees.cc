#include "leetcode.h"

class Solution {
  TreeNode* mergeTrees1(TreeNode* t1, TreeNode* t2) {
    if (t1 == nullptr and t2 == nullptr) {
      return nullptr;
    }

    int sum = t1 ? t1->val : 0;
    sum += t2 ? t2->val : 0;

    TreeNode* cur = new TreeNode(sum);
    TreeNode* l1 = t1 ? t1->left : nullptr;
    TreeNode* r1 = t1 ? t1->right : nullptr;
    TreeNode* l2 = t2 ? t2->left : nullptr;
    TreeNode* r2 = t2 ? t2->right : nullptr;
    cur->left = mergeTrees(l1, l2);
    cur->right = mergeTrees(r1, r2);

    return cur;
  }

public:
    TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {
      return mergeTrees1(t1, t2);
    }
};

int main() {
  return 0;
}
