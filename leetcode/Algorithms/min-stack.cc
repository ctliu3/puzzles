#include <iostream>
#include <vector>
#include <stack>
#include <cassert>

using namespace std;

class MinStack {
  stack<int> stk, minstk;

public:
  explicit MinStack() {
    while (!stk.empty()) {
      stk.pop();
    }
    while (!minstk.empty()) {
      minstk.pop();
    }
  }

  void push(int x) {
    stk.push(x);
    if (minstk.empty() or minstk.top() >= x) {
      minstk.push(x);
    }
  }

  void pop() {
    assert(stk.size() > 0);
    if (!stk.empty()) {
      if (!minstk.empty() and minstk.top() == stk.top()) {
        minstk.pop();
      }
      stk.pop();
    }
  }

  int top() {
    assert(stk.size() > 0);
    return stk.top();
  }

  int getMin() {
    assert(minstk.size() > 0);
    return minstk.top();
  }
};

int main() {
}
