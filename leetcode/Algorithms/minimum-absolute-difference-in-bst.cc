#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, int& prev, int& ans) {
    if (!cur) {
      return ;
    }
    dfs1(cur->left, prev, ans);
    if (prev != INT_MAX) {
      ans = min(ans, cur->val - prev);
    }
    prev = cur->val;
    dfs1(cur->right, prev, ans);
  }

  int getMinimumDifference1(TreeNode* root) {
    int prev = INT_MAX;
    int ans = INT_MAX;
    dfs1(root, prev, ans);
    return ans;
  }

public:
  int getMinimumDifference(TreeNode* root) {
    return getMinimumDifference1(root);
  }
};

int main() {
  return 0;
}
