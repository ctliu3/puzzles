#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Solution {
  vector<vector<int>> tree;
  int maxdep;

  void dfs(int u, int prev, int& end, int dep, vector<int>& track) {
    if (dep > maxdep) {
      maxdep = dep;
      end = u;
    }
    for (int v : tree[u]) {
      if (v != prev) {
        track[v] = u;
        dfs(v, u, end, dep + 1, track);
      }
    }
  }

  vector<int> findMinHeightTrees1(int n, vector<pair<int, int>>& edges) {
    if (n == 1) {
      return {0};
    }
    tree.resize(n);

    vector<int> d(n, 0);
    for (auto& p : edges) {
      int u = p.first, v = p.second;
      tree[u].push_back(v);
      tree[v].push_back(u);
      ++d[u];
      ++d[v];
    }

    vector<int> track(n, -1);
    track[0] = 0;
    int end = 0, dep = 0;
    maxdep = 0;
    dfs(0, -1, end, dep, track);
    track.resize(n, -1);
    track[end] = end;
    dep = maxdep = 0;
    dfs(end, -1, end, dep, track);

    vector<int> path;
    int cur = end;
    while (cur != track[cur]) {
      path.push_back(cur);
      cur = track[cur];
    }
    path.push_back(cur);

    int len = path.size();
    // printf("len = %d\n", len);
    if (len & 1) {
      return {path[len / 2]};
    }
    return {path[len / 2 - 1], path[len / 2]};
  }
  
public:
  vector<int> findMinHeightTrees(int n, vector<pair<int, int>>& edges) {
    return findMinHeightTrees1(n, edges);
  }
};

int main() {
  vector<pair<int, int>> edges = {{0, 3}, {1, 3}, {2, 3}, {4, 3}, {5, 4}};
  // vector<pair<int, int>> edges = {{0,1},{1,2},{2,3},{0,4},{4,5},{4,6},{6,7}};
  // vector<pair<int, int>> edges = {};
  Solution sol;
  vector<int> ans = sol.findMinHeightTrees(6, edges);
  for (int v : ans) {
    cout << v << endl;
  }
  return 0;
}
