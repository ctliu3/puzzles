#include "leetcode.h"

class Solution {
public:
  vector<string> findRestaurant(vector<string>& list1, vector<string>& list2) {
    unordered_map<string, int> mp;
    vector<string> ans;
    int minsum;

    for (int i = 0; i < (int)list1.size(); ++i) {
      mp[list1[i]] = i;
    }
    for (int i = 0; i < (int)list2.size(); ++i) {
      auto itr = mp.find(list2[i]);
      if (itr == mp.end()) {
        continue;
      }
      int cur = i + itr->second;
      if (ans.empty() || cur == minsum) {
        ans.push_back(list2[i]);
        minsum = cur;
      } else if (cur < minsum) {
        minsum = cur;
        ans = vector<string>(1, list2[i]);
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
