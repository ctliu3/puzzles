#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;

class Solution {
  int move(vector<int>& nums, int target) {
    int ret = 0;
    for (auto val : nums) {
      ret += abs(val - target);
    }
    return ret;
  }

  int minMoves21(vector<int>& nums) {
    if (nums.size() < 2) {
      return 0;
    }
    int minval = nums[0], maxval = nums[0];
    for (auto val : nums) {
      minval = min(minval, val);
      maxval = max(maxval, val);
    }
    int start = minval, end = maxval;
    int ret = INT_MAX;
    while (start <= end) {
      int mid = start + (end - start) / 2;
      int move_num = move(nums, mid);
      if (move_num <= ret) {
        ret = move_num;
        end = mid - 1;
      } else {
        start = mid + 1;
      }
    }
    return ret;
  }

public:
  int minMoves2(vector<int>& nums) {
    return minMoves21(nums);
  }
};

int main() {
  return 0;
}
