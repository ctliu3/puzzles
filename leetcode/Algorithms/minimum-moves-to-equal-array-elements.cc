class Solution {
  int minMoves1(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    int minval = *min_element(nums.begin(), nums.end());
    int ret = 0;
    for (auto val : nums) {
      ret += (val - minval);
    }
    return ret;
  }

public:
  int minMoves(vector<int>& nums) {
    return minMoves1(nums);
  }
};
