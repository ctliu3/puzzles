#include "leetcode.h"

typedef pair<int, int> PII;

bool comp(const PII& lhs, const PII& rhs) {
  if (lhs.second == rhs.second) {
    return lhs.first < rhs.first;
  }
  return lhs.second < rhs.second;
}

class Solution {
public:
  int findMinArrowShots(vector<PII>& points) {
    if (points.empty()) {
      return 0;
    }
    sort(points.begin(), points.end(), comp);

    int n = (int)points.size();
    int ans = 0;
    for (int i = 0; i < n; ) {
      int j = i;
      while (j < n && points[j].first <= points[i].second) {
        ++j;
      }
      ans += 1;
      i = j;
    }
    return ans;
  }
};

int main() {
  return 0;
}
