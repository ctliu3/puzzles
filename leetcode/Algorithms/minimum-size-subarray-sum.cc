#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int minSubArrayLen(int s, vector<int>& nums) {
    int res = nums.size() + 1;
    int start = 0, sum = 0;
    bool flag = false;

    for (int i = 0; i < (int)nums.size(); ++i) {
      sum += nums[i];
      while (sum >= s) {
        flag = true;
        res = min(i - start + 1, res);
        sum -= nums[start];
        ++start;
      }
      if (flag) {
        flag = false;
        --start;
        sum += nums[start];
      }
    }

    if (res > (int)nums.size()) {
      return 0;
    }
    return res;
  }
};

int main() {
  vector<int> arr = {10,5,13,4,8,4,5,11,14,9,16,10,20,8};
  int s = 80;
  Solution sol;

  int res = sol.minSubArrayLen(s, arr);
  cout << res << endl;

  return 0;
}
