#include "leetcode.h"

class Solution {
public:
  int findMinDifference(vector<string>& timePoints) {
    int n = (int)timePoints.size();
    vector<int> time(n);
    for (int i = 0; i < n; ++i) {
      string t = timePoints[i];
      int h = (t[0] - '0') * 10 + (t[1] - '0');
      int m = (t[3] - '0') * 10 + (t[4] - '0');
      time[i] = h * 60 + m;
    }
    sort(time.begin(), time.end());

    int ans = INT_MAX;
    for (int i = 0; i < n; ++i) {
      int prev = i ? time[i - 1] : time[n - 1] - 24 * 60;
      int next = i != n - 1 ? time[i + 1] : time[0] + 24 * 60;
      ans = min(ans, time[i] - prev);
      ans = min(ans, next - time[i]);
    }
    return ans;
  }
};

int main() {
  return 0;
}
