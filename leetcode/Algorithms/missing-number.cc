#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int missingNumber(vector<int>& nums) {
    int missing = 0;
    for (int i = 0; i < (int)nums.size(); ++i) {
      missing += i - nums[i];
    }
    return missing + (int)nums.size();
  }
};

int main() {
  return 0;
}
