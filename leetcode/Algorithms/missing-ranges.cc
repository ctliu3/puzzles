#include "leetcode.h"

class Solution {
public:

  string to_s(int val) {
    ostringstream out;
    out << val;
    return out.str();
  }

  void append2vec(int start, int end, vector<string>& res) {
    string ele;
    if (start == end) {
      ele.append(to_s(start));
    } else {
      ele.append(to_s(start));
      ele.append("->");
      ele.append(to_s(end));
    }
    res.push_back(ele);
  }

  vector<string> findMissingRanges(int A[], int n, int lower, int upper) {
    vector<string> res;

    int cur = lower;
    for (int i = 0; i < n; ++i) {
      if (cur != A[i]) {
        if (cur + 1 == A[i]) {
          append2vec(cur, cur, res);
        } else {
          append2vec(cur, A[i] - 1, res);
        }
        cur = A[i] + 1;
      } else {
        ++cur;
      }
    }
    if (cur <= upper) {
      if (cur == upper) {
        append2vec(cur, cur, res);
      } else {
        append2vec(cur, upper, res);
      }
    }

    return res;
  }
};

int main() {
  int A[] = {0, 1, 3, 50, 75};

  Solution sol;
  vector<string> res = sol.findMissingRanges(A, 5, -2, 99);
  for (auto& str : res) {
    cout << str << ' ';
  }
  cout << endl;

  return 0;
}
