#include "leetcode.h"

class Solution {
  int dfs1(TreeNode* cur, unordered_map<int, int>& counter) {
    if (!cur) {
      return 0;
    }
    int lsum = dfs1(cur->left, counter);
    int rsum = dfs1(cur->right, counter);
    int sum = lsum + rsum + cur->val;
    counter[sum] += 1;
    return sum;
  }

  vector<int> findFrequentTreeSum1(TreeNode* root) {
    if (!root) {
      return vector<int>();
    }
    unordered_map<int, int> counter;
    dfs1(root, counter);
    int maxval = 0;
    for (auto& p : counter) {
      maxval = max(maxval, p.second);
    }
    vector<int> ans;
    for (auto& p : counter) {
      if (p.second == maxval) {
        ans.push_back(p.first);
      }
    }

    return ans;
  }

public:
  vector<int> findFrequentTreeSum(TreeNode* root) {
    return findFrequentTreeSum1(root);
  }
};

int main() {
  return 0;
}
