#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  void moveZeroes(vector<int>& nums) {
    if (nums.empty()) {
      return ;
    }
    int n = (int)nums.size();
    int i = 0;
    while (i < n) {
      while (i < n && nums[i]) {
        ++i;
      }
      if (i == n) {
        break;
      }
      int j = i + 1;
      while (j < n && !nums[j]) {
        ++j;
      }
      if (j == n) {
        break;
      }
      swap(nums[i++], nums[j]);
    }
  }
};

int main() {
  return 0;
}
