class MovingAverage {
public:
  list<int> lst;
  int size;
  double sum;
	/** Initialize your data structure here. */
	MovingAverage(int size): size(size), sum(0.0) {
	}

	double next(int val) {
	  lst.push_back(val);
	  sum += val;
	  if (lst.size() > size) {
	    sum -= lst.front();
	    lst.pop_front();
    }
    return sum / lst.size();
	}
};

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage obj = new MovingAverage(size);
 * double param_1 = obj.next(val);
 */
