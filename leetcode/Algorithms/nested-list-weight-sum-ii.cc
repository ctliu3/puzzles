class Solution {
  int getDepth(vector<NestedInteger>& nestedList) {
    if (nestedList.empty()) {
      return 0;
    }

    int dep = 0;
    for (auto& lst : nestedList) {
      if (lst.isInteger()) {
        dep = max(dep, 0);
      } else {
        dep = max(getDepth(lst.getList()), dep);
      }
    }
    return dep + 1;
  }

  void dfs1(vector<NestedInteger>& nestedList, int dep, int& ret) {
    for (auto& lst : nestedList) {
      if (lst.isInteger()) {
        ret += dep * lst.getInteger();
      } else {
        dfs1(lst.getList(), dep - 1, ret);
      }
    }
  }

  int depthSumInverse1(vector<NestedInteger>& nestedList) {
    int dep = getDepth(nestedList);
    int ret = 0;
    dfs1(nestedList, dep, ret);
    return ret;
  }

public:
  int depthSumInverse(vector<NestedInteger>& nestedList) {
    return depthSumInverse1(nestedList);
  }
};
