class Solution {
  void dfs1(vector<NestedInteger>& nestedList, int dep, int& sum) {
    for (auto& lst : nestedList) {
      if (lst.isInteger()) {
        sum += dep * lst.getInteger();
      } else {
        dfs1(lst.getList(), dep + 1, sum);
      }
    }
  }

  int depthSum1(vector<NestedInteger>& nestedList) {
    int ret = 0;
    dfs1(nestedList, 1, ret);
    return ret;
  }

public:
  int depthSum(vector<NestedInteger>& nestedList) {
    return depthSum1(nestedList);
  }
};
