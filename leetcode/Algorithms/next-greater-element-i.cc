#include "leetcode.h"

class Solution {
public:
  vector<int> nextGreaterElement(vector<int>& findNums, vector<int>& nums) {
    int n = (int)nums.size();
    stack<int> st;
    unordered_map<int, int> larger(n);
    for (int i = n - 1; i >= 0; --i) {
      while (!st.empty() && nums[i] > st.top()) {
        st.pop();
      }
      larger[nums[i]] = st.empty() ? -1 : st.top();
      st.push(nums[i]);
    }

    vector<int> ans;
    for (auto num : findNums) {
      ans.push_back(larger[num]);
    }
    return ans;
  }
};

int main() {
  return 0;
}
