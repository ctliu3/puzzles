#include "leetcode.h"

class Solution {
public:
  vector<int> nextGreaterElements(vector<int>& nums) {
    int n = (int)nums.size();
    if (n == 0) {
      return {};
    }

    vector<int> ans(n);
    stack<int> stk;
    for (int i = n - 1; i >= 0; --i) {
      while (!stk.empty() && stk.top() <= nums[i]) {
        stk.pop();
      }
      stk.push(nums[i]);
    }
    for (int i = n - 1; i >= 0; --i) {
      while (!stk.empty() && stk.top() <= nums[i]) {
        stk.pop();
      }
      ans[i] = stk.empty() ? -1 : stk.top();
      stk.push(nums[i]);
    }

    return ans;
  }
};

int main() {
  return 0;
}
