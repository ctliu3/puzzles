#include "leetcode.h"

class Solution {
  int nextGreaterElement1(int n) {
    if (n == INT_MAX) { // INT_MAX = ?
      return -1;
    }
    vector<int> digits;
    while (n) {
      digits.push_back(n % 10);
      n /= 10;
    }
    reverse(digits.begin(), digits.end());
    int i = 0, index = -1;
    for (i = digits.size() - 1; i > 0; --i) {
      if (digits[i - 1] < digits[i]) {
        index = i - 1;
        break;
      }
    }
    if (index == -1) {
      return -1;
    }
    int j = -1;
    for (i = digits.size() - 1; i > index; --i) {
      if (digits[i] > digits[index]) {
        j = i;
        break;
      }
    }
    if (j == -1) {
      return -1;
    }
    swap(digits[index], digits[j]);
    sort(digits.begin() + index + 1, digits.end());
    long long larger = 0;
    for (int i = 0; i < digits.size(); ++i) {
      larger = larger * 10 + digits[i];
    }
    if (larger > INT_MAX) {
      return -1;
    }
    return larger;
  }

public:
  int nextGreaterElement(int n) {
    return nextGreaterElement1(n);
  }
};

int main() {
  return 0;
}
