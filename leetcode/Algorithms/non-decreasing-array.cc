#include "leetcode.h"

class Solution {
  bool checkPossibility1(vector<int>& nums) {
    if (nums.size() <= 2) {
      return true;
    }
    int count = 0;
    for (int i = 1; i < (int)nums.size(); ++i) {
      if (nums[i - 1] > nums[i]) {
        if (i - 2 < 0 || nums[i - 2] <= nums[i]) {
          nums[i - 1] = nums[i - 2];
        } else {
          nums[i] = nums[i - 1];
        }
        ++count;
      }
    }
    return count <= 1;
  }

public:
  bool checkPossibility(vector<int>& nums) {
    return checkPossibility1(nums);
  }
};

int main() {
  return 0;
}
