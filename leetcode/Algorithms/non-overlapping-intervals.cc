#include "leetcode.h"

class Solution {
  int eraseOverlapIntervals1(vector<Interval>& intervals) {
    int n = (int)intervals.size();
    if (n < 2) {
      return 0;
    }
    sort(intervals.begin(), intervals.end(), [](const Interval& lhs, const Interval& rhs) {
      return lhs.end < rhs.end;
    });

    vector<int> dp(n + 1, 0);
    dp[1] = 1;
    int ret = 1;
    for (int i = 2; i <= n; ++i) {
      dp[i] = dp[i - 1];
      for (int j = 1; j < i; ++j) {
        if (intervals[i - 1].start >= intervals[j - 1].end) {
          dp[i] = max(dp[i], dp[j] + 1);
        }
      }
      ret = max(ret, dp[i]);
    }
    return n - ret;
  }

public:
  int eraseOverlapIntervals(vector<Interval>& intervals) {
    return eraseOverlapIntervals1(intervals);
  }
};
