#include <iostream>

using namespace std;

class Solution {
  int kthDight(int num, int num_digit, int k) {
    int base = 1;
    k = num_digit - k - 1;
    while (k) {
      base *= 10;
      --k;
    }
    return num / base % 10;
  }

public:
  int findNthDigit(int n) {
    if (n <= 0) {
      return 0;
    }
    if (n <= 9) {
      return n;
    }

    long long acc = 9, base = 9;
    int count = 1;
    while (acc + base * 10 * (count + 1) <= n) {
      count += 1;
      base *= 10;
      acc += base * count;
    }
    n -= acc;
    if (n == 0) {
      return 9;
    }
    ++count;
    base = base / 9 * 10;
    if (n % count == 0) {
      return (base + (n / count ? n / count - 1 : 0)) % 10;
    }
    return kthDight(base + n / count, count, n % count - 1);
  }
};

int main() {
  Solution sol;
  int ret = sol.findNthDigit(1000);
  cout << ret << endl;
}
