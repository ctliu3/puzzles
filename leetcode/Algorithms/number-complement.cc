#include "leetcode.h"

class Solution {
public:
  int findComplement(int num) {
    int bits = 0;
    int tmp = num;
    while (num) {
      int bit = num & 1;
      num >>= 1;
      bits += 1;
    }
    return tmp ^ ((1 << bits) - 1);
  }
};

int main() {
  return 0;
}
