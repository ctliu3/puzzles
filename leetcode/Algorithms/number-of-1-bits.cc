#include <iostream>

using namespace std;

class Solution {
  int hammingWeight1(uint32_t n) {
    int res = 0;
    for (int i = 0; i < 32; ++i) {
      if ((1 << i) & n) {
        ++res;
      }
    }
    return res;
  }

  int hammingWeight2(uint32_t n) {
    int res = 0;
    while (n) {
      n &= (n - 1);
      ++res;
    }
    return res;
  }

public:
  int hammingWeight(uint32_t n) {
    //return hammingWeight1(n);
    return hammingWeight2(n);
  }
};

int main() {
  return 0;
}
