#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

using llong = long long;

llong sqr(llong x) {
  return x * x;
}

class Solution {
  int numberOfBoomerangs1(vector<pair<int, int>>& points) {
    if (points.size() < 3) {
      return 0;
    }
    llong ret = 0;
    for (int i = 0; i < (int)points.size(); ++i) {
      unordered_map<llong, int> counter;
      for (int j = 0; j < (int)points.size(); ++j) {
        if (i == j) {
          continue;
        }
        llong dist = sqr(points[i].first - points[j].first) + sqr(points[i].second - points[j].second);
        counter[dist] += 1;
      }
      for (auto p : counter) {
        if (p.second > 1) {
          ret += p.second * (p.second - 1);
        }
      }
    }
    return ret;
  }

public:
  int numberOfBoomerangs(vector<pair<int, int>>& points) {
    return numberOfBoomerangs1(points);
  }
};

int main() {
  vector<pair<int, int>> points = {{0, 0}, {1, 0}, {2, 0}};
  Solution sol;
  int ret = sol.numberOfBoomerangs(points);
  cout << ret << endl;
  return 0;
}
