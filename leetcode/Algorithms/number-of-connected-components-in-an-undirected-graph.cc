
class DisjointSet {
public:
  explicit DisjointSet(int n) {
    parent.resize(n);
    for (int i = 0; i < n; ++i) {
      parent[i] = i;
    }
  }

  int find(int u) {
    if (parent[u] == u) {
      return u;
    }
    return parent[u] = find(parent[u]);
  }

  int merge(int u, int v) {
    u = find(u);
    v = find(v);
    if (u != v) {
      parent[u] = v;
      return true;
    }
    return false;
  }

private:
  vector<int> parent;
};

class Solution {
    int countComponents1(int n, vector<pair<int, int>>& edges) {
      if (edges.empty()) {
        return n;
      }

      DisjointSet dset(n);
      for (auto& e : edges) {
        int u = e.first, v = e.second;
        dset.merge(u, v);
      }

      vector<bool> island(n, false);
      for (int i = 0; i < n; ++i) {
        island[dset.find(i)] = true;
      }
      int ret = 0;
      for (int i = 0; i < n; ++i) {
        ret += island[i];
      }
      return ret;
    }

public:
    int countComponents(int n, vector<pair<int, int>>& edges) {
      return countComponents1(n, edges);
    }
};
