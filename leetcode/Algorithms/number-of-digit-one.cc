#include <iostream>

using namespace std;

class Solution {
public:
  int countDigitOne(int n) {
    if (n <= 0) {
      return 0;
    }
    int res = 0;

    int hpart = 0, lpart = 0, b = 1, cur;
    while (n) { // ?
      hpart = n / 10;
      cur = n % 10;

      if (cur == 0) {
        res += hpart * b;
      } else if (cur == 1) {
        res += hpart * b;
        res += lpart + 1;
      } else { // cur > 1
        res += (hpart + 1) * b;
      }

      lpart = cur * b + lpart;
      n /= 10;
      b *= 10;
    }

    return res;
  }
};

int main() {
  Solution sol;
  int res = sol.countDigitOne(13);
  cout << res << endl;
  return 0;
}
