#include <vector>

using namespace std;

class DisjointSet {
public:
  explicit DisjointSet(int n) {
    parent.resize(n);
    for (int i = 0; i < n; ++i) {
      parent[i] = i;
    }
  }

  int find(int u) {
    if (parent[u] == u) {
      return u;
    }
    return parent[u] = find(parent[u]);
  }

  int merge(int u, int v) {
    u = find(u);
    v = find(v);
    if (u != v) {
      parent[u] = v;
      return true;
    }
    return false;
  }

private:
  vector<int> parent;
};

const vector<int> dx = {0, 0, -1, 1};
const vector<int> dy = {-1, 1, 0, 0};

class Solution {
  bool inside(int i, int j, int m, int n) {
    return i >= 0 && i < m && j >= 0 && j < n;
  }

public:
  vector<int> numIslands2(int m, int n, vector<pair<int, int>>& positions) {
    if (m * n == 0) {
      return {};
    }

    DisjointSet dset(m * n);
    vector<vector<int>> matrix(m, vector<int>(n, 0));
    vector<int> ret;
    int cur = 0;
    for (auto& pos : positions) {
      int i = pos.first, j = pos.second;
      if (matrix[i][j]) {
        continue;
      }
      matrix[i][j] = 1;
      cur += 1;
      for (int k = 0; k < 4; ++k) {
        int ni = i + dx[k];
        int nj = j + dy[k];
        if (inside(ni, nj, m, n) && matrix[ni][nj]) {
          int flag = dset.merge(i * n + j, ni * n + nj);
          if (flag) {
            cur -= 1;
          }
        }
      }
      ret.push_back(cur);
    }

    return ret;
  }
};

int main() {
  return 0;
}
