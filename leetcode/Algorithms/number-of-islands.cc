#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int dx[] = {-1, 1, 0, 0};
int dy[] = {0, 0, -1, 1};

class Solution {
  bool inGrid(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

  void dfs(int x, int y, vector<vector<char>>& grid, vector<vector<bool>>& visit) {
    visit[x][y] = true;

    for (int i = 0; i < 4; ++i) {
      int nx = x + dx[i];
      int ny = y + dy[i];
      if (inGrid(nx, ny, grid.size(), grid[0].size()) && !visit[nx][ny]
        && grid[nx][ny] == '1') {
        dfs(nx, ny, grid, visit);
      }
    }
  }

  void bfs(int x, int y, vector<vector<char>>& grid, vector<vector<bool>>& visit) {
    queue<pair<int, int>> que;

    que.push({x, y});
    visit[x][y] = true;
    while (!que.empty()) {
      x = que.front().first;
      y = que.front().second;
      que.pop();

      for (int i = 0; i < 4; ++i) {
        int nx = x + dx[i];
        int ny = y + dy[i];
        if (inGrid(nx, ny, grid.size(), grid[0].size()) && !visit[nx][ny]
          && grid[nx][ny] == '1') {
          visit[nx][ny] = true;
          que.push({nx, ny});
        }
      }
    }
  }

  int numIslands1(vector<vector<char>>& grid) {
    int n = grid.size(), m = grid[0].size();
    vector<vector<bool>> visit(n, vector<bool>(m, false));

    int res = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (!visit[i][j] && grid[i][j] == '1') {
          //dfs(i, j, grid, visit);
          bfs(i, j, grid, visit);
          ++res;
        }
      }
    }

    return res;
  }

public:
  int numIslands(vector<vector<char>>& grid) {
    if (!grid.size()) {
      return 0;
    }
    return numIslands1(grid);
  }
};

int main() {
  return 0;
}
