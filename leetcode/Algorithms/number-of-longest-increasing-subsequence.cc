#include "leetcode.h"

class Solution {
  int findNumberOfLIS1(vector<int>& nums) {
    int n = (int)nums.size();
    if (n < 2) {
      return n;
    }
    vector<unordered_map<int, int> > dp(n);
    int maxlen = 1, count = 1;

    for (int i = 0; i < n; ++i) {
      dp[i][1] = 1;
    }

    for (int i = 1; i < n; ++i) {
      int submaxlen = 1, subcount = 1;
      for (int j = 0; j < i; ++j) {
        if (nums[j] >= nums[i]) {
          continue;
        }

        for (auto& mp : dp[j]) {
          int next = mp.first + 1;
          if (!dp[i].count(next)) {
            dp[i][next]  = mp.second;
          } else {
            dp[i][next] += mp.second;
          }

          int cur = dp[i][next];
          if (next >= submaxlen) {
            submaxlen = next;
            subcount = cur;
          }
        }
      }
      if (submaxlen == maxlen) {
        count += subcount;
      } else if (submaxlen > maxlen) {
        maxlen = submaxlen;
        count = subcount;
      }
    }
    return count;
  }

public:
  int findNumberOfLIS(vector<int>& nums) {
    return findNumberOfLIS1(nums);
  }
};

int main() {
  return 0;
}
