class Solution {
  int countSegments1(string s) {
    int ret = 0, n_space = 0;
    int i = 0;

    for (int i = 0; i < (int)s.size(); ++i) {
      if (i > n_space and s[i] != ' ' and s[i - 1] == ' ') {
        ++ret;
      }
      n_space += s[i] == ' ';
    }
    return n_space == (int)s.size() ? 0 : ret + 1;
  }

public:
  int countSegments(string s) {
    return countSegments1(s);
  }
};
