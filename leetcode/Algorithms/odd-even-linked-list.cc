#include "leetcode.h"

class Solution {
public:
  ListNode* oddEvenList(ListNode* head) {
    if (!head || !head->next) {
      return head;
    }
    ListNode odummy(0), edummy(0), *optr = &odummy,
             *eptr = &edummy;
    ListNode *cur = head;
    int i = 0;
    while (cur) {
      if (i & 1) {
        eptr->next = cur;
        eptr = eptr->next;
      } else {
        optr->next = cur;
        optr = optr->next;
      }
      cur = cur->next;
      ++i;
    }

    optr->next = edummy.next;
    eptr->next = nullptr;
    return head;
  }
};

int main() {
  int a[] = {1, 2, 3, 4};
  ListNode* head = convert_to_list(a, 4);
  Solution sol;
  head = sol.oddEvenList(head);
  print_list(head);

  return 0;
}
