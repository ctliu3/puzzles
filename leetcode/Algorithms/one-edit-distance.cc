#include "leetcode.h"

class Solution {
public:
  bool isOneEditDistance(string s, string t) {
    if (s.size() > t.size()) {
      swap(s, t);
    }
    int ns = (int)s.size();
    int nt = (int)t.size();

    int diff = nt - ns;
    if (diff > 1) {
      return false;
    }

    int count = 0;
    for (int i = 0; i < nt; ++i) {
      if (t[i] != s[i - diff * count]) {
        ++count;
        if (count > 1) {
          return false;
        }
      }
    }

    return count == 1;
  }
};

int main() {
  return 0;
}
