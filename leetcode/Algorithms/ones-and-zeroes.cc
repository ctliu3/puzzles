#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Solution {
  // Use 3D dp array will lead to MLE
  int findMaxForm1(vector<string>& strs, int m, int n) {
    int size = (int)strs.size();
    vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));

    vector<pair<int, int>> count(strs.size());
    for (int i = 0; i < size; ++i) {
      int n0 = 0, n1 = 0;
      for (auto c : strs[i]) {
        c == '0' ? ++n0 : ++n1;
      }
      count[i] = {n0, n1};
    }

    for (int k = 1; k <= size; ++k) {
      auto& cur = count[k - 1];
      for (int i = m; i >= 0; --i) {
        for (int j = n; j >= 0; --j) {
          if (i - cur.first >= 0 and j - cur.second >= 0) {
            int val = dp[i - cur.first][j - cur.second];
            dp[i][j] = max(dp[i][j], val + 1);
          }
        }
      }
    }

    return dp[m][n];
  }

public:
  int findMaxForm(vector<string>& strs, int m, int n) {
    return findMaxForm1(strs, m, n);
  }
};

int main() {
  vector<string> strs = {"10", "0001", "111001", "1", "0"};
  Solution sol;
  int ret = sol.findMaxForm(strs, 5, 3);
  cout << ret << endl;
}
