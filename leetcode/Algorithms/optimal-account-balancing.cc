#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
  int minTransfers1(vector<vector<int>>& transactions) {
    unordered_map<int, int> mp;
    for (int i = 0; i < (int)transactions.size(); ++i) {
      auto& cur = transactions[i];
      mp[cur[0]] += cur[2];
      mp[cur[1]] -= cur[2];
    }

    vector<int> nums;
    for (auto& p : mp) {
      if (p.second != 0) {
        nums.push_back(p.second);
      }
    }
    if (nums.empty()) {
      return 0;
    }

    int n = (int)nums.size();
    vector<int> dp(1 << n, INT_MAX / 2);
    for (int i = 1; i < (1 << n); ++i) {
      int cnt = 0;
      int sum = 0;
      for (int j = 0; j < n; ++j) {
        if ((1 << j) & i) {
          sum += nums[j];
          ++cnt;
        }
      }
      if (sum == 0) {
        dp[i] = min(dp[i], cnt - 1);
        for (int j = 0; j < i; ++j) {
          if ((j & i) == j && dp[j] + dp[i - j] < dp[i]) {
            dp[i] = dp[j] + dp[i - j];
          }
        }
      }
    }
    return dp[(1 << n) - 1];
  }

public:
  int minTransfers(vector<vector<int>>& transactions) {
    return minTransfers1(transactions);
  }
};
