#include "leetcode.h"

const int MOD = 1000000000 + 7;

const int dx[] = {-1, 1, 0, 0};
const int dy[] = {0, 0, -1, 1};

struct Node {
  int x;
  int y;
  int step;
  int dir;
  Node(int x, int y, int step, int dir): x(x), y(y), step(step), dir(dir) { 
  }
};

class Solution {
  int inside(int x, int y, int m, int n) {
    return x >= 0 && x < m && y >= 0 && y < n;
  }

public:
  int findPaths(int m, int n, int N, int i, int j) {
    if (N == 0) {
      return 0;
    }
    queue<Node> que;
    vector<vector<vector<int> > > dp(m, vector<vector<int> >(n, vector<int>(N, 0)));
    que.push(Node(i, j, 0, -1));
    dp[i][j][0] = 1;
    while (!que.empty()) {
      auto cur = que.front();
      que.pop();
      for (int i = 0; i < 4; ++i) {
        int nx = cur.x + dx[i];
        int ny = cur.y + dy[i];
        if (inside(nx, ny, m, n) && cur.step + 1 <= N - 1) {
          bool first = dp[nx][ny][cur.step + 1] == 0;
          dp[nx][ny][cur.step + 1] += dp[cur.x][cur.y][cur.step];
          if (dp[nx][ny][cur.step + 1] >= MOD) {
            dp[nx][ny][cur.step + 1] -= MOD;
          }
          if (first) {
            que.push(Node(nx, ny, cur.step + 1, i));
          }
        }
      }
    }
    
    int ans = 0;
    for (int i = 0; i < m; ++i) {
      for (int j = 0; j < n; ++j) {
        int count = 0;
        count += i == 0;
        count += i == m - 1;
        count += j == 0;
        count += j == n - 1;
        for (int k = 0; k < N; ++k) {
          ans = (ans + (dp[i][j][k] * count) % MOD) % MOD;
        }
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
