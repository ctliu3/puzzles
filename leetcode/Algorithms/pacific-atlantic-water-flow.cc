#include <vector>

using namespace std;

const int dx[] = {0, 0, -1, 1};
const int dy[] = {1, -1, 0, 0};

class Solution {
  int inBound(int n, int m, int i, int j) {
    return i >= 0 && i < n && j >= 0 && j < m;
  }

  void dfs1(vector<vector<int>>& matrix, vector<vector<int>>& pa, int i, int j) {
    if (i == 0 || j == 0) {
      pa[i][j] = 1;
      return ;
    }
    if (pa[i][j] == 1) {
      return ;
    }
    pa[i][j] = 0;
    for (int k = 0; k < 4; ++k) {
      int ni = i + dx[k];
      int nj = j + dy[k];
      if (!inBound(matrix.size(), matrix[0].size(), ni, nj) || matrix[i][j] < matrix[ni][nj]) {
        continue;
      }
      if (pa[ni][nj] == 0) {
        continue;
      }
      dfs1(matrix, pa, ni, nj);
      if (pa[ni][nj] == 1) {
        pa[i][j] = 1;
      }
    }
    if (pa[i][j] == 0) {
      pa[i][j] = -1;
    }
  }

  void dfs2(vector<vector<int>>& matrix, vector<vector<int>>& at, int i, int j) {
    if (i == matrix.size() - 1 || j == matrix[0].size() - 1) {
      at[i][j] = 1;
      return ;
    }
    if (at[i][j] == 1) {
      return ;
    }
    at[i][j] = 0;
    for (int k = 0; k < 4; ++k) {
      int ni = i + dx[k];
      int nj = j + dy[k];
      if (!inBound(matrix.size(), matrix[0].size(), ni, nj) || matrix[i][j] < matrix[ni][nj]) {
        continue;
      }
      if (at[ni][nj] == 0) {
        continue;
      }
      dfs2(matrix, at, ni, nj);
      if (at[ni][nj] == 1) {
        at[i][j] = 1;
      }
    }
    // not sure why I should add this.
    if (at[i][j] == 0) {
      at[i][j] = -1;
    }
  }

  vector<pair<int, int>> pacificAtlantic1(vector<vector<int>>& matrix) {
    if (matrix.empty()) {
      return {};
    }
    int n = matrix.size(), m = (int)matrix[0].size();
    vector<vector<int>> pa(n, vector<int>(m, -1));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (pa[i][j] < 1) {
          dfs1(matrix, pa, i, j);
        }
      }
    }

    vector<vector<int>> at(n, vector<int>(m, -1));
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (at[i][j] < 1) {
          dfs2(matrix, at, i, j);
        }
      }
    }

    vector<pair<int, int>> ret;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (pa[i][j] == 1 && at[i][j] == 1) {
          ret.push_back({i, j});
        }
      }
    }
    return ret;
  }

public:
  vector<pair<int, int>> pacificAtlantic(vector<vector<int>>& matrix) {
    return pacificAtlantic1(matrix);
  }
};
