#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Solution {
public:
  int numWays(int n, int k) {
    if (n == 0 || k == 0) {
      return 0;
    }
    if (n > 2 && k == 1) {
      return 0;
    }
    if (n <= 2) {
      return pow(k, n);
    }

    vector<vector<int>> dp(n, vector<int>(2, -1));
    dp[0][0] = 0;
    dp[0][1] = k;
    for (int i = 1; i < n; ++i) {
      dp[i][0] = dp[i - 1][1];
      dp[i][1] = (dp[i - 1][0] + dp[i - 1][1]) * (k - 1);
    }

    return dp[n - 1][0] + dp[n - 1][1];
  }
};

int main() {
  Solution sol;
  int ret = sol.numWays(3, 3);
  cout << ret << endl;
  return 0;
}
