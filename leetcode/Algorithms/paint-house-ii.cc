#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Solution {
public:
  int minCostII(vector<vector<int>>& costs) {
    if (costs.empty()) {
      return 0;
    }
    int n = (int)costs.size(), k = (int)costs[0].size();
    vector<bool> visit(n * k, false);
    priority_queue<pair<int, int>> pq; // {cost, node_id}

    for (int i = 0; i < k; ++i) {
      pq.push({-costs[0][i], i});
    }

    while (!pq.empty()) {
      int cost = -pq.top().first;
      int u = pq.top().second;
      pq.pop();

      if (visit[u]) {
        continue;
      }
      visit[u] = true;
      if (u >= k * (n - 1)) {
        return cost;
      }
      int c = u / k, r = u % k;
      for (int i = 0; i < k; ++i) {
        if (r != i && !visit[(c + 1) * k + i]) {
          pq.push({-cost - costs[c + 1][i], (c + 1) * k + i});
        }
      }
    }

    return 0;
  }
};

int main() {
  vector<vector<int>> costs = {{1,2},{5,9},{3,4}};
  Solution sol;
  int ret = sol.minCostII(costs);
  cout << ret << endl;

  return 0;
}
