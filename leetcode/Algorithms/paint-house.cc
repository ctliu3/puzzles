#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  int minCost(vector<vector<int>>& costs) {
    if (costs.empty()) {
      return 0;
    }

    int n = (int)costs.size();
    vector<vector<int>> dp(n, vector<int>(3, -1));
    for (int i = 0; i < 3; ++i) {
      dp[0][i] = costs[0][i];
    }

    for (int i = 1; i < n; ++i) {
      for (int j = 0; j < 3; ++j) {
        int& val = dp[i][j];
        for (int k = 0; k < 3; ++k) {
          if (j == k) {
            continue;
          }
          if (val == -1 || dp[i - 1][k] + costs[i][j] < val) {
            val = dp[i - 1][k] + costs[i][j];
          }
        }
      }
    }

    return *min_element(dp[n - 1].begin(), dp[n - 1].end());
  }
};

int main() {
  return 0;
}
