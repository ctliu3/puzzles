#include "leetcode.h"

class Solution {

  void _reverse(ListNode* head) {
    ListNode* cur = head->next;

    while (cur && cur->next) {
      ListNode* move = cur->next;
      cur->next = move->next;
      move->next = head->next;
      head->next = move;
    }
  }

public:
  bool isPalindrome(ListNode* head) {
    if (!head || !head->next) {
      return true;
    }
    ListNode* slow = head, *fast = head;
    while (fast->next && fast->next->next) {
      slow = slow->next;
      fast = fast->next->next;
    }
    _reverse(slow);

    ListNode* former = head, *latter = slow->next;
    while (former && latter) {
      if (former->val != latter->val) {
        return false;
      }
      former = former->next;
      latter = latter->next;
    }
    return true;
  }
};

int main() {
  //int a[] = {1, 2, 3};
  int a[] = {1, 2, 2, 1};
  ListNode* head = convert_to_list(a, 4);

  Solution sol;
  bool res = sol.isPalindrome(head);
  cout << res << endl;

  return 0;
}
