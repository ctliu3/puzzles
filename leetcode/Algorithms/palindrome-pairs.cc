#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
  struct TrieNode {
    char* next[52];
    int end;
    explicit TrieNode(): end(-1) {
    }
  };

  int getidx(char c) {
    if (c >= 'a' and c <= 'z') {
      return c - 'a';
    }
    return c - 'a' + 26;
  }

  void find(TrieNode& cur, string& word, int i, vector<vector<int>>& ret) {
    for (char& c : word) {
      int idx = getidx(c);
      if (!cur.next[idx]) {
        return ;
      }
      if (cur.end != -1) {
      }
    }
  }

  void insert(TrieNode& cur, string& word, vector<vector<int>>& ret) {
  }

  vector<vector<int>> palindromePairs1(vector<string>& words) {
    if (words.size() < 2) {
      return {};
    }

    TrieNode root;
    vector<vector<int>> ret;
    for (size_t i = 0; i < words.size(); ++i) {
      string rword = string(words[i].rbegin(), words[i].rbegin());
      find(root, words[i], i, ret);
      insert(root, words[i], ret);
    }

    return ret;
  }

public:
  vector<vector<int>> palindromePairs(vector<string>& words) {
    return palindromePairs1(words);
  }
};

int main() {
  return 0;
}
