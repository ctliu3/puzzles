#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
  void dfs(vector<int>& freq, int dep, string& perm,
           vector<string>& ret) {
    if (dep == perm.size() / 2) {
      ret.push_back(perm);
      return ;
    }

    for (int i = 0; i < (int)freq.size(); ++i) {
      if (!freq[i]) {
        continue;
      }
      --freq[i];
      perm[dep] = perm[perm.size() - 1 - dep] = i;
      dfs(freq, dep + 1, perm, ret);
      ++freq[i];
    }
  }

public:
  vector<string> generatePalindromes(string s) {
    if (s.empty()) {
      return {};
    }
    vector<int> freq(256, 0);
    for (auto& chr : s) {
      ++freq[chr];
    }
    int count = 0;
    char pivot;
    for (int i = 0; i < 256; ++i) {
      if (freq[i] & 1) {
        pivot = i;
        ++count;
        --freq[i];
      }
      freq[i] >>= 1;
    }
    if (count > 1) {
      return {};
    }

    int n = s.size();
    vector<string> ret;
    string perm(n, ' ');
    if (count) {
      perm[n / 2] = pivot;
    }
    dfs(freq, 0, perm, ret);
    return ret;
  }
};

int main() {
  Solution sol;
  auto ret = sol.generatePalindromes("aaaa");
  for (auto& str : ret) {
    cout << str << endl;
  }
  return 0;
}
