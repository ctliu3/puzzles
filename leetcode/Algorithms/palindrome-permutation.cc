class Solution {
public:
  bool canPermutePalindrome(string s) {
    vector<int> freq(256, 0);
    for (char& chr : s) {
      ++freq[chr];
    }
    int count = 0;
    for (auto& num : freq) {
      if (num & 1) {
        ++count;
      }
    }
    if (s.size() & 1) {
      return count == 1;
    } else {
      return count == 0;
    }
  }
};
