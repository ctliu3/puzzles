#include "leetcode.h"

class Solution {
public:
  int countSubstrings(string s) {
    int n = (int)s.size();
    vector<vector<int> > dp(n, vector<int>(n, 0));
    int ans = n;
    for (int i = 0; i < n; ++i) {
      dp[i][i] = 1;
    }
    for (int len = 2; len <= n; ++len) {
      for (int i = 0; i + len - 1 < n; ++i) {
        int j = i + len - 1;
        if (len == 2) {
          dp[i][j] = s[i] == s[j] ? 1 : 0;
          if (s[i] == s[j]) {
            ans += 1;
          }
        } else {
          if (s[i + 1] == s[j - 1] && s[i] == s[j]) {
            dp[i][j] = dp[i + 1][j - 1];
            ans += dp[i][j];
          }
        }
      }
    }
    return ans;
  }
};


int main() {
  return 0;
}
