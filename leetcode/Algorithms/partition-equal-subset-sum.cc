#include <vector>
#include <iostream>

using namespace std;

class Solution {
  bool canPartition1(vector<int>& nums) {
    if (nums.size() < 2) {
      return false;
    }
    int sum = 0;
    for (auto val : nums) {
      sum += val;
    }
    if (sum & 1) {
      return false;
    }

    int n = (int)nums.size();
    int m = sum / 2;
    vector<vector<bool>> dp(n, vector<bool>(m + 1, false));
    dp[0][nums[0]] = true;
    for (int i = 1; i < n; ++i) {
      for (int j = 0; j <= m; ++j) {
        dp[i][j] = dp[i - 1][j];
        if (j - nums[i] >= 0 and dp[i - 1][j - nums[i]]) {
          dp[i][j] = true;
        }
        if (dp[i][m]) {
          return true;
        }
      }
    }
    return false;
  }

  bool canPartition2(vector<int>& nums) {
    if (nums.size() < 2) {
      return false;
    }
    int sum = 0;
    for (auto val : nums) {
      sum += val;
    }
    if (sum & 1) {
      return false;
    }

    int n = (int)nums.size();
    int m = sum / 2;
    vector<bool> dp(m + 1, false);
    for (int i = 0; i < n; ++i) {
      for (int j = m; j >= 0; --j) {
        if (j - nums[i] >= 0 and dp[j - nums[i]]) {
          dp[j] = true;
        }
        if (dp[m]) {
          return true;
        }
      }
      dp[nums[i]] = true;
    }
    return false;
  }

public:
  bool canPartition(vector<int>& nums) {
    // return canPartition1(nums);
    return canPartition2(nums);
  }
};

int main() {
  vector<int> input = {1, 3, 2, 1, 1};
  Solution sol;
  auto ret = sol.canPartition(input);
  cout << ret << endl;

  return 0;
}
