#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution {
public:
  int minPatches(vector<int>& nums, int n) {
    int i = 0, ret = 0;
    long long miss = 1;
    while (miss <= n) {
      if (i < (int)nums.size() and nums[i] <= miss) {
        miss += nums[i++];
      } else {
        ++ret;
        miss *= 2;
      }
    }
    return ret;
  }
};

int main() {
  vector<int> nums = {1};
  int n = 2;
  Solution sol;
  int ret = sol.minPatches(nums, n);
  cout << ret << endl;

  return 0;
}
