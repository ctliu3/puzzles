#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, unordered_map<int, int>& counter, int target, int sum, int& ret) {
    if (!cur) {
      return ;
    }

    sum += cur->val;
    auto p = counter.find(sum - target);
    if (p != counter.end()) {
      ret += p->second;
    }
    ++counter[sum];

    if (cur->left) {
      dfs1(cur->left, counter, target, sum, ret);
    }
    if (cur->right) {
      dfs1(cur->right, counter, target, sum, ret);
    }
    --counter[sum];
  }

  int pathSum1(TreeNode* root, int target) {
    if (!root) {
      return 0;
    }
    unordered_map<int, int> counter;
    int ret = 0, sum = 0;
    counter[0] = 1;
    dfs1(root, counter, target, sum, ret);
    return ret;
  } 

public:
  int pathSum(TreeNode* root, int sum) {
    return pathSum1(root, sum);
  }
};

int main() {
  string tree = "1";
  TreeNode* root = getTree(tree);

  Solution sol;
  auto ret = sol.pathSum(root, 0);
  cout << ret << endl;
}
