#include "leetcode.h"

class Solution {
  int mid1(int num) {
    return num % 100 / 10;
  }

  int pathSum1(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    vector<int> sum(nums.size(), 0), count(nums.size(), 0);
    for (int i = (int)nums.size() - 1; i >= 0; --i) {
      if (count[i] == 0) {
        count[i] = 1;
      }
      int d = nums[i] / 100;
      int p = mid1(nums[i]);
      int v = nums[i] % 10;
      int parent = (p + 1) / 2;
      if (i == 0) {
        sum[0] += count[0] * v;
      }
      for (int j = i - 1; j >= 0; --j) {
        if (d - 1 == nums[j] / 100) {
          if (mid1(nums[j]) == parent) {
            sum[j] += sum[i] + count[i] * v;
            count[j] += count[i];
          }
        } else if (d - 1 > nums[j] / 100) {
          break;
        }
      }
    }

    return sum[0];
  }

public:
  int pathSum(vector<int>& nums) {
    return pathSum1(nums);
  }
};

int main() {
  return 0;
}
