#include <iostream>
#include <vector>

using namespace std;

// Below is the interface for Iterator, which is already defined for you.
// **DO NOT** modify the interface for Iterator.
class Iterator {
  struct Data;
  Data* data;
public:
  Iterator(const vector<int>& nums);
  Iterator(const Iterator& iter);
  virtual ~Iterator();
  // Returns the next element in the iteration.
  int next();
  // Returns true if the iteration has more elements.
  bool hasNext() const;
};


class PeekingIterator : public Iterator {
  bool has_peek_;
  int peek_elem_;

public:
  PeekingIterator(const vector<int>& nums) : Iterator(nums) {
    // Initialize any member here.
    // **DO NOT** save a copy of nums and manipulate it directly.
    // You should only use the Iterator interface methods.
    has_peek_ = false;
  }

  // Returns the next element in the iteration without advancing the iterator.
  int peek() {
    peek_elem_ = has_peek_ ? peek_elem_ : next();
    has_peek_ = true;
    return peek_elem_;
  }

  // hasNext() and next() should behave the same as in the Iterator interface.
  // Override them if needed.
  int next() {
    int next_element = has_peek_ ? peek_elem_ : Iterator::next();
    has_peek_ = false;
    return next_element;
  }

  bool hasNext() const {
    return has_peek_ || Iterator::hasNext();
  }
};

int main() {
  return 0;
}
