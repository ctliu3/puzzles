#include "leetcode.h"

struct PairHash {
  size_t operator()(const pair<int, int>& rhs) const {
    size_t l = hash<int>()(rhs.first);
    size_t r = hash<int>()(rhs.second);
    return l + 0x9e3779b9 + (r << 6) + (r >> 2);
  }
};

class Solution {
  bool isRectangleCover1(vector<vector<int>>& rectangles) {
    int x1 = INT_MAX;
    int y1 = INT_MAX;
    int x2 = INT_MIN;
    int y2 = INT_MIN;

    unordered_map<pair<int, int>, int, PairHash> mp;
    int area = 0;
    for (auto& rect : rectangles) {
      x1 = min(x1, rect[0]);
      y1 = min(y1, rect[1]);
      x2 = max(x2, rect[2]);
      y2 = max(y2, rect[3]);
      area += (rect[2] - rect[0]) * (rect[3] - rect[1]);

      mp[{rect[0], rect[1]}] += 1;
      mp[{rect[2], rect[1]}] += 1;
      mp[{rect[2], rect[3]}] += 1;
      mp[{rect[0], rect[3]}] += 1;
    }
    if (mp[{x1, y1}] != 1) {
      return false;
    }
    if (mp[{x2, y2}] != 1) {
      return false;
    }
    if (mp[{x2, y1}] != 1) {
      return false;
    }
    if (mp[{x1, y2}] != 1) {
      return false;
    }

    int count = 0;
    for (auto& itr : mp) {
      if (itr.second > 1) {
        if (itr.second & 1) {
          return false;
        }
      } else {
        count += 1;
        if (count > 4) {
          return false;
        }
      }
    }
    return area == (x2 - x1) * (y2 - y1);
  }

public:
  bool isRectangleCover(vector<vector<int>>& rectangles) {
    return isRectangleCover1(rectangles);
  }
};

int main() {
  return 0;
}
