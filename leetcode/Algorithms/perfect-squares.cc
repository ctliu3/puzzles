#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;

class Solution {
  int treedp1(int n, vector<int>& dp) {
    if (dp[n] != -1) {
      return dp[n];
    }
    int minval = INT_MAX;
    for (int i = 1; (ll) i * i <= (ll)n; ++i) {
      minval = min(minval, treedp1(n - i * i, dp) + 1);
    }
    return dp[n] = minval;
  }

  int numSquares1(int n) {
    vector<int> dp(n + 1, -1);
    dp[0] = 0;
    for (int i = 1; (ll) i * i <= (ll)n; ++i) {
      dp[i * i] = 1;
    }
    return treedp1(n, dp);
  }

public:
  int numSquares(int n) {
    return numSquares1(n);
  }
};

int main() {
  Solution sol;
  int ret = sol.numSquares(12);
  cout << ret << endl;

  return 0;
}
