#include "leetcode.h"

class Solution {
  bool checkInclusion1(const string& s1, const string& s2) {
    if (s1.size() > s2.size()) {
      return false;
    }
    int n1 = s1.size();
    int n2 = s2.size();
    vector<int> need(26, 0);
    for (auto& c : s1) {
      need[c - 'a'] += 1;
    }
    vector<int> counter(26, 0);
    int total = 0;
    int start = 0;
    for (int i = 0; i < n2; ++i) {
      int index = s2[i] - 'a'; 
      ++counter[index];
      ++total;
      while (start <= i && counter[index] > need[index]) {
        --counter[s2[start] - 'a'];
        --total;
        ++start;
      }
      if (total == n1) {
        return true;
      }
    }
    return false;
  }

public:
  bool checkInclusion(string s1, string s2) {
    return checkInclusion1(s1, s2);
  }
};
