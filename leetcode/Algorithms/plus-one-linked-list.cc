class Solution {
  int dfs1(ListNode* cur) {
    if (cur && !cur->next) {
      int tmp = cur->val + 1;
      cur->val = tmp % 10;
      return tmp / 10;
    }
    int carry = dfs1(cur->next);
    int tmp = cur->val + carry;
    cur->val = tmp % 10;
    return tmp / 10;
  }

  ListNode* plusOne1(ListNode* head) {
    int carry = dfs1(head);
    if (!carry) {
      return head;
    }
    ListNode* newhead = new ListNode(1);
    newhead->next = head;
    return newhead;
  }

public:
  ListNode* plusOne(ListNode* head) {
    return plusOne1(head);
  }
};
