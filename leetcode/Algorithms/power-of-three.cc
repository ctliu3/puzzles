#include <iostream>
#include <cmath>

class Solution {
  bool isPowerOfThree1(int n) {
    return n > 0 and (n == 1 or (n % 3 == 0 and isPowerOfThree1(n / 3)));
  }

  bool isPowerOfThree2(int n) {
    while (n > 1) {
      if (n % 3) {
        return false;
      }
      n /= 3;
    }
    return n == 1;
  }

  bool isPowerOfThree3(int n) {
    if (n < 0) {
      return false;
    }
    double val = log10(n * 1.0) / log10(3);
    return ceil(val) - floor(val) < 1e-9;
  }

public:
  bool isPowerOfThree(int n) {
    // return isPowerOfThree1(n);
    // return isPowerOfThree2(n);
    return isPowerOfThree3(n);
  }
};

int main() {
  return 0;
}
