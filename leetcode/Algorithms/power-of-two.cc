#include <iostream>

using namespace std;

class Solution {
public:
  bool isPowerOfTwo(int n) {
    if (n <= 0) {
      return false;
    }
    if (n == 1) {
      return true;
    }
    return (n & (n - 1)) == 0;
  }
};

int main() {
  return 0;
}
