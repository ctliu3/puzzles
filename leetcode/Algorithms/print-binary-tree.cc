#include "leetcode.h"

class Solution {
  int getTreeHight(TreeNode* cur) {
    if (!cur) {
      return 0;
    }
    return max(getTreeHight(cur->left), getTreeHight(cur->right)) + 1;
  }

  void dfs1(TreeNode* cur, int l, int r, int dep, vector<vector<string> >& output) {
    if (!cur) {
      return;
    }
    int mid = l + (r - l) / 2;
    output[dep][mid] = std::to_string(cur->val);
    dfs1(cur->left, l, mid - 1, dep + 1, output);
    dfs1(cur->right, mid + 1, r, dep + 1, output);
  }
  
  vector<vector<string> > printTree1(TreeNode* root) {
    int h = getTreeHight(root);
    vector<vector<string> > ret(h, vector<string>(pow(2, h) - 1, ""));
    dfs1(root, 0, pow(2, h) - 1, 0, ret);
    return ret;
  }

public:
  vector<vector<string> > printTree(TreeNode* root) {
    return printTree1(root);
  }
};

int main() {
  return 0;
}
