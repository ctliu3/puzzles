#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> productExceptSelf(vector<int>& nums) {
    int n = (int)nums.size();
    if (n == 0) {
      return {};
    } else if (n == 1) {
      return {0};
    }

    vector<int> ret(n, 0);
    int product = 1;
    for (int i = 0; i < n; ++i) {
      ret[i] = product;
      product *= nums[i];
    }

    product = 1;
    for (int i = n - 1; i >= 0; --i) {
      ret[i] *= product;
      product *= nums[i];
    }
    return ret;
  }
};

int main() {
}
