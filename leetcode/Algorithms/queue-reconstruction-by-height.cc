#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class Solution {
public:
  vector<pair<int, int>> reconstructQueue(vector<pair<int, int>>& people) {
    vector<int> indices(people.size());
    for (int i = 0; i < (int)indices.size(); ++i) {
      indices[i] = i;
    }

    sort(indices.begin(), indices.end(), [&](const int lhs, const int rhs) {
      if (people[lhs].first == people[rhs].first) {
        return people[lhs].second < people[rhs].second;
      }
      return people[lhs].first < people[rhs].first;
    });

    vector<pair<int, int>> ret(people.size());
    vector<bool> stand(people.size(), false);
    for (int index : indices) {
      auto cur = people[index];
      int count = 0;
      for (int i = 0; i < (int)stand.size(); ++i) {
        if (count == cur.second and !stand[i]) {
          ret[i] = cur;
          stand[i] = true;
          break;
        }
        if (!stand[i] || cur.first <= ret[i].first) {
          ++count;
        }
      }
    }

    return ret;
  }
};

int main() {
  vector<pair<int, int>> people = {{7,0},{4,4},{7,1},{5,0},{6,1},{5,2}};
  Solution sol;
  sol.reconstructQueue(people);
}
