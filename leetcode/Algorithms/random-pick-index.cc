#include "leetcode.h"

// TODO
// 1. Try using equal_range
// 2. O(n) approach

class Solution {

  unordered_map<int, pair<int, int>> target_range;
  vector<pair<int, int>> nums2;

public:
  Solution(vector<int> nums) {
    nums2.resize(nums.size());
    for (int i = 0; i < (int)nums.size(); ++i) {
      nums2[i] = {nums[i], i};
    }
    sort(nums2.begin(), nums2.end());
  }

  int pick(int target) {
    auto cmp = [](const pair<int, int>& lhs, const pair<int, int>& rhs) {
      return lhs.first < rhs.first;
    };

    auto rng = target_range.find(target);
    if (rng == target_range.end()) {
      auto pa = lower_bound(nums2.begin(), nums2.end(), make_pair(target, 0), cmp);
      if (pa == nums2.end()) {
        target_range[target] = {-1, -1};
        return -1;
      } else {
        // If use lower_bound with key {target + 1, 0}, remember using long long.
        auto pb = upper_bound(nums2.begin(), nums2.end(), make_pair(target, 0), cmp) - 1;
        pair<int, int> range = {pa - nums2.begin(), pb - nums2.begin()};
        target_range[target] = range;
      }
    }
    rng = target_range.find(target);
    if (rng->second.first == -1) {
      return -1;
    }
    int idx = rng->second.first + (rand() % (rng->second.second - rng->second.first + 1));
    return nums2[idx].second;
  }
};

int main() {
  vector<int> nums = {1,2,3,3,3};
  Solution obj = Solution(nums);
  int index = obj.pick(3);
  cout << index << endl;
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int param_1 = obj.pick(target);
 */
