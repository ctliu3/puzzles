#include "leetcode.h"

class Solution {
public:
  int maxCount(int m, int n, vector<vector<int>>& ops) {
    int row_min = m;
    int col_min = n;
    for (auto& op : ops) {
      row_min = min(row_min, op[0]);
      col_min = min(col_min, op[1]);
    }
    return row_min * col_min;
  }
};

int main() {
  return 0;
}
