#include "leetcode.h"

class Solution {
public:
  vector<int> getModifiedArray(int length, vector<vector<int>>& updates) {
    vector<int> add(length, 0), minus(length, 0);
    for (auto& op : updates) {
      add[op[0]] += op[2];
      minus[op[1]] += op[2];
    }
    vector<int> ans(length, 0);
    int acc = 0;
    for (int i = 0; i < length; ++i) {
      acc += add[i];
      ans[i] = acc;
      acc -= minus[i];
    }

    return ans;
  }
};

int main() {
  return 0;
}
