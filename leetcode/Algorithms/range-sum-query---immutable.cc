#include <iostream>
#include <vector>

using namespace std;

class NumArray {
  vector<int> acc;

public:
  NumArray(vector<int> &nums) {
    if (nums.empty()) {
      return ;
    }

    acc.resize(nums.size());
    acc[0] = nums[0];
    for (int i = 1; i < (int)nums.size(); ++i) {
      acc[i] = acc[i - 1] + nums[i];
    }
  }

  int sumRange(int i, int j) {
    return acc[j] - (i == 0 ? 0 : acc[i - 1]);
  }
};


// Your NumArray object will be instantiated and called as such:
// NumArray numArray(nums);
// numArray.sumRange(0, 1);
// numArray.sumRange(1, 2);
//
int main() {
}
