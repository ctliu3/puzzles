#include <iostream>
#include <vector>

using namespace std;

class SegTree {
#define LL(x) ((x) << 1 | 1)
#define RR(x) ((LL(x)) + 1)
public:
  void init(const vector<int>& nums) {
    if (nums.empty()) {
      return ;
    }
    tree.resize(nums.size() * 4);
    build(0, 0, (int)nums.size() - 1, nums);
  }

  void build(int idx, int l, int r, const vector<int>& nums) {
    tree[idx].l = l, tree[idx].r = r;
    if (l == r) {
      tree[idx].val = nums[l];
      return ;
    }
    int mid = tree[idx].mid();
    build(LL(idx), l, mid, nums);
    build(RR(idx), mid + 1, r, nums);
    tree[idx].val = tree[LL(idx)].val + tree[RR(idx)].val;
  }

  void update(int idx, int id, int val) {
    if (tree[idx].l == tree[idx].r) {
      tree[idx].val = val;
      return ;
    }
    int mid = tree[idx].mid();
    if (id <= mid) {
      update(LL(idx), id, val);
    } else {
      update(RR(idx), id, val);
    }
    tree[idx].val = tree[LL(idx)].val + tree[RR(idx)].val;
  }

  int query(int idx, int l, int r) {
    if (l == tree[idx].l and r == tree[idx].r) {
      return tree[idx].val;
    }
    int mid = tree[idx].mid();
    if (r <= mid) {
      return query(LL(idx), l, r);
    } else if (l > mid) {
      return query(RR(idx), l, r);
    }
    return query(LL(idx), l, mid) + query(RR(idx), mid + 1, r);
  }

private:
  class SegTreeNode {
  public:
    int l;
    int r;
    int val;
    explicit SegTreeNode(int l = 0, int r = 0, int val = 0) {
      this->l = l;
      this->r = r;
      this->val = val;
    }
    int mid() {
      return (this->l + this->r) >> 1;
    }
  };
  vector<SegTreeNode> tree;
};

class NumArray {
  SegTree segtree;
public:
  NumArray(vector<int> &nums) {
    segtree.init(nums);
  }

  void update(int i, int val) {
    segtree.update(0, i, val);
  }

  int sumRange(int i, int j) {
    return segtree.query(0, i, j);
  }
};

int main() {
  vector<int> nums = {1, 3, 5};
  NumArray numArray(nums);
  numArray.sumRange(0, 2);
  numArray.update(1, 2);
  int ret = numArray.sumRange(0, 2);
  cout << ret << endl;
}

// Your NumArray object will be instantiated and called as such:
// NumArray numArray(nums);
// numArray.sumRange(0, 1);
// numArray.update(1, 10);
// numArray.sumRange(1, 2);
