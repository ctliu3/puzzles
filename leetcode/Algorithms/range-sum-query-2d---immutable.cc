#include <iostream>
#include <vector>

using namespace std;

class NumMatrix {
  vector<vector<int>> dp;

public:
  NumMatrix(vector<vector<int>> &matrix) {
    if (matrix.empty()) {
      return ;
    }

    int n = matrix.size(), m = matrix[0].size();
    dp.assign(n, vector<int>(m, 0));

    for (int i = 0; i < n; ++i) {
      int sum = 0;
      for (int j = 0; j < m; ++j) {
        if (i == 0 && j == 0) {
          dp[i][j] = matrix[i][j];
        } else {
          if (i - 1 >= 0) {
            dp[i][j] += dp[i - 1][j];
          }
          dp[i][j] += sum + matrix[i][j];
        }
        sum += matrix[i][j];
      }
    }
  }

  int sumRegion(int row1, int col1, int row2, int col2) {
    int ret = dp[row2][col2];
    if (row1 - 1 >= 0) {
      ret -= dp[row1 - 1][col2];
    }
    if (col1 - 1 >= 0) {
      ret -= dp[row2][col1 - 1];
    }
    if (row1 - 1 >= 0 && col1 - 1 >= 0) {
      ret += dp[row1 - 1][col1 - 1];
    }
    return ret;
  }
};


// Your NumMatrix object will be instantiated and called as such:
// NumMatrix numMatrix(matrix);
// numMatrix.sumRegion(0, 1, 2, 3);
// numMatrix.sumRegion(1, 2, 3, 4);

int main() {
  return 0;
}
