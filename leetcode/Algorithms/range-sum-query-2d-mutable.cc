#include <iostream>
#include <vector>

using namespace std;

class SegTree2D {

#define TOP_LT(x) ((x) * 4 + 1)
#define TOP_RT(x) ((x) * 4 + 2)
#define BOT_LT(x) ((x) * 4 + 3)
#define BOT_RT(x) ((x) * 4 + 4)

public:
  explicit SegTree2D() {
  }

  explicit SegTree2D(vector<vector<int>>& matrix) {
    init(matrix);
  }

  void init(vector<vector<int>>& matrix) {
    if (matrix.empty()) {
      return ;
    }
    int r = (int)matrix.size(), c = (int)matrix[0].size();
    tree.resize(r * c * 5);
    for (int i = 0; i < (int)tree.size(); ++i) {
      tree[i].val = 0;
    }
    build(0, 0, 0, r - 1, c - 1, matrix);
  }

  void build(int idx, int r1, int c1, int r2, int c2, vector<vector<int>>& matrix) {
    if (r1 > r2 or c1 > c2) {
      return ;
    }
    tree[idx].r1 = r1, tree[idx].c1 = c1;
    tree[idx].r2 = r2, tree[idx].c2 = c2;
    if (r1 == r2 and c1 == c2) {
      tree[idx].val = matrix[r1][c1];
      return ;
    }

    int mr, mc;
    tree[idx].mid(mr, mc);
    build(TOP_LT(idx), r1, c1, mr, mc, matrix);
    build(TOP_RT(idx), r1, mc + 1, mr, c2, matrix);
    build(BOT_LT(idx), mr + 1, c1, r2, mc, matrix);
    build(BOT_RT(idx), mr + 1, mc + 1, r2, c2, matrix);
    tree[idx].val = tree[TOP_LT(idx)].val + tree[TOP_RT(idx)].val
      + tree[BOT_LT(idx)].val + tree[BOT_RT(idx)].val;
  }

  void update(int idx, int r, int c, int value) {
    if (tree[idx].r1 == tree[idx].r2 and tree[idx].c1 == tree[idx].c2
      and tree[idx].r1 == r and tree[idx].c1 == c) {
      tree[idx].val = value;
      return ;
    }

    int r1 = tree[idx].r1, r2 = tree[idx].r2, c1 = tree[idx].c1, c2 = tree[idx].c2;
    int mr, mc;
    tree[idx].mid(mr, mc);
    if (intersect(r, c, r, c, r1, c1, mr, mc)) {
      update(TOP_LT(idx), r, c, value);
    }
    if (intersect(r, c, r, c, r1, mc + 1, mr, c2)) {
      update(TOP_RT(idx), r, c, value);
    }
    if (intersect(r, c, r, c, mr + 1, c1, r2, mc)) {
      update(BOT_LT(idx), r, c, value);
    }
    if (intersect(r, c, r, c, mr + 1, mc + 1, r2, c2)) {
      update(BOT_RT(idx), r, c, value);
    }
    tree[idx].val = tree[TOP_LT(idx)].val + tree[TOP_RT(idx)].val
      + tree[BOT_LT(idx)].val + tree[BOT_RT(idx)].val;
  }

  int query(int idx, int qr1, int qc1, int qr2, int qc2) {
    if (tree[idx].r1 >= qr1 and tree[idx].c1 >= qc1 and tree[idx].r2 <= qr2
      and tree[idx].c2 <= qc2) {
      return tree[idx].val;
    }

    int r1 = tree[idx].r1, r2 = tree[idx].r2, c1 = tree[idx].c1, c2 = tree[idx].c2;
    int mr, mc, sum = 0;
    tree[idx].mid(mr, mc);
    if (intersect(qr1, qc1, qr2, qc2, r1, c1, mr, mc)) {
      sum += query(TOP_LT(idx), qr1, qc1, qr2, qc2);
    }
    if (intersect(qr1, qc1, qr2, qc2, r1, mc + 1, mr, c2)) {
      sum += query(TOP_RT(idx), qr1, qc1, qr2, qc2);
    }
    if (intersect(qr1, qc1, qr2, qc2, mr + 1, c1, r2, mc)) {
      sum += query(BOT_LT(idx), qr1, qc1, qr2, qc2);
    }
    if (intersect(qr1, qc1, qr2, qc2, mr + 1, mc + 1, r2, c2)) {
      sum += query(BOT_RT(idx), qr1, qc1, qr2, qc2);
    }
    return sum;
  }

  bool intersect(int r1, int c1, int r2, int c2, int r3, int c3, int r4, int c4) {
    if (r2 < r3 or r1 > r4 or c2 < c3 or c1 > c4) {
      return false;
    }
    return true;
  }
  
private:

  struct SegNode2D {
    int r1, c1, r2, c2;
    int val;
    SegNode2D() {
    }
    void mid(int& r, int& c) {
      r = (r2 + r1) / 2;
      c = (c2 + c1) / 2;
    }
  };
  vector<SegNode2D> tree;
};

class NumMatrix {
  SegTree2D segtree;

public:
  NumMatrix(vector<vector<int>> &matrix) {
    segtree.init(matrix);
  }

  void update(int row, int col, int val) {
    segtree.update(0, row, col, val);
  }

  int sumRegion(int row1, int col1, int row2, int col2) {
    return segtree.query(0, row1, col1, row2, col2);
  }
};

int main() {
  vector<vector<int>> matrix = {
    {1},
    {2},
  };

  NumMatrix numMatrix(matrix);
  cout << numMatrix.sumRegion(0, 0, 0, 0) << endl;
  cout << numMatrix.sumRegion(1, 0, 1, 0) << endl;
  cout << numMatrix.sumRegion(0, 0, 1, 0) << endl;
  numMatrix.update(0, 0, 3);
  numMatrix.update(1, 0, 5);
  cout << numMatrix.sumRegion(0, 0, 1, 0) << endl;

  return 0;
}

// Your NumMatrix object will be instantiated and called as such:
// NumMatrix numMatrix(matrix);
// numMatrix.sumRegion(0, 1, 2, 3);
// numMatrix.update(1, 1, 10);
// numMatrix.sumRegion(1, 2, 3, 4);
