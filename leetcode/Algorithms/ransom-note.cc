class Solution {
public:
  bool canConstruct(string ransomNote, string magazine) {
    if (ransomNote.size() > magazine.size()) {
      return false;
    }
    vector<int> hash(256, 0);
    for (auto c : magazine) {
      ++hash[c];
    }
    for (auto c : ransomNote) {
      if (!hash[c]) {
        return false;
      }
      --hash[c];
    }
    return true;
  }
};
