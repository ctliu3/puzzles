#include "leetcode.h"

// Forward declaration of the read4 API.
int read4(char *buf);

class Solution {

  int remain;
  int buf_offset;
  char* buffer;

public:

  explicit Solution(): remain(0), buf_offset(0) {
    buffer = new char[4];
  }

  /**
   * @param buf Destination buffer
   * @param n   Maximum number of characters to read
   * @return    The number of characters read
   */
  int read(char *buf, int n) {
    int n_read = 0;
    int offset = 0;

    while (n_read < n) {
      int n_chr = 0;
      if (remain > 0) {
        n_chr = remain;
      } else {
        n_chr = read4(buffer);
        buf_offset = 0;
      }
      if (!n_chr) {
        break;
      }

      int n_cur = min(n - n_read, n_chr);
      memcpy(buf + offset, buffer + buf_offset, n_cur);
      offset += n_cur;
      n_read += n_cur;
      buf_offset += n_cur;
      remain = n_chr - n_cur;
    }

    buf[offset] = '\0';
    return n_read;
  }
};

int main() {
  return 0;
}
