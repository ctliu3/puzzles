#include "leetcode.h"

// Forward declaration of the read4 API.
int read4(char *buf);

class Solution {
public:
  /**
   * @param buf Destination buffer
   * @param n   Maximum number of characters to read
   * @return    The number of characters read
   */
  int read(char *buf, int n) {
    int n_read = 0, offset = 0;

    while (n_read < n) {
      int n_rest = n - n_read;
      int n_cur = read4(buf + offset);
      n_read += min(n_cur, n_rest);
      offset += n_cur;
      if (n_cur < 4 || n_rest < 4) {
        break;
      }
    }

    buf[n_read] = '\0';
    return n_read;
  }
};
