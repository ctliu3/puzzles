#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>

using namespace std;

class Solution {
  unordered_map<string, map<string, int>> edgehash;

  bool search(const string& u, int n, int dep, vector<string>& ret) {
    if (dep == n) {
      return true;
    }

    for (auto& to : edgehash[u]) {
      const string& v = to.first;
      if (edgehash[u][v] <= 0) {
        continue;
      }
      --edgehash[u][v];
      ret[dep] = v;
      if (search(v, n, dep + 1, ret)) {
        return true;
      }
      ++edgehash[u][v];
    }

    return false;
  }

public:
  vector<string> findItinerary(vector<pair<string, string>> tickets) {
    if (tickets.empty()) {
      return {};
    }

    for (auto& ticket : tickets) {
      ++edgehash[ticket.first][ticket.second];
    }

    int n = (int)tickets.size() + 1;
    vector<string> ret(n);
    ret[0] = "JFK";
    search("JFK", n, 1, ret);
    return ret;
  }
};

int main() {
  vector<pair<string, string>> tickets =
    {{"MUC", "LHR"}, {"JFK", "MUC"}, {"SFO", "SJC"}, {"LHR", "SFO"}};
    // {{"JFK","SFO"}, {"JFK","ATL"}, {"SFO","ATL"}, {"ATL","JFK"}, {"ATL","SFO"}};
  Solution sol;
  auto ret = sol.findItinerary(tickets);
  for (auto& ele : ret) {
    cout << ele << endl;
  }
  return 0;
}
