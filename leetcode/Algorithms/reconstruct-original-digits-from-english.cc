#include "leetcode.h"

class Solution {
public:
  string originalDigits(string s) {
    vector<int> counter(26, 0);
    for (auto& c : s) {
      counter[c - 'a'] += 1;
    }
    vector<string> orders = {"zero", "two", "six", "eight", "seven", "five", "four", "three", "one", "nine"};
    vector<int> digits = {0, 2, 6, 8, 7, 5, 4, 3, 1, 9};
    vector<char> chars = {'z', 'w', 'x', 'g', 's', 'v', 'f', 'h', 'o', 'i'};

    vector<int> ans(10, 0);
    for (int i = 0; i < 10; ++i) {
      int index = chars[i] - 'a';
      int c = counter[index];
      if (c == 0) {
        continue;
      }
      ans[digits[i]] = c;
      for (int j = 0; j < orders[i].size(); ++j) {
        counter[orders[i][j] - 'a'] -= c;
      }
    }
    string ret;
    for (int i = 0; i < 10; ++i) {
      if (ans[i] > 0) {
        ret.append(string(ans[i], i + '0'));
      }
    }
    return ret;
  }
};

int main() {
  return 0;
}
