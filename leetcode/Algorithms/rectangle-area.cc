#include <iostream>

using namespace std;

class Solution {
  // a: left bottom, x
  // b: left bottom, y
  // c: right top, x
  // d: right top, y
  int _area(int a, int b, int c, int d) {
    if (d - b <= 0 || c - a <= 0) {
      return 0;
    }
    return (d - b) * (c - a);
  }

public:
  int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
    int inter_area = _area(max(A, E), max(B, F), min(C, G), min(D, H));
    return _area(A, B, C, D) + _area(E, F, G, H) - inter_area;
  }
};

int main() {
  Solution sol;
  int res = sol.computeArea(-2, -2, 2, 2, -2, -2, 2, 2);
  cout << res << endl;
  return 0;
}
