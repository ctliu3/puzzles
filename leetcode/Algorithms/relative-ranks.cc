#include "leetcode.h"

class Solution {
public:
  vector<string> findRelativeRanks(vector<int>& nums) {
    vector<int> order(nums.size());
    for (int i = 0; i < order.size(); ++i) {
      order[i] = i;
    }
    sort(order.begin(), order.end(), [&](const int lhs, const int rhs){
      return nums[lhs] > nums[rhs];
    });

    vector<string> ans(nums.size());
    for (int i = 0; i < order.size(); ++i) {
      int pos = order[i];
      if (i == 0) {
        ans[pos] = "Gold Medal";
      } else if (i == 1) {
        ans[pos] = "Silver Medal";
      } else if (i == 2) {
        ans[pos] = "Bronze Medal";
      } else {
        ans[pos] = to_string(i + 1);
      }
    }
    return ans;
  }
};

int main() {
  return 0;
}
