#include <iostream>
#include <string>
#include <vector>
#include <set>

using namespace std;

class Solution {
  string removeDuplicateLetters1(string s) {
    int n = (int)s.size();
    if (n == 1) {
      return s;
    }

    string ret;
    vector<bool> used(26, false);
    vector<int> count(26, 0);
    int start = 0;
    while (true) {
      count.assign(26, 0);
      for (int i = start; i < n; ++i) {
        if (used[s[i] - 'a']) {
          continue;
        }
        ++count[s[i] - 'a'];
      }

      int pos = -1;
      for (int i = start; i < n; ++i) {
        if (used[s[i] - 'a']) {
          continue;
        }
        if (pos == -1 or s[i] < s[pos]) {
          pos = i;
        }
        if (--count[s[i] - 'a'] == 0) {
          break;
        }
      }
      if (pos == -1) {
        break;
      }
      ret.push_back(s[pos]);
      used[s[pos] - 'a'] = true;
      start = pos + 1;
    }

    return ret;
  }

  string removeDuplicateLetters2(string s) {
    int n = (int)s.size();
    if (n == 1) {
      return s;
    }

    vector<int> count(26, 0);
    for (char& chr : s) {
      ++count[chr - 'a'];
    }
    vector<bool> used(26, false);
    string ret;
    for (char& chr : s) {
      --count[chr - 'a'];
      if (used[chr - 'a']) {
        continue;
      }
      while (!ret.empty() and chr < ret.back() and count[ret.back() - 'a'] > 0) {
        used[ret.back() - 'a'] = false;
        ret.pop_back();
      }
      ret.push_back(chr);
      used[chr - 'a'] = true;
    }

    return ret;
  }

public:
  string removeDuplicateLetters(string s) {
    // return removeDuplicateLetters1(s);
    return removeDuplicateLetters2(s);
  }
};

int main() {
  string s = "cbacdcbc";
  Solution sol;
  string ans = sol.removeDuplicateLetters(s);
  cout << ans << endl;
}
