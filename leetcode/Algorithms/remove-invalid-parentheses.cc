#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {

  void dfs1(const string& s, int pos, int nleft, int nright, vector<string>& ret,
    int& maxlen, string& one) {
    if (nleft == nright) {
      if (ret.empty() || one.size() > ret[0].size()) {
        ret.assign(1, one);
        maxlen = max(maxlen, (int)one.size());
      } else if (one.size() == ret[0].size()) {
        if (find(ret.begin(), ret.end(), one) == ret.end()) {
          ret.push_back(one);
          maxlen = max(maxlen, (int)one.size());
        }
      }
    }

    if (pos == (int)s.size()) {
      return ;
    }
    if ((int)one.size() + (int)s.size() - pos < maxlen) {
      return ;
    }

    dfs1(s, pos + 1, nleft, nright, ret, maxlen , one);

    if (s[pos] == ')' and nright + 1 > nleft) {
      return ;
    }

    one.push_back(s[pos]);
    dfs1(s, pos + 1, nleft + (s[pos] == '('), nright + (s[pos] == ')'), ret,
      maxlen, one);
    one.pop_back();
  }

  vector<string> removeInvalidParentheses1(string s) {
    vector<string> ret;
    string one;
    int maxlen = 0;
    dfs1(s, 0, 0, 0, ret, maxlen, one);
    return ret;
  }

public:
  vector<string> removeInvalidParentheses(string s) {
    return removeInvalidParentheses1(s);
  }
};

int main() {
  string s = "(a)())()";
  Solution sol;
  auto ret = sol.removeInvalidParentheses(s);
  for (auto& s : ret) {
    cout << s << endl;
  }
  return 0;
}
