#include <iostream>
#include <string>
#include <vector>
#include <queue>

using namespace std;

class Solution {
  // MLE
  string removeKdigits1(string num, int k) {
    if (k >= (int)num.size()) {
      return "0";
    }
    if (k == 0) {
      return num;
    }

    string ret;
    int num_select = num.size() - k;
    static vector<vector<string>> dp(num.size() + 1, vector<string>(num_select + 1));
    for (int i = 1; i <= (int)num.size(); ++i) {
      for (int k = 1; k <= min(i, num_select); ++k) {
        dp[i][k] = string(k, '9');
        for (int j = 0; j < i; ++j) {
          if ((int)dp[j][k - 1].size() == k - 1) {
            dp[i][k] = min(dp[j][k - 1] + num[i - 1], dp[i][k]);
          }
        }
      }
      if ((int)dp[i][num_select].size() == num_select and (ret.empty() or dp[i][num_select] < ret)) {
        ret = dp[i][num_select];
      }
    }

    int st = 0;
    while (st + 1 < (int)ret.size() and ret[st] == '0') {
      ++st;
    }
    return ret.substr(st);
  }

public:
  string removeKdigits(string num, int k) {
    if (k >= (int)num.size()) {
      return "0";
    }
    if (k == 0) {
      return num;
    }
    vector<queue<int>> hash(10);
    for (int i = 0; i < (int)num.size(); ++i) {
      hash[num[i] - '0'].push(i);
    }

    string ret;
    int last = -1;
    for (int i = 0; i < (int)num.size() - k; ++i) {
      for (int j = 0; j <= 9; ++j) {
        while (!hash[j].empty() and hash[j].front() <= last) {
          hash[j].pop();
        }
        if (hash[j].empty()) {
          continue;
        }
        if ((int)num.size() - hash[j].front() >= (int)num.size() - k - i) {
          ret.push_back(j + '0');
          last = hash[j].front();
          break;
        }
      }
    }

    int st = 0;
    while (st + 1 < (int)ret.size() and ret[st] == '0') {
      ++st;
    }
    return ret.substr(st);
  }
};

int main() {
  Solution sol;
  string ans = sol.removeKdigits("43214321", 4);
  cout << ans << endl;
  return 0;
}
