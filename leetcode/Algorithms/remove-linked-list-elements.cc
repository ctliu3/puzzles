#include "leetcode.h"

class Solution {
public:
  ListNode* removeElements(ListNode* head, int val) {
    if (!head) {
      return nullptr;
    }
    ListNode dummy(0), *cur = &dummy, *del = nullptr;

    cur->next = head;
    while (cur->next) {
      if (cur->next->val == val) {
        del = cur->next;
        cur->next = del->next;
        delete del;
      } else {
        cur = cur->next;
      }
    }

    return dummy.next;
  }
};

int main() {
  return 0;
}
