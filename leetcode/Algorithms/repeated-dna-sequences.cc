#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Trie {
  struct TrieNode {
    TrieNode* child[4];
    bool is_word;

    explicit TrieNode() {
      for (int i = 0; i < 4; ++i) {
        child[i] = nullptr;
      }
      is_word = false;
    }
  };

  TrieNode* root;
public:

  explicit Trie() {
    root = new TrieNode();
  }

  int get_idx(const char& chr) {
    switch(chr) {
    case 'A':
      return 0;
    case 'C':
      return 1;
    case 'G':
      return 2;
    case 'T':
      return 3;
    }
    return -1;
  }

  void insert(const string& s, int st, int ed) {
    TrieNode* cur = root;
    for (int i = st; i <= ed; ++i) {
      int idx = get_idx(s[i]);
      if (!cur->child[idx]) {
        cur->child[idx] = new TrieNode();
      }
      cur = cur->child[idx];
    }
    cur->is_word = true;
  }

  bool search(const string& s, int st, int ed) {
    TrieNode* cur = root;
    for (int i = st; i <= ed; ++i) {
      int idx = get_idx(s[i]);
      if (!cur->child[idx]) {
        return false;
      }
      cur = cur->child[idx];
    }
    return cur->is_word = true;
  }
};

class Hash {
  int get_idx(const char& chr) {
    switch(chr) {
    case 'A':
      return 0;
    case 'C':
      return 1;
    case 'G':
      return 2;
    case 'T':
      return 3;
    }
    return -1;
  }

  int hashcode(const string& s, int st, int ed) {
    int code = 0;
    int base = 4;
    for (int i = st; i <= ed; ++i) {
      int cur = get_idx(s[i]);
      code = code * base + cur;
    }
    //printf("code = %d\n", code);
    return code;
  }

  unordered_set<int> hash;

public:
  explicit Hash() {
    hash.clear();
  }

  void add(const string& s, int st, int ed) {
    int code = hashcode(s, st, ed);
    hash.insert(code);
  }

  bool find(const string& s, int st, int ed) {
    int code = hashcode(s, st, ed);
    return hash.find(code) != hash.end();
  }
};

class Solution {
  // MLE
  vector<string> findRepeatedDnaSequences1(string s) {
    vector<string> res;
    unordered_map<string, int> mp;

    for (int i = 0; i + 9 < (int)s.size(); ++i) {
      string sub = s.substr(i, 10);
      if (mp.find(sub) ==mp.end()) {
        mp[sub] = 1;
      } else {
        int size = mp.size();
        mp[sub] = size + 1;
      }
    }
    for (auto& element : mp) {
      if (element.second > 1) {
        res.push_back(element.first);
      }
    }

    return res;
  }

  // MLE
  vector<string> findRepeatedDnaSequences2(string s) {
    vector<string> res;

    Trie trie;
    for (int i = 0; i + 9 < (int)s.size(); ++i) {
      string sub = s.substr(i, 10);
      if (trie.search(s, i, i + 9) and
        find(res.begin(), res.end(), sub) == res.end()) {
        res.push_back(sub);
      } else {
        trie.insert(s, i, i + 9);
      }
    }

    return res;
  }

  vector<string> findRepeatedDnaSequences3(string s) {
    vector<string> res;

    Hash hash;
    for (int i = 0; i + 9 < (int)s.size(); ++i) {
      string sub = s.substr(i, 10);
      if (hash.find(s, i, i + 9) and
        find(res.begin(), res.end(), sub) == res.end()) {
        res.push_back(sub);
      } else {
        hash.add(s, i, i + 9);
      }
    }

    return res;
  }

public:
  vector<string> findRepeatedDnaSequences(string s) {
    //return findRepeatedDnaSequences1(s);
    //return findRepeatedDnaSequences2(s);
    return findRepeatedDnaSequences3(s);
  }
};

int main() {
  Solution sol;
  string s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";
  vector<string> res = sol.findRepeatedDnaSequences(s);
  for (auto& str : res) {
    cout << str << endl;
  }
  return 0;
}
