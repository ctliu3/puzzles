class Solution {
  bool repeatedSubstringPattern1(string str) {
    int n = (int)str.size();
    for (int i = 1; i < n; ++i) {
      if (n % i != 0) {
        continue;
      }
      bool flag = true;
      for (int j = 0; j < n; j += i) {
        if (str.substr(0, i) != str.substr(j, i)) {
          flag = false;
          break;
        }
      }
      if (flag) {
        return true;
      }
    }
    return false;
  }

public:
  bool repeatedSubstringPattern(string str) {
    return repeatedSubstringPattern1(str);
  }
};
