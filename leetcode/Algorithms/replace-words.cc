#include "leetcode.h"

struct Node {
  Node* child[26];
  bool end;
  explicit Node(): end(false) {
    for (int i = 0; i < 26; ++i) {
      child[i] = nullptr;
    }
  }
};

struct Trie {

  Node* root;

  explicit Trie() {
    root = new Node();
  }
  ~Trie() {
    if (root) {
      delete root;
    }
  }

  void insert(const string& s) {
    Node* cur = root;
    int index = 0;
    while (index < (int)s.size()) {
      int pos = s[index] - 'a';
      if (!cur->child[pos]) {
        cur->child[pos] = new Node();
      }
      cur = cur->child[pos];
      index += 1;
    }
    cur->end = true;
  }

  int find(const string& s, int start) {
    Node* cur = root;
    for (int i = start; i < (int)s.size() && s[i] != ' '; ++i) {
      if (cur->end) {
        return i - start;
      }
      int pos = s[i] - 'a';
      if (cur->child[pos]) {
        cur = cur->child[pos];
      } else {
        break;
      }
    }
    return 0;
  }
};

class Solution {
public:
  string replaceWords(vector<string>& dict, string sentence) {
    Trie tree;
    for (size_t i = 0; i < dict.size(); ++i) {
      tree.insert(dict[i]);
    }

    string ans;
    int i = 0;
    int n = (int)sentence.size();
    while (i < n) {
      if (sentence[i] == ' ') {
        ans.push_back(' ');
        ++i;
      } else {
        int len = tree.find(sentence, i);
        if (len == 0) {
          while (i < n && sentence[i] != ' ') {
            ans.push_back(sentence[i]);
            ++i;
          }
        } else {
          for (int j = 0; j < len; ++j) {
            ans.push_back(sentence[i + j]);
          }
          i += len;
          while (i < n && sentence[i] != ' ') {
            ++i;
          }
        }
      }
    }

    return ans;
  }
};

int main() {
  return 0;
}
