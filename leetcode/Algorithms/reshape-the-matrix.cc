#include "leetcode.h"

class Solution {
  vector<vector<int>> matrixReshape1(vector<vector<int>>& nums, int r, int c) {
    vector<vector<int>> ans;

    int count = 0;
    ans.push_back({});
    for (int i = 0; i < nums.size(); ++i) {
      for (int j = 0; j < nums[i].size(); ++j) {
        if (count == r * c) {
          return nums;
        }
        ans.back().push_back(nums[i][j]);
        ++count;
        if (count % c == 0 && count != r * c) {
          ans.push_back({});
        }
      }
    }

    return count != r * c ? nums : ans;
  }

public:
  vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
    return matrixReshape1(nums, r, c);
  }
};

int main() {
  return 0;
}
