#include <iostream>

using namespace std;

class Solution {
  int bin(int n) {
    return 1 << n;
  }

public:
  uint32_t reverseBits(uint32_t n) {
    for (int i = 0; i < 16; ++i) {
      int lbit = bin(i) & n;
      int rbit = bin(31 - i) & n;
      if ((lbit && !rbit) || (!lbit && rbit)) {
        n ^= bin(i);
        n ^= bin(31 - i);
      }
    }
    return n;
  }
};

int main () {
  return 0;
}
