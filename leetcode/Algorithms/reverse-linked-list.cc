#include "leetcode.h"

using namespace std;

class Solution {

  // Iteration
  ListNode* reverseList1(ListNode* head) {
    if (!head || !head->next) {
      return head;
    }

    ListNode dummy(0), *cur = &dummy;
    cur->next = head;
    cur = cur->next;
    while (cur && cur->next) {
      ListNode* move = cur->next;
      cur->next = move->next;
      move->next = dummy.next;
      dummy.next = move;
    }
    return dummy.next;
  }

  // Recursion
  ListNode* reverseList2(ListNode* head) {
    if (!head) {
      return nullptr;
    }

    ListNode dummy(0);
    dummy.next = head;
    _dfs(&dummy, head);
    return dummy.next;
  }

  void _dfs(ListNode* dummy, ListNode* cur) {
    if (!cur->next) {
      return ;
    }

    ListNode* move = cur->next;
    cur->next = move->next;
    move->next = dummy->next;
    dummy->next = move;
    _dfs(dummy, cur);
  }

public:
  ListNode* reverseList(ListNode* head) {
    //return reverseList1(head);
    return reverseList2(head);
  }
};

int main() {
}
