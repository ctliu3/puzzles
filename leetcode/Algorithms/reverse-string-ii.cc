#include "leetcode.h"

class Solution {
  void reverse1(string& s, int i, int j) {
    while (i < j) {
      swap(s[i++], s[j--]);
    }
  }

  void reverseStr1(string& s, int k) {
    int i = 0;
    int n = (int)s.size();
    while (true) {
      if (n - i >= 2 * k) {
        reverse1(s, i, i + k - 1);
        i += 2 * k;
      } else if (n - i >= k) {
        reverse1(s, i, i + k - 1);
        break;
      } else {
        reverse1(s, i, n - 1);
        break;
      }
    }
  }

public:
  string reverseStr(string s, int k) {
     reverseStr1(s, k);
     return s;
  }
};

int main() {
  return 0;
}
