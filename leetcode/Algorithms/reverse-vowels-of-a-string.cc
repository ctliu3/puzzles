#include <string>

using namespace std;

class Solution {
public:
  string reverseVowels(string s) {
    if (s.size() < 2) {
      return s;
    }

    string vowels = "aeiou";

    int i = 0, j = s.size() - 1;
    while (i < j) {
      while (i < j && i < s.size() && vowels.find(tolower(s[i])) == string::npos) {
        ++i;
      }
      while (i < j && j >= 0 && vowels.find(tolower(s[j])) == string::npos) {
        --j;
      }
      if (i < j) {
        swap(s[i++], s[j--]);
      }
    }

    return s;
  }
};
