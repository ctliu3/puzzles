#include <iostream>
#include <string>

using namespace std;

class Solution {
  void _reverse(string& s, int start, int end) {
    while (start < end) {
      swap(s[start], s[end]);
      ++start;
      --end;
    }
  }

public:
  void reverseWords(string &s) {
    if (s.empty()) {
      return ;
    }
    _reverse(s, 0, s.size() - 1);
    int start = 0;
    for (int i = 0; i < (int)s.size(); ++i) {
      if (s[i] == ' ') {
        _reverse(s, start, i - 1);
        start = i + 1;
      }
    }
    _reverse(s, start, s.size() - 1);
  }
};

int main() {
  return 0;
}
