#include "leetcode.h"

class Solution {
  string reverseWords1(string& s) {
    if (s.empty()) {
      return s;
    }

    stringstream sin(s);
    string ans, word;

    while (sin >> word) {
      reverse(word.begin(), word.end());
      ans += word;
      ans.push_back(' ');
    }
    ans.resize(ans.size() - 1);

    return ans;
  }

public:
  string reverseWords(string s) {
    return reverseWords1(s);
  }
};

int main() {
  return 0;
}
