#include "leetcode.h"

class Solution {

  void rotate1(vector<int>& nums, int k) {
    int n = (int)nums.size();
    vector<int> temp(n, 0);
    int idx = 0;

    k %= n;
    for (int i = n - k; i < n; ++i) {
      temp[idx++] = nums[i];
    }
    for (int i = 0; i < n - k; ++i) {
      temp[idx++] = nums[i];
    }
    for (int i = 0; i < n; ++i) {
      nums[i] = temp[i];
    }
  }

  void rotate2(vector<int>& nums, int k) {
    int n = (int)nums.size();
    k %= n;
    if (!k) {
      return ;
    }
    reverse(nums.begin(), nums.end());
    reverse(nums.begin(), nums.begin() + k);
    reverse(nums.begin() + k, nums.end());
  }

public:
  void rotate(vector<int>& nums, int k) {
    //rotate1(nums, k);
    rotate2(nums, k);
  }
};

int main() {
  vector<int> nums = {1,2,3,4,5,6,7};
  Solution sol;

  sol.rotate(nums, 3);
  for (int i = 0; i < 7; ++i) {
    printf("%d ", nums[i]);
  }
  putchar('\n');
  return 0;
}
