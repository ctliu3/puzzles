class Solution {
  int maxRotateFunction1(vector<int>& A) {
    if (A.empty()) {
      return 0;
    }
    int n = (int)A.size();
    int sum = 0;
    int ret = 0;
    for (int i = 0; i < n; ++i) {
      sum += A[i];
      ret += i * A[i];
    }
    int prev = ret;
    for (int i = 0; i < n - 1; ++i) {
      int cur = prev - (sum - A[i]) + A[i] * (n - 1);
      ret = max(ret, cur);
      prev = cur;
    }
    return ret;
  }

public:
  int maxRotateFunction(vector<int>& A) {
    return maxRotateFunction1(A);
  }
};
