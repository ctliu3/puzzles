#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int maxEnvelopes1(vector<pair<int, int>>& envelopes) {
    int n = (int)envelopes.size();
    if (n <= 1) {
      return n;
    }
    sort(envelopes.begin(), envelopes.end());
    vector<int> dp(n, 0);
    dp[0] = 1;
    for (int i = 1; i < n; ++i) {
      dp[i] = 1;
      for (int j = 0; j < i; ++j) {
        if (envelopes[i].first > envelopes[j].first && envelopes[i].second > envelopes[j].second) {
          dp[i] = max(dp[j] + 1, dp[i]);
        }
      }
    }
    return *max_element(dp.begin(), dp.end());
  }

public:
  int maxEnvelopes(vector<pair<int, int>>& envelopes) {
    return maxEnvelopes1(envelopes);
  }
};

int main() {
  vector<pair<int, int>> envelopes = {{10, 8}, {1, 12}, {6, 15}, {2, 18}};
  Solution sol;
  int ret = sol.maxEnvelopes(envelopes);
  cout << ret << endl;
  return 0;
}
