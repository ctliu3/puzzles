class Solution {

  // Start from the right-top position
  bool searchMatrix1(vector<vector<int>>& matrix, int target) {
    if (matrix.empty()) {
      return false;
    }
    int n = (int)matrix.size(), m = (int)matrix[0].size();
    int i = 0, j = m - 1;
    while (i < n && j >= 0) {
      if (matrix[i][j] == target) {
        return true;
      }
      if (matrix[i][j] > target) {
        --j;
      } else {
        ++i;
      }
    }
    return false;
  }

  // Start from the left-bottom position
  bool searchMatrix2(vector<vector<int>>& matrix, int target) {
    if (matrix.empty()) {
      return false;
    }
    int n = (int)matrix.size(), m = (int)matrix[0].size();
    int i = n - 1, j = 0;
    while (i >= 0 && j < m) {
      if (matrix[i][j] == target) {
        return true;
      }
      if (matrix[i][j] > target) {
        --i;
      } else {
        ++j;
      }
    }
    return false;
  }

public:
  bool searchMatrix(vector<vector<int>>& matrix, int target) {
    //return searchMatrix1(matrix, target);
    return searchMatrix2(matrix, target);
  }
};
