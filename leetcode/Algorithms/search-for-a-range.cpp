// Author:     ct.Liu, lcndn3@gmail.com
// Date:       May 17, 2014
// Date:       May 17, 2014
//
// Search for a Range
// Given a sorted array of integers, find the starting and ending position of a
// given target value.
//
// Your algorithm's runtime complexity must be in the order of O(log n).
//
// If the target is not found in the array, return [-1, -1].
//
// For example,
// Given [5, 7, 7, 8, 8, 10] and target value 8,
// return [3, 4].

#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

class Solution {

  int lower1(vector<int>& nums, int target) {
    int start = -1, end = nums.size();
    while (end - start > 1) {
      int mid = start + (end - start) / 2;
      if (nums[mid] >= target) {
        end = mid;
      } else {
        start = mid;
      }
    }
    return end;
  }

  int lower2(vector<int>& nums, int target) {
    int st = 0, ed = nums.size() - 1;
    while (st <= ed) {
      int mid = st + (ed - st) / 2;
      if (nums[mid] < target) {
        st = mid + 1;
      } else {
        ed = mid - 1;
      }
    }
    return st;
  }

  int upper1(vector<int>& nums, int target) {
    int start = -1, end = nums.size();
    while (end - start > 1) {
      int mid = start + (end - start) / 2;
      if (nums[mid] > target) {
        end = mid;
      } else {
        start = mid;
      }
    }
    return end;
  }

  int upper2(vector<int>& nums, int target) {
    int st = 0, ed = nums.size() - 1;
    while (st <= ed) {
      int mid = st + (ed - st) / 2;
      if (nums[mid] <= target) {
        st = mid + 1;
      } else {
        ed = mid - 1;
      }
    }
    return st;
  }

public:
  vector<int> searchRange(vector<int>& nums, int target) {
    if (nums.empty()) {
      return {-1, -1};
    }
    int l = lower1(nums, target);
    int r = upper1(nums, target);
    if (l > r - 1) {
      return {-1, -1};
    }
    return {l, r - 1};
  }
};

int main() {
  vector<int> nums = {1, 1, 1, 2};
  Solution sol;
  auto res = sol.searchRange(nums, 1);
  for (auto& ele : res) {
    cout << ele << ' ';
  }
  cout << endl;

  return 0;
}
