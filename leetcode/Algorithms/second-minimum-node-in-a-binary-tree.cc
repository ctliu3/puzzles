#include "leetcode.h"

class Solution {
  void dfs1(TreeNode* cur, int& ans) {
    if (!cur) {
      return ;
    }
    if (!cur->left && !cur->right) {
      return ;
    }
    if (cur->left->val != cur->right->val) {
      ans = min(ans, max(cur->left->val, cur->right->val));
    }
    dfs1(cur->left, ans);
    dfs1(cur->right, ans);
  }

public:
  int findSecondMinimumValue(TreeNode* root) {
    int ans = INT_MAX;
    dfs1(root, ans);
    return ans == INT_MAX ? -1 : ans;
  }
};

int main() {
  return 0;
}
