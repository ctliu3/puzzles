#include <vector>
#include <cassert>
#include <iostream>

using namespace std;

class Solution {

public:
  bool isSelfCrossing(vector<int>& x) {
    if (x.size() < 4) {
      return false;
    }

    for (int i = 3; i < (int)x.size(); ++i) {
      if (x[i] >= x[i - 2] && x[i - 1] <= x[i - 3]) {
        return true;
      }
      if (i >= 4 && x[i] + x[i - 4] >= x[i - 2] && x[i - 1] == x[i - 3]) {
        return true;
      }
      if (i >= 5 && x[i - 2] >= x[i - 4] && x[i] + x[i - 4] >= x[i - 2]
        && x[i - 1] <= x[i - 3] && x[i - 5] + x[i - 1] >= x[i - 3]) {
        return true;
      }
    }
    return false;
  }
};

int main() {
  vector<int> x = {1, 2, 3, 4};
  Solution sol;
  bool ret = sol.isSelfCrossing(x);
  cout << ret << endl;
  return 0;
}
