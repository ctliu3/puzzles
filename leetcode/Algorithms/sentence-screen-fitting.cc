#include <string>
#include <vector>

using namespace std;

class Solution {
  // TLE
  int wordsTyping1(vector<string>& sentence, int rows, int cols) {
    if (rows == 0) {
      return 0;
    }

    int ret = 0, j = 0;
    for (int i = 0; i < rows; ++i) {
      int cur = 0;
      while (cur + sentence[j].size() + (cur != 0) <= cols) {
        cur += sentence[j++].size() + (cur != 0);
        if (j == (int)sentence.size()) {
          j = 0;
          ++ret;
        }
      }
    }
    return ret;
  }

  int wordsTyping2(vector<string>& sentence, int rows, int cols) {
  }

public:
  int wordsTyping(vector<string>& sentence, int rows, int cols) {
    return wordsTyping2(sentence, rows, cols);
  }
};
