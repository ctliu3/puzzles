#include "leetcode.h"

class Solution {
  bool sequenceReconstruction1(vector<int>& org, vector<vector<int>>& seqs) {
    int n = (int)org.size();
    vector<bool> hash(n, true);
    for (auto& seq : seqs) {
      for (auto num : seq) {
        if (num - 1 >= n || num - 1 < 0) {
          return false;
        }
        hash[num - 1] = false;
      }
    }
    for (int i = 0; i < n; ++i) {
      if (hash[i]) {
        return false;
      }
    }

    vector<vector<int>> edges(n);
    vector<int> d(n, 0);
    for (auto& seq : seqs) {
      for (int i = 0; i < (int)seq.size() - 1; ) {
        if (seq[i] - 1 >= n) {
          ++i;
          continue;
        }
        int j = i + 1;
        while (j < (int)seq.size() && (seq[j] - 1 >= n || seq[j] - 1 < 0)) {
          ++j;
        }
        if (j == (int)seq.size()) {
          break;
        }
        int u = seq[i] - 1, v = seq[j] - 1;
        edges[u].push_back(v);
        ++d[v];
        i = j;
      }
    }

    queue<int> que;
    for (int i = 0; i < n; ++i) {
      if (!d[i]) {
        que.push(i);
      }
    }
    if (que.size() != 1) {
      return false;
    }
    vector<int> path;
    while (que.size() == 1) {
      int u = que.front();
      que.pop();

      path.push_back(u + 1);
      for (int v : edges[u]) {
        if (d[v] > 0) {
          --d[v];
          if (!d[v]) {
            que.push(v);
          }
        }
      }
    }
    if (path.size() != n) {
      return false;
    }
    return path == org;
  }

public:
  bool sequenceReconstruction(vector<int>& org, vector<vector<int>>& seqs) {
    return sequenceReconstruction1(org, seqs);
  }
};
