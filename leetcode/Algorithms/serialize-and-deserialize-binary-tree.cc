#include "leetcode.h"

class Codec {
public:

  // Encodes a tree to a single string.
  string serialize(TreeNode* root) {
    if (!root) {
      return "";
    }

    string ret;
    queue<TreeNode*> que;
    que.push(root);
    while (!que.empty()) {
      TreeNode* cur = que.front();
      que.pop();

      ret.append(cur ? to_string(cur->val) : "#");
      ret.append(" ");
      if (!cur) {
        continue;
      }
      que.push(cur->left);
      que.push(cur->right);
    }

    return ret;
  }

  // Decodes your encoded data to tree.
  TreeNode* deserialize(string data) {
    if (data.empty()) {
      return nullptr;
    }

    istringstream sin(data);
    string s;
    vector<TreeNode*> nodes;
    TreeNode* cur = nullptr, *root = nullptr;
    int idx = 0;
    bool isleft = true;
    while (sin >> s) {
      if (s == "#") {
        isleft = !isleft;
        if (isleft && idx + 1 < (int)nodes.size()) {
          cur = nodes[++idx];
        }
        continue;
      }
      nodes.push_back(new TreeNode(stoi(s)));
      if (!cur) {
        cur = root = nodes.back();
      } else {
        if (isleft) {
          cur->left = nodes.back();
        } else {
          cur->right = nodes.back();
        }
        isleft = !isleft;
        if (isleft && idx + 1 < (int)nodes.size()) {
          cur = nodes[++idx];
        }
      }
    }

    return root;
  }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));

int main() {

  string tree = "{3,2,4}";
  TreeNode* cur = getTree(tree);
  Codec codec;
  TreeNode *root = codec.deserialize(codec.serialize(cur));
  auto ret = level_order(root);
  print_vector2(ret);

  return 0;
}
