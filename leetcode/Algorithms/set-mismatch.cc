class Solution {
public:
  vector<int> findErrorNums(vector<int>& nums) {
    if (nums.empty()) {
      return {};
    }
    vector<bool> hash(nums.size(), false);
    vector<int> ans;
    for (auto num : nums) {
      if (hash[num - 1]) {
        ans = {num};
      }
      hash[num - 1] = true;
    }
    for (int i = 0; i < hash.size(); ++i) {
      if (!hash[i]) {
        ans.push_back(i + 1);
        break;
      }
    }
    return ans;
  }
};
