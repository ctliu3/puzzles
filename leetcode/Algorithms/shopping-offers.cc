#include "leetcode.h"

class Solution {
  struct State {
    vector<int> st;
  };

  int shoppingOffers1(vector<int>& price, vector<vector<int>>& special, vector<int>& needs) {
    int ans = 0;

    for (int i = 0; i < (int)price.size(); ++i) {
      ans += price[i] * needs[i];
    }

    int n = (int)price.size();
    vector<vector<int>> combines(n + special.size());
    vector<int> cost(n + special.size());
    for (int i = 0; i < n; ++i) {
      combines[i].push_back({i});
      cost[i] = price[i];
    }
    for (int i = 0; i < special.size(); ++i) {
      combines[n + i] = vector<int>(special[i].begin(), special[i].end() - 1);
      cost[n + i] = special[i].back();
    }

    int m = combines.size();
    vector<int> dp(m);
    for (int i = 0; i < ) {
    }

    dfs1(price, special, needs, ans);

    return ans;
  }

public:
  int shoppingOffers(vector<int>& price, vector<vector<int>>& special, vector<int>& needs) {
    return shoppingOffers1(price, special, needs);
  }
};

int main() {
  return 0;
}
