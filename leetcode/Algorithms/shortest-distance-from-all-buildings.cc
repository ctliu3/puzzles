#include <vector>
#include <queue>

using namespace std;

const vector<int> dx = {0, 0, -1, 1};
const vector<int> dy = {-1, 1, 0, 0};

class Solution {
  bool inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

  int shortestDistance1(vector<vector<int>>& grid) {
    if (grid.empty()) {
      return -1;
    }

    int n = grid.size(), m = grid[0].size();
    int ret = -1, pass = 0;
    queue<pair<pair<int, int>, int>> que;
    auto totdist = grid;

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (grid[i][j] != 1) {
          continue;
        }

        ret = -1;
        que.push({{i, j}, 0});
        while (!que.empty()) {
          auto& cur = que.front();
          int x = cur.first.first, y = cur.first.second;
          int step = cur.second;
          que.pop();

          for (int k = 0; k < 4; ++k) {
            int nx = x + dx[k];
            int ny = y + dy[k];
            if (inside(nx, ny, n, m) && grid[nx][ny] == pass) {
              --grid[nx][ny];
              totdist[nx][ny] += step + 1;
              que.push({{nx, ny}, step + 1});
              if (ret == -1 || totdist[nx][ny] < ret) {
                ret = totdist[nx][ny];
              }
            }
          }
        }

        --pass;
      }
    }

    return ret;
  }

public:
    int shortestDistance(vector<vector<int>>& grid) {
      return shortestDistance1(grid);
    }
};

int main() {
  return 0;
}
