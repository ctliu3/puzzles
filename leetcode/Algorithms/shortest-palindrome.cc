#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
  // MLE
  string shortestPalindrome1(string s) {
    if (s.empty()) {
      return s;
    }

    int n = s.size();
    vector<vector<bool>> dp(n, vector<bool>(n, false));
    for (int j = 0; j < n; ++j) {
      dp[j][j] = true;
      for (int i = 0; i < j; ++i) {
        if (j - i == 1) {
          dp[i][j] = s[i] == s[j];
        } else {
          dp[i][j] = s[i] == s[j] and dp[i + 1][j - 1];
        }
      }
    }
    if (dp[0][n - 1]) {
      return s;
    }

    string prefix;
    int n_add = 0;
    while (true) {
      prefix.insert(0, 1, s[n - 1]);
      for (int i = 1; i <= n_add; ++i) {
        prefix[i] = s[n - 1 - i];
      }
      if (dp[0][n - 2 - n_add]) {
        break;
      }
      ++n_add;
    }

    return prefix + s;
  }

  string shortestPalindrome2(string s) {
    string rs = s;
    reverse(rs.begin(), rs.end());
    string str = s + "@" + rs;

    vector<int> p(str.size());
    int j = -1;
    p[0] = -1;
    for (int i = 1; i < (int)str.size(); ++i) {
      while (j != -1 && str[j + 1] != str[i]) {
        j = p[j];
      }
      if (str[i] == str[j + 1]) {
        p[i] = j + 1;
        ++j;
      } else {
        p[i] = -1;
      }
    }

    return rs.substr(0, s.size() - (p.back() + 1)) + s;
  }

public:
  string shortestPalindrome(string s) {
    //return shortestPalindrome1(s);
    return shortestPalindrome2(s);
  }
};

int main() {
  Solution sol;
  string s = "aacecaaa";
  string res = sol.shortestPalindrome(s);
  cout << res << endl;
  return 0;
}

