#include "leetcode.h"

class Solution {
public:
  int findUnsortedSubarray(vector<int>& nums) {
    vector<int> ids(nums.size());
    for (int i = 0; i < (int)nums.size(); ++i) {
      ids[i] = i;
    }
    sort(ids.begin(), ids.end(), [&](const int& lhs, const int& rhs){
      if (nums[lhs] == nums[rhs]) {
        return lhs < rhs;
      }
      return nums[lhs] < nums[rhs];
    });
    int start = 0, end = nums.size() - 1;
    while (start < nums.size() && ids[start] == start) {
      ++start;
    }
    while (end >= start && ids[end] == end) {
      --end;
    }

    return end - start + 1;
  }
};

int main() {
  return 0;
}
