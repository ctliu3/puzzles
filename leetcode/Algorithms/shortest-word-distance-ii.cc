#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class WordDistance {
  unordered_map<string, vector<int>> pos_;

public:
  WordDistance(vector<string>& words) {
    int n = (int)words.size();

    pos_.clear();
    for (int i = 0; i < n; ++i) {
      pos_[words[i]].push_back(i);
    }
  }

  int shortest(string word1, string word2) {
    vector<int>& pos1 = pos_[word1];
    vector<int>& pos2 = pos_[word2];
    int i = 0, j = 0;
    int mindis = INT_MAX;

    while (i < (int)pos1.size() && j < (int)pos2.size()) {
      mindis = min(mindis, abs(pos1[i] - pos2[j]));
      if (pos1[i] < pos2[j]) {
        ++i;
      } else {
        ++j;
      }
    }
    while (i < (int)pos1.size()) {
      mindis = min(mindis, abs(pos1[i] - pos2.back()));
      ++i;
    }
    while (j < (int)pos2.size()) {
      mindis = min(mindis, abs(pos1.back() - pos2[j]));
      ++j;
    }

    return mindis;
  }
};

// Your WordDistance object will be instantiated and called as such:
// WordDistance wordDistance(words);
// wordDistance.shortest("word1", "word2");
// wordDistance.shortest("anotherWord1", "anotherWord2");

int main() {
  return 0;
}
