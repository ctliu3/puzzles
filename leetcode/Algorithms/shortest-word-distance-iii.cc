#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
  int shortestWordDistance(vector<string>& words, string word1, string word2) {
    int n = (int)words.size();
    if (word1 == word2) {
      int pos = -1, mindis = n;

      for (int i = 0; i < n; ++i) {
        if (words[i] == word1) {
          if (pos != -1) {
            mindis = min(mindis, i - pos);
          }
          pos = i;
        }
      }

      return mindis;
    }

    int pos1 = -1, pos2 = -1;
    int mindis = n;
    for (int i = 0; i < n; ++i) {
      if (words[i] == word1) {
        pos2 = i;
        if (pos1 != -1) {
          mindis = min(mindis, pos2 - pos1);
        }
      } else if (words[i] == word2) {
        pos1 = i;
        if (pos2 != -1) {
          mindis = min(mindis, pos1 - pos2);
        }
      }
    }

    return mindis;
  }
};

int main() {
  return 0;
}
