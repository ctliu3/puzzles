#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
  int shortestDistance(vector<string>& words, string word1, string word2) {
    int n = (int)words.size();
    int pos1 = -1, pos2 = -1;
    int mindis = n;

    for (int i = 0; i < n; ++i) {
      if (words[i] == word1) {
        pos1 = i;
        if (pos2 != -1) {
          mindis = min(mindis, pos1 - pos2);
        }
      } else if (words[i] == word2) {
        pos2 = i;
        if (pos1 != -1) {
          mindis = min(mindis, pos2 - pos1);
        }
      }
    }

    return mindis;
  }
};

int main() {
  return 0;
}
