#include "leetcode.h"

class Solution {
  vector<int> nums;

public:
  Solution(vector<int> nums) {
    this->nums = nums;
  }

  /** Resets the array to its original configuration and return it. */
  vector<int> reset() {
    return nums;
  }

  /** Returns a random shuffling of the array. */
  vector<int> shuffle() {
    vector<int> cur = nums;
    int n = (int)cur.size();
    for (int i = 0; i < n; ++i) {
      int index = rand() % n;
      swap(cur[i], cur[index]);
    }
    return cur;
  }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * vector<int> param_1 = obj.reset();
 * vector<int> param_2 = obj.shuffle();
 */

int main() {
  return 0;
}
