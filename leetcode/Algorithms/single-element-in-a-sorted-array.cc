#include "leetcode.h"

class Solution {
  int singleNonDuplicate1(vector<int>& nums) {
    if (nums.size() == 1) {
      return nums[0];
    }
    int start = 0, end = nums.size() - 1;
    while (end > start) {
      int mid = start + (end - start) / 2;
      if ((mid & 1) == 0) { // even
        if (nums[mid] != nums[mid - 1]) {
          start = mid;
        } else {
          end = mid - 2;
        }
      } else {
        if (nums[mid] == nums[mid - 1]) {
          start = mid + 1;
        } else {
          end = mid - 1;
        }
      }
    }
    return nums[start];
  }

public:
  int singleNonDuplicate(vector<int>& nums) {
    return singleNonDuplicate1(nums);
  }
};

int main() {
  return 0;
}
