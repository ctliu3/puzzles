#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> singleNumber(vector<int>& nums) {
    if (nums.size() < 2) {
      return {};
    }
    int xorsum = 0;
    for (int& num : nums) {
      xorsum ^= num;
    }

    int mask = 0;
    for (int i = 0; i < 32; ++i) {
      if (xorsum & (1 << i)) {
        mask = 1 << i;
        break;
      }
    }

    vector<int> ret(2, 0);
    for (int& num : nums) {
      if (num & mask) {
        ret[0] ^= num;
      } else {
        ret[1] ^= num;
      }
    }

    return ret;
  }
};

int main() {
  vector<int> nums = {1, 2, 1, 3, 2, 5};
  Solution sol;
  auto ret = sol.singleNumber(nums);
  for (auto& ele : ret) {
    cout << ele << endl;
  }
  return 0;
}
