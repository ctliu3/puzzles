#include <iostream>
#include <queue>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> maxSlidingWindow(vector<int>& nums, int k) {
    vector<int> ret;
    deque<pair<int, int>> deq;

    for (int i = 0; i < (int)nums.size(); ++i) {
      while (!deq.empty() && deq.back().first <= nums[i]) {
        deq.pop_back();
      }
      while (!deq.empty() && i - deq.front().second >= k) {
        deq.pop_front();
      }
      deq.push_back({nums[i], i});
      if (i >= k - 1) {
        ret.push_back(deq.front().first);
      }
    }

    return ret;
  }
};

int main() {
  vector<int> nums = {7, 2, 4};
  Solution sol;
  auto ret = sol.maxSlidingWindow(nums, 2);
  for (auto ele : ret) {
    cout << ele << endl;
  }
}
