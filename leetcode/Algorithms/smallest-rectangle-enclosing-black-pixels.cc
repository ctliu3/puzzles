#include <vector>
#include <queue>

using namespace std;

const vector<int> dir = {0, -1, 0, 1, 0};

class Solution {
  bool inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

public:
  int minArea(vector<vector<char>>& image, int x, int y) {
    if (image.empty()) {
      return 0;
    }

    int n = image.size(), m = image[0].size();
    int ret = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) { 
        if (image[i][j] == '0') {
          continue;
        }

        queue<pair<int, int>> que;
        int row_min = n, row_max = 0;
        int col_min = m, col_max = 0;
        que.push({i, j});
        image[i][j] = '0';
        while (!que.empty()) {
          auto cur = que.front();
          que.pop();
          int x = cur.first, y = cur.second;
          row_min = min(row_min, x);
          row_max = max(row_max, x);
          col_min = min(col_min, y);
          col_max = max(col_max, y);

          for (int k = 0; k < 4; ++k) {
            int nx = x + dir[k], ny = y + dir[k + 1];
            if (inside(nx, ny, n, m) && image[nx][ny] == '1') {
              image[nx][ny] = '0';
              que.push({nx, ny});
            }
          }
        }
        ret = (row_max - row_min + 1) * (col_max - col_min + 1);
        break;
      }
    }
    return ret;
  }
};

int main() {
  return 0;
}
