#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

class Solution {
  string frequencySort1(string s) {
    if (s.size() < 2) {
      return s;
    }

    vector<int> chr(256, 0);
    for (auto c : s) {
      ++chr[c];
    }
    vector<pair<int, int>> pairs;
    for (int i = 0; i < (int)chr.size(); ++i) {
      if (chr[i]) {
        pairs.push_back({chr[i], i});
      }
    }

    sort(pairs.rbegin(), pairs.rend());
    ostringstream out;
    for (auto& p : pairs) {
      out << string(p.first, (char)p.second);
    }
    return out.str();
  }

public:
  string frequencySort(string s) {
    return frequencySort1(s);
  }
};

int main() {
  Solution sol;
  string ret = sol.frequencySort("tree");
  cout << ret << endl;
}
