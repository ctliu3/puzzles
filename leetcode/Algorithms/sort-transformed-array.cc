#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

bool isInc(const vector<int>& nums) {
  return nums[0] < nums.back();
}

int f(int x, int a, int b, int c) {
  return x * x * a + x * b + c;
}

vector<int> compose(const vector<int>&nums, int a, int b, int c, bool inverse) {
  int n = (int)nums.size();
  vector<int> ret(n);
  if (!inverse) {
    for (int i = 0; i < n; ++i) {
      ret[i] = f(nums[i], a, b, c);
    }
  } else {
    for (int i = n - 1; i >= 0; --i) {
      ret[n - 1 - i] = f(nums[i], a, b, c);
    }
  }
  return ret;
}

class Solution {
  vector<int> sortTransformedArray1(vector<int>& nums, int a, int b, int c) {
    int n = (int)nums.size();
    if (!n) {
      return {};
    }
    if (n == 1) {
      return {nums[0] * nums[0] * a + nums[0] * b + c};
    }


    bool inc = isInc(nums);
    if (a == 0) {
      if (b > 0) {
        return inc ? compose(nums, a, b, c, false) : compose(nums, a, b, c, true);
      } else {
        return inc ? compose(nums, a, b, c, true) : compose(nums, a, b, c, false);
      }
    }

    double x = -b * 1. / (2 * a);
    vector<int> ret;
    if (nums[0] > x && nums[n - 1] > x) {
      if (a > 0) {
        return inc ? compose(nums, a, b, c, false) : compose(nums, a, b, c, true);
      } else {
        return inc ? compose(nums, a, b, c, true) : compose(nums, a, b, c, false);
      }
    } else if (nums[0] < x && nums[n - 1] < x) {
      if (a > 0) {
        return inc ? compose(nums, a, b, c, true) : compose(nums, a, b, c, false);
      } else {
        return inc ? compose(nums, a, b, c, false) : compose(nums, a, b, c, true);
      }
    } else {
      auto lf = [&](int x) { return x * x * a + x * b + c; };
      if (a > 0) {
        double close = 1e10;
        int index = 0;
        for (int i = 0; i < n; ++i) {
          if (fabs(x - nums[i] * 1.0) < close) {
            close = fabs(x - nums[i] * 1.0);
            index = i;
          }
        }
        ret.push_back(lf(nums[index]));
        int i = index - 1, j = index + 1;
        while (i >= 0 && j < n) {
          if (lf(nums[i]) < lf(nums[j])) {
            ret.push_back(lf(nums[i--]));
          } else {
            ret.push_back(lf(nums[j++]));
          }
        }
        while (i >= 0) {
          ret.push_back(lf(nums[i--]));
        }
        while (j < n) {
          ret.push_back(lf(nums[j++]));
        }
      } else {
        int i = 0, j = n - 1;
        while (i <= j) {
          if (lf(nums[i]) < lf(nums[j])) {
            ret.push_back(lf(nums[i++]));
          } else {
            ret.push_back(lf(nums[j--]));
          }
        }
      }
    }
    return ret;
  }

public:
  vector<int> sortTransformedArray(vector<int>& nums, int a, int b, int c) {
    return sortTransformedArray1(nums, a, b, c);
  }
};
