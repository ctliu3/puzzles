#include <vector>

using namespace std;

class Solution {
  vector<vector<int>> multiply1(vector<vector<int>>& A, vector<vector<int>>& B) {
    if (A.empty() || B.empty()) {
      return {};
    }
    int an = A.size(), am = A[0].size();
    int bn = B.size(), bm = B[0].size();

    vector<bool> a_row(an, false), a_col(am, false);
    vector<bool> b_row(bn, false), b_col(bm, false);
    for (int i = 0; i < an; ++i) {
      for (int j = 0; j < am; ++j) {
        if (A[i][j]) {
          a_row[i] = a_col[j] = true;
        }
      }
    }
    for (int i = 0; i < bn; ++i) {
      for (int j = 0; j < bm; ++j) {
        if (B[i][j]) {
          b_row[i] = b_col[j] = true;
        }
      }
    }

    vector<vector<int>> ret(an, vector<int>(bm, 0));
    for (int i = 0; i < an; ++i) {
      for (int j = 0; j < bm; ++j) {
        if (!a_row[i] || !b_col[j]) {
          continue;
        }
        for (int k = 0; k < am; ++k) {
          ret[i][j] += A[i][k] * B[k][j];
        }
      }
    }
    return ret;
  }

public:
  vector<vector<int>> multiply(vector<vector<int>>& A, vector<vector<int>>& B) {
    return multiply1(A, B);
  }
};

int main() {
  return 0;
}
