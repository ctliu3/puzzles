// Author:     ct.Liu, lcndn3@gmail.com
// Date:       May 21, 2014
// Update:     May 21, 2014
//
// Spiral Matrix
// Given a matrix of m x n elements (m rows, n columns), return all elements of
// the matrix in spiral order.
// For example,
// Given the following matrix:
// 
// [
//  [ 1, 2, 3 ],
//  [ 4, 5, 6 ],
//  [ 7, 8, 9 ]
// ]
// You should return [1,2,3,6,9,8,7,4,5].

#include "leetcode.h"

class Solution {
public:
  vector<int> spiralOrder(vector<vector<int> >& matrix) {
    if (matrix.empty()) {
      return vector<int>();
    }
    int n = matrix.size(), m = matrix[0].size();
    int index = 0;

    vector<int> ret(n * m, 0);
    int r1 = 0, r2 = n - 1, c1 = 0, c2 = m - 1;
    while (index < n * m) {
      // row, top, left->right
      for (int j = c1; j <= c2; ++j) {
        ret[index++] = matrix[r1][j];
      }
      if (index == n * m) {
        break;
      }
      ++r1;
      // col, right, top -> down
      for (int i = r1; i <= r2; ++i) {
        ret[index++] = matrix[i][c2];
      }
      if (index == n * m) {
        break;
      }
      --c2;
      // row, bottom, right->left
      for (int j = c2; j >= c1; --j) {
        ret[index++] = matrix[r2][j];
      }
      --r2;
      if (index == n * m) {
        break;
      }
      // col, left, bottom->top
      for (int i = r2; i >= r1; --i) {
        ret[index++] = matrix[i][c1];
      }
      if (index == n * m) {
        break;
      }
      ++c1;
    }
    return ret;
  }
};

int main() {
  vector<vector<int>> v(4);
  v[0] = {1, 2, 3 ,4};
  v[1] = {5, 6, 7, 8};
  v[2] = {9, 10, 11, 12};
  v[3] = {13, 14, 15, 16};

  Solution sol;
  vector<int> res = sol.spiralOrder(v);
  print_vector(res);

  return 0;
}
