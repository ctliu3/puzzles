#include "leetcode.h"

class Solution {
  bool isPossible1(vector<int>& nums) {
    unordered_map<int, priority_queue<int, vector<int>, greater<int> > > mp;

    int rest = 0;
    for (int num : nums) {
      if (!mp.count(num - 1) || mp[num - 1].empty()) {
        mp[num].push(1);
        rest += 1;
      } else {
        auto& pq = mp[num - 1];
        int count = pq.top();
        pq.pop();
        mp[num].push(count + 1);
        if (count == 2) {
          --rest;
        }
      }
    }

    return rest == 0;
  }

public:
  bool isPossible(vector<int>& nums) {
    return isPossible1(nums);
  }
};

int main() {
  return 0;
}
