#include <iostream>
#include <vector>

using namespace std;

using llong = long long;

class Solution {
  bool hasAns(vector<int>&nums, llong cur, int m) {
    llong sum = 0, count = 1;
    for (auto val : nums) {
      if (sum + val <= cur) {
        sum += val;
      } else {
        sum = val;
        ++count;
      }
      if (count > m) {
        return false;
      }
    }
    return true;
  }

  int splitArray1(vector<int>& nums, int m) {
    llong sum = 0;
    for (auto val : nums) {
      sum += val;
    }

    llong start = *max_element(nums.begin(), nums.end()), end = sum;
    llong ans = end;
    while (start <= end) {
      llong mid = start + (end - start) / 2;
      if (hasAns(nums, mid, m)) {
        end = mid - 1;
        ans = min(ans, mid);
      } else {
        start = mid + 1;
      }
    }
    return ans;
  }

public:
  int splitArray(vector<int>& nums, int m) {
    return splitArray1(nums, m);
  }
};

int main() {
  vector<int> nums = {1,2,3,4,5};
  int m = 2;

  Solution sol;
  int ret = sol.splitArray(nums, m);
  cout << ret << endl;
}
