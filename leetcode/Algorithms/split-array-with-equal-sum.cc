#include "leetcode.h"

class Solution {
public:
  bool splitArray(vector<int>& nums) {
    int n = (int)nums.size();
    if (n < 7) {
      return false;
    }
    unordered_map<int, vector<int> > lt;
    unordered_map<int, vector<int> > rt;

    rt[nums[n - 1]].push_back(n - 1);
    int sum = nums.back();
    int acc = nums.back();
    for (int i = n - 2; i >= 0; --i) {
      acc += nums[i];
      sum += nums[i];
      rt[acc].push_back(i);
    }

    lt[nums[0]].push_back(0);
    acc = nums[0];
    for (int i = 1; i < n; ++i) {
      acc += nums[i];
      lt[acc].push_back(i);
    }

    acc = nums[0];
    for (int k = 1; k + 5 < n; ++k) {
      int val = acc;
      acc += nums[k];
      auto& l = lt[val + acc];
      auto& r = rt[val];
      for (int i = 0; i < l.size(); ++i) {
        for (int j = 0; j < r.size(); ++j) {
          if (l[i] + 1 >= r[j]) {
            continue;
          }
          if (sum - 3 * val - nums[k] - nums[r[j] - 1] - nums[l[i] + 1] == val) {
            return true;
          }
        }
      }
    }
    return false;
  }
};

int main() {
  return 0;
}
