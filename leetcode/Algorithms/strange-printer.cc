#include "leetcode.h"

class Solution {
  int treedp(const string& s, vector<vector<int>>& dp, int st, int ed) {
    int& ret = dp[st][ed];
    if (ed < st) {
      return ret = 0;
    }
    if (ed == st) {
      return ret = 1;
    }
    if (ret != -1) {
      return ret;
    }
    ret = treedp(s, dp, st, ed - 1) + 1;
    for (int i = st; i < ed; ++i) {
      if (s[i] == s[ed]) {
        ret = min(ret, treedp(s, dp, st, i - 1) + treedp(s, dp, i + 1, ed));
      }
    }
    return ret;
  }

  int strangePrinter1(string s) {
    if (s.empty()) {
      return 0;
    }
    int n = s.size();
    vector<vector<int>> dp(n, vector<int>(n, -1));
    return treedp(s, dp, 0, n - 1);
  }

public:
  int strangePrinter(string s) {
    return strangePrinter1(s);
  }
};

int main() {
  return 0;
}
