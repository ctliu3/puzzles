#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
  void _findStrobogrammatic(const vector<pair<char, char>>& mp, string& str,
                            vector<string>& res, int dep) {
    if (dep >= (int)str.size()) {
      res.push_back(str);
      return ;
    }

    if (str[dep] != '-') {
      _findStrobogrammatic(mp, str, res, dep + 1);
      return ;
    }

    int n = (int)str.size();
    for (auto& p : mp) {
      char fst = p.first, snd = p.second;

      if (fst == '0' && dep == 0 && n > 1) {
        continue;
      }
      if (fst == '0' || fst == '1' || fst == '8') {
        str[dep] = str[n - dep - 1] = fst;
        _findStrobogrammatic(mp, str, res, dep + 1);
        str[dep] = str[n - dep - 1] = '-';
      } else if (dep <= n && dep != n - dep - 1) {
        str[dep] = fst;
        str[n - dep - 1] = snd;
        _findStrobogrammatic(mp, str, res, dep + 1);
        str[dep] = str[n - dep - 1] = '-';
      }
    }
  }

  vector<string> findStrobogrammatic(int n) {
    vector<pair<char, char>> mp
      = {{'0', '0'}, {'1', '1'}, {'6', '9'}, {'8', '8'}, {'9', '6'}};

    vector<string> res;
    string str(n, '-');
    _findStrobogrammatic(mp, str, res, 0);

    return res;
  }

  int strobogrammaticInRange1(string low, string high) {
    if (low.size() > high.size()) {
      return 0;
    }
    int res = 0;
    for (size_t i = low.size() + 1; i < high.size(); ++i) {
      auto ret = findStrobogrammatic(i);
      res += ret.size();
    }

    auto ret = findStrobogrammatic(low.size());
    res += ret.size();
    for (auto& str : ret) {
      if (lessthen(str, low)) {
        --res;
      }
    }

    if (low.size() != high.size()) {
      ret = findStrobogrammatic(high.size());
      res += ret.size();
    }
    for (auto& str : ret) {
      if (str == high) {
        continue;
      }
      if (lessthen(high, str)) {
        --res;
      }
    }

    return res;
  }

  bool lessthen(string& lhs, string& rhs) {
    for (size_t i = 0; i < lhs.size(); ++i) {
      if (lhs[i] != rhs[i]) {
        return lhs[i] < rhs[i];
      }
    }
    return false;
  }

public:
  int strobogrammaticInRange(string low, string high) {
    return strobogrammaticInRange1(low, high);
  }
};

int main() {
  string low = "0", high = "1680";
  Solution sol;
  int ret = sol.strobogrammaticInRange(low, high);
  cout << ret << endl;
  return 0;
}
