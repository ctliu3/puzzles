#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  bool isStrobogrammatic(string num) {
    vector<int> mp(10, -1);
    mp[0] = 0;
    mp[1] = 1;
    mp[6] = 9;
    mp[8] = 8;
    mp[9] = 6;

    int st = 0, ed = (int)num.size() - 1;
    while (st <= ed) {
      int a = num[st] - '0', b = num[ed] - '0';
      if (mp[a] == -1 || mp[b] == -1) {
        return false;
      }
      if (mp[a] != b || mp[b] != a) {
        return false;
      }
      ++st;
      --ed;
    }

    return true;
  }
};

int main() {
  Solution sol;
  cout << sol.isStrobogrammatic("9");
  return 0;
}
