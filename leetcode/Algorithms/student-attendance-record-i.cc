#include "leetcode.h"

class Solution {
  bool checkRecord1(const string& s) {
    int a_count = 0;
    for (int i = 0; i < s.size(); ++i) {
      if (s[i] == 'A') {
        a_count += 1;
      }
      if (a_count == 2) {
        return false;
      }
      if (s[i] == 'L' && i + 2 < s.size() && s[i + 1] == 'L' && s[i + 2] == 'L') {
        return false;
      }
    }
    return true;
  }

public:
  bool checkRecord(string s) {
    return checkRecord1(s);
  }
};

int main() {
  return 0;
}
