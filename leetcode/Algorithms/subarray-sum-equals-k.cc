#include "leetcode.h"

class Solution {
  int subarraySum1(vector<int>& nums, int k) {
    unordered_map<int, int> counter;
    int ans = 0, acc = 0;
    counter[0] = 1;
    for (int i = 0; i < nums.size(); ++i) {
      acc += nums[i];
      if (counter.find(acc - k) != counter.end()) {
        ans += counter[acc - k];
      }
      counter[acc] += 1;
    }
    return ans;
  }

public:
  int subarraySum(vector<int>& nums, int k) {
    return subarraySum1(nums, k);
  }
};

int main() {
  return 0;
}
