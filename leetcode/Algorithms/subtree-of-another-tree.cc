#include "leetcode.h"

class Solution {
  bool isSame1(TreeNode* l, TreeNode* r) {
    if (!l && !r) {
      return true;
    }
    if (!l || !r) {
      return false;
    }
    return l->val == r->val and isSame1(l->left, r->left) and isSame1(l->right, r->right);
  }

  bool dfs1(TreeNode* cur, TreeNode* t) {
    if (!cur) {
      return false;
    }
    if (cur->val == t->val) {
      if (isSame1(cur, t)) {
        return true;
      }
    }
    if (dfs1(cur->left, t)) {
      return true;
    }
    if (dfs1(cur->right, t)) {
      return true;
    }

    return false;
  }

  bool isSubtree1(TreeNode* s, TreeNode* t) {
    if (!t) {
      return true;
    }
    return dfs1(s, t);
  }

public:
  bool isSubtree(TreeNode* s, TreeNode* t) {
    return isSubtree1(s, t);
  }
};

int main() {
  return 0;
}
