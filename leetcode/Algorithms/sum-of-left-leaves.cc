#include "leetcode.h"

class Solution {
  int dfs(TreeNode* node, bool flag) {
    if (node && !node->left && !node->right) {
      return flag ? node->val : 0;
    }
    int sum = 0;
    if (node->left) {
      sum += dfs(node->left, true);
    }
    if (node->right) {
      sum += dfs(node->right, false);
    }
    return sum;
  }

  int sumOfLeftLeaves1(TreeNode* root) {
    if (!root) {
      return 0;
    }

    return dfs(root, false);
  }
  
public:
  int sumOfLeftLeaves(TreeNode* root) {
    return sumOfLeftLeaves1(root);
  }
};
