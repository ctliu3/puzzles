class Solution {
public:
  bool judgeSquareSum(int c) {
    for (int i = 0; i <= (int)sqrt(c); ++i) {
      int a = c - i * i;
      int b = sqrt(a);
      if (b * b == a) {
        return true;
      }
    }
    return false;
  }
};
