#include <iostream>

using namespace std;

class Solution {
  int getSum1(int a, int b) {
    if (a == 0) {
      return b;
    }
    if (b == 0) {
      return a;
    }
    while (b) {
      int carry = a & b;
      a = a ^ b;
      b = carry << 1;
    }
    return a;
  }

public:
  int getSum(int a, int b) {
    return getSum1(a, b);
  }
};

int main() {
  return 0;
}
