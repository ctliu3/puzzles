#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
  vector<string> summaryRanges(vector<int>& nums) {
    if (nums.empty()) {
      return {};
    }
    vector<string> res;
    int n = (int)nums.size();

    for (int i = 0; i < n;) {
      int j = i;
      while (j + 1 < n && nums[j] + 1 == nums[j + 1]) {
        ++j;
      }
      if (i == j) {
        res.push_back(to_string(nums[i]));
      } else {
        res.push_back(to_string(nums[i]) + "->" + to_string(nums[j]));
      }
      i = j + 1;
    }

    return res;
  }
};

int main() {
  return 0;
}
