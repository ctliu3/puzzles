#include "leetcode.h"

const int MOD = 1337;

class Solution {
  // TLE
  int pow1(int a, vector<int> b) {
    if (b.empty()) {
      return 1;
    }
    if (b.size() == 1 && b[0] == 1) {
      return a;
    }
    vector<int> half;
    if (b.back() & 1) {
      half = b;
      half.back() -= 1;
      return ((a % MOD) * pow1(a, half) % MOD);
    }
    int cur = 0;
    for (int i = 0; i < b.size(); ++i) {
      cur = cur * 10 + b[i];
      if (cur > 1) {
        half.push_back(cur / 2);
        cur %= 2;
      } else {
        half.push_back(cur);
      }
    }
    if (cur) {
      half.push_back(cur); // cur = 1
    }
    int m = b.size() - half.size();
    if (b[0] < 2) {
      m -= 1;
    }
    for (int i = 0; i < m; ++i) {
      half.push_back(0);
    }
    int ret = pow1(a, half);
    return (ret % MOD) * (ret % MOD) % MOD;
  }

  int superPow1(int a, vector<int>& b) {
    if (a == 1) {
      return 1;
    }
    int ret = pow1(a, b);
    return ret;
  }

public:
  int superPow(int a, vector<int>& b) {
    return superPow1(a, b);
  }
};

int main() {
  return 0;
}
