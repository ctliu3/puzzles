#include <vector>
#include <cassert>

using namespace std;

class Solution {
  int nthSuperUglyNumber1(int n, vector<int>& primes) {
    assert(n > 0);
    
    vector<int> ugly(n, INT_MAX), index(primes.size(), 0);
    ugly[0] = 1;

    for (int i = 1; i < n; ++i) {
      int& minval = ugly[i];
      for (int j = 0; j < (int)primes.size(); ++j) {
        minval = min(minval, primes[j] * ugly[index[j]]);
      }
      for (int j = 0; j < (int)primes.size(); ++j) {
        index[j] += minval == primes[j] * ugly[index[j]];
      }
    }

    return ugly[n - 1];
  }

public:
  int nthSuperUglyNumber(int n, vector<int>& primes) {
    return nthSuperUglyNumber1(n, primes);
  }
};
