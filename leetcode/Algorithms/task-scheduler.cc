#include "leetcode.h"

class Solution {
  // Sort
  int leastInterval1(vector<char>& tasks, int n) {
    vector<pair<int, int>> counter(26);
    for (int i = 0; i < 26; ++i) {
      counter[i] = {i, 0};
    }
    for (auto t : tasks) {
      ++counter[t - 'A'].second;
    }
    auto cmp = [&](const pair<int, int>& lhs, const pair<int, int>& rhs) {
      return lhs.second > rhs.second;
    };

    int ans = 0;
    while (counter[0].second > 0) {
      int i;
      for (i = 0; i < n + 1; ++i) {
        if (counter[i].second == 0) {
          break;
        }
        --counter[i].second;
        ++ans;
      }
      sort(counter.begin(), counter.end(), cmp);
      if (counter[0].second == 0) {
        break;
      }
      ans += n + 1 - i;
    }

    return ans;
  }

public:
  int leastInterval(vector<char>& tasks, int n) {
    return leastInterval1(tasks, n);
  }
};

int main() {
  return 0;
}
