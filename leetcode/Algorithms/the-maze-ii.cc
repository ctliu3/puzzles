#include "leetcode.h"

const int dx[] = {-1, 1, 0, 0};
const int dy[] = {0, 0, -1, 1};

struct Node {
  int x;
  int y;
  int step;
  Node(int x, int y, int step): x(x), y(y), step(step) {
  }
  bool operator<(const Node& o) const {
    return step > o.step;
  }
};

class Solution {
  bool inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

public:
  int shortestDistance(vector<vector<int> >& maze, vector<int>& start, vector<int>& destination) {
    int n = maze.size();
    if (n == 0) {
      return 0;
    }
    if (start == destination) {
      return 0;
    }
    int m = maze[0].size();
    int sx = start[0], sy = start[1];
    int ex = destination[0], ey = destination[1];
    vector<vector<bool> > used(n, vector<bool>(m, false));
    
    priority_queue<Node> pq;
    pq.push(Node(sx, sy, 0));
    while (!pq.empty()) {
      auto cur = pq.top();
      if (cur.x == ex && cur.y == ey) {
        return cur.step;
      }
      used[cur.x][cur.y] = true;
      pq.pop();
      for (int i = 0; i < 4; ++i) {
        int nx = cur.x;
        int ny = cur.y;
        int count = 0;
        while (inside(nx + dx[i], ny + dy[i], n, m) && maze[nx + dx[i]][ny + dy[i]] != 1) {
          nx += dx[i];
          ny += dy[i];
          count += 1;
        }
        if (count > 0 && !used[nx][ny]) {
          pq.push(Node(nx, ny, cur.step + count));
        }
      }
    }
    return -1;
  }
};

int main() {
  return 0;
}
