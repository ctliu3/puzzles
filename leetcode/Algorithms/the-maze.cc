#include "leetcode.h"

const int dx[] = {-1, 1, 0, 0};
const int dy[] = {0, 0, -1, 1};

class Solution {
  int inside(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

  bool dfs(vector<vector<int> >& maze, vector<vector<bool> >& used,
           int sx, int sy, int ex, int ey, int prev) {
    if (sx == ex && sy == ey) {
      return true;
    }
    used[sx][sy] = true;
    int n = maze.size(), m = maze[0].size();
    for (int i = 0; i < 4; ++i) {
      if ((i ^ 1) == prev) {
        continue;
      }
      int nx = sx + dx[i], ny = sy + dy[i];
      int count = 1;
      while (inside(nx, ny, n, m) && maze[nx][ny] != 1) {
        nx += dx[i];
        ny += dy[i];
        count += 1;
      }
      nx -= dx[i];
      ny -= dy[i];
      count -= 1;
      if (!inside(nx, ny, n, m) || used[nx][ny]) {
        continue;
      }
      if (count <= 0) {
        continue;
      }
      if (dfs(maze, used, nx, ny, ex, ey, i)) {
        return true;
      }
    }
    return false;
  }

public:
  bool hasPath(vector<vector<int> >& maze, vector<int>& start, vector<int>& destination) {
    int n = maze.size(), m = maze[0].size();
    int sx = start[0], sy = start[1];
    int ex = destination[0], ey = destination[1];

    int count = 0;
    for (int i = 0; i < 4; ++i) {
      int nx = ex + dx[i];
      int ny = ey + dy[i];
      if (!inside(nx, ny, n, m) || maze[nx][ny] == 1) {
        count += 1;
      }
    }
    if (count == 0) {
      return false;
    }

    vector<vector<bool> > used(n, vector<bool>(m, false));
    return dfs(maze, used, sx, sy, ex, ey, -1);
  }
};

int main() {
  return 0;
}
