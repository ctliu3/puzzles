#include <iostream>
#include <vector>
#include <set>
#include <map>

using namespace std;

class SegmentTree {
#define LL(x) (x << 1)
#define RR(x) (x << 1 | 1)

  struct TreeNode {
    int l;
    int r;
    int h;
    explicit TreeNode(int l = 0, int r = 0, int h = 0): l(l), r(r), h(h) {
    }
    int mid() {
      return l + (r - l) / 2;
    }
  };

  vector<TreeNode> tree;

  void _init(int idx, int l, int r) {
    tree[idx] = TreeNode(l, r, 0);
    int mid = tree[idx].mid();
    if (l == r) {
      return ;
    }
    _init(LL(idx), l, mid);
    _init(RR(idx), mid + 1, r);
  }

  void _goDown(int idx) {
    TreeNode& cur = tree[idx];
    if (cur.h != -1) {
      if (tree[LL(idx)].h != -1) {
        tree[LL(idx)].h = max(tree[LL(idx)].h, cur.h);
      }
      if (tree[RR(idx)].h != -1) {
        tree[RR(idx)].h = max(tree[RR(idx)].h, cur.h);
      }
      cur.h = -1;
    }
  }

  void _goUp(int idx) {
    TreeNode& cur = tree[idx];
    int lh = tree[LL(idx)].h;
    int rh = tree[RR(idx)].h;
    if (lh == rh) {
      if (lh == -1) {
        cur.h = -1;
      } else {
        cur.h = max(cur.h, lh);
      }
    } else {
      cur.h = -1;
    }
  }

  void _insert(int idx, int l, int r, int h) {
    TreeNode& cur = tree[idx];
    if (cur.l == l && cur.r == r && cur.h != -1) {
      cur.h = max(cur.h, h);
      return ;
    }

    _goDown(idx);

    int mid = cur.mid();
    if (r <= mid) {
      _insert(LL(idx), l, r, h);
    } else if (l > mid) {
      _insert(RR(idx), l, r, h);
    } else {
      _insert(LL(idx), l, mid, h);
      _insert(RR(idx), mid + 1, r, h);
    }

    _goUp(idx);
  }

  void _dfs(int idx, vector<pair<int, int>>& res) {
    TreeNode& cur = tree[idx];
    if (cur.h != -1) {
      if (res.empty() || res.back().second != cur.h) {
        res.push_back({cur.l, cur.h});
      }
      return ;
    }
    _dfs(LL(idx), res);
    _dfs(RR(idx), res);
  }

public:
  explicit SegmentTree(int n) {
    tree.resize(n * 4);
    _init(1, 1, n);
  }

  void insert(int l, int r, int h) {
    _insert(1, l, r, h);
  }

  vector<pair<int, int>> getSkyline() {
    vector<pair<int, int>> res;
    _dfs(1, res);
    return res;
  }

};

class Solution {
public:
  vector<pair<int, int>> getSkyline(vector<vector<int>>& buildings) {
    if (buildings.empty()) {
      return {};
    }

    vector<int> points;
    for (auto& building : buildings) {
      points.push_back(building[0]);
      points.push_back(building[1] - 1);
      points.push_back(building[1]);
    }
    sort(points.begin(), points.end());

    map<int, int> mp1, mp2;
    for (auto& p : points) {
      if (mp1.find(p) == mp1.end()) {
        int sz = mp1.size();
        mp1[p] = sz + 1;
      }
    }
    for (auto& element : mp1) {
      mp2[element.second] = element.first;
    }

    SegmentTree segtree(mp1.size());
    for (auto& building : buildings) {
      int l = mp1[building[0]];
      int r = mp1[building[1] - 1];
      segtree.insert(l, r, building[2]);
    }

    vector<pair<int, int>> res = segtree.getSkyline();
    for (auto& seg : res) {
      seg = {mp2[seg.first], seg.second};
    }

    return res;
  }
};

int main() {
  Solution sol;
  vector<vector<int>> buildings
    = { {2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8} };

  auto res = sol.getSkyline(buildings);
  for (auto& element : res) {
    printf("%d %d\n", element.first, element.second);
  }

  return 0;
}
