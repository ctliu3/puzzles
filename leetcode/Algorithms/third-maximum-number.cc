class Solution {
  int thirdMax1(vector<int>& nums) {
    int max1 = *max_element(nums.begin(), nums.end());
    if (nums.size() < 3) {
      return max1;
    }
    int max2 = INT_MIN;
    for (int num : nums) {
      if (num != max1) {
        max2 = max(max2, num);
      }
    }
    if (max2 == max1) {
      return max1;
    }
    int max3 = INT_MIN;
    bool flag = false;
    for (int num : nums) {
      if (num != max1 && num != max2) {
        max3 = max(max3, num);
        flag = true;
      }
    }
    return flag ? max3 : max1;
  }

public:
  int thirdMax(vector<int>& nums) {
    return thirdMax1(nums);
  }
};
