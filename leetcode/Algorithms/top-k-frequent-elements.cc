#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

class Solution {
  vector<int> topKFrequent1(vector<int>& nums, int k) {
    if (nums.empty()) {
      return {};
    }
    unordered_map<int, int> hash;
    for (auto val : nums) {
      ++hash[val];
    }
    vector<pair<int, int>> pairs;
    for (auto p : hash) {
      pairs.push_back({-p.second, p.first});
    }
    nth_element(pairs.begin(), pairs.begin() + k, pairs.end());

    vector<int> ret;
    for (int i = 0; i < k; ++i) {
      ret.push_back(pairs[i].second);
    }
    return ret;
  }

public:
  vector<int> topKFrequent(vector<int>& nums, int k) {
    return topKFrequent1(nums, k);
  }
};

int main() {
}
