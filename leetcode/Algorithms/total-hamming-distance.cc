#include <iostream>
#include <vector>

using namespace std;

class Solution {
  int totalHammingDistance1(vector<int>& nums) {
    if (nums.size() < 2) {
      return 0;
    }
    vector<int> bits(32, 0);
    for (auto val : nums) {
      for (int i = 0; i < 32; ++i) {
        bits[i] += (val & (1 << i)) ? 1 : 0;
      }
    }

    int ret = 0;
    for (auto val : nums) {
      for (int i = 0; i < 32; ++i) {
        if (val & (1 << i)) {
          ret += nums.size() - bits[i];
        } else {
          ret += bits[i];
        }
      }
    }
    return ret / 2;
  }

public:
  int totalHammingDistance(vector<int>& nums) {
    return totalHammingDistance1(nums);
  }
};

int main() {
  vector<int> nums = {4, 14, 2};
  Solution sol;
  int ret = sol.totalHammingDistance(nums);
  cout << ret << endl;
  return 0;
}
