#include <iostream>
#include <vector>
#include <queue>

using namespace std;

struct Edge {
  int v;
  int w;
  explicit Edge() {
  }
  explicit Edge(int v, int w): v(v), w(w) {
  }

  bool operator<(const Edge& e) const {
    return w > e.w;
  }
};

void sssp(int source, vector<vector<Edge>> edges, vector<int>& dist) {
  priority_queue<Edge> pq;
  dist = vector<int>(edges.size(), -1);

  pq.push(Edge(source, 0));
  while (!pq.empty()) {
    auto edge = pq.top();
    pq.pop();

    int u = edge.v;
    int w = edge.w;
    if (dist[u] == -1) {
      dist[u] = w;
    }
    for (auto& e : edges[u]) {
      if (dist[e.v] != -1) {
        continue;
      }
      pq.push(Edge(e.v, e.w + w));
    }
  }
}

class Solution {
  const int dx[4] = {-1, 1, 0, 0};
  const int dy[4] = {0, 0, 1, -1};
  vector<vector<Edge>> edges;

  int trapRainWater1(vector<vector<int>>& heightMap) {
    if (heightMap.size() < 2 || heightMap[0].size() < 2) {
      return 0;
    }
    int n = heightMap.size(), m = heightMap[0].size();

    edges.resize(n * m + 1);
    int source = n * m;
    int maxh = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (i == 0 || i == n - 1 || j == 0 || j == m - 1) {
          maxh = max(maxh, heightMap[i][j]);
        }
      }
    }

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        int u = i * m + j;
        if (i == 0 || i == n - 1 || j == 0 || j == m - 1) {
          edges[source].push_back(Edge(u, 0));
        }

        int high_count = 0;
        for (int k = 0; k < 4; ++k) {
          int ni = i + dx[k];
          int nj = j + dy[k];
          if (ni < 0 || ni >= n || nj < 0 || nj >= m) {
            continue;
          }
          int v = ni * m + nj;
          if (heightMap[i][j] >= heightMap[ni][nj]) {
            int w = heightMap[i][j] - heightMap[ni][nj];
            edges[u].push_back(Edge(v, w));
            ++high_count;
          } else {
            edges[u].push_back(Edge(v, 0));
          }
        }

        if (high_count == 4) {
          edges[source].push_back(Edge(u, maxh - heightMap[i][j]));
        }
      }
    }

    vector<int> dist(edges.size());
    sssp(source, edges, dist);

    int ret = 0;
    for (int k = 0; k < (int)dist.size() - 1; ++k) {
      int i = k / m;
      int j = k % m;
      if (i == 0 || i == n - 1 || j == 0 || j == m - 1) {
        continue;
      }
      ret += dist[k];
    }
    return ret;
  }

public:
  int trapRainWater(vector<vector<int>>& heightMap) {
    return trapRainWater1(heightMap);
  }
};

int main() {
  vector<vector<int>> matrix =
  {
    {1,4,3,1,3,2},
    {3,2,1,3,2,4},
    {2,3,3,2,3,1}
  };

  Solution sol;
  int ret = sol.trapRainWater(matrix);
  cout << ret << endl;
}
