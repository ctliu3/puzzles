#include "leetcode.h"

class Solution {
  void dfs(TreeNode* root, TreeNode** cur, int L, int R) {
    if (!root) {
      return ;
    }

    if (root->val >= L && root->val <= R) {
      TreeNode* node = new TreeNode(root->val);
      if (*cur == nullptr) {
        *cur = node;
      } else {
        if (root->val > (*cur)->val) {
          (*cur)->right = node;
        } else {
          (*cur)->left = node;
        }
      }
    }

    if ((*cur) == nullptr) {
      dfs(root->left, cur, L, R);
      dfs(root->right, cur, L, R);
    } else {
      dfs(root->left, &((*cur)->left), L, R);
      dfs(root->right, &((*cur)->right), L, R);
    }
  }

public:
  TreeNode* trimBST(TreeNode* root, int L, int R) {
    if (!root) {
      return nullptr;
    }
    TreeNode* cur = nullptr;
    dfs(root, &cur, L, R);
    return cur;
  }
};

int main() {
  return 0;
}
