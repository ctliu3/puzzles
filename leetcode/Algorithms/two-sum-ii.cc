#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {

  vector<int> twoSum1(vector<int>& numbers, int target) {
    unordered_map<int, int> hash;

    for (int i = 0; i < (int)numbers.size(); ++i) {
      int other = target - numbers[i];
      if (hash.count(other)) {
        return {hash[other] + 1, i + 1};
      }
      hash.insert({numbers[i], i});
    }

    return {};
  }

  vector<int> twoSum2(vector<int>& numbers, int target) {
    int n = (int)numbers.size();
    if (n < 2) {
      return {};
    }

    int st = 0, ed = n - 1;

    while (st < ed) {
      int sum = numbers[st] + numbers[ed];
      if (sum == target) {
        return {st + 1, ed + 1};
      } else if (sum > target) {
        --ed;
      } else {
        +st;
      }
    }

    return {};
  }

public:
  vector<int> twoSum(vector<int>& numbers, int target) {
    //return twoSum1(numbers, target);
    return twoSum2(numbers, target);
  }
};

int main() {
  return 0;
}
