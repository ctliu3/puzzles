#include <iostream>
#include <unordered_map>

using namespace std;

class TwoSum {
  unordered_map<int, int> mp;

public:
  explicit TwoSum() {
    mp.clear();
  }

  void add(int number) {
    if (mp.find(number) == mp.end()) {
      mp[number] = 1;
    } else {
      int count = mp[number];
      mp[number] = count + 1;
    }
  }

  bool find(int value) {
    for (auto& pair : mp) {
      int key = pair.first;
      int other = value - key;
      if (key != other) {
        if (mp.find(other) != mp.end()) {
          return true;
        }
      } else {
        if (mp[key] > 1) {
          return true;
        }
      }
    }
    return false;
  }
};

int main() {
  TwoSum twosum;

  twosum.add(0);
  twosum.add(0);
  bool res = twosum.find(0);
  cout << res << endl;
  return 0;
}
