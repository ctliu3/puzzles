#include "leetcode.h"

class Solution {
  bool findTarget1(TreeNode* root, int k) {
    if (!root) {
      return false;
    }

    unordered_set<int> hash;
    queue<TreeNode*> que;
    que.push(root);
    while (!que.empty()) {
      TreeNode* cur = que.front();
      que.pop();
      if (hash.find(k - cur->val) != hash.end()) {
        return true;
      }
      hash.insert(cur->val);
      if (cur->left) {
        que.push(cur->left);
      }
      if (cur->right) {
        que.push(cur->right);
      }
    }
    return false;
  }

public:
  bool findTarget(TreeNode* root, int k) {
    return findTarget1(root, k);
  }
};

int main() {
  return 0;
}
