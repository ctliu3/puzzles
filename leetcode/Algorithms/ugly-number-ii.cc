#include <iostream>
#include <queue>

using namespace std;

class Solution {
public:
  int nthUglyNumber(int n) {
    if (n == 1) {
      return 1;
    }
    --n;

    const vector<int> factors = {2, 3, 5};
    priority_queue<pair<long long, int>> pq[3];
    for (int i = 0; i < (int)factors.size(); ++i) {
      pq[i].push({-factors[i], 1});
    }
    while (n > 0) {
      pair<long long, int> minval = {-1, 1};
      int minidx = 0;
      for (int i = 0; i < 3; ++i) {
        auto& top = pq[i].top();
        if (minval.first == -1 || -top.first < -minval.first) {
          minval = top;
          minidx = i;
        }
      }
      pq[minidx].pop();

      --n;
      if (n == 0) {
        return (int)-minval.first;
      }
      for (int i = minidx; i < (int)factors.size(); ++i) {
        if (factors[i] >= minval.second) {
          pq[minidx].push({minval.first * factors[i], factors[i]});
        }
      }
    }

    return -1;
  }
};

int main() {
  Solution sol;
  int ret = sol.nthUglyNumber(1407);
  cout << ret << endl;
  return 0;
}
