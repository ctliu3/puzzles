#include <iostream>

using namespace std;

class Solution {
public:
  bool isUgly(int num) {
    if (!num) {
      return false;
    }
    if (num == 1) {
      return true;
    }

    for (auto& fact : {2, 3, 5}) {
      while (!(num % fact)) {
        num /= fact;
      }
    }
    return num == 1;
  }
};

int main() {
  return 0;
}
