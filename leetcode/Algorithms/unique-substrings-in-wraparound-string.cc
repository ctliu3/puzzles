#include "leetcode.h"

class Solution {
  int findSubstringInWraproundString1(const string& s) {
    if (s.empty()) {
      return 0;
    }
    int n = (int)s.size();
    vector<int> last(26, 0);
    vector<int> dp(s.size(), 0);
    dp[0] = last[s[0] - 'a'] = 1;
    for (int i = 1; i < n; ++i) {
      char prev = s[i - 1];
      if (prev == 'z') {
        prev = 'a' - 1;
      }
      if (prev + 1 == s[i]) {
        dp[i] = dp[i - 1] + 1;
      } else {
        dp[i] = 1;
      }
      last[s[i] - 'a'] = max(last[s[i] - 'a'], dp[i]);
    }

    int ans = 0;
    for (int i = 0; i < 26; ++i) {
      ans += last[i];
    }
    return ans;
  }

public:
  int findSubstringInWraproundString(string p) {
    return findSubstringInWraproundString1(p);
  }
};

int main() {
  return 0;
}
