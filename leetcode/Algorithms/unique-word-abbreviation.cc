#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class ValidWordAbbr {
  unordered_map<string, vector<string>> mp_;

  string genAbbr(const string& word) {
    if (word.size() < 3) {
      return word;
    }
    return word[0] + to_string(word.size() - 2) +  word.back();;
  }

public:
  ValidWordAbbr(vector<string> &dictionary) {
    this->mp_.clear();

    for (auto& word : dictionary) {
      this->mp_[genAbbr(word)].push_back(word);;
    }
  }

  bool isUnique(string word) {
    string abbr = genAbbr(word);
    vector<string> abbrs = this->mp_[abbr];
    if (abbrs.empty() || (abbrs.size() == 1 && abbrs[0] == word)) {
      return true;
    }
    return false;
  }
};

// Your ValidWordAbbr object will be instantiated and called as such:
// ValidWordAbbr vwa(dictionary);
// vwa.isUnique("hello");
// vwa.isUnique("anotherWord");

int main() {
  return 0;
}
