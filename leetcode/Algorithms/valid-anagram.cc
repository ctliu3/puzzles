#include <iostream>
#include <vector>

using namespace std;

class Solution {
  bool isAnagram1(string s, string t) {
    if (s.size() != t.size()) {
      return false;
    }
    sort(s.begin(), s.end());
    sort(t.begin(), t.end());
    return s == t;
  }

  bool isAnagram2(string s, string t) {
    if (s.size() != t.size()) {
      return false;
    }

    vector<int> count(256, 0);
    for (auto& chr : s) {
      ++count[chr];
    }
    for (auto& chr : t) {
      if (!count[chr]) {
        return false;
      }
      --count[chr];
    }
    return true;
  }

public:
  bool isAnagram(string s, string t) {
    //return isAnagram1(s, t);
    return isAnagram2(s, t);
  }
};

int main() {
}
