#include "leetcode.h"

class Solution {
  bool isPalindrome(const string& s, int i, int j) {
    while (i < j) {
      if (s[i++] != s[j--]) {
        return false;
      }
    }
    return true;
  }

public:
  bool validPalindrome(string s) {
    if (s.empty()) {
      return false;
    }
    int st = 0, ed = int(s.size()) - 1;
    int used = false;
    for (; st < ed; ++st, --ed) {
      if (s[st] != s[ed]) {
        if (used) {
          return false;
        } else {
          bool ret = false;
          ret = isPalindrome(s, st + 1, ed) || isPalindrome(s, st, ed - 1);
          return ret;
        }
      }
    }
    return true;
  }
};
