class Solution {
public:
  bool checkValidString(string s) {
    if (s.empty()) {
      return true;
    }
    int n = (int)s.size();
    if ((n & 1) && s[n / 2] != '*') {
      return false;
    }
  }
};
