#include <iostream>

using namespace std;

using llong = long long;

class Solution {
  bool isPerfectSquare1(int num) {
    if (num == 1) {
      return true;
    }

    llong start = 1, end = num;
    while (end - start > 1) {
      llong mid = start + (end - start) / 2;
      if (mid * mid < num) {
        start = mid;
      } else {
        end = mid;
      }
    }
    return end * end == num;
  }

public:
  bool isPerfectSquare(int num) {
    return isPerfectSquare1(num);
  }
};

int main() {
  Solution sol;
  bool ret = sol.isPerfectSquare(143);
  cout << ret << endl;
}
