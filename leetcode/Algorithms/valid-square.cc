#include "leetcode.h"

class Solution {
  int getSide1(vector<int>& p1, vector<int>& p2) {
    return (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
  }
  bool checkSlope1(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
    if (p1[0] == p2[0] && p3[0] == p4[0]) {
      return true;
    }
    if ((p1[1] - p2[1]) * (p3[0] - p4[0]) != (p1[0] - p2[0]) * (p3[1] - p4[1])) {
      return false;
    }
    return true;
  }

  bool is_square(vector<vector<int>>& pts) {
    int side = -1;
    for (int i = 0; i < pts.size() - 1; ++i) {
      int cur = getSide1(pts[i], pts[i + 1]);
      if (side < 0) {
        side = cur;
      } else if (side != cur || side == 0) {
        return false;
      }
    }
    if (!checkSlope1(pts[0], pts[1], pts[2], pts[3])) {
      return false;
    }
    if (!checkSlope1(pts[0], pts[3], pts[1], pts[2])) {
      return false;
    }

    int diag = getSide1(pts[1], pts[3]);
    if (diag != getSide1(pts[0], pts[1]) + getSide1(pts[0], pts[3])) {
      return false;
    }

    return true;
  }

  bool dfs1(vector<vector<int>>& pts, vector<vector<int>>& sub,
            vector<bool>& used, int dep) {
    if (dep == pts.size()) {
      if (is_square(sub)) {
        return true;
      }
      return false;
    }
    for (int i = 0; i < pts.size(); ++i) {
      if (!used[i]) {
        used[i] = true;
        sub.push_back(pts[i]);
        if (dfs1(pts, sub, used, dep + 1)) {
          return true;
        }
        sub.pop_back();
        used[i] = false;
      }
    }
    return false;
  }

  bool validSquare1(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
    vector<vector<int>> pts = {p1, p2, p3, p4};
    vector<vector<int>> sub;
    vector<bool> used(pts.size(), false);
    return dfs1(pts, sub, used, 0);
  }

public:
  bool validSquare(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
    return validSquare1(p1, p2, p3, p4);
  }
};

int main() {
  return 0;
}
