#include "leetcode.h"

class Solution {
  int triangleNumber1(vector<int>& nums) {
    if (nums.size() < 3) {
      return 0;
    }
    sort(nums.begin(), nums.end());
    int ans = 0;
    for (int i = 1; i < nums.size() - 1; ++i) {
      for (int j = i + 1; j < nums.size(); ++j) {
        int diff = nums[j] - nums[i];
        int pos = upper_bound(nums.begin(), nums.begin() + i, diff) - nums.begin();
        ans += i - pos;
      }
    }
    return ans;
  }

public:
  int triangleNumber(vector<int>& nums) {
    return triangleNumber1(nums);
  }
};

int main() {
  return 0;
}
