class Solution {
public:
  bool validWordAbbreviation(string word, string abbr) {
    int i = 0, j = 0;
    while (i < (int)abbr.size()) {
      int num = 0, k = i;
      if (abbr[k] == '0') {
        return false;
      }
      while (isdigit(abbr[k])) {
        num = num * 10 + abbr[k] - '0';
        ++k;
      }
      if (num == 0) {
        if (abbr[i] != word[j]) {
          return false;
        }
        ++i;
        ++j;
      } else {
        if (num + j > word.size()) {
          return false;
        }
        j += num;
        i = k;
      }
    }
    if (j < word.size()) {
      return false;
    }
    return true;
  }
};
