#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

class Solution {
  bool validWordSquare1(vector<string>& words) {
    if (words.empty()) {
      return false;
    }

    int n = (int)words.size();
    int k = n;
    for (int i = 0; i < n; ++i) {
      k = max(k, (int)words[i].size());
    }
    if (k != n) {
      return false;
    }

    for (int k = 0; k < n; ++k) {
      vector<int> col;
      for (int i = 0; i < n; ++i) {
        if (k < words[i].size()) {
          col.push_back(words[i][k]);
        }
      }
      if (col.size() != words[k].size()) {
        return false;
      }
      for (int i = 0; i < (int)col.size(); ++i) {
        if (col[i] != words[k][i]) {
          return false;
        }
      }
    }
    return true;
  }

public:
  bool validWordSquare(vector<string>& words) {
    return validWordSquare1(words);
  }
};
