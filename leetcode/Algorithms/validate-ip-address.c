#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isIPV4(char* IP) {
  int i = 0, start = 0;
  int val = 0;
  while (IP[i] != '\0') {
    if (IP[i] != '.') {
      val = val * 10 + IP[i] - '0';
      ++i;
      if ((i - start > 1 && val <= 10) || val > 255) {
        return 0;
      }
    } else {
      val = 0;
      ++i;
      start = i;
    }
  }
  return 1;
}

int isIPV6(char* IP) {
  int i = 0, start = 0;
  while (IP[i] != '\0') {
    if (IP[i] != ':') {
      ++i;
    } else {
      if (i - start > 4) {
        return 0;
      }
      ++i;
      start = i;
    }
  }
  if (i - start > 4) {
    return 0;
  }
  return 1;
}

char* validIPAddress(char* IP) {
  int num_dot = 0, num_col = 0, num_digit = 0, num_letter = 0; char* cur = IP;
  int i = 0;
  while (cur[i] != '\0') {
    int valid = 0;
    if (cur[i] == '.' && (cur[i + 1] == '.' || cur[i + 1] == '\0')) {
      return "Neither";
    }
    if (cur[i] == ':' && (cur[i + 1] == ':' || cur[i + 1] == '\0')) {
      return "Neither";
    }
    if ((cur[i] > 'f' && cur[i] <= 'z') || (cur[i] > 'F' && cur[i] <= 'Z')) {
      return "Neither";
    }
    if ((cur[i] >= 'a' && cur[i] <= 'f') || (cur[i] >= 'A' && cur[i] <= 'F')) {
      num_letter += 1;
      valid += 1;
    }
    if (cur[i] >= '0' && cur[i] <= '9') {
      num_digit += 1;
      valid += 1;
    }
    if (cur[i] == '.') {
      num_dot += 1;
      valid += 1;
    }
    if (cur[i] == ':') {
      num_col += 1;
      valid += 1;
    }
    if (num_letter + num_dot + num_digit + num_dot == 0 || valid == 0) {
      return "Neither";
    }
    i += 1;
  }
  if (num_dot == 3 && num_letter == 0 && num_col == 0 && isIPV4(IP)) {
    return "IPv4";
  }
  if (num_col == 7 && num_dot == 0&& isIPV6(IP)) {
    return "IPv6";
  }
  return "Neither";
}

int main() {
  return 0;
}
