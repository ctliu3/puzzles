#include <vector>
#include <stack>

using namespace std;

class Solution {

  // TLE
  bool verifyPreorder1(vector<int>& preorder) {
    if (preorder.size() < 2) {
      return true;
    }
    return verify1(preorder, 0, (int)preorder.size() - 1);
  }

  bool verify1(vector<int>& preorder, int st, int ed) {
    if (st >= ed) {
      return true;
    }

    int pivot = preorder[st], mid = st + 1;
    while (mid < ed && preorder[mid] < pivot) {
      ++mid;
    }
    if (!verify1(preorder, st + 1, mid - 1)) {
      return false;
    }

    int i = mid;
    while (i < ed) {
      if (preorder[i++] <= pivot) {
        return false;
      }
    }
    if (!verify1(preorder, mid, ed)) {
      return false;
    }
    return true;
  }

  bool verifyPreorder2(vector<int>& preorder) {
    int bound = INT_MIN;
    stack<int> stk;

    for (auto val : preorder) {
      if (val < bound) {
        return false;
      }
      while (!stk.empty() && val > stk.top()) {
        bound = stk.top();
        stk.pop();
      }
      stk.push(val);
    }

    return true;
  }

public:
  bool verifyPreorder(vector<int>& preorder) {
    // return verifyPreorder1(preorder);
    return verifyPreorder2(preorder);
  }
};

int main() {
  return 0;
}
