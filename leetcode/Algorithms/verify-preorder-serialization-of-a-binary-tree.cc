#include <string>

using namespace std;

class Solution {
public:
  bool isValidSerialization(string preorder) {
    if (preorder.empty()) {
      return true;
    }
    if (preorder.size() == 1 && preorder[0] == '#') {
      return true;
    }

    int cnt = 1;
    for (int i = 0; i < (int)preorder.size(); ++i) {
      if (preorder[i] == ',') {
        continue;
      }
      if (preorder[i] == '#') {
        cnt -= 1;
      } else {
        while (i < (int)preorder.size() && isdigit(preorder[i])) {
          ++i;
        }
        cnt += 1;
        --i;
      }
      if (cnt < 0 || (cnt == 0 && i != (int)preorder.size() - 1)) {
        return false;
      }
    }
    return cnt == 0;
  }
};

int main() {
  return 0;
}
