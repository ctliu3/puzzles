#include <iostream>
#include <vector>
#include <queue>

using namespace std;

const vector<int> dx = {0, 0, -1, 1};
const vector<int> dy = {-1, 1, 0, 0};

class Solution {
  const static int INF = 2147483647;

  int n;
  int m;

  struct State {
    int x;
    int y;
    int step;
    explicit State(int x, int y, int step): x(x), y(y), step(step) {
    }
  };

  bool canReach(int x, int y) {
    return x >= 0 && x < this->n && y >= 0 && y < this->m;
  }

  void wallsAndGates1(vector<vector<int>>& rooms) {
    if (rooms.empty()) {
      return ;
    }

    this->n = (int)rooms.size();
    this->m = (int)rooms[0].size();

    queue<State> que;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (rooms[i][j] == 0) {
          que.push(State(i, j, 0));
        }
      }
    }

    vector<vector<bool>> visit(n, vector<bool>(m, false));
    while (!que.empty()) {
      int x = que.front().x, y = que.front().y;
      int step = que.front().step;
      que.pop();

      for (int i = 0; i < 4; ++i) {
        int nx = x + dx[i], ny = y + dy[i];
        if (canReach(nx, ny) && !visit[nx][ny] && rooms[nx][ny] == this->INF) {
          visit[nx][ny] = true;
          rooms[nx][ny] = step + 1;
          que.push(State(nx, ny, step + 1));
        }
      }
    }
  }

public:
  void wallsAndGates(vector<vector<int>>& rooms) {
    wallsAndGates1(rooms);
  }
};

int main() {
}
