#include <iostream>
#include <queue>

using namespace std;

class Solution {
  bool canMeasureWater1(int x, int y, int z) {
    if (x + y < z) {
      return false;
    }

    vector<vector<bool>> used(x + 1, vector<bool>(y + 1, false));
    queue<pair<int, int>> que;

    que.push({0, 0});
    used[0][0] = true;
    while (!que.empty()) {
      auto cur = que.front();
      que.pop();

      int i = cur.first, j = cur.second;
      if (i + j == z) {
        return true;
      }

      if (!used[x][j]) {
        used[x][j] = true;
        que.push({x, j});
      }

      if (!used[i][y]) {
        used[i][y] = true;
        que.push({i, y});
      }

      if (!used[0][j]) {
        used[0][j] = true;
        que.push({0, j});
      }

      if (!used[i][0]) {
        used[i][0] = true;
        que.push({i, 0});
      }

      int ni = max(i - (y - j), 0);
      int nj = min(j + i, y);
      if (!used[ni][nj]) {
        used[ni][nj] = true;
        que.push({ni, nj});
      }

      ni = min(i + j, x);
      nj = max(j - (x - i), 0);
      if (!used[ni][nj]) {
        used[ni][nj] = true;
        que.push({ni, nj});
      }
    }
    return false;
  }

public:
  bool canMeasureWater(int x, int y, int z) {
    return canMeasureWater1(x, y, z);
  }
};

int main() {
  return 0;
}
