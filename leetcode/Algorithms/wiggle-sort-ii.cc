#include <vector>

using namespace std;

class Solution {
  // O(n) space + O(nlogn) time
  void wiggleSort1(vector<int>& nums) {
    if (nums.size() < 2) {
      return ;
    }
    sort(nums.rbegin(), nums.rend());
    vector<int> sorted_nums = nums;
    int j = 0;
    for (int i = 1; i < (int)nums.size(); i += 2) {
      nums[i] = sorted_nums[j++];
    }
    for (int i = 0; i < (int)nums.size(); i += 2) {
      nums[i] = sorted_nums[j++];
    }
  }

  // O(1) space + O(n) time
  void wiggleSort2(vector<int>& nums) {
    if (nums.size() < 2) {
      return ;
    }
    int n = (int)nums.size();
    auto ptr = nums.begin() + n / 2;
    nth_element(nums.begin(), ptr, nums.end());
    int mid = *ptr;

    #define A(i) nums[(1 + 2 *  (i)) % (n | 1)]
    
    int i = 0, j = 0, k = n - 1;
    while (j <= k) {
      // printf("%d %d %d\n", i, j, k);
      if (A(j) > mid) {
        swap(A(j++), A(i++));
      } else if (A(j) < mid) {
        swap(A(j), A(k--));
      } else {
        ++j;
      }
    }
  }

public:
  void wiggleSort(vector<int>& nums) {
    // return wiggleSort1(nums);
    return wiggleSort2(nums);
  }
};

int main() {
  vector<int> nums = {1,1,2,1,2,2,1};
  Solution sol;
  sol.wiggleSort(nums);
  for (int num : nums) {
    printf("%d ", num);
  }
  return 0;
}
