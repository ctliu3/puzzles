#include <iostream>
#include <vector>

using namespace std;

class Solution {
  void wiggleSort1(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    if (nums.size() < 3) {
      return ;
    }
    for (int i = 1; i + 1 < (int)nums.size(); i += 2) {
      swap(nums[i], nums[i + 1]);
    }
  }

public:
  void wiggleSort(vector<int>& nums) {
    wiggleSort1(nums);
  }
};

int main() {
  return 0;
}
