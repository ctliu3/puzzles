#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Solution {
  int wiggleMaxLength1(vector<int>& nums) {
    int n = (int)nums.size();
    if (n < 2) {
      return n;
    }
    vector<vector<int> > dp(n, vector<int>(2, 1));
    int ans = 0;
    for (int i = 1; i < n; ++i) {
      for (int j = 0; j < i; ++j) {
        if (nums[j] > nums[i]) {
          dp[i][0] = max(dp[i][0], dp[j][1] + 1);
        }
        if (nums[j] < nums[i]) {
          dp[i][1] = max(dp[i][1], dp[j][0] + 1);
        }
        ans = max(ans, dp[i][0]);
        ans = max(ans, dp[i][1]);
      }
    }
    return ans;
  }

public:
  int wiggleMaxLength(vector<int>& nums) {
    return wiggleMaxLength1(nums);
  }
};

int main() {
  vector<int> nums = {1,7,4,9};
  Solution sol;

  int ret = sol.wiggleMaxLength(nums);
  cout << ret << endl;
}
