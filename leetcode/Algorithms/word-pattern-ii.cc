#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Solution {
  bool match(const string& pattern, const string& str, vector<string>& lmp,
             unordered_set<string>& hash, int dep, int pos) {
    if (dep == (int)pattern.size()) {
      return pos == (int)str.size();
    }

    int idx = pattern[dep] - 'a';
    if (!lmp[idx].empty()) {
      int len = lmp[idx].size();
      if (pos + len - 1 >= (int)str.size()
          || str.substr(pos, len) != lmp[idx]) {
        return false;
      }
      return match(pattern, str, lmp, hash, dep + 1, pos + len);
    }

    for (int i = pos; i < (int)str.size(); ++i) {
      string sub = str.substr(pos, i - pos + 1);
      if (hash.find(sub) != hash.end()) {
        continue;
      }
      lmp[idx] = sub;
      hash.insert(sub);
      if (match(pattern, str, lmp, hash, dep + 1, i + 1)) {
        return true;
      }
      lmp[idx].clear();
      hash.erase(sub);
    }

    return false;
  }

public:
  bool wordPatternMatch(string pattern, string str) {
    if (pattern.empty() && str.empty()) {
      return true;
    }
    if (pattern.empty() || str.empty() || pattern.size() > str.size()) {
      return false;
    }

    vector<string> lmp(26);
    unordered_set<string> hash;
    return match(pattern, str, lmp, hash, 0, 0);
  }
};

int main() {
  Solution sol;
  bool ret = sol.wordPatternMatch("aa", "ddabc");
  cout << ret << endl;
  return 0;
}
