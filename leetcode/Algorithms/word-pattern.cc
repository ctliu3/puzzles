#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
  bool wordPattern(string pattern, string str) {
    vector<string> strs;
    string temp;
    istringstream sin(str);
    while (sin >> temp) {
      strs.push_back(temp);
    }

    if (pattern.size() != strs.size()) {
      return false;
    }

    vector<bool> hash(26, false);
    unordered_map<string, char> mp;
    int n = (int)pattern.size();
    for (int i = 0; i < n; ++i) {
      if (mp.find(strs[i]) != mp.end()) {
        if (mp[strs[i]] != pattern[i]) {
          return false;
        }
      } else if (hash[pattern[i] - 'a']) {
        return false;
      }
      mp[strs[i]] = pattern[i];
      hash[pattern[i] - 'a'] = true;
    }
    return true;
  }
};

int main() {
  return 0;
}
