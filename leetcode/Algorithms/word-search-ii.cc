#include <iostream>
#include <vector>
#include <string>
#include <set>

using namespace std;

int dx[] = {-1, 1, 0, 0};
int dy[] = {0, 0, -1, 1};

class TrieNode {
public:
  TrieNode* child[26];
  bool word;

  explicit TrieNode() {
    for (int i = 0; i < 26; ++i) {
      child[i] = nullptr;
    }
    word = false;
  }
};

class Trie {
public:
  TrieNode* root;
  explicit Trie() {
    root = new TrieNode();
  }

  void insert(string& s) {
    TrieNode* cur = root;
    for (auto& chr : s) {
      if (!cur->child[chr - 'a']) {
        cur->child[chr - 'a'] = new TrieNode();
      }
      cur = cur->child[chr - 'a'];
    }
    cur->word = true;
  }
};

class Solution {
  bool inBoard(int x, int y, int n, int m) {
    return x >= 0 && x < n && y >= 0 && y < m;
  }

  void searchWord(vector<vector<char>>& board, int x, int y, string& word,
    vector<vector<bool>>& visit, TrieNode* cur, set<string>& hash) {
    if (cur->word) {
      hash.insert(word);
    }

    for (int i = 0; i < 4; ++i) {
      int nx = x + dx[i];
      int ny = y + dy[i];
      if (inBoard(nx, ny, board.size(), board[0].size())) {
        char letter = board[nx][ny];
        TrieNode* next = cur->child[letter - 'a'];

        if (!visit[nx][ny] && next) {
          visit[nx][ny] = true;
          word.push_back(letter);
          searchWord(board, nx, ny, word, visit, next, hash);
          word.pop_back();
          visit[nx][ny] = false;
        }
      }
    }
  }

public:
  vector<string> findWords(vector<vector<char>>& board, vector<string>& words) {
    if (board.empty() || words.empty()) {
      return {};
    }

    Trie trie;
    for (auto& word : words) {
      trie.insert(word);
    }

    int n = board.size(), m = board[0].size();
    vector<vector<bool>> visit(n, vector<bool>(m, false));
    set<string> hash;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        visit.assign(n, vector<bool>(m, false));
        visit[i][j] = true;
        TrieNode* cur = trie.root->child[board[i][j] - 'a'];
        string word = string(1, board[i][j]);
        if (cur) {
          searchWord(board, i, j, word, visit, cur, hash);
        }
      }
    }
    vector<string> res;
    for (auto& word : hash) {
      res.push_back(word);
    }
    return res;
  }
};

int main() {
  Solution sol;
  vector<vector<char>> board =
  {
    {'o', 'a', 'a', 'n'},
    {'e', 't', 'a', 'e'},
    {'i', 'h', 'k', 'r'},
    {'i', 'f', 'l', 'v'}
  };
  vector<string> words = {"a"};

  vector<string> res = sol.findWords(board, words);
  for (auto& word : res) {
    cout << word << endl;
  }

  return 0;
}
