#include <iostream>
#include <vector>
#include <list>

using namespace std;

class ZigzagIterator {
  list<vector<int>> lst;
  list<int> idx;
  pair<list<vector<int>>::iterator, list<int>::iterator> which;

  void advance_which() {
    ++which.first;
    ++which.second;
  }

  void mod_which() {
    if (which.first == lst.end()) {
      which = { lst.begin(), idx.begin() };
    }
  }

public:
  ZigzagIterator(vector<int>& v1, vector<int>& v2) {
    lst.push_back(v1);
    lst.push_back(v2);
    idx.assign(lst.size(), 0);
    which = { lst.begin(), idx.begin() };
  }

  int next() {
    int value = (*which.first)[*which.second];
    ++(*which.second);
    advance_which();
    mod_which();
    return value;
  }

  bool hasNext() {
    if (lst.empty()) {
      return false;
    }
    while ((int)which.first->size() == *which.second) {
      which = { lst.erase(which.first), idx.erase(which.second) };
      mod_which();
    }
    if (lst.empty()) {
      return false;
    }
    return true;
  }

};

int main() {
  vector<int> v1 = {};
  vector<int> v2 = {3, 4, 5, 6};
  ZigzagIterator i(v1, v2);
  while (i.hasNext()) cout << i.next();
  return 0;
}

/**
 * Your ZigzagIterator object will be instantiated and called as such:
 * ZigzagIterator i(v1, v2);
 * while (i.hasNext()) cout << i.next();
 */


