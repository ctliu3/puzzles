#include "lintcode.h"

class Solution {
public:
  /**
   * This method will be invoked first, you should design your own algorithm 
   * to serialize a binary tree which denote by a root node to a string which
   * can be easily deserialized by your own "deserialize" method later.
   */
  string serialize(TreeNode *root) {
    if (!root) {
      return {};
    }
    string res;

    queue<TreeNode*> que;
    que.push(root);
    while (!que.empty()) {
      TreeNode* cur = que.front();
      que.pop();
      
      if (cur) {
        res.append(to_string(cur->val) + ",");
      } else {
        res.append("#,");
        continue;
      }

      que.push(cur->left);
      que.push(cur->right);
    }

    return res;
  }

  /**
   * This method will be invoked second, the argument data is what exactly
   * you serialized at method "serialize", that means the data is not given by
   * system, it's given by your own serialize method. So the format of data is
   * designed by yourself, and deserialize it here as you serialize it in 
   * "serialize" method.
   */
  TreeNode *deserialize(string data) {
    for (auto& chr : data) {
      if (chr == ',') {
        chr = ' ';
      }
    }

    vector<TreeNode*> nodes;
    istringstream sin(data);
    string s;
    while (sin >> s) {
      if (s == "#") {
        nodes.push_back(nullptr);
      } else {
        nodes.push_back(new TreeNode(atoi(s.c_str())));
      }
    }
    if (nodes.empty()) {
      return nullptr;
    }

    int j = 0;
    for (int i = 1; i < (int)nodes.size(); i += 2) {
      TreeNode* left = nodes[i], *right = nodes[i + 1];
      nodes[j]->left = left;
      nodes[j]->right = right;
      ++j;
      while (j < (int)nodes.size() && !nodes[j]) {
        ++j;
      }
    }

    return nodes[0];
  }
};

int main() {
  return 0;
}
