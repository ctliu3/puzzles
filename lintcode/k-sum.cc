#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

typedef unordered_map<int, int> MAPII;

class Solution {
public:
  /**
   * @param A: an integer array.
   * @param k: a positive integer (k <= length(A))
   * @param target: a integer
   * @return an integer
   */
  int kSum(vector<int> A, int k, int target) {
    int n = (int)A.size();
    vector<vector<MAPII>> dp(n + 1, vector<MAPII>(k + 1, MAPII()));
    dp[0][0][0] = 1;

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j <= min(i, k); ++j) {
        for (auto& pair : dp[i][j]) {
          dp[i + 1][j][pair.first] += pair.second;
          if (j + 1 <= k && pair.first + A[i] <= target) {
            dp[i + 1][j + 1][pair.first + A[i]] += pair.second;
          }
        }
      }
    }

    return dp[n][k][target];
  }
};

int main() {
  Solution sol;
  vector<int> A = {1, 2, 3, 4};
  int res = sol.kSum(A, 2, 5);
  cout << res << endl;

  return 0;
}
