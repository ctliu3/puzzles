#ifndef _LINTCODE_HEADER
#define _LINTCODE_HEADER

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cassert>
#include <vector>
#include <stack>
#include <queue>
#include <sstream>
#include <string>
#include <algorithm>
#include <limits> // std::numeric_limits
#include <climits> // INT_MIN, INT_MAX
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

using namespace std;

#define dump(x) cerr << #x << " = " << x << endl;

// Definition of TreeNode:
class TreeNode {
public:
  int val;
  TreeNode *left, *right;
  TreeNode(int val) {
    this->val = val;
    this->left = this->right = NULL;
  }
};

// Definition of Interval:
class Interval {
public:
  int start, end;
  Interval(int start, int end) {
    this->start = start;
    this->end = end;
  }
};

#endif
