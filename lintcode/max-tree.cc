#include "lintcode.h"

class Solution {
public:
  /**
   * @param A: Given an integer array with no duplicates.
   * @return: The root of max tree.
   */
  TreeNode* maxTree(vector<int> A) {
    if (A.empty()) {
      return nullptr;
    }

    stack<TreeNode*> stk;
    TreeNode* root = nullptr;
    int maxval = A[0];
    for (auto& num : A) {
      TreeNode* cur = new TreeNode(num);
      if (num >= maxval) {
        maxval = num;
        root = cur;
      }

      if (!stk.empty()) {
        while (stk.size() > 1 && stk.top()->val < num) {
          stk.pop();
        }
        if (stk.top()->val < num) {
          cur->left = stk.top();
          stk.pop();
          stk.push(cur);
        } else {
          cur->left = stk.top()->right;
          stk.top()->right = cur;
          stk.push(cur);
        }
      } else {
        stk.push(cur);
      }
    }

    return root;
  }
};

int main() {
  return 0;
}
