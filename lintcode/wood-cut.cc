#include <iostream>
#include <vector>

using namespace std;

class Solution {

  bool _isOk(const vector<int>& L, int k, int len) {
    int count = 0;
    for (auto& l : L) {
      count += l / len;
      if (count >= k) {
        return true;
      }
    }
    return false;
  }

public:
  /**
   *@param L: Given n pieces of wood with length L[i]
   *@param k: An integer
   *return: The maximum length of the small pieces.
   */
  int woodCut(vector<int> L, int k) {
    if (!k || L.empty()) {
      return 0;
    }

    int maxl = L[0];
    for (auto& l : L) {
      maxl = max(l, maxl);
    }

    int st = 1, ed = maxl;
    int res = 0;
    while (st <= ed) {
      int mid = st + (ed - st) / 2;
      if (_isOk(L, k, mid)) {
        res = max(res, mid);
        st = mid + 1;
      } else {
        ed = mid - 1;
      }
    }

    return res;
  }
};

int main() {
  Solution sol;
  vector<int> L = {232, 124, 456};

  int res = sol.woodCut(L, 7);
  cout << res << endl;

  return 0;
}
