#include <iostream>
#include <vector>

using namespace std;

int main() {
  freopen("in", "r", stdin);
  int n;
  vector<int> arr;

  while (cin >> n) {
    arr.resize(n);
    int sum = 0, single_max = 0;
    for (int i = 0; i < n; ++i) {
      scanf("%d", &arr[i]);
      sum += arr[i];
      single_max = i ? max(single_max, arr[i]) : arr[0];
    }

    int maxval = arr[0], maxacc = arr[0];
    int minval = arr[0], minacc = arr[0];
    for (int i = 1; i < n; ++i) {
      maxacc = maxacc > 0 ? maxacc + arr[i] : arr[i];
      maxval = max(maxval, maxacc);

      minacc = minacc < 0 ? minacc + arr[i] : arr[i];
      minval = min(minval, minacc);
    }
    if (sum == minval) {
      cout << single_max << endl;
    } else {
      cout << max(maxval, sum - minval) << endl;
    }
  }

  return 0;
}
