#include <iostream>
#include <vector>

using namespace std;

class SegNode {
public:
  int left, right;
  int val;
  explicit SegNode(): val(0) {
  }
  int mid() {
    return left + (right - left) / 2;
  }
};

class SegTree {
public:
  explicit SegTree(vector<int>& v) {
    root = 1;
    nodes.resize(v.size() * 4);
    build(v, root, 1, v.size());
  }

  void insert(int idx, int pos, int val) {
    nodes[idx].val += val;
    if (nodes[idx].left == pos && nodes[idx].right == pos) {
      return ;
    }
    int mid = nodes[idx].mid();
    if (pos <= mid) {
      insert(L(idx), pos, val);
    } else if (pos >= mid + 1) {
      insert(R(idx), pos, val);
    }
  }

  int query(int idx, int left, int right) {
    if (nodes[idx].left == left && nodes[idx].right == right) {
      return nodes[idx].val;
    }
    int mid = nodes[idx].mid();
    if (right <= mid) {
      return query(L(idx), left, right);
    } else if (left >= mid + 1) {
      return query(R(idx), left, right);
    } else {
      return query(L(idx), left, mid) + query(R(idx), mid + 1, right);
    }
  }

private:
 void build(vector<int>& v, int idx, int left, int right) {
    nodes[idx].left = left;
    nodes[idx].right = right;
    if (left == right) {
      nodes[idx].val = v[left - 1];
      return ;
    }
    int mid = left + (right - left) / 2;
    build(v, L(idx), left, mid);
    build(v, R(idx), mid + 1, right);
    nodes[idx].val = nodes[L(idx)].val + nodes[R(idx)].val;
  }

  int L(int x) {
    return x << 1;
  }

  int R(int x) {
    return (x << 1) | 1;
  }

  vector<SegNode> nodes;
  int root;
};

int main() {
  int t, n, cas = 1;
  int l, r, pos, val;
  vector<int> v;
  char buf[8];

  freopen("in", "r", stdin);
  scanf("%d", &t);
  while (t--) {
    printf("Case %d:\n", cas++);
    scanf("%d", &n);
    v.resize(n);
    for (int i = 0; i < n; ++i) {
      scanf("%d", &v[i]);
    }
    SegTree st = SegTree(v);

    while (scanf("%s", buf) && buf[0] != 'E') {
      if (buf[0] == 'Q') {
        scanf("%d %d", &l, &r);
        printf("%d\n", st.query(1, l, r));
      } else {
        scanf("%d %d", &pos, &val);
        if (buf[0] == 'S') {
          st.insert(1, pos, -val);
        } else {
          st.insert(1, pos, val);
        }
      }
    }
  }

  return 0;
}
