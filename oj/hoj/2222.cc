#include <iostream>
#include <queue>

using namespace std;

#define nullptr NULL

const int MAXN = 26;

class TrieNode {
public:
  TrieNode* child[MAXN];
  TrieNode* fail;
  int cnt;
  explicit TrieNode(): fail(nullptr), cnt(0) {
    for (int i = 0; i < MAXN; ++i) {
      child[i] = nullptr;
    }
  }
};

class ACAutomaton {
public:
  TrieNode* root;
  explicit ACAutomaton() {
    root = new TrieNode();
  }

  int get_index(char c) {
    return c - 'a';
  }

  void insert(const char* s) {
    TrieNode* cur = root;
    while (*s) {
      int idx = get_index(*s);
      if (! cur->child[idx]) {
        cur->child[idx] = new TrieNode();
      }
      cur = cur->child[idx];
      ++s;
    }
    ++cur->cnt;
  }

  void build(){
    queue<TrieNode*> que;

    for (int i = 0; i < MAXN; ++i) {
      if (root->child[i]) {
        root->child[i]->fail = root;
        que.push(root->child[i]);
      }
    }
    while (que.size()) {
      TrieNode* cur = que.front();
      que.pop();
      for (int i = 0; i < MAXN; ++i) {
        if (! cur->child[i]) {
          continue;
        }
        que.push(cur->child[i]);
        TrieNode* u = cur->fail, *& v = cur->child[i]->fail;
        v = root;
        while (u != root && ! u->child[i]) {
          u = u->fail;
        }
        if (u->child[i]) {
          v = u->child[i];
        }
      }
    }
  }

  int search(char* s) {
    TrieNode* cur = root;
    int res = 0;

    while (*s) {
      int idx = get_index(*s);
      while (! cur->child[idx] && cur != root) {
        cur = cur->fail;
      }
      if (cur->child[idx]) {
        cur = cur->child[idx];
        TrieNode* p = cur;
        while (p != root && p->cnt > 0) {
          res += p->cnt;
          p->cnt = 0;
          p = p->fail;
        }
      }
      ++s;
    }

    return res;
  }
}ac;

int main() {
  int t, n;
  static char word[1000000 + 5];

  scanf("%d", &t);
  while (t--) {
    ac = ACAutomaton();
    scanf("%d", &n);
    while (n--) {
      scanf(" %s", word);
      ac.insert(word);
    }
    ac.build();

    scanf("%s", word);
    int res = ac.search(word);
    printf("%d\n", res);
  }

  return 0;
}
