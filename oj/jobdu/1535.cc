#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>

using namespace std;

typedef unsigned long long ull;

const ull R = 256;
const ull MOD = 99999989;

void mod_val(ull& hash, const ull& MOD) {
  if (hash >= MOD) {
    hash %= MOD;
  }
}

bool check(const string& sa, const string& sb, int n) {
  for (int i = 0; i < n; ++i) {
    if (sa[sa.size() - n + i] != sb[i]) {
      return false;
    }
  }
  return true;
}

char sa[1000000], sb[1000000];

int main() {
  //string sa, sb;
  freopen("in", "r", stdin);

  while (scanf("%s %s", sa, sb) == 2) {
    int res = 0;
    //int minn = min(sa.size(), sb.size());
    int na = strlen(sa), nb = strlen(sb);
    int minn = min(na, nb);
    ull ahash = 0, bhash = 0;
    ull MR = 1;
    for (int i = 0; i < minn; ++i) {
      ahash = ((sa[na - i - 1] - 'a') * MR % MOD) + ahash;
      bhash = bhash * R + sb[i] - 'a';
      mod_val(ahash, MOD);
      mod_val(bhash, MOD);
      if (ahash == bhash/* && check(sa, sb, i + 1)*/) {
        res = i + 1;
      }
      MR *= R;
      mod_val(MR, MOD);
    }
    cout << res << endl;
  }

  return 0;
}
