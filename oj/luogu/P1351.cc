#include <iostream>
#include <vector>
#include <cstring>
#include <cstdio>

using namespace std;

const int MAXN = 200005;
const int MOD = 10007;
vector<int> e[MAXN];
int path[MAXN], w[MAXN], children[MAXN];
bool visit[MAXN];
int n, maxw, sum = 0;

void dfs(int u, int dep, int fa) {
  path[dep] = u;

  visit[u] = true;
  if (dep - 2 >= 0) {
    maxw = max(maxw, w[u] * w[path[dep - 2]]);
    sum += w[u] * w[path[dep - 2]];
    if (sum > MOD) {
      sum %= MOD;
    }
  }
  if (e[u].empty()) {
    return ;
  }

  int m = 0, tot = 0;
  int maxfst = -1, maxsnd = -1;
  for (int i = 0; i < (int)e[u].size(); ++i) {
    int v = e[u][i];
    if (v == fa) {
      continue;
    }
    children[m++] = w[v];
    if (w[v] > maxfst) {
      maxsnd = maxfst;
      maxfst = w[v];
    } else if (w[v] > maxsnd) {
      maxsnd = w[v];
    }
    tot += w[v];
  }
  if (m > 1) {
    maxw = max(maxw, maxfst * maxsnd);
    for (int i = 0; i < m - 1; ++i) {
      tot -= children[i];
      sum = sum + ((children[i] % MOD) * (tot % MOD)) % MOD;
      if (sum > MOD) {
        sum %= MOD;
      }
    }
  }

  for (int i = 0; i < (int)e[u].size(); ++i) {
    int v = e[u][i];
    if (!visit[v]) {
      dfs(v, dep + 1, u);
    }
  }
}

int main() {
  freopen("in", "r", stdin);
  int u, v;

  scanf("%d", &n);
  for (int i = 0; i < n - 1; ++i) {
    scanf("%d%d", &u, &v);
    --u;
    --v;
    e[u].push_back(v);
    e[v].push_back(u);
  }
  for (int i = 0; i < n; ++i) {
    scanf("%d", w + i);
  }

  memset(visit, false, sizeof(visit));
  maxw = -1;
  for (int i = 0; i < n; ++i) {
    if (!visit[i]) {
      dfs(i, 0, -1);
    }
  }
  printf("%d %d\n", maxw, (sum * 2) % MOD);

  return 0;
}
