#include <iostream>
#include <vector>
#include <cstring>
#include <queue>
#include <cstdio>

using namespace std;

const int MAXN = 505;
int dx[] = {-1, 0, 1, 0};
int dy[] = {0, 1, 0, -1};
int h[MAXN][MAXN], l[MAXN], r[MAXN], dp[MAXN];
int visit[MAXN][MAXN];
bool last[MAXN];
int n, m;

bool inside(int x, int y) {
  return x >= 0 && x < n && y >= 0 && y < m;
}

int bfs() {
  queue<pair<int, int> > que;
  memset(last, false, sizeof(last));
  memset(visit, -1, sizeof(visit));
  memset(l, -1, sizeof(l));
  memset(r, -1, sizeof(r));

  for (int j = 0; j < m; ++j) {
    visit[0][j] = j;
    que.push(make_pair(0, j));
    while (que.size()) {
      int x = que.front().first, y = que.front().second;
      que.pop();

      for (int i = 0; i < 4; ++i) {
        int nx = x + dx[i];
        int ny = y + dy[i];
        if (inside(nx, ny) && visit[nx][ny] != j && h[x][y] > h[nx][ny]) {
          visit[nx][ny] = j;
          que.push(make_pair(nx, ny));
        }
      }
    }

    for (int k = 0; k < m; ++k) {
      if (visit[n - 1][k] == j) {
        last[k] = true;
        if (l[j] == -1 || k < l[j]) {
          l[j] = k;
        }
        if (r[j] == -1 || k > r[j]) {
          r[j] = k;
        }
      }
    }
  }

  int cnt = 0;
  for (int j = 0; j < m; ++j) {
    if (last[j]) {
      ++cnt;
    }
  }
  //printf("cnt = %d\n", cnt);
  return cnt;
}

int main() {
  freopen("in", "r", stdin);

  scanf("%d%d", &n, &m);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      scanf("%d", &h[i][j]);
    }
  }

  int res = bfs();
  if (res != m) {
    printf("0\n%d\n", m - res);
    return 0;
  }

  //for (int j = 0; j < m; ++j) {
    //printf("l[%d] = %d, r[%d] = %d\n", j, l[j], j, r[j]);
  //}

  fill(dp, dp + m, INT_MAX);
  dp[0] = 1;
  for (int j = 1; j < m; ++j) {
    for (int i = 0; i < m; ++i) {
      if (r[i] >= j && l[i] <= j) {
        dp[j] = min(dp[j], dp[l[i] - 1] + 1);
      }
    }
  }
  printf("1\n%d\n", dp[m - 1]);

  return 0;
}
