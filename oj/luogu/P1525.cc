#include <iostream>
#include <vector>

using namespace std;

const int MAXN = 20005;
int color[MAXN];
int weight[100005];
vector<pair<int, int> > e[MAXN];
int n, m;

bool dfs(int u, int flag, int mw) {
  color[u] = flag;
  for (int i = 0; i < (int)e[u].size(); ++i) {
    int v = e[u][i].first, w = e[u][i].second;
    if (w <= mw) {
      continue;
    }
    if (color[v] == -1) {
      if (!dfs(v, 1 - flag, mw)) {
        return false;
      }
    } else {
      if (color[v] == flag) {
        return false;
      }
    }
  }
  return true;
}

int main() {
  freopen("in", "r", stdin);
  int u, v, w;

  scanf("%d%d", &n, &m);
  for (int i = 0; i < m; ++i) {
    scanf("%d%d%d", &u, &v, &w);
    --u;
    --v;
    weight[i] = w;
    e[u].push_back(make_pair(v, w));
    e[v].push_back(make_pair(u, w));
  }
  weight[m++] = 0;

  sort(weight, weight + m);
  int start = 0, end = m - 1;
  int res = m - 1;
  while (start <= end) {
    int mid = start + (end - start) / 2;

    fill(color, color + n, -1);
    bool ok = true;
    for (int i = 0; i < n; ++i) {
      if (color[i] == -1 && !dfs(i, 0, weight[mid])) {
        ok = false;
        break;
      }
    }

    if (ok) {
      res = min(res, mid);
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }
  printf("%d\n", weight[res]);

  return 0;
}
