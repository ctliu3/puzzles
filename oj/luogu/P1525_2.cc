#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int MAXN = 40005;

struct Edge {
  int u;
  int v;
  int w;
  Edge(int u = 0, int v = 0, int w = 0): u(u), v(v), w(w) {
  }

  bool operator<(const Edge& e) const {
    return w < e.w;
  }
}e[100005];

int fa[MAXN];
int n, m;

int find(int u) {
  if (u != fa[u]) {
    fa[u] = find(fa[u]);
  }
  return fa[u];
}

int main() {
  freopen("in", "r", stdin);
  int u, v, w;

  scanf("%d%d", &n, &m);
  for (int i = 0; i < m; ++i) {
    scanf("%d%d%d", &u, &v, &w);
    e[i] = Edge(u - 1, v - 1, w);
  }

  sort(e, e + m);

  for (int i = 0; i < n + n; ++i) {
    fa[i] = i;
  }
  for (int i = m - 1; i >= 0; --i) {
    int u = find(e[i].u);
    int v = find(e[i].v);

    if (u == v) {
      printf("%d\n", e[i].w);
      return 0;
    }

    fa[find(v + n)] = u;
    fa[find(u + n)] = v;
  }
  printf("0\n");

  return 0;
}

