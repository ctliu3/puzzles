#include <iostream>
#include <queue>

using namespace std;

int main() {
  freopen("in", "r", stdin);
  int n, m, cur, res = 0;
  queue<int> que;
  bool word[1005] = {false};

  scanf("%d %d", &m, &n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &cur);
    if (word[cur]) {
      continue;
    }
    ++res;
    if ((int)que.size() < m) {
      word[cur] = 1;
      que.push(cur);
    } else {
      word[que.front()] = false;
      que.pop();
      que.push(cur);
      word[cur] = true;
    }
  }
  printf("%d\n", res);

  return 0;
}
