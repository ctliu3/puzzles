#include <iostream>

using namespace std;

int dp[41][41][41][41];
int score[400];
int cnt[4];

int main() {
  freopen("in", "r", stdin);
  int n, m, digit;

  scanf("%d %d", &n, &m);
  for (int i = 0; i < n; ++i) {
    scanf("%d", score + i);
  }
  fill(cnt, cnt + 4, 0);
  for (int i = 0; i < m; ++i) {
    scanf("%d", &digit);
    ++cnt[digit - 1];
  }

  dp[0][0][0][0] = score[0];
  for (int i = 0; i <= cnt[0]; ++i) {
    for (int j = 0; j <= cnt[1]; ++j) {
      for (int k = 0; k <= cnt[2]; ++k) {
        for (int o = 0; o <= cnt[3]; ++o) {
          int cur = i + j * 2 + k * 3 + o * 4;
          if (i > 0) {
            dp[i][j][k][o] = max(dp[i][j][k][o], dp[i - 1][j][k][o] + score[cur]);
          }
          if (j > 0) {
            dp[i][j][k][o] = max(dp[i][j][k][o], dp[i][j - 1][k][o] + score[cur]);
          }
          if (k > 0) {
            dp[i][j][k][o] = max(dp[i][j][k][o], dp[i][j][k - 1][o] + score[cur]);
          }
          if (o > 0) {
            dp[i][j][k][o] = max(dp[i][j][k][o], dp[i][j][k][o - 1] + score[cur]);
          }
        }
      }
    }
  }
  printf("%d\n", dp[cnt[0]][cnt[1]][cnt[2]][cnt[3]]);

  return 0;
}
