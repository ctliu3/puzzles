#include <iostream>
#include <vector>
#include <cstring>
#include <cstdio>

using namespace std;

const int MAXN = 135;
int mat[MAXN][MAXN], sum[MAXN][MAXN];
int d, n;

int main() {
  //freopen("wireless.in", "r", stdin);
  //freopen("wireless.out", "w", stdout);
  freopen("in", "r", stdin);
  int x, y, k;

  scanf("%d", &d);
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    scanf("%d%d%d", &x, &y, &k);
    mat[x + 1][y + 1] = k;
  }

  for (int i = 1; i <= 129; ++i) {
    int cnt = 0;
    for (int j = 1; j <= 129; ++j) {
      cnt += mat[i][j];
      sum[i][j] = sum[i - 1][j] + cnt;
    }
  }

  int maxpot = 0, cnt = 0;
  for (int i = 1; i <= 129; ++i) {
    for (int j = 1; j <= 129; ++j) {
      int sx = max(0, i - 2 * d - 1);
      int sy = max(0, j - 2 * d - 1);
      int cur = sum[i][j] + sum[sx][sy] - sum[sx][j] - sum[i][sy];
      //printf("cur = %d, sx = %d, sy = %d\n", cur, sx, sy);
      if (cur <= 0) {
        continue;
      }
      if (cur > maxpot) {
        maxpot = cur;
        cnt = 1;
      } else if (cur == maxpot) {
        ++cnt;
      }
    }
  }
  printf("%d %d\n", cnt, maxpot);

  return 0;
}
