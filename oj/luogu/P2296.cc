#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>
#include <queue>

using namespace std;

const int MAXN = 10000 + 5;
vector<int> e[MAXN], re[MAXN];
bool valid[MAXN], visit[MAXN];
int invalid[MAXN];
int n, m, s, t;

struct Node {
  int u;
  int w;
  Node(int u = 0, int w = 0): u(u), w(w) {
  }
  bool operator<(const Node& o) const {
    return w > o.w;
  }
};

void dfs(int u) {
  valid[u] = true;
  for (int i = 0; i < (int)re[u].size(); ++i) {
    if (!valid[re[u][i]]) {
      dfs(re[u][i]);
    }
  }
}

int sp() {
  priority_queue<Node> pq;

  memset(visit, false, sizeof(visit));
  pq.push(Node(s, 0));
  visit[s] = true;
  while (pq.size()) {
    int u = pq.top().u, w = pq.top().w;
    pq.pop();
    visit[u] = true;
    if (u == t) {
      return w;
    }
    for (int i = 0; i < (int)e[u].size(); ++i) {
      int v = e[u][i];
      if (valid[v] && !visit[v]) {
        pq.push(Node(v, w + 1));
      }
    }
  }

  return -1;
}

int main() {
  freopen("in", "r", stdin);
  int u, v;

  scanf("%d%d", &n, &m);
  while (m--) {
    scanf("%d%d", &u, &v);
    if (u == v) {
      continue;
    }
    --u;
    --v;
    e[u].push_back(v);
    re[v].push_back(u);
  }
  scanf("%d%d", &s, &t);
  if (s == t) {
    printf("0\n");
    return 0;
  }

  --s;
  --t;
  memset(valid, false, sizeof(valid));
  dfs(t);

  int cnt = 0;
  for (int i = 0; i < n; ++i) {
    if (valid[i]) {
      bool flag = true;
      for (vector<int>::const_iterator it = e[i].begin(); it != e[i].end(); ++it) {
        if (!valid[*it]) {
          flag = false;
          break;
        }
      }
      if (!flag) {
        invalid[cnt++] = i;
      }
    }
  }
  for (int i = 0; i < cnt; ++i) {
    valid[invalid[i]] = false;
  }

  if (!valid[s]) {
    printf("0\n");
    return 0;
  }
  int res = sp();
  printf("%d\n", res);

  return 0;
}
