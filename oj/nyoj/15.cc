#include <iostream>
#include <cstdio>
#include <string>

using namespace std;

const int MAXN = 100;
int dp[MAXN][MAXN];

bool is_match(const char& a, const char& b) {
  if (a == '(' && b == ')') {
    return true;
  }
  if (a == '[' && b == ']') {
    return true;
  }
  return false;
}

int main() {
  freopen("in", "r", stdin);
  int t;
  string s;

  scanf("%d", &t);
  while (t--) {
    cin >> s;

    int n = s.size();
    for (int i = 1; i <= n; ++i) {
      dp[i][i] = 1;
    }
    for (int l = 2; l <= n; ++l) {
      for (int i = 1; i + l - 1 <= n; ++i) {
        int j = i + l - 1;
        //if (l == 2) {
          //dp[i][j] = is_match(s[i - 1], s[j - 1]) ? 0 : 2;
          //continue;
        //}
        dp[i][j] = dp[i][j - 1] + 1;
        for (int k = i; k < j; ++k) {
          if (is_match(s[k - 1], s[j - 1])) {
            dp[i][j] = min(dp[i][j], dp[i][k - 1] + dp[k + 1][j - 1]);
          }
        }
      }
    }
    printf("%d\n", dp[1][n]);
  }
  return 0;
}
