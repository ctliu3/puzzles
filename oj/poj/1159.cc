#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdio>

using namespace std;

const int MAXN = 5000 + 5;
short dp[MAXN][MAXN];
char s[MAXN];

int main() {
  int n;
  while (cin >> n) {
    scanf("%s", s);

    memset(dp, 0, sizeof(dp));
    for (int j = 1; j < n; ++j) {
      for (int i = j - 1; i >= 0; --i) {
        if (s[i] == s[j]) {
          dp[i][j] = dp[i + 1][j - 1];
        } else {
          dp[i][j] = min(dp[i][j - 1], dp[i + 1][j]) + 1;
        }
        //printf("dp[%d][%d] = %d\n", i, j, dp[i][j]);
      }
    }

    printf("%d\n", dp[0][n - 1]);
  }
}
