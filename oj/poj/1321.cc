#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <cstdio>

using namespace std;

vector<string> board;
vector<bool> row, col;
vector<pair<int, int> > pos;
int n, k;

void dfs(int cur, int dep, int& res) {
  if (dep == k) {
    ++res;
    return ;
  }
  if (cur >= (int)pos.size()) {
    return ;
  }
  
  int x = pos[cur].first, y = pos[cur].second;
  if (!row[x] && !col[y]) {
    row[x] = col[y] = true;
    dfs(cur + 1, dep + 1, res);
    row[x] = col[y] = false;
  }

  if ((int)pos.size() - cur - 1 + dep >= k) {
    dfs(cur + 1, dep, res);
  }
}

int main() {
  freopen("in", "r", stdin);
  string line;

  while (scanf("%d%d", &n, &k) == 2) {
    if (n == -1 && k == -1) {
      break;
    }

    board.resize(n);
    pos.clear();
    for (int i = 0; i < n; ++i) {
      cin >> line;
      board[i] = line;
      for (int j = 0; j < (int)line.size(); ++j) {
        if (line[j] == '#') {
          pos.push_back(make_pair(i, j));
        }
      }
    }

    if ((int)pos.size() < k) {
      puts("0");
      continue;
    }

    row.assign(n, false);
    col.assign(n, false);
    int res = 0;
    dfs(0, 0, res);
    printf("%d\n", res);
  }
    
  return 0;
}
