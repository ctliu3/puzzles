#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>

using namespace std;

struct Point {
  double x;
  double y;
  Point(double x = 0, double y = 0): x(x), y(y) {
  }

  Point operator-(const Point& o) {
    return Point(x - o.x, y - o.y);
  }

  bool operator<(const Point& o) const {
    if (x != o.x) {
      return x < o.x;
    }
    return y < o.y;
  }

  int cross(const Point o) {
    return x * o.y - y * o.x;
  }
};

vector<Point> graham_scan(vector<Point>& points) {
  int n = (int)points.size();
  if (n < 3) {
    return {};
  }

  int m = 0;
  vector<Point> res(n);

  sort(points.begin(), points.end());
  for (int i = 0; i < n; ++i) {
    if (m > 1 && (points[i] - res[m - 1]).cross(res[m - 1] - res[m - 2]) <= 0) {
      --m;
    }
    res[m++] = points[i];
  }
  for (int i = n - 1, j = m; i >= 0; --i) {
    if (m > j && (points[i] - res[m - 1]).cross(res[m - 1] - res[m - 2]) <= 0) {
      --m;
    }
    res[m++] = points[i];
  }
  res.resize(m);

  return res;
}

int sqr(const Point& p) {
  return p.x * p.x + p.y * p.y;
}

vector<Point> points;

int main() {
  freopen("in", "r", stdin);
  int n;

  while (scanf("%d", &n) == 1) {
    points.resize(n);
    for (int i = 0; i < n; ++i) {
      scanf("%lf%lf", &points[i].x, &points[i].y);
    }
    vector<Point> hull = graham_scan(points);
    if (hull.empty()) {
      hull = points;
    }
    int res = 0;
    for (int i = 0; i < (int)hull.size(); ++i) {
      for (int j = i + 1; j < (int)hull.size(); ++j) {
        res = max(res, sqr(hull[i] - hull[j]));
      }
    }
    printf("%d\n", res);
  }
  return 0;
}
