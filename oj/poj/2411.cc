#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>

using namespace std;

typedef long long llong;

llong dp[12][1 << 12];
int n, m;

void dfs(int row, int j, int cur) {
  dp[row][cur] = 1;
  if (j == m) {
    return ;
  }
  dfs(row, j + 1, cur << 1);
  if (j + 2 <= m) {
    dfs(row, j + 2, (cur << 2) | 3);
  }
}

void dfs(int row, int j, int cur, int pre) {
  if (j == m) {
    dp[row][cur] += dp[row - 1][pre];
    return ;
  }

  if (j + 1 <= m) {
    dfs(row, j + 1, cur << 1, (pre << 1) | 1);
    dfs(row, j + 1, (cur << 1) | 1, pre << 1);
  }
  if (j + 2 <= m) {
    dfs(row, j + 2, (cur << 2) | 3, (pre << 2) | 3);
  }
}

int main() {
  freopen("in", "r", stdin);

  while (cin >> n >> m) {
    if (n == 0 && m == 0) {
      break;
    }
    if (n == 1) {
      printf("%d\n", !(m & 1));
      continue;
    }

    memset(dp, 0, sizeof dp);
    dfs(0, 0, 0);
    for (int i = 1; i < n; ++i) {
      dfs(i, 0, 0, 0);
    }
    printf("%lld\n", dp[n - 1][(1 << m) - 1]);
  }
  return 0;
}
