#include <iostream>
#include <vector>
#include <cstring>
#include <cstdio>

using namespace std;

const int MAXN = 30;
const int dx[] = {-2, -2, -1, -1, 1, 1, 2, 2};
const int dy[] = {-1, 1, -2, 2, -2, 2, -1, 1};
int board[MAXN][MAXN];
bool visit[MAXN][MAXN];
vector<pair<int, int> > path;
int n, m;

bool inside(int x, int y) {
  return x >= 0 && x < n && y >= 0 && y < m;
}

bool dfs(int x, int y, int dep) {
  if (dep == n * m) {
    return true;
  }

  for (int i = 0; i < 8; ++i) {
    int nx = x + dx[i];
    int ny = y + dy[i];
    if (inside(nx, ny) && !visit[nx][ny]) {
      visit[nx][ny] = true;
      path[dep] = make_pair(nx, ny);
      if (dfs(nx, ny, dep + 1)) {
        return true;
      }
      visit[nx][ny] = false;
    }
  }

  return false;
}

int main() {
  freopen("in", "r", stdin);
  int t, cas = 1;

  scanf("%d", &t);
  while (t--) {
    scanf("%d%d", &m, &n);

    path.resize(n * m);
    memset(visit, false, sizeof(visit));

    printf("Scenario #%d:\n", cas++);
    visit[0][0] = true;
    path[0] = make_pair(0, 0);
    if (dfs(0, 0, 1)) {
      for (int i = 0; i < (int)path.size(); ++i) {
        printf("%c%d", path[i].first + 'A', path[i].second + 1);
      }
      puts("\n");
    } else {
      puts("impossible\n");
    }
  }
  return 0;
}
