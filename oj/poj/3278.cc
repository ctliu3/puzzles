#include <iostream>
#include <queue>
#include <cstdio>
#include <cstring>

using namespace std;

const int MAXN = 200000 + 5;
int minstep[MAXN];
int n, k;

int main() {
  freopen("in", "r", stdin);

  while (scanf("%d%d", &n, &k) == 2) {
    if (n >= k) {
      printf("%d\n", n - k);
      continue;
    }
    queue<int> que;
    memset(minstep, -1, sizeof(minstep));

    que.push(n);
    minstep[n] = 0;
    while (que.size()) {
      int pos = que.front();
      que.pop();

      for (int i = -1; i <= 1; ++i) {
        int next = pos;
        if (!i) {
          next *= 2;
        } else {
          next += i;
        }
        if (next >= 0 && next <= 2 * k && minstep[next] == -1) {
          minstep[next] = minstep[pos] + 1;
          if (next == k) {
            break;
          }
          que.push(next);
        }
      }
    }
    printf("%d\n", minstep[k]);
  }

  return 0;
}
