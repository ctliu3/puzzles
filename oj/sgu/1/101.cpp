#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>
#include <cstring>
#include <cassert>

using namespace std;

class Fleury {
public:
  static const int MAXN = 205;
  static const int MAXE = 405;
  vector<pair<int, int> > ans;
  vector<pair<int, int> > e[MAXN];
  bool hash[MAXE];
  int d[MAXN];
  int b_euler; // -1: haven't check, 0: doesn't have euler path or cycle and 1 otherwise 
  int start;
  int n;
  int m;

  explicit Fleury(vector<pair<int, int> > e, int n) { // <u, v>
    this->n = n;
    this->m = (int)e.size();
    fill(d, d + n, 0);
    for (vector<pair<int, int> >::iterator i = e.begin(); i != e.end(); ++i) {
      int u = i->first, v = i->second;
      this->e[u].push_back(make_pair(v, i - e.begin()));
      this->e[v].push_back(make_pair(u, i - e.begin()));
      ++d[u];
      ++d[v];
    }
    fill(hash, hash + m, false);
    this->b_euler = -1;
    this->start = -1;
    ans.clear();
  }

  void _dfs(int u) {
    for (vector<pair<int, int> >::iterator i = e[u].begin(); i != e[u].end(); ++i) {
      if (!hash[i->second]) {
        hash[i->second] = true;
        _dfs(i->first);
        ans.push_back(make_pair(i->first, u));
      }
    }
  }

  void _check(int u, set<int>& hash) {
    hash.insert(u);
    for (vector<pair<int, int> >::iterator i = e[u].begin(); i != e[u].end(); ++i) {
      if (!hash.count(i->first)) {
        _check(i->first, hash);
      }
    }
  }

  bool is_euler() {
    set<int> ver, hash;
    for (int i = 0; i < n; ++i) {
      if (d[i]) {
        ver.insert(i);
        start = i;
      }
    }
    _check(start, hash);
    if (hash.size() != ver.size()) {
      b_euler = false;
      return b_euler;
    }

    int cnt = 0;
    for (int i = 0; i < n; ++i) {
      if (d[i] & 1) {
        ++cnt;
        start = i;
      }
    }
    b_euler = cnt == 0 || cnt == 2;
    return b_euler;
  }

  vector<pair<int, int> > euler_path() {
    if (b_euler == -1) {
      is_euler();
    }
    if (b_euler == 0) {
      return vector<pair<int, int> >();
    }

    assert(start != -1);
    _dfs(start);
    return ans;
  }
};

vector<pair<int, int> > e;
bool b[205];

int main() {
  int n, u, v;

  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> u >> v;
    e.push_back(make_pair(u, v));
  }

  static Fleury fle = Fleury(e, 7);

  if (fle.is_euler() == 0) {
    puts("No solution");
    return 0;
  }

  fill(b, b + n, true);
  vector<pair<int, int> > ans = fle.euler_path();

  for (vector<pair<int, int> >::iterator i = ans.begin(); i != ans.end(); ++i) {
    for (vector<pair<int, int> >::iterator j = e.begin(); j != e.end(); ++j) {
      if (!b[j - e.begin()]) {
        continue;
      }
      if (*i == *j) {
        printf("%d %c\n", j - e.begin() + 1, '+');
        b[j - e.begin()] = false;
        break;
      } else if (i->first == j->second && i->second == j->first) {
        printf("%d %c\n", j - e.begin() + 1, '-');
        b[j - e.begin()] = false;
        break;
      }
    }
  }

  return 0;
}
