#include <iostream>
#include <cstdio>
#include <limits>
#include <algorithm>
#include <cstring>
#include <vector>

using namespace std;

const int MAXN = 105;
int dp[MAXN][MAXN], path[MAXN][MAXN];
int a[MAXN][MAXN];

int main() {
  int n, m;

  cin >> n >> m;
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      scanf("%d", &a[i][j]);
      dp[i][j] = -1E8;
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = i; j <= m - (n - i); ++j) {
      for (int k = i - 1; k <= j - 1; ++k) {
        if (dp[i - 1][k] + a[i][j] > dp[i][j]) {
          dp[i][j] = dp[i - 1][k] + a[i][j];
          path[i][j] = k;
        }
      }
      //printf("dp[%d][%d] = %d\n", i, j, dp[i][j]);
    }
  }
  int ans = *max_element(dp[n] + 1, dp[n] + m + 1);
  vector<int> arr;
  for (int i = 1; i <= m; ++i) {
    if (dp[n][i] == ans) {
      arr.push_back(i);
      break;
    }
  }
  for (int i = n; i >= 2; --i) {
    int j = path[i][arr.back()];
    arr.push_back(j);
  }

  printf("%d\n", ans);
  for (int i = 0; i < n; ++i) {
    printf("%d%c", arr[n - i - 1], " \n"[i == n - 1]);
  }

  return 0;
}
