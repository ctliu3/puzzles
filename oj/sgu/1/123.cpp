#include <iostream>

using namespace std;

const int MAXN = 50;
int f[MAXN];

int main() {
  int k, sum = 2;

  cin >> k;
  f[1] = f[2] = 1;
  for (int i = 3; i <= k; ++i) {
    f[i] = f[i - 1] + f[i - 2];
    sum += f[i];
  }
  if (k < 3) {
    cout << k << endl;
  } else {
    cout << sum << endl;
  }

  return 0;
}
