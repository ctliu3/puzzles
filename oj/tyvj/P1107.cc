#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> prime, fact, cnt;
int a0, a1, b0, b1;

void sieve_prime(int n) {
  vector<bool> b(n, true);

  b[0] = b[1] = false;
  for (int i = 2; i < n; ++i) {
    if (b[i]) {
      prime.push_back(i);
      for (long long j = (long long)i * i; j < (long long)n; j += i) {
        b[j] = false;
      }
    }
  }
}

void get_fact(int n) {
  fact.clear();
  cnt.clear();
  for (int i = 0; i < (int)prime.size(); ++i) {
    int p = prime[i];
    if (n % p == 0) {
      int m = 0;
      while (n % p == 0) {
        n /= p;
        ++m;
      }
      fact.push_back(p);
      cnt.push_back(m);
    }
  }
  if (n > 50000 && fact.empty()) {
    fact.push_back(n);
    cnt.push_back(1);
  }
}

void dfs(int pos, int cur, int& res) {
  if (pos == (int)cnt.size()) {
    if (cur % a1 == 0) {
      if (__gcd(cur, a0) == a1 && __gcd(b1 / b0, b1 / cur) == 1) {
        ++res;
      }
    }
    return ;
  }

  dfs(pos + 1, cur, res);

  int val = 1;
  for (int i = 0; i < cnt[pos]; ++i) {
    val *= fact[pos];
    dfs(pos + 1, cur * val, res);
  }
}

int main() {
  int t, res;

  sieve_prime(50000);
  freopen("in", "r", stdin);
  scanf("%d", &t);
  while (t--) {
    scanf("%d %d %d %d", &a0, &a1, &b0, &b1);
    get_fact(b1);
    //for (int i = 0; i < fact.size(); ++i) {
      //printf("%d %d\n", fact[i], cnt[i]);
    //}
    res = 0;
    dfs(0, 1, res);
    printf("%d\n", res);
  }

  return 0;
}
