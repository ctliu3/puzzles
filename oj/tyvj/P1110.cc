#include <iostream>
#include <string>
#include <cstdio>
#include <vector>

using namespace std;

int main() {
  freopen("in", "r", stdin);
  string sa, sb, s;
  vector<int> mp(256, -1);

  cin >> sa >> sb >> s;
  for (int i = 0; i < (int)sa.size(); ++i) {
    if (mp[sa[i]] != -1 && mp[sa[i]] != sb[i]) {
      puts("Failed");
      return 0;
    }
    mp[sa[i]] = sb[i];
  }

  for (int i = 0; i < (int)sa.size(); ++i) {
    for (int j = i + 1; j < (int)sb.size(); ++j) {
      if (sb[i] == sb[j] && sa[i] != sa[j]) {
        puts("Failed");
        return 0;
      }
    }
  }

  for (int i = 0; i < 26; ++i) {
    if (mp['A' + i] == -1) {
      puts("Failed");
      return 0;
    }
  }

  string res(s.size(), ' ');
  for (int i = 0; i < (int)s.size(); ++i) {
    if (mp[s[i]] == -1) {
      puts("Failed");
      return 0;
    }
    res[i] = mp[s[i]];
  }

  puts(res.c_str());

  return 0;
}
