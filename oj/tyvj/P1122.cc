#include <iostream>
#include <vector>
#include <cstdio>
#include <set>
#include <queue>
#include <climits>
#include <cstdlib>

using namespace std;

const int MAXN = 100000 + 5;

vector<int> e[MAXN], re[MAXN];
vector<int> p, minp, maxp;
vector<int> visit;

void sp1(int n, int start) {

  visit.assign(n, false);

  priority_queue<pair<int, int> > pq;
  minp[start] = -p[start];
  pq.push({start, minp[start]});
  while (pq.size()) {
    int u = pq.top().first;
    int curp = pq.top().second;
    pq.pop();
    visit[u] = true;
    for (int i = 0; i < (int)e[u].size(); ++i) {
      int v = e[u][i];
      if (!visit[v]) {
        minp[v] = max(-p[v], curp);
        pq.push({v, minp[v]});
      }
    }
  }
}

void sp2(int n, int start) {
  queue<int> q;
  q.push(-p[start]);
  maxp[start] = -p[start];



  visit.assign(n, false);

  priority_queue<pair<int, int> > pq;
  maxp[start] = p[start];
  pq.push({start, maxp[start]});
  while (pq.size()) {
    int u = pq.top().first;
    int curp = pq.top().second;
    pq.pop();
    visit[u] = true;
    for (int i = 0; i < (int)re[u].size(); ++i) {
      int v = re[u][i];
      if (!visit[v]) {
        maxp[v] = max(p[v], curp);
        pq.push({v, maxp[v]});
      }
    }
  }
}

int main() {
  int n, m, x, y, z;

  scanf("%d %d", &n, &m);
  p.resize(n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &p[i]);
  }
  while (m--) {
    scanf("%d %d %d", &x, &y, &z);
    --x;
    --y;
    e[x].push_back(y);
    re[y].push_back(x);
    if (z == 2) {
      e[y].push_back(x);
      re[x].push_back(y);
    }
  }
  maxp.assign(n, -1);
  minp.assign(n, -1);
  sp1(n, 0);
  sp2(n, n - 1);
  int res = 0;
  for (int i = 0; i < n; ++i) {
    //printf("%d %d\n", maxp[i], -minp[i]);
    if (maxp[i] == -1 || minp[i] == -1) {
      continue;
    }
    res = max(res, maxp[i] + minp[i]);
  }
  printf("%d\n", res);

  return 0;
}
