myLast :: [a] -> a
myLast [x] = x
myLast (_:xs) = myLast xs

-- const id = flip const
myLast' = foldr1 (const id)

-- or foldl1
myLast'' = foldr1 (curry snd)
