myButLast :: [a] -> a
myButLast = last . init

myButLast' :: [a] -> a
myButLast' x = reverse x !! 1

myButLast'' :: [a] -> a
myButLast'' [x,_] = x
myButLast'' (x:xs) = myButLast'' xs

myButLast''' :: [a] -> a
myButLast''' (x:_:[]) = x
myButLast''' (x:xs) = myButLast'' xs

myButLast'''' :: [a] -> a
myButLast'''' = fst . (foldl (\(a, b) c -> (b, c)) (e1, e2))
  where e2 = error "list is too small"
        e1 = error "list is empty"
