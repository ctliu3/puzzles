elementAt :: [a] -> Int -> a
elementAt list i = list !! (i - 1)

elementAt' :: [a] -> Int -> a
elementAt' (x:_) 1 = x
elementAt' [] _ = error "index out of bound"
elementAt' (_:xs) k
  | k < 1 = error "index out of bound"
  | otherwise = elementAt' xs (k - 1)

elementAt'' :: (Num c, Enum c) => [a] -> Int -> c
elementAt'' xs n
  | length xs < n = error "index out of bound"
  | otherwise = snd . last . take n $ zip xs [1..]

-- http://stackoverflow.com/questions/14526254/find-the-kth-element-of-a-list-using-foldr-and-function-application-explana/14526467#14526467
elementAt''' :: [a] -> Int -> a
elementAt''' xs n = head $ foldr ($) xs $ replicate (n - 1) tail
