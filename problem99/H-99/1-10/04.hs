myLength :: [a] -> Int
myLength list = length list

myLength' :: [a] -> Int
-- (\_ -> 1) is a lambda function, can not match mulit-pattern
myLength' list = sum $ map (\_ -> 1) list

myLength'' :: [a] -> Int
-- fst/snd only can handle tuple contain two type, say, (_, _)
myLength'' = fst . last . zip [1..]

myLength''' :: [a] -> Int
myLength''' [] = 0
myLength''' (x:xs) = 1 + myLength''' xs

myLength'''' :: [a] -> Int
{-myLength'''' = foldr (\_ n -> n + 1) 0-}
myLength'''' = foldl (\n _ -> n + 1) 0
