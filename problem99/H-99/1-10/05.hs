reverse :: [a] -> [a]
reverse list = reverse' list []
  where
    reverse' [] reversed = reversed
    reverse' (x:xs) reversed = reverse' xs (x:reversed)

reverse' :: [a] -> [a]
reverse' (x:[]) = [x]
reverse' (x:xs) = reverse' xs ++ [x]

reverse'' :: [a] -> [a]
reverse'' = foldl (flip (:)) []
