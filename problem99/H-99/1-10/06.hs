isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == reverse xs

isPalindrome' :: (Eq a) => [a] -> Bool
isPalindrome' a = f a (reverse a)
    where f a b = if a == b then True else False
    -- Totally wrong, (a == b) is not a format of pattern matching
    -- But using in `if .. then .. else` will be OK.
    {-where f a b -}
        {-| a == b = True-}
        {-| otherwise = False-}

isPalindrome'' :: (Eq a) => [a] -> Bool
isPalindrome'' a = f a (reverse a)
    where f a b = a == b

isPalindrome''' [] = True
isPalindrome''' (_:[]) = True -- [_]
isPalindrome''' xs = (head xs) == (last xs) && (isPalindrome''' $ init $ tail xs)

isPalindrome'''' :: (Eq a) => [a] -> Bool
isPalindrome'''' xs = foldl (\acc (a, b) -> if a == b then acc else False) True pairs
  where
    pairs = zip xs (reverse xs)

isPalindrome''''' :: (Eq a) => [a] -> Bool
isPalindrome''''' xs = foldl (&&) True $ zipWith (==) xs (reverse xs)

main = do
  let res1 = isPalindrome''''' "abc"
      res2 = isPalindrome''''' ""
      res3 = isPalindrome''''' "bb"
  putStrLn $ show res1
  putStrLn $ show res2
  putStrLn $ show res3
