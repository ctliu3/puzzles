-- Recursive data structures
data NestedList a = Elem a | List [NestedList a]

flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
{-flatten (List []) = []-}
flatten (List xs) = foldr (++) [] $ map flatten xs

flatten' :: NestedList a -> [a]
flatten' (Elem x) = [x]
flatten' (List xs) = concatMap flatten xs

main = do
  let res1 = flatten' (Elem 5)
      res2 = flatten' (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]])
      res3 = flatten' (List [])
  putStrLn $ show res1
  putStrLn $ show res2
  putStrLn $ show $ if null res3 then True else False
