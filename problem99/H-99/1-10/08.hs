import Data.List

compress :: (Eq a) => [a] -> [a]
compress = nub 

compress' :: (Eq a) => [a] -> [a]
compress' = map head . group

compress'' :: (Eq a) => [a] -> [a]
-- use foldr
compress'' = foldr skipDups []
  where
    skipDups x [] = [x]
    skipDups x acc
      | x == head acc = acc
      | otherwise = x : acc
        
compress''' :: (Eq a) => [a] -> [a]
-- use foldl
compress''' = foldl skipDups []
  where
    skipDups [] x = [x]
    skipDups acc x
      | x == head acc = acc
      | otherwise = acc ++ [x]

compress'''' :: (Eq a) => [a] -> [a]
compress'''' [] = []
compress'''' (x:xs) = x : dropWhile (==x) xs

main = do
  let res1 = compress'''' [1]
  putStrLn $ show res1
