import Data.List

-- Same function as `group`
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x : takeWhile (==x) xs) : pack (dropWhile (==x) xs)

pack' :: (Eq a) => [a] -> [[a]]
pack' [] = []
pack' (x:xs) = let (first, rest) = span (==x) xs
               in (x:first) : pack' rest

pack'' :: (Eq a) => [a] -> [[a]]
pack'' [] = []
pack'' (x:xs) = (x:reps) : (pack'' rest)
   where
     -- How to use maybe
     (reps, rest) = maybe (xs, []) (\i -> splitAt i xs)
        (findIndex (/=x) xs)
