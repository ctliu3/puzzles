import Data.List

encode :: Eq a => [a] -> [(Int, a)]
encode xs = map (\x -> (length x, head x)) $ group xs

encode' :: Eq a => [a] -> [(Int, a)]
encode' xs = zip (map length l) h 
  where
    l = (group xs)
    h = map head l
