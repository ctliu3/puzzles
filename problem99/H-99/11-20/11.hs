import Data.List

data ListItem a = Single a | Mulitple Int a
  deriving (Show)

encode :: Eq a => [a] -> [(Int, a)]
encode xs = map (\x -> (length x, head x)) $ group xs

encodeModified :: Eq a => [a] -> [ListItem a]
encodeModified = map encodeHelper . encode
  where
    encodeHelper (1, x) = Single x
    encodeHelper (n, x) = Mulitple n x

encodeModified' :: (Eq a) => [a] -> [ListItem, a]
encodeModified' xs = [y | x <- group xs, let y = if (length x) == 1 then Single (head x) else Mulitple (length x) (head x)]

main :: IO ()
main = do
  putStrLn $ show $ encodeModified' "aaaabccaadeeee"
