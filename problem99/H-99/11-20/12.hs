data ItemList a = Single a | Multiple Int a
  deriving (Show)

decodeModified :: [ItemList a] -> [a]
decodeModified [] = []
decodeModified (x:xs) = decode x ++ (decodeModified xs)
  where
    decode (Single c) = [c]
    decode (Multiple n c) = replicate n c

decodeModified' :: [ItemList a] -> [a]
-- concatMap = concat $ map ..
decodeModified' = concatMap decode
  where
    decode (Single c) = [c]
    decode (Multiple n c) = replicate n c

decodeModified'' :: [ItemList a] -> [a]
decodeModified'' = concatMap decode
  -- case expression
  where decode e = 
          case e of Single c -> [c]
                    Multiple n c -> replicate n c

toTuple :: ItemList a -> (Int, a)
toTuple (Single a) = (1, a)
toTuple (Multiple n a) = (n, a)

decodeModified''' :: [ItemList a] -> [a]
decodeModified''' = concatMap ((uncurry replicate) . toTuple)

main = do
  putStrLn $ decodeModified''' [Multiple 4 'a',Single 'b',Multiple 2 'c', Multiple 2 'a',Single 'd',Multiple 4 'e']
