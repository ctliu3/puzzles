import Data.List

data ListItem a = Single a | Multiple Int a
  deriving (Show)

encode :: (Eq a) => [a] -> [(Int, a)]
-- group
encode = foldl helper [] . group
  where helper acc x = acc ++ (if length x == 1 then [(1, head x)] else [(length x, head x)])

encodeDirect :: (Eq a) => [a] -> [ListItem a]
encodeDirect = map encodeHelper . encode
  where
    encodeHelper (1, x) = Single x
    encodeHelper (n, x) = Multiple n x

encodeDirect' :: (Eq a) => [a] -> [ListItem a]
encodeDirect' [] = []
encodeDirect' (x:xs)
  | count == 1 = (Single x) : (encodeDirect' xs)
  | otherwise = (Multiple count x) : (encodeDirect' rest)
  where
    (matched, rest) = span (==x) xs
    count = 1 + (length matched)

main :: IO ()
main = do
  {-mapM_ print . (\x -> replicate 2 x) $ "abc"-}
  mapM_ print $ encodeDirect "aaaabccaadeeee"
  putStrLn $ replicate 10 '-'
  mapM_ print $ encodeDirect' "aaaabccaadeeee"
