-- Many soluction
dupli :: [a] -> [a]
dupli = foldl (\acc x -> acc ++ (replicate 2 x)) []

dupli' :: [a] -> [a]
dupli' [] = []
dupli' (x:xs) = x:x:dupli' xs

dupli'' :: [a] -> [a]
dupli'' lst = concat [[x, x] | x <- lst]
