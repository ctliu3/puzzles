repli :: (Eq a) => [a] -> Int -> [a]
repli s n
  | s == [] = []
  | otherwise = replicate n (head s) ++ repli (tail s) n

repli' :: [a] -> Int -> [a]
repli' xs n = concatMap (replicate n) xs

-- use `take n` to replace `replicate`
repli'' :: [a] -> Int -> [a]
repli'' xs n = concatMap (take n . repeat) xs

-- use `flip` to solve this problem
repli''' :: [a] -> Int -> [a]
repli''' = flip $ concatMap . replicate

main = do
  print $ repli''' "abc" 3
