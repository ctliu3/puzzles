import Data.List

dropEvery :: [a] -> Int -> [a]
dropEvery s n
  | length s < n = s
  | otherwise = init ns ++ dropEvery rest n
    where (ns, rest) = splitAt n s

-- using zip and list comprehensions
dropEvery' :: [a] -> Int -> [a]
dropEvery' xs n = [i | (i, c) <- (zip xs [1,2..]), c `mod` n /= 0]
