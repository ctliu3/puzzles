import Data.List

split :: [a] -> Int -> ([a], [a])
split s n = splitAt n s

-- using `take` and `drop`
split' :: [a] -> Int -> ([a], [a])
split' s n = (take n s, drop n s)

-- using flip
split'' :: [a] -> Int -> ([a], [a])
split'' = flip splitAt
