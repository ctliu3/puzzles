slice :: String -> Int -> Int -> String
slice s st ed = take (ed - st + 1) $ drop (st - 1) s

-- using list comprehension
slice' :: String -> Int -> Int -> String
slice' s st ed = [x | (x, i) <- zip s [1..ed], i >= st]
