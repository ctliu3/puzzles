rotate :: String -> Int -> String
rotate s n
  | null s || n == 0 = s
  | n > 0 = drop n s ++ take n s
  | otherwise = drop m s ++ take m s
  where m = length s + n

rotate' :: String -> Int -> String
rotate' s n
  | n >= 0 =
    let (fst, snd) = splitAt n s
    in snd ++ fst
  | otherwise = rotate' s (n `mod` (length s))
