removeAt :: Int -> String -> (Char, String)
removeAt n xs = (a, b)
  where
    a = (last . take n) xs
    b = take (n - 1) xs ++ drop n xs

-- point-free style
removeAt' :: Int -> String -> (Char, String)
removeAt' n = (\(a, b) -> (head b, a ++ tail b)) . splitAt (n - 1)

removeAt'' :: Int -> String -> (Char, String)
removeAt'' n (x:xs)
  | n == 1 = (x, xs)
  | otherwise = 
    let (y, ys) = (removeAt'' (n - 1) xs)
    in (y, x:ys)
