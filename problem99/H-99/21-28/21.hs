insertAt :: Char -> String -> Int -> String
insertAt c xs n = take (n - 1) xs ++ c:(drop (n - 1) xs)

insertAt' :: a -> [a] -> Int -> [a]
insertAt' c xs n = let (a, b) = splitAt (n - 1) xs in a ++ c:b

insertAt'' :: a -> [a] -> Int -> [a]
insertAt'' x ys 1 = x:ys
insertAt'' x (y:ys) n = y:insertAt'' x ys (n - 1)
