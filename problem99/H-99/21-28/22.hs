import Data.List

range :: Int -> Int -> [Int]
range s t = [s..t]

range' :: Int -> Int -> [Int]
range' s t = take (t - s + 1) [s..]

range'' :: Int -> Int -> [Int]
range'' l r = scanl (+) l (replicate (r - l) 1)

range''' :: Int -> Int -> [Int]
range''' l r 
  | l == r = [l]
  | l < r = l:(range (l + 1) r)

range'''' :: Int -> Int -> [Int]
-- using `iterate`
range'''' l r = take (r - l + 1) $ iterate (+1) l
