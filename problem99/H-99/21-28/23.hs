import Random

-- first time using `randomRs/mkStdGen`
rnd_select :: String -> Int -> String
rnd_select xs n = getSub xs numbers
  where numbers = take 3 $ randomRs (0, (length xs) - 1) (mkStdGen 1) :: [Int]

getSub :: String -> [Int] -> String
getSub s (x:[]) = [s !! x]
getSub s (x:xs) = s !! x:getSub s xs

rnd_select' :: [a] -> Int -> [a]
rnd_select' xs n = do
  -- getStdGen
  gen <- getStdGen
  return $ take n [xs !! x | x <- randomRs (0, (length xs) - 1) gen]
