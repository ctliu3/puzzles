import Random
import List

diff_select :: Int -> Int -> IO [Int]
diff_select n limits = do
  gen <- getStdGen
  return $ take n $ randomRs (1, limits) gen

diff_select' :: Int -> Int -> [Int]
diff_select' n m = take n $ nub $ (randomRs (1, m) (mkStdGen 1)) :: [Int]
