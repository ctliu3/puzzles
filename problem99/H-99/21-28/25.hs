import Random
import List

rnd_permu :: [a] -> [a]
rnd_permu xs = [xs !! x | x <- genIdx (length xs)]

genIdx :: Int -> [Int]
genIdx n = take n $ nub $ randomRs (0, n - 1) (mkStdGen 1)

rnd_permu' :: [a] -> IO [a]
rnd_permu' [] = return []
rnd_permu' (x:xs) = do
  rand <- randomRIO (0, (length xs))
  rest <- rnd_permu' xs
  return $ let (ys, zs) = splitAt rand rest
           in ys++(x:zs)
