import Data.List

combinations :: Int -> [a] -> [[a]]
-- subsequences
combinations k ns = filter ((==k) . length) (subsequences ns)

-- recursion is always hard to understand!
combinations' :: Int -> [a] -> [[a]]
combinations' 0 _ = [[]]
combinations' _ [] = []
combinations' n (x:xs) = 
  (map (x:) $ combinations' (n - 1) xs) ++ combinations' n xs

main = print $ combinations 3 "abcdef"
