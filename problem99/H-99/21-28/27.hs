combination :: Int -> [a] -> [([a], [a])]
combination 0 xs =  [([], xs)]
combination n [] = []
combination n (x:xs) = res1 ++ res2
  where res1 = [(x:fs, bs) | (fs, bs) <- combination (n - 1) xs]
        res2 = [(fs, x:bs) | (fs, bs) <- combination n xs]

group :: [Int] -> [a] -> [[[a]]]
group [] _ = [[]]
group (n:ns) lst =
  [fs:rest | (fs, bs) <- combination n lst, rest <- group ns bs]

main = do
  print $ length $ group [2, 3, 4] ["aldo","beat","carla","david","evi","flip","gary","hugo","ida"]
  print $ length $ group [2, 2, 5] ["aldo","beat","carla","david","evi","flip","gary","hugo","ida"]
