import Data.List

lsort :: (Ord a) => [[a]] -> [[a]]
lsort = sortBy compareLen
        where compareLen lhs rhs = length lhs `compare` length rhs

lfsort :: (Ord a) => [[a]] -> [[a]]
lfsort lst = concat . sortBy compareFre $ groupBy (\x y -> length x == length y) $ lsort lst
             where compareFre x y = length x `compare` length y

main = do
  print $ lsort ["abc","de","fgh","de","ijkl","mn","o"]
  print $ lfsort ["abc","de","fgh","de","ijkl","mn","o"]
