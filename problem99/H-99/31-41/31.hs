isPrime :: Int -> Bool
isPrime n
  | n < 2 = False
  | n == 2 = True
  | otherwise = let res = any (\x -> if n `rem` x == 0 then True else False)
                          [x | x <- [2..truncate $ sqrt $ fromIntegral n] :: [Int]]
                in not res

main = do
  res <- fmap (isPrime . read) getLine
  print res

