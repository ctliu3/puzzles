myGCD :: Int -> Int -> Int
myGCD a b
  | a == 0 = b
  | otherwise = myGCD (b `mod` a) a

main = print . show $ map abs [myGCD 36 63, myGCD (-3) (-6), myGCD (-3) 6]
