myGCD :: Int -> Int -> Int
myGCD a b
  | a == 0 = b
  | otherwise = myGCD (b `mod` a) a

coprime :: Int -> Int -> Bool
coprime a b = if comDiv == 1 then True else False
              where comDiv = myGCD a b

main = print $ coprime 35 64
