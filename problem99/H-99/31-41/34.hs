myGCD :: Int -> Int -> Int
myGCD a b
  | a == 0 = b
  | otherwise = myGCD (b `mod` a) a

coprime :: Int -> Int -> Bool
coprime a b = if comDiv == 1 then True else False
              where comDiv = myGCD a b

totient :: Int -> Int
totient n = foldl (\acc x -> if (coprime n x) == True then acc + 1 else acc) 0 [1..n]

main = print $ totient 10
