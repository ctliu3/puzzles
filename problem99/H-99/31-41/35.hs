getFactor :: Int -> Int -> ([Int], Int)
getFactor n a
  | n `mod` a == 0 = let (fac, rest) = getFactor (n `div` a) a in (a:fac, rest)
  | otherwise = ([], n)

primeFactors' :: Int -> Int -> [Int]
primeFactors' n a
  | n == a = [n]
  | otherwise = if n `mod` a == 0
                then let (fac, rest) = getFactor n a in fac ++ (primeFactors' rest 2)
                else primeFactors' n (a + 1)

primeFactors :: Int -> [Int]
primeFactors n = primeFactors' n 2

main :: IO()
main = print $ show $ primeFactors 315
