import Data.List

getFactor :: Int -> Int -> ([Int], Int)
getFactor n a
  | n `mod` a == 0 = let (fac, rest) = getFactor (n `div` a) a in (a:fac, rest)
  | otherwise = ([], n)

primeFactors' :: Int -> Int -> [Int]
primeFactors' n a
  | n == a = [n]
  | otherwise = if n `mod` a == 0
                then let (fac, rest) = getFactor n a in fac ++ (primeFactors' rest 2)
                else primeFactors' n (a + 1)

primeFactors :: Int -> [Int]
primeFactors n = primeFactors' n 2

prime_factors_mult :: Int -> [(Int, Int)]
prime_factors_mult n = getRes $ groupBy (\x y -> x == y) $ sort $ primeFactors n
                       where getRes [] = []
                             getRes (x:xs) = (x !! 0, length x):getRes xs

toient :: Int -> Int
toient n = product [(p - 1) * p ^ (m - 1) | (p, m) <- prime_factors_mult n]

main :: IO()
main = print . show $ toient 315
