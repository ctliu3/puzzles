isPrime :: Int -> Bool
isPrime n = not res
            where res = any (\x -> if n `mod` x == 0 then True else False)
                            [x | x <- [2..truncate $ sqrt $ fromIntegral n]]

primesR :: Int -> Int -> [Int]
primesR l r = concat [if isPrime x == True then [x] else [] | x <- [l..r]]

main :: IO()
{-main = print $ show $ isPrime 5-}
main = print $ show $ primesR 10 20
