isPrime :: Int -> Bool
isPrime n = not res
            where res = any (\x -> if n `mod` x == 0 then True else False)
                            [x | x <- [2..truncate $ sqrt $ fromIntegral n]]


goldbach' :: Int -> Int -> (Int, Int)
goldbach' n a
  | isPrime a == True && isPrime (n - a) = (a, n - a)
  | otherwise = goldbach' n (a + 1)

goldbach :: Int -> (Int, Int)
goldbach n = goldbach' n 2

main :: IO()
main = print $ show $ goldbach 28
