isPrime :: Int -> Bool
isPrime n = not res
            where res = any (\x -> if n `mod` x == 0 then True else False)
                            [x | x <- [2..truncate $ sqrt $ fromIntegral n]]

primesR :: Int -> Int -> [Int]
primesR n m = filter isPrime [n..m]

goldbach' :: Int -> Int -> (Int, Int)
goldbach' n a
  | isPrime a == True && isPrime (n - a) = (a, n - a)
  | otherwise = goldbach' n (a + 1)

goldbach :: Int -> (Int, Int)
goldbach n = goldbach' n 2

goldbachList :: Int -> Int -> [(Int, Int)]
goldbachList l r = [goldbach x | x <- [l + 1, l + 3..r]]

-- for the goldbachList'
goldbachWithLower :: Int -> Int -> [(Int, Int)]
goldbachWithLower n a
  | isPrime a && isPrime (n - a) = [(a, n - a)]
  | otherwise = goldbachWithLower n (a + 1)

goldbachList' :: Int -> Int -> Int -> [(Int, Int)]
goldbachList' l r m = filter (\(x, y) -> x > m && y > m) $ concat
                      [goldbachWithLower x 2 | x <- filter even [l..r]]

main :: IO()
main = do
  {-print $ show $ primesR 4 2000-}
  {-print $ show $ goldbachList 9 20-}
  print $ show $ goldbachList' 4 2000 50

