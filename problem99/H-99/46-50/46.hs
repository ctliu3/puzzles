-- and'
and' :: Bool -> Bool -> Bool
and' True True = True
and' _ _ = False

-- nand' (not and)
nand' :: Bool -> Bool -> Bool
nand' a b = not $ and' a b

-- or'
or' :: Bool -> Bool -> Bool
or' True True = True
or' _ True = True
or' True _ = True
or' _ _ = False

-- nor'
nor' :: Bool -> Bool -> Bool
nor' a b
  | not a && not b = True
  | otherwise = False

table :: (Bool -> Bool -> Bool) -> [(Bool, Bool, Bool)]
table f = [(x, y, f x y) | x <- [True, False], y <- [True, False]]

main :: IO()
main = do
  let res = table (\a b -> (and' a (or' a b)))
  mapM_ print res
