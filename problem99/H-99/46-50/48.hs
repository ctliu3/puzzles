import Control.Monad (replicateM)

infixl 4 `or'`
infixl 4 `nor'`
infixl 5 `xor'`
infixl 6 `and'`
infixl 6 `nand'`
infixl 3 `equ'`

-- and'
and' :: Bool -> Bool -> Bool
and' True True = True
and' _ _ = False

-- nand' (not and)
nand' :: Bool -> Bool -> Bool
nand' a b = not $ and' a b

-- or'
or' :: Bool -> Bool -> Bool
or' True True = True
or' _ True = True
or' True _ = True
or' _ _ = False

-- nor'
nor' :: Bool -> Bool -> Bool
nor' a b
  | not a && not b = True
  | otherwise = False

-- equ'
equ' :: Bool -> Bool -> Bool
equ' True True = True
equ' False False = True
equ' _ _ = False

-- xor'
xor' :: Bool -> Bool -> Bool
xor' a b
  | a == b = False
  | otherwise = True

tablen :: Show a => Int -> ([Bool] -> a) -> [[Char]]
tablen n f = [toStr a ++ " => " ++ show (f a) | a <- args n]
    where args n = replicateM n [True, False]
          toStr = unwords . map (\x -> show x ++ space x)
          space True = "  "
          space False = " "

main :: IO()
main = do
  mapM_ print $ tablen 3 (\[a, b, c] -> a `and'` (b `or'` c) `equ'` a `and'` b `or'` a `and'` c)
