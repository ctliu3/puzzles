gray :: (Eq a, Num a) => a -> [String]
gray 1 = ["0", "1"]
gray n = let res = gray (n - 1)
             ls = ["0" ++ x | x <- res]
             rs = ["1" ++ x | x <- reverse res]
         in ls ++ rs

main = print $ gray 3
