import Data.List
import Data.Ord (comparing)

data TreeNode a = Leaf a | Branch (TreeNode a) (TreeNode a)
                  deriving (Show)

-- 
serialize (Leaf x) = [(x, "")]
serialize (Branch l r) = [(x, '0':code) | (x, code) <- serialize l] ++ 
                         [(x, '1':code) | (x, code) <- serialize r]

--
huffman freq = sortBy (comparing fst) $ serialize $ htree $ 
                 sortBy (comparing snd) [(Leaf x, w) | (x, w) <- freq]
               where htree [(t, _)] = t
                     htree ((x1, w1):(x2, w2):rest) = 
                       htree $ insertBy (comparing snd) (Branch x1 x2, w1 + w2) rest

main = print $ huffman [('a',45),('b',13),('c',12),('d',16),('e',9),('f',5)]
