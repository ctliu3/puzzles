data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show)

getRange :: Integral a => a -> [a]
getRange n
  | n `mod` 2 == 0 = [n `div` 2]
  | otherwise = [n `div` 2, n `div` 2 + 1]

cbalTree :: Integral a => a -> [Tree Char]
cbalTree 0 = [Empty]
cbalTree n = [Branch 'x' ltree rtree |
  l <- getRange (n - 1),  ltree <- cbalTree l, rtree <- cbalTree (n - l - 1)]

main = mapM_ print $ cbalTree 4
