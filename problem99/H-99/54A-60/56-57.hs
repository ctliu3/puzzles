data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show)

mirror :: Tree a -> Tree a -> Bool
mirror Empty Empty = True
mirror Empty (Branch _ _ _) = False
mirror (Branch _ _ _) Empty = False
mirror (Branch _ la lb) (Branch _ ra rb) = mirror la rb && mirror lb ra

symmetric :: Tree a -> Bool
symmetric Empty = True
symmetric (Branch _ a b) = mirror a b

add x Empty = Branch x Empty Empty
add x node@(Branch c l r) = if x `compare` c == GT
                            then Branch c l (add x r)
                            else Branch c (add x l) r

construct [] = Empty
construct xs = foldl (flip add) Empty xs

main = do
  print $ symmetric (Branch 'x' (Branch 'x' Empty Empty) Empty)
  print $ symmetric (Branch 'x' (Branch 'x' Empty Empty) (Branch 'x' Empty Empty))
  print $ symmetric . construct $ [5, 3, 18, 1, 4, 12, 21]
  print $ symmetric $ construct $ [3, 2, 5, 7, 1]
