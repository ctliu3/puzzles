data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show)

symCbalTrees :: Int -> [Tree Char]
symCbalTrees 0 = []
symCbalTrees 1 = [Branch 'x' Empty Empty]
symCbalTrees n
  | n `mod` 2 == 0 = []
  | otherwise = [(Branch 'x' l r) | (l, r) <- construct (n `div` 2)]

construct :: Int -> [(Tree Char, Tree Char)]
construct 1 = [(Branch 'x' Empty Empty, Branch 'x' Empty Empty)]
construct n
  | n == 2 = 
    [(Branch 'x' l Empty, Branch 'x' Empty r) | (l, r) <- construct (n - 1)] ++
    [(Branch 'x' Empty r, Branch 'x' l Empty) | (l, r) <- construct (n - 1)]
  | otherwise = 
    [(Branch 'x' l r, Branch 'x' l r) | (l, r) <- construct (n - 2)] ++ 
    [(Branch 'x' l Empty, Branch 'x' Empty r) | (l, r) <- construct (n - 1)] ++
    [(Branch 'x' Empty r, Branch 'x' l Empty) | (l, r) <- construct (n - 1)]

main = let res = symCbalTrees 5
  in mapM_ print res
