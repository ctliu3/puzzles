data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show)

hbalTree :: Char -> Int -> [Tree Char]
hbalTree _ 0 = [Empty]
hbalTree val 1 = [Branch val Empty Empty]
hbalTree val h = [Branch val l r | (hl, hr) <- [(h - 1, h - 1), (h - 1, h - 2), (h - 2, h - 1)],
                                    l <- hbalTree val hl, r <- hbalTree val hr]

main = mapM_ print $ take 4 $ hbalTree 'x' 3
