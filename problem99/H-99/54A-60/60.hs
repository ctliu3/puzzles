data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show)

maxNodes :: Int -> Int
maxNodes h = 2 ^ h - 1

minHeight :: Int -> Int
minHeight n = ceiling $ logBase 2 $ fromIntegral (n + 1)

minNodes :: Int -> Int
minNodes h = fibs !! (h + 2) - 1

maxHeight :: Int -> Int
maxHeight n = length (takeWhile (<= n + 1) fibs) - 3

fibs :: [Int]
fibs = 0:1:zipWith (+) fibs (tail fibs)

hbalTreeNodes :: a -> Int -> [Tree a]
hbalTreeNodes x n = [t | h <- [minHeight n .. maxHeight n], t <- baltree h n]
  where
    baltree 0 n = [Empty]
    baltree 1 n = [Branch x Empty Empty]
    baltree h n = [Branch x l r |
      (hl, hr) <- [(h - 2, h - 1), (h - 1, h - 1), (h - 1, h - 2)],
      let min_nl = max (minNodes hl) (n - 1 - maxNodes hr),
      let max_nl = min (maxNodes hl) (n - 1 - minNodes hr),
      nl <- [min_nl..max_nl],
      let nr = n - 1 - nl,
      l <- baltree hl nl,
      r <- baltree hr nr]

main = do
  print $ length $ hbalTreeNodes 'x' 15
  mapM_ print $ map (hbalTreeNodes 'x') [0..3]
