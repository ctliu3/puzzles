data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show)

countLeaves :: (Num a) => Tree a -> Int
countLeaves Empty = 0
countLeaves (Branch _ Empty Empty) = 1
countLeaves (Branch _ l r) = countLeaves l + countLeaves r

leaves :: (Num a) => Tree a -> [a]
leaves Empty = []
leaves (Branch val Empty Empty) = [val]
leaves (Branch _ l r) = leaves l ++ leaves r

internals :: (Num a) => Tree a -> [a]
internals Empty = []
internals (Branch _ Empty Empty) = []
internals (Branch val l r) = val : internals l ++ internals r

atLevel :: (Num a, Eq a) => Tree a1 -> a -> [a1]
atLevel tree dep = atLevel' tree 1 dep
  where
    atLevel' Empty _ _ = []
    atLevel' (Branch val l r) cur dep
      | cur == dep = [val]
      | otherwise = atLevel' l (cur + 1) dep ++ atLevel' r (cur + 1) dep

tree4 = Branch 1 (Branch 2 Empty (Branch 4 Empty Empty))
                 (Branch 2 Empty Empty)

main = do
  print $ countLeaves tree4
  print $ leaves tree4
  print $ internals tree4
  print $ atLevel tree4 2
