;(*) Find the last box of a list.
;Example:
;* (my-last '(a b c d))
;(D)


#-(and)"

P01 (*) Find the last box of a list.
    Example:
    * (my-last '(a b c d))
    (D)

"

(defun my-last (list)
  (last list))

(defun my-last-2 (list)
  (if (not nil) (last list))) ; (not nil) is list

(defun my-last-3 (list)
  (if 1 (last list))) ; 1 is atom

(defun my-last-4 (list)
  (if (listp (list 1)) (last list)))

(defun my-last-5 (list)
  (cond ; cond is the same to switch in C++
    ((endp list) (error "Empty list"))
    ;; what the distinction between function rest and cdr
    ((endp (cdr list)) list)
    ;; symbol t is opposite to symbol nil
    (t (my-last-5 (cdr list)))))

;;; loop

;;; test
;(print (my-last '(a b c d)))
;(print (my-last-2 '(a b c d)))
;(print (my-last-3 '(a b c d)))
;(print (my-last-4 '(a b c d)))
(print (my-last-5 '(a b c d)))
