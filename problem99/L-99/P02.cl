#-(and) "

P02 (*) Find the last but one box of a list.
    Example:
    * (my-but-last '(a b c d))
    (C D)
"

;;; simple solution
(defun my-but-last-1 (list)
  (cond
    ((endp list) (error "Empty"))
    ((endp (rest list)) (error "one element only"))
    (t (last list 2))))

;;; recursive solution
(defun my-but-last-2 (list)
  (cond
    ((endp list) (error "Empty"))
    ((endp (cdr list)) (error "one element only"))
    ((endp (cdr (cdr list))) list)
    (t (my-but-last-2 (cdr list)))))

;;; interative solution
(defun my-but-last-3 (list)
  (cond
    ((endp list) (error "Empty"))
    ((endp (cdr list)) (error "one element only"))
    ((endp (cdr (cdr list))) list)
    (t (loop
         :for result :on list ;
         :until (endp (cdr (cdr result)))
         :finally (return result)))))


(format t "~A: ~A ~%" 1 (my-but-last-1 `(a b c d)))
(format t "~A: ~A ~%" 2 (my-but-last-2 `(a b c d)))
(format t "~A: ~A ~%" 3 (my-but-last-3 `(a b c d)))
