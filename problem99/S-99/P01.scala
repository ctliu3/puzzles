object P01 {
  def last1[T](lst: List[T]): T = {
    lst.last
  }

  def last2[T](lst: List[T]): T = lst match {
    case x :: Nil   => x
    case _ :: tail  => last2(tail)
    case _          => throw new NoSuchElementException
  }
}

val lst = List(1, 2, 3, 4)
println(P01.last1(lst))
println(P01.last2(lst))
