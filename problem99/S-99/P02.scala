object P02 {
  def penultimate1[T](lst: List[T]): T = {
    lst.init.last
  }

  def penultimate2[T](lst: List[T]): T = lst match {
    case x :: _ :: Nil => x
    case _ :: tail     => penultimate2(tail)
    case _             => throw new NoSuchElementException
  }

  def penultimate3[T](lst: List[T]): T = {
    if (lst.length < 2) {
      throw new NoSuchElementException
    } else {
      lst(lst.length - 2)
    }
  }
}

val lst = List(1, 2, 3, 4)
println(P02.penultimate1(lst))
println(P02.penultimate2(lst))
println(P02.penultimate3(lst))
