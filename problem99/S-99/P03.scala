object P03 {
  def nth1[T](n: Int, lst: List[T]): T = {
    lst.apply(n)
  }

  def nth2[T](n: Int, lst: List[T]): T = (n, lst) match {
    case (0, x :: _)    => x
    case (n, _ :: tail) => nth2(n - 1, tail)
    case (_, Nil)       => throw new NoSuchElementException
  }
}

val lst = List(1, 1, 2, 3, 5, 8)
println(P03.nth1(2, lst))
println(P03.nth2(2, lst))
