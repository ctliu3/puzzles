object P04 {
  def length1[T](lst: List[T]): Int = {
    lst.length
  }

  def length2[T](lst: List[T]): Int = lst match {
    case x :: Nil  => 1
    case _ :: tail => length2(tail) + 1
    case Nil       => 0
  }
}

val lst = List(1, 1, 2, 3, 5, 8)
println(P04.length1(lst))
println(P04.length2(lst))
