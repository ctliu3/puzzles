object P05 {
  def reverse1[T](lst: List[T]): List[T] = {
    (List[T]() /: lst) ((x, y) => List(y) ::: x)
  }

  def reverse2[T](lst: List[T]): List[T] = lst match {
    case x :: Nil   => List(x)
    case x :: tail  => reverse2(tail) ::: List(x)
    case Nil        => List()
  }
}

val lst = List(1, 1, 2, 3, 5, 8)
println(P05.reverse1(lst))
println(P05.reverse2(lst))
