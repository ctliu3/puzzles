object P06 {

  def isPalindrome1[T](lst: List[T]): Boolean = lst match {
    case Nil       => true
    case x :: Nil  => true
    case x :: tail => if (x == tail.last) isPalindrome1(tail.init) else false
  }

  def isPalindrome2[T](lst: List[T]): Boolean = {
    lst.reverse == lst
  }
}

val lst = List(1, 2, 3, 2, 1)
println(P06.isPalindrome1(lst))
println(P06.isPalindrome2(lst))
