object P07 {
  def flatten1(lst: List[Any]): List[Any] = lst flatMap {
    case xs: List[Any] => flatten1(xs)
    case x             => List(x)
  }
}

val lst: List[Any] = List(List(1, 1), 2, List(3, List(5, 8)))
println(P07.flatten1(lst))
