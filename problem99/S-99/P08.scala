object P08 {
  def compress1[T](lst: List[T]): List[T] = lst match {
    case Nil            => Nil
    case x :: Nil       => List(x)
    case x :: y :: tail => if (x == y) compress1(x :: tail) else x :: compress1(y :: tail)
  }

  def compress2[T](lst: List[T]): List[T] = {
    (List[T]() /: lst) { (xs, y) =>
      if (xs.isEmpty) List(y)
      else if (xs.last == y) xs
      else xs ::: List(y)
    }
  }
}

val lst = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
println(P08.compress1(lst))
println(P08.compress2(lst))
