object P10 {

  def pack1[T](lst: List[T]): List[List[T]] = lst match {
    case Nil      => List()
    case x :: Nil => List(List(x))
    case _        =>
      List(lst.takeWhile(_ == lst.head)) ::: pack1(lst.dropWhile(_ == lst.head))
  }

  def encode1[T](lst: List[T]): List[(Int, T)] = {
    pack1(lst) map { x => (x.length, x.head) }
  }
}

val lst = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
println(P10.encode1(lst))
