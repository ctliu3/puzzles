object P11 {
  def encodeModified1[T](lst: List[T]): List[Any] = {
    if (lst.isEmpty) return List()

    val (same, rest) = lst span (_ == lst.head)
    if (same.length == 1) {
      same ::: encodeModified1(rest)
    } else {
      List((same.length, same.head)) ::: encodeModified1(rest)
    }
  }

  def encodeModified2[T](lst: List[T]): List[Any] = lst match {
    case Nil            => List()
    case x :: Nil       => List(x)
    case x :: y :: tail => {
      val (same, rest) = lst span (_ == lst.head)
      val end = encodeModified2(rest)
      if (same.length == 1) {
        same ::: end
      } else {
        List((same.length, same.head)) ::: end
      }
    }
  }
}

val lst = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
println(P11.encodeModified1(lst))
println(P11.encodeModified2(lst))
