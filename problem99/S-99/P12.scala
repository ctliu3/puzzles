object P12 {
  def decode1[T](lst: List[(Int, T)]): List[T] = lst flatMap {
    e => List.fill(e._1)(e._2)
  }
}

val lst = List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))
println(P12.decode1(lst))
