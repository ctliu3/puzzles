object P13 {
  def encodeDirect1[T](lst: List[T]): List[(Int, T)] = {
    if (lst.isEmpty) Nil
    else {
      val (packed, rest) = lst span (_ == lst.head)
      (packed.length, packed.head) :: encodeDirect1(rest)
      //List((packed.length, packed.head)) ::: encodeDirect1(rest)
    }
  }
}

val lst = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
println(P13.encodeDirect1(lst))
