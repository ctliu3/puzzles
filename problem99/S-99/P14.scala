object P14 {
  def duplicate1[T](lst: List[T]): List[T] = lst match {
    case Nil       => Nil
    case x :: rest => List.fill(2)(x) ::: duplicate1(rest)
  }

  def duplicate2[T](lst: List[T]): List[T] = {
    val res = for (e <- lst) yield List.fill(2)(e)
    res.flatten
  }

  def duplicate3[T](lst: List[T]): List[T] = lst flatMap {
    e => List(e, e)
  }
}

val lst = List('a, 'b, 'c, 'c, 'd)
println(P14.duplicate1(lst))
println(P14.duplicate2(lst))
println(P14.duplicate3(lst))
