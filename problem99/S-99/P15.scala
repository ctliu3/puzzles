object P15 {
  def duplicateN1[T](n: Int, lst: List[T]): List[T] = lst flatMap {
    e => List.fill(n)(e)
  }
}

val lst = List('a, 'b, 'c, 'c, 'd)
println(P15.duplicateN1(3, lst))
