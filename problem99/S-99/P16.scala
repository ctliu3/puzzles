object P16 {
  def drop1[T](n: Int, lst: List[T]): List[T] = lst match {
    case Nil                 => Nil
    case xs if xs.length < n => xs
    case x :: y :: rest      => x :: y :: drop1(n, rest.tail)
  }

  def drop2[T](n: Int, lst: List[T]): List[T] = {
    lst.zipWithIndex.filter ({e => (e._2 + 1) % n != 0}) map {_._1}
  }
}

val lst = List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k) 
println(P16.drop1(3, lst))
println(P16.drop2(3, lst))
