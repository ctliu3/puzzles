object P17 {
  def split1[T](n: Int, lst: List[T]): (List[T], List[T]) = {
    lst.splitAt(n)
  }

  def split2[T](n: Int, lst: List[T]): (List[T], List[T]) = {
    (lst take n, lst drop n)
  }

  def split3[T](n: Int, lst: List[T]): (List[T], List[T]) = (n, lst) match {
    case (_, Nil)       => (Nil, Nil)
    case (0, xs)        => (Nil, xs)
    case (n, x :: tail) => {
      val (pre, post) = split3(n - 1, tail)
      (x :: pre, post)
    }
  }
}

val lst = List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)
println(P17.split1(3, lst))
println(P17.split2(3, lst))
println(P17.split3(3, lst))
