object P18 {
  def slice1[T](start: Int, end: Int, lst: List[T]): List[T] = {
    lst take end drop start
  }

  def slice2[T](start: Int, end: Int, lst: List[T]): List[T] = {

    def _slice[T](sn: Int, en: Int, lst: List[T]): List[T] = (sn, en, lst) match {
      case (_, _, Nil)       => Nil
      case (0, 0, xs)        => xs
      case (0, e, xs)        => _slice(0, e - 1, xs.init)
      case (s, 0, h :: tail) => _slice(s - 1, 0, tail)
      case (s, e, h :: tail) => _slice(s - 1, e - 1, tail.init)
    }

    if (start > end || end > lst.length) List() 
    else {
      _slice(start, lst.length - end, lst)
    }
  }
}

val lst = List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)
println(P18.slice1(3, 7, lst))
println(P18.slice2(3, 7, lst))
