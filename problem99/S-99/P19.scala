object P19 {
  def rotate1[T](n: Int, lst: List[T]): List[T] = {
    val m = if (n > 0) n else (lst.length + n)
    val (pre, post) = lst.splitAt(m)
    post ::: pre
  }
}

val lst = List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)
println(P19.rotate1(3, lst))
println(P19.rotate1(-2, lst))
