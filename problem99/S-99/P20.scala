object P20 {
  def removeAt1[T](n: Int, lst: List[T]): (List[T], T) = {
    ((lst take n) ::: (lst drop (n + 1)), lst(n))
  }

  def removeAt2[T](n: Int, lst: List[T]): (List[T], T) = lst.splitAt(n) match {
    case (Nil, _) if n < 0 => throw new NoSuchElementException
    case (pre, e :: post)  => (pre ::: post, e)
    case (pre, Nil)        => throw new NoSuchElementException
  }
}

val lst = List('a, 'b, 'c, 'd)
println(P20.removeAt1(1, lst))
println(P20.removeAt2(1, lst))
