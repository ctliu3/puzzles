object P21 {
  def insertAt1[T](e: T, n: Int, lst: List[T]): List[T] = {
    val (pre, post) = lst splitAt n
    pre ::: List(e)::: post
  }
}

val lst = List('a, 'b, 'c, 'd)
println(P21.insertAt1('new, 1, lst))
