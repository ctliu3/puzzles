object P22 {
  def range1(start: Int, end: Int): List[Int] = {
    List.range(start, end + 1)
  }

  // pattern matching
  def range2(start: Int, end: Int): List[Int] = (start, end) match {
    case (start, end) if start > end      => Nil
    case (start, end)                     => start :: range2(start + 1, end)
  }

  // recursion
  def range3(start: Int, end: Int): List[Int] = {
    if (start > end) Nil
    else start :: range3(start + 1, end)
  }

  // tail recursion
  def range4(start: Int, end: Int): List[Int] = {
    def range(end: Int, res: List[Int]): List[Int] = {
      if (end < start) res
      else range(end - 1, end :: res)
    }
    range(end, Nil)
  }
}

println(P22.range1(4, 9))
println(P22.range2(4, 9))
println(P22.range3(4, 9))
println(P22.range4(4, 9))
