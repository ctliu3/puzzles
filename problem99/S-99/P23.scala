object P23 {
  def removeAt[T](n: Int, lst: List[T]): (List[T], T) = {
    ((lst take n) ::: (lst drop (n + 1)), lst(n))
  }

  def randomSelect1[T](n: Int, lst: List[T]): List[T] = {
    if (n <= 0) Nil
    else {
      val (rest, e) = removeAt((new util.Random).nextInt(lst.length), lst)
      e :: randomSelect1(n - 1, rest)
    }
  }

  def randomSelect2[T](n: Int, lst: List[T]): List[T] = {
    def randomSelect(n: Int, lst: List[T], rnd: util.Random): List[T] = {
      if (n <= 0) Nil
      else {
        val (rest, e) = removeAt(rnd.nextInt(lst.length), lst)
        e :: randomSelect(n - 1, rest, rnd)
      }
    }
    randomSelect(n, lst, new util.Random)
  }
}

val lst = List('a, 'b, 'c, 'd, 'f, 'g, 'h)
println(P23.randomSelect1(3, lst))
println(P23.randomSelect2(3, lst))
