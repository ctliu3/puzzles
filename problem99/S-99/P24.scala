object P24 {
  def lotto1(n: Int, m: Int): List[Int] = {
    def lotto(n: Int, lst: List[Int], rnd: util.Random): List[Int] = {
      if (n <= 0) Nil
      else {
        val (pre, h :: post) = lst splitAt (rnd.nextInt(lst.length))
        h :: lotto(n - 1, pre ::: post, rnd)
      }
    }
    lotto(n, List.range(1, m + 1), new util.Random)
  }
}

println(P24.lotto1(6, 49))
