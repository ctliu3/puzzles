object P25 {
  def randomPermute1(lst: List[Char]): List[Char] = {
    val rnd = new util.Random
    val arr = lst.toArray
    for (i <- 1 to lst.length - 1) {
      val idx = rnd.nextInt(i + 1)
      if (idx != i) {
        arr.update(i, arr(idx))
        arr.update(idx, lst(i))
      }
    }
    arr.toList
  }
}

val lst = List('a', 'b', 'c', 'd', 'e', 'f')
println(P25.randomPermute1(lst))
