object P26 {

  def genSubComb[T](lst: List[T])(f: (List[T]) => List[List[T]]): List[List[T]] = {
    lst match {
      case Nil => Nil
      case sublst@(_ :: tail) => {
        f(sublst) ::: genSubComb(tail)(f)
      }
    }
  }

  def combinations1[T](n: Int, lst: List[T]): List[List[T]] = {
    if (n == 0) List(Nil)
    else genSubComb(lst) { sublst =>
      combinations1(n - 1, sublst.tail) map {sublst.head :: _}
    }
  }

  def combinations2[T](n: Int, lst: List[T]): List[List[T]] = {
    if (n == 0) List(Nil)
    else {
      for {
        x <- (lst take (lst.length - n + 1))
        (_, post) = lst span (_ != x)
        ys <- combinations2(n - 1, post.tail)
      } yield x :: ys
    }
  }
}

val lst = List('a, 'b, 'c, 'd, 'e, 'f)
//val lst = List('a, 'b, 'c)
println(P26.combinations1(2, lst))
println(P26.combinations2(2, lst))
