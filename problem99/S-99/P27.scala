object P27 {

  def combinations[T](n: Int, lst: List[T]): List[List[T]] = {
    if (n == 0) List(Nil)
    else {
      for {
        x <- (lst take (lst.length - n + 1))
        (_, post) = lst span (_ != x)
        ys <- combinations(n - 1, post.tail)
      } yield x :: ys
    }
  }

  def group31[T](lst: List[T]): List[List[List[T]]] = {
      for {
      x1 <- combinations(1, lst)
      rest = lst diff x1
      x2 <- combinations(2, rest)
    } yield List(x1, x2, (rest diff x2))
  }

  def group1[T](ns: List[Int], lst: List[T]): List[List[List[T]]] = ns match {
    case Nil => List(Nil)
    case n :: ns => combinations(n, lst) flatMap {
      e => group1(ns, lst diff e) map {e :: _}
    }
  }
}

//val lst = List("Aldo", "Beat", "Carla", "David", "Evi", "Flip", "Gary", "Hugo", "Ida")
val lst = List("Aldo", "Beat", "Carla", "David")
println(P27.group31(lst))
println(P27.group1(List(1, 2, 1), lst))
