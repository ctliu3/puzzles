object P28 {
  def lsort1[T](lst: List[List[T]]): List[List[T]] = {
    lst sortWith { _.length < _.length }
  }

  def lsortFreq1[T](lst: List[List[T]]): List[List[T]] = {
    val freq = for(e <- lst; sublst = lst filter (_.length == e.length)) yield sublst.length
    (lst zip freq) sortWith { _._2 < _._2 } map { case (e, _) => e }
  }
}

val lst = List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o))
println(P28.lsort1(lst))
println(P28.lsortFreq1(lst))
