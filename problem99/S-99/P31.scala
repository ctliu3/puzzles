import scala.math._

class S99Int(val num: Int) {
  def isPrime: Boolean = {
    !((2 to sqrt(num).toInt) exists (x => num % x == 0))
  }
}

implicit def int2Int(i: Int): S99Int = new S99Int(i)

println(7.isPrime)
