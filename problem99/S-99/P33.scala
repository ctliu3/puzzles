class S99Int(val num: Int) {
  def isCoprimeTo(b: Int): Boolean = {
    if (gcd(num, b) > 1) false else true
  }

  def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }
}

implicit def int2S99Int(a: Int): S99Int = new S99Int(a)

println(35.isCoprimeTo(64))
