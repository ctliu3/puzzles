class S99Int(val num: Int) {
  def totient = (1 to num) count (isCoprimeTo(_))

  def isCoprimeTo(a: Int): Boolean = {
    if (gcd(num, a) == 1) true else false
  }

  def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }
}

implicit def int2S99Int(a: Int): S99Int = new S99Int(a)

println(10.totient)
