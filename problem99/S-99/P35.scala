import scala.math._

class S99Int(val num: Int) {
  def primeFactors(): List[Int] = {

    def primeFactors2(cur: Int, rest: Int): List[Int] = (cur, rest) match {
      case (_, 1) => List()
      case (cur, b) => {
        if (b % cur == 0) cur :: primeFactors2(cur, b / cur)
        else primeFactors2(cur + 1, rest)
      }
    }

    primeFactors2(2, num)
  }

  def isPrime = !((2 to sqrt(num).toInt) exists (num % _ == 0))
}

implicit def int2S99Int(a: Int): S99Int = new S99Int(a)

println(315.primeFactors)
println(7.isPrime)
