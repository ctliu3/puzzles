import scala.math._
import scala.collection.mutable.Map 

class S99Int(val num: Int) {
  def primeFactorMultiplicity1: List[(Int, Int)] = {
    val res = num.primeFactors

    res.foldLeft(List(): List[(Int, Int)]) ((x, y) => {
      if (x.isEmpty || x.last._1 != y) x ::: List((y, 1))
      else x.init ::: List((x.last._1, x.last._2 + 1))
    })
  }

  def primeFactorMultiplicity2: Map[Int, Int] = {
    val res = num.primeFactors

    res.foldLeft(Map.empty[Int, Int]) ((x, y) => {
      if (x.isEmpty || !x.contains(y)) x += (y -> 1)
      else {
        x(y) += 1
        x
      }
    })
  }

  def primeFactors(): List[Int] = {

    def primeFactors2(cur: Int, rest: Int): List[Int] = (cur, rest) match {
      case (_, 1) => List()
      case (cur, b) => {
        if (b % cur == 0) cur :: primeFactors2(cur, b / cur)
        else primeFactors2(cur + 1, rest)
      }
    }

    primeFactors2(2, num)
  }

  def isPrime = !((2 to sqrt(num).toInt) exists (num % _ == 0))
}

implicit def int2S99Int(a: Int): S99Int = new S99Int(a)

println(315.primeFactorMultiplicity1)
println(315.primeFactorMultiplicity2)
