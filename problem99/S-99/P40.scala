import scala.math.sqrt

class S99Int(val num: Int) {
  def goldbach: (Int, Int) = {

    def goldbach_(fst: Int): (Int, Int) = (fst, num - fst) match {
      case (fst, snd) if (fst.isPrime && snd.isPrime) => (fst, snd)
      case (fst, _) => goldbach_(fst + 1)
    }

    if (num % 2 != 0) {
      throw new Exception
    }
    goldbach_(2)
  }

  def isPrime: Boolean = !((2 to sqrt(num.toDouble).toInt) exists (num % _ == 0))
}

implicit def int2S99Int(a: Int): S99Int = new S99Int(a)

println(28.goldbach)
