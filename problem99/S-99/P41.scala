import scala.math.sqrt

class S99Int(val num: Int) {
  def goldbach: (Int, Int) = {

    def goldbach_(fst: Int): (Int, Int) = (fst, num - fst) match {
      case (fst, snd) if (fst.isPrime && snd.isPrime) => (fst, snd)
      case (fst, _) => goldbach_(fst + 1)
    }

    goldbach_(2)
  }

  def isPrime: Boolean = !((2 to sqrt(num.toDouble).toInt) exists (num % _ == 0))
}

implicit def int2S99Int(a: Int): S99Int = new S99Int(a)

def printGoldbachList(range: Range) {
  val res = for (x <- range if x % 2 == 0) yield x.goldbach
  println(res)
}

def printGoldbachListLimited(range: Range, limit: Int) {
  val res = for (x <- range if x % 2 == 0) yield x.goldbach
  val res1 = res filter (x => x._1 > limit && x._2 > limit)
  println(res1)
}

printGoldbachList(9 to 20)
printGoldbachListLimited(1 to 2000, 50)
