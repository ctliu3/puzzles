object P46 {
  def and(a: Boolean, b: Boolean): Boolean = (a, b) match {
    case (false, false) => false
    case (_, _) => true
  }

  def or(a: Boolean, b: Boolean): Boolean = (a, b) match {
    case (true, _) => true
    case (_, true) => true
    case (_, _) => false
  }

  def nand(a: Boolean, b: Boolean): Boolean = {
    not(and(a, b))
  }

  def nor(a: Boolean, b: Boolean): Boolean = (a, b) match {
    case (false, false) => true
    case (_, _) => false
  }

  def xor(a: Boolean, b: Boolean): Boolean = (a, b) match {
    case (false, false) => true
    case (true, true) => true
    case (_, _) => false
  }

  def not(a: Boolean): Boolean = a match {
    case false => true
    case true => false
  }

  def table2(f: (Boolean, Boolean) => Boolean) = {
    println("A     B     result")
    for {
      a <- List(false, true);
      b <- List(false, true)
    } println(f"$a%-5s $b%-5s ${f(a,b)}%-5s")
  }
}

P46.table2((a: Boolean, b: Boolean) => P46.and(a, P46.or(a, b)))
