class S99Logic(a: Boolean) {
  // or use `S99Logic` as prefix in the methods of S99Logic object
  import S99Logic._

  def and(b: Boolean): Boolean = (a, b) match {
    case (false, false) => false
    case _ => true
  }

  def or(b: Boolean): Boolean = (a, b) match {
    case (true, _) => true
    case (_, true) => true
    case (_, _) => false
  }

  def equ(b: Boolean): Boolean = (a and b) or (not(a) and not(b))
  def xor(b: Boolean): Boolean = not(a equ b) 
  def nor(b: Boolean): Boolean = not(a or b)
  def nand(b: Boolean): Boolean = not(a and b)
}

object S99Logic {
  implicit def bool2S99Logic(a: Boolean): S99Logic = new S99Logic(a)
  def not(a: Boolean): Boolean = !a

  def table2(f: (Boolean, Boolean) => Boolean) = {
    println("A     B     result")
    for {
      a <- List(true, false);
      b <- List(true, false)
    } println(f"$a%-5s $b%-5s ${f(a, b)}%-5s")
  }
}

import S99Logic._
table2((a: Boolean, b: Boolean) => a and (a or not(b)))
