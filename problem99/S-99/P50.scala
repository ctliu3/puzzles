object P50 {
  private abstract sealed class Tree[T] {
    val freq: Int
    def toCode: List[(T, String)] = toCodePrefixed("")
    def toCodePrefixed(prefix: String): List[(T, String)]
  }

  private final case class InternalNode[T](left: Tree[T], right: Tree[T]) extends Tree[T] {
    val freq: Int = left.freq + right.freq
    def toCodePrefixed(prefix: String): List[(T, String)] =
      left.toCodePrefixed(prefix + "0") ::: right.toCodePrefixed(prefix + "1")
  }

  private final case class LeafNode[T](element: T, freq: Int) extends Tree[T] {
    def toCodePrefixed(prefix: String): List[(T, String)] = List((element, prefix))
  }

  def huffman[T](ls: List[(T, Int)]): List[(T, String)] = {
    import collection.immutable.Queue

    def dequeueSmallest(q1: Queue[Tree[T]], q2: Queue[Tree[T]]): (Tree[T], Queue[Tree[T]], Queue[Tree[T]]) = {
      if (q2.isEmpty) (q1.front, q1.dequeue._2, q2)
      else if (q1.isEmpty || q2.front.freq < q1.front.freq) (q2.front, q1, q2.dequeue._2)
      else (q1.front, q1.dequeue._2, q2)
    }

    def huffman_(q1: Queue[Tree[T]], q2: Queue[Tree[T]]): List[(T, String)] = {
      if (q1.length + q2.length == 1) {
        (if (q1.isEmpty) q2.front else q1.front).toCode
      } else {
        val (v1, q3, q4) = dequeueSmallest(q1, q2)
        val (v2, q5, q6) = dequeueSmallest(q3, q4)
        huffman_(q5, q6.enqueue(InternalNode(v1, v2)))
      }
    }

    huffman_(Queue.empty[Tree[T]].enqueue(ls sortWith { _._2 < _._2 } map { e => LeafNode(e._1, e._2) }),
             Queue.empty[Tree[T]])
  }
}

import P50._
val res = huffman(List(("a", 45), ("b", 13), ("c", 12), ("d", 16), ("e", 9), ("f", 5)))
println(res)
