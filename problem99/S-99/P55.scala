package binarytree {

sealed abstract class Tree[+T]

// class
case class Node[+T](value: T, left: Tree[T], right: Tree[T]) extends Tree[T] {
  override def toString = "T(" + value.toString + " " + left.toString + " " + right.toString + ")"
}

// object
case object End extends Tree[Nothing] {
  override def toString = "."
}

// companion object
object Node {
  def apply[T](value: T): Node[T] = new Node(value, End, End)
}

//val tree = Node('a',
  //Node('b', Node('d'), Node('e')),
  //Node('c', End, Node('f', Node('g'), End)))
}

import binarytree._

object Tree {
  def cBalanced[T](nodes: Int, value: T): List[Tree[T]] = nodes match {
    case n if n < 1 => List(End)
    case n if n % 2 == 1 => {
      val subtrees = cBalanced(n / 2, value)
      subtrees.flatMap(l => subtrees.map(r => Node(value, l, r)))
    }
    case n if n % 2 == 0 => {
      val lesserSubtrees = cBalanced((n - 1) / 2, value)
      val greaterSubtrees = cBalanced((n - 1) / 2 + 1, value)
      lesserSubtrees.flatMap(l => greaterSubtrees.flatMap(g => List(Node(value, l, g), Node(value, g, l))))
    }
  }

  def main(args: Array[String]) {
    println(Tree.cBalanced(4, "x"))
  }
}
