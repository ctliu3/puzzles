package binarytree {

sealed abstract class Tree[+T]

// class
case class Node[+T](value: T, left: Tree[T], right: Tree[T]) extends Tree[T] {
  override def toString = "T(" + value.toString + " " + left.toString + " " + right.toString + ")"
}

// object
case object End extends Tree[Nothing] {
  override def toString = "."
}

// companion object
object Node {
  def apply[T](value: T): Node[T] = new Node(value, End, End)
}

//val tree = Node('a',
  //Node('b', Node('d'), Node('e')),
  //Node('c', End, Node('f', Node('g'), End)))
}
