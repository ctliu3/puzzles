MAXN = 2000000
b = [1] * MAXN

b[0] = b[1] = 0
s = 0
for i in xrange(2, MAXN):
    if b[i] == 1:
        s = s + i 
        j = i * i
        while j < MAXN:
            b[j] = 0
            j = j + i
print s
