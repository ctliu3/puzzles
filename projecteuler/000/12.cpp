#include <cstdio>
#include <algorithm>

using namespace std;

const int MAXN = 1000000;
bool p[MAXN];

void init() {
  fill(p, p + MAXN, true);
  p[0] = p[1] = false;
  for (int i = 2; (long long)i * i < MAXN; ++i) {
    if (p[i]) {
      for (int j = i * i; j < MAXN; j += i) {
        p[j] = false;
      }
    }
  }
}

int factors(int n) {
  int ret = 1;
  for (int i = 2; i < n; ++i) {
    int c = 0;
    while (n % i == 0) {
      ++c;
      n /= i;
    }
    ret *= c + 1;

    if (n < MAXN && p[n]) {
      ret *= 2;
      break;
    }
  }
  return ret;
}

int main() {
  init();
  int a = 1, n = 2;

  while (true) {
    a += n;
    int ret = factors(a);
    if (ret > 500) {
      printf("%d\n", a);
      break;
    }
    ++n;
  }
  
  return 0;
}
