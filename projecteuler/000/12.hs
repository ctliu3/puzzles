import Data.List

primes :: [Int]
primes = sieve [2..]
  where sieve (x:xs) = x : sieve (filter (\i -> i `mod` x /= 0) xs)

factors :: Int -> [Int]
factors n = factors' n primes
  where
    factors' n all@(p:ps)
      | p * p > n = [n]
      | n `mod` p == 0 = p : factors' (n `div` p) all
      | otherwise = factors' n ps

p12 = head $ filter ((>500) . nDivisors) triangleNumbers
  where nDivisors n = product $ map (\x -> (length x) + 1) $ group $ factors n
        triangleNumbers = scanl1 (+) [1,2..]
