import System.IO

gao :: String -> String
gao lines = show . take 10 . show $ sum xs
  where
    xs = map read $ words lines

main = do
  -- file operation
  handle <- openFile "p13.txt" ReadMode
  contents <- hGetContents handle
  putStrLn $ gao contents
  hClose handle
