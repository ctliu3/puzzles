collatz :: Int -> [Int]
collatz n = collatz' n [n]
  where 
    collatz' 1 xs = xs
    collatz' n xs = 
      let next = if even n then n `div` 2 else 3 * n + 1 in xs ++ collatz' next [next]

max' :: [(Int, Int)] -> Int
max' (x:xs)
  | xs == [] = snd x
  | otherwise = max (snd x) $ max' xs

-- This version take several minutes, use Data.Array will be faster
p14 = fst $ head $ filter (\(_, i) -> i == mx) xs
  where xs = zip [1..999999] $ map (length . collatz) [1..999999]
        mx = max' xs

-- TODO
-- rewrite with Data.Array
