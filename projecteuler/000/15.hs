p15 = let
  n = foldl (\acc x -> acc * x) 1 [40,39..21] 
  m = foldl (\acc x -> acc * x) 1 [1..20]
  in n `div` m

-- using `product`
p15' = product [21..40] `div` product [1..20]
