isLeap :: Int -> Bool
isLeap y = y `mod` 400 == 0 || y `mod` 4 == 0 && y `mod` 100 /= 0

year :: Bool -> [Int]
year leap = map (\(x, y) -> if y == 2 && leap == True then x + 1 else x) (zip month [1..])
  where month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

count :: Int -> Int -> Int
count start y = length $ filter (==True) $ map (\x -> x `mod` 7 == 0) firstDayOfMonth
  where firstDayOfMonth = init $ scanl (+) start $ year $ isLeap y

p19 = sum yearCount
  where years = init $ map (\x -> if x == True then 366 else 365) $ map isLeap [1900..2000]
        days = tail $ scanl (+) 1 years
        yearCount = map (\(x, y) -> count x y) (zip days [1901..])
