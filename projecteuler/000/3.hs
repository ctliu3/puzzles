import Data.IntSet

primes :: Int -> IntSet
primes n = sieve 2 $ fromList [2..n]
    where
        sieve p s
            | p * p > n = s
            | otherwise = sieve (next (p + 1) s) (merge p s)
            where
                next p s
                    | member p s = p
                    | otherwise = next (p + 1) s
                merge p s =
                    s \\ fromList (takeWhile (<= n) $ Prelude.map (p*) [p..])

primeList = toList $ primes 800000

primeFactors :: Int -> [Int]
primeFactors n = primeFactors' n primeList
    where
        primeFactors' n ps@(p:s)
            | n == 1            = []
            | p * p > n         = [n]
            | n `mod` p == 0    = p : primeFactors' (n `div` p) ps
            | otherwise         = primeFactors' n s

main = putStrLn $ show $ last $ primeFactors 600851475143
