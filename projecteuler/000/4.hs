main = putStrLn $ show $ maximum $ filter isPalindrome $ [x * y | x <-[100..999], y <- [100..999]]
    where isPalindrome x =
            (show x) == (reverse $ show x)
