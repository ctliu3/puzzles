from fractions import gcd

def lcm(a, b):
    return a * b // gcd(a, b)

ans = 1
for i in xrange(1, 21):
    ans = lcm(ans, i)
print ans
