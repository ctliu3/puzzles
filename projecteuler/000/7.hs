import Data.IntSet

primes :: Int -> IntSet
primes n = sieve 2 (fromList [2 .. n])
    where
        sieve p s
            | p * p > n = s
            | otherwise = sieve (next (p + 1) s) (merge p s)
                where
                    next p s
                        | member p s = p
                        | otherwise = next (p + 1) s
                    merge p s =
                        s \\ (fromList $ takeWhile (<= n) $ (Prelude.map (p*) [p ..]))

main =
    let n = 1000000
    in putStrLn $ show $ (toList $ primes n) !! 10000
