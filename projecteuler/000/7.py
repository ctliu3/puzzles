MAXN = 1000000
b = [1] * MAXN

c = 0
b[0] = b[1] = 0
for i in xrange(2, MAXN):
    if b[i] == 1:
        c = c + 1
        if c == 10001:
            print i
            break
        j = i * i
        while j < MAXN:
            b[j] = 0
            j += i
