import Data.Char (digitToInt)
import Data.List

main = do
    str <- readFile "p8.txt"
    let number = map digitToInt (concat $ lines str)
    putStrLn $ show $ maximum $ map (product . take 5) $ tails number
