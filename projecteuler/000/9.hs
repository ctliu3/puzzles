main =
    putStrLn $ show $ filter (> 0) $ map check triple
    where
        triple = [(a, b, 1000 - a - b) | a <- [1 .. 1000], b <- [1 .. 1000]]
        check (a, b, c)
            | a < b && c > 0 && a ^ 2 + b ^ 2 == c ^ 2 = a * b * c
            | otherwise = 0
