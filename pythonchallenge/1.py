import sys, string

code = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.";

sys.stdout = open('ans\\1.ans', 'w')

from_, to = [], []
for c in xrange(ord('a'), ord('z') + 1):
    from_.append(chr(c))
for c in xrange(ord('c'), ord('z') + 1):
    to.append(chr(c))
to.append('a')
to.append('b')

table = string.maketrans(''.join(from_), ''.join(to))
print code.translate(table)
