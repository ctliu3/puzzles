import sys, string

sys.stdout = open('ans\\2.ans', 'w')

ans = []
with open('2.data') as f:
    for line in f:
        for c in line:
            if c in string.letters:
                ans += [c]
print ''.join(ans)
