import sys, re

f = open('3.data', 'r')
sys.stdout = open('ans\\3.ans', 'w')

ans = []
for line in f:
    s = ''.join(re.findall('[^A-Z][A-Z]{3}([a-z])[A-Z]{3}[^A-Z]', line))
    if len(s):
        ans.append(s)
print ''.join(ans)
