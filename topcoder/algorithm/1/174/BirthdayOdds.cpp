#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

class BirthdayOdds {
public:
  int minPeople(int, int);
};

int BirthdayOdds::minPeople(int minOdds, int daysInYear) {
  int n = 2;
  double p = 1;
  while (true) {
    p = p * ((daysInYear - (n - 1)) / (daysInYear * 1.));
    if ((1. - p) * 100. > minOdds) {
      return n;
    }
    ++n;
  }
}


double test0() {
  int p0 = 75;
  int p1 = 5;
  BirthdayOdds * obj = new BirthdayOdds();
  clock_t start = clock();
  int my_answer = obj->minPeople(p0, p1);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p2 = 4;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p2 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p2 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test1() {
  int p0 = 50;
  int p1 = 365;
  BirthdayOdds * obj = new BirthdayOdds();
  clock_t start = clock();
  int my_answer = obj->minPeople(p0, p1);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p2 = 23;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p2 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p2 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test2() {
  int p0 = 1;
  int p1 = 365;
  BirthdayOdds * obj = new BirthdayOdds();
  clock_t start = clock();
  int my_answer = obj->minPeople(p0, p1);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p2 = 4;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p2 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p2 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test3() {
  int p0 = 84;
  int p1 = 9227;
  BirthdayOdds * obj = new BirthdayOdds();
  clock_t start = clock();
  int my_answer = obj->minPeople(p0, p1);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p2 = 184;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p2 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p2 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}

int main() {
  int time;
  bool errors = false;

  time = test0();
  if (time < 0)
    errors = true;

  time = test1();
  if (time < 0)
    errors = true;

  time = test2();
  if (time < 0)
    errors = true;

  time = test3();
  if (time < 0)
    errors = true;

  if (!errors)
    cout <<"You're a stud (at least on the example cases)!" <<endl;
  else
    cout <<"Some of the test cases had errors." <<endl;
}

//Powered by [KawigiEdit] 2.0!
