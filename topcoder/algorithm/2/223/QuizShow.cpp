#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

class QuizShow {
public:
  int wager(vector <int>, int, int);
};

int QuizShow::wager(vector <int> scores, int wager1, int wager2) {
  int res = 0, m = 0;
  for (int wager = 0; wager <= scores[0]; ++wager) {
    int cnt = 0;
    for (int i = -1; i <= 1; i += 2) {
      for (int j = -1; j <= 1; j += 2) {
        for (int k = -1; k <= 1; k += 2) {
          if (scores[0] + wager * i > scores[1] + wager1 * j &&
            scores[0] + wager * i > scores[2] + wager2 * k) {
            ++cnt;
          }
        }
      }
    }
    if (cnt > m) {
      res = wager;
      m = cnt;
    }
  }

  return res;
}
