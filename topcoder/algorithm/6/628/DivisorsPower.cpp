#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

typedef long long llong;

class DivisorsPower {
public:
  long long findArgument(long long);
  long long getd(long long n) {
    long long res = 0;
    for (long long i = 1; i * i <= n; ++i) {
      if (n % i == 0) {
        res += 2;
        if (i * i == n) {
          --res;
        }
      }
    }
    return res;
  }
};

long long DivisorsPower::findArgument(long long n) {
  if (n == 1) {
    return -1;
  }

  long long res = -1;
  for (int i = 2; i < 70; ++i) {
    if (i >= n) {
      break;
    }
    long long x = pow((double)n, 1. / i);
    long long l = max((llong)2, x - 1), r = x + 1;
    for (llong j = l; j <= r; ++j) {
      llong t = n;
      int k = 0;
      for (; k < i; ++k) {
        if (t % j != 0) {
          break;
        }
        t /= j;
      }
      if (k == i && t == 1 && i == getd(j)) {
        if (res == -1 || j < res) {
          res = j;
        }
      }
    }
  }
  return res;
}
