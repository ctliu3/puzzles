#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>

using namespace std;

class PotentialArithmeticSequence {
  vector<int> bins;
  vector<int> d;
public:
  int numberOfSubsequences(vector <int>);

  bool check(int s, int t) {
    for (int i = 0; i < (int)bins.size(); ++i) {
      int j = 0, k = i;
      for (; j < t - s + 1; ++j) {
        if (bins[k] != d[s + j]) {
          break;
        }
        k = (k + 1) % 128;
      }
      if (j == t - s + 1) {
        return true;
      }
    }
    return false;
  }
};

int PotentialArithmeticSequence::numberOfSubsequences(vector <int> d) {
  int n = d.size();
  int res = n;

  for (int i = 0; i < n; ++i) {
    if (d[i] > 7) {
      d[i] = 7;
    }
  }

  this->d = d;
  bins.assign(128, 0);
  for (int i = 1; i <= 128; ++i) {
    bins[i - 1] = __builtin_ctz(i);
  }

  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      if (check(i, j)) {
        ++res;
      }
    }
  }

  return res;
}

double test0() {
  int t0[] = {0,1,0,2,0,1,0};
  vector <int> p0(t0, t0+sizeof(t0)/sizeof(int));
  PotentialArithmeticSequence * obj = new PotentialArithmeticSequence();
  clock_t start = clock();
  int my_answer = obj->numberOfSubsequences(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p1 = 28;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p1 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test1() {
  int t0[] = {0,0,0,0,0,0,0};
  vector <int> p0(t0, t0+sizeof(t0)/sizeof(int));
  PotentialArithmeticSequence * obj = new PotentialArithmeticSequence();
  clock_t start = clock();
  int my_answer = obj->numberOfSubsequences(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p1 = 7;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p1 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test2() {
  int t0[] = {0,0,0,0,1,1,1};
  vector <int> p0(t0, t0+sizeof(t0)/sizeof(int));
  PotentialArithmeticSequence * obj = new PotentialArithmeticSequence();
  clock_t start = clock();
  int my_answer = obj->numberOfSubsequences(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p1 = 8;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p1 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test3() {
  int t0[] = {0,100,0,2,0};
  vector <int> p0(t0, t0+sizeof(t0)/sizeof(int));
  PotentialArithmeticSequence * obj = new PotentialArithmeticSequence();
  clock_t start = clock();
  int my_answer = obj->numberOfSubsequences(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p1 = 11;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p1 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test4() {
  int t0[] = {1,11,3,0,1,0,1,0,1,0,1,0,3,0,2,0,0,0,0,1,2,3,20};
  vector <int> p0(t0, t0+sizeof(t0)/sizeof(int));
  PotentialArithmeticSequence * obj = new PotentialArithmeticSequence();
  clock_t start = clock();
  int my_answer = obj->numberOfSubsequences(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  int p1 = 49;
  cout <<"Desired answer: " <<endl;
  cout <<"\t" << p1 <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t" << my_answer <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}

int main() {
  int time;
  bool errors = false;

  time = test0();
  if (time < 0)
    errors = true;

  time = test1();
  if (time < 0)
    errors = true;

  time = test2();
  if (time < 0)
    errors = true;

  time = test3();
  if (time < 0)
    errors = true;

  time = test4();
  if (time < 0)
    errors = true;

  if (!errors)
    cout <<"You're a stud (at least on the example cases)!" <<endl;
  else
    cout <<"Some of the test cases had errors." <<endl;
}

//Powered by [KawigiEdit] 2.0!
