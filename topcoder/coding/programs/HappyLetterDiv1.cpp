#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <queue>

using namespace std;

class HappyLetterDiv1 {
public:
  string getHappyLetters(string);

  bool gao(char c, string s) {
    string rest;
    int cnt = 0;

    for (char& chr : s) {
      if (chr != c) {
        rest.push_back(chr);
      } else {
        ++cnt;
      }
    }
    sort(rest.begin(), rest.end());

    priority_queue<int> pq;
    int i = 0;
    while (i < rest.size()) {
      int j = i;
      while (j < rest.size() and rest[j] == rest[i]) {
        ++j;
      }
      pq.push(j - i);
      i = j;
    }

    while (pq.size() >= 2) {
      int n = pq.top();
      pq.pop();
      int m = pq.top();
      pq.pop();
      --n;
      --m;
      if (n) {
        pq.push(n);
      }
      if (m) {
        pq.push(m);
      }
    }

    if (pq.size() == 0) {
      return true;
    }
    return pq.top() < cnt;
  }
};

string HappyLetterDiv1::getHappyLetters(string letters) {
  set<char> st;

  for (char chr : letters) {
    st.insert(chr);
  }

  string res;
  for (auto& c : st) {
    if (gao(c, letters)) {
      res.push_back(c);
    }
  }

  return res;
}


double test0() {
  string p0 = "aabbacccc";
  HappyLetterDiv1 * obj = new HappyLetterDiv1();
  clock_t start = clock();
  string my_answer = obj->getHappyLetters(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  string p1 = "abc";
  cout <<"Desired answer: " <<endl;
  cout <<"\t\"" << p1 <<"\"" <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t\"" << my_answer<<"\"" <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test1() {
  string p0 = "aaaaaaaccdd";
  HappyLetterDiv1 * obj = new HappyLetterDiv1();
  clock_t start = clock();
  string my_answer = obj->getHappyLetters(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  string p1 = "a";
  cout <<"Desired answer: " <<endl;
  cout <<"\t\"" << p1 <<"\"" <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t\"" << my_answer<<"\"" <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test2() {
  string p0 = "ddabccadb";
  HappyLetterDiv1 * obj = new HappyLetterDiv1();
  clock_t start = clock();
  string my_answer = obj->getHappyLetters(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  string p1 = "abcd";
  cout <<"Desired answer: " <<endl;
  cout <<"\t\"" << p1 <<"\"" <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t\"" << my_answer<<"\"" <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test3() {
  string p0 = "aaabbb";
  HappyLetterDiv1 * obj = new HappyLetterDiv1();
  clock_t start = clock();
  string my_answer = obj->getHappyLetters(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  string p1 = "";
  cout <<"Desired answer: " <<endl;
  cout <<"\t\"" << p1 <<"\"" <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t\"" << my_answer<<"\"" <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}
double test4() {
  string p0 = "rdokcogscosn";
  HappyLetterDiv1 * obj = new HappyLetterDiv1();
  clock_t start = clock();
  string my_answer = obj->getHappyLetters(p0);
  clock_t end = clock();
  delete obj;
  cout <<"Time: " <<(double)(end-start)/CLOCKS_PER_SEC <<" seconds" <<endl;
  string p1 = "cos";
  cout <<"Desired answer: " <<endl;
  cout <<"\t\"" << p1 <<"\"" <<endl;
  cout <<"Your answer: " <<endl;
  cout <<"\t\"" << my_answer<<"\"" <<endl;
  if (p1 != my_answer) {
    cout <<"DOESN'T MATCH!!!!" <<endl <<endl;
    return -1;
  }
  else {
    cout <<"Match :-)" <<endl <<endl;
    return (double)(end-start)/CLOCKS_PER_SEC;
  }
}

int main() {
  int time;
  bool errors = false;

  time = test0();
  if (time < 0)
    errors = true;

  time = test1();
  if (time < 0)
    errors = true;

  time = test2();
  if (time < 0)
    errors = true;

  time = test3();
  if (time < 0)
    errors = true;

  time = test4();
  if (time < 0)
    errors = true;

  if (!errors)
    cout <<"You're a stud (at least on the example cases)!" <<endl;
  else
    cout <<"Some of the test cases had errors." <<endl;
}

//Powered by [KawigiEdit] 2.0!
